$(document).ready(function() {
    $('table').DataTable( {
        "scrollX": true,
        "pageLength": 50,
        "order": [[ 3, 'desc' ]]
    } );
    $('.btn').click(function() {
    	$('.btn').toggleClass('active');
    	$('.player-table').toggleClass('active')
    })
} );