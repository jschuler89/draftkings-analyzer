<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class DKController extends Controller
{
	public function rosterUpload(Request $request)
	{
		$csv = file_get_contents($_FILES['file']['tmp_name']);
		$functions = \App::make('functions');
		$functions->rosterUpload($csv);

		return response()->json([
		    'success' => true
		]);
	}

	public function players()
	{
		$functions = \App::make('functions');
		$player_ids = $functions->getPlayerIds();
		$pitchers = $functions->getPitchers($player_ids);
		$batters = $functions->getBatters($player_ids);

		$variables['pitchers'] = $pitchers;
		$variables['batters'] = $batters;

		return view('players', $variables);
	}

	public function sync()
	{
		$functions = \App::make('functions');
		$player_ids = $functions->getPlayerIds();
		$team_ids = $functions->getTeamIds();
		$variables = array();
		$variables['player_ids'] = $player_ids;
		$variables['team_ids'] = $team_ids;

		return view('sync', $variables);
	}

	public function syncPlayer(Request $request)
	{
		$player_id = $request->input('player_id');
		$functions = \App::make('functions');

		$return = $functions->syncPlayer($player_id);

		return response()->json($return);
	}

	public function syncTeam(Request $request)
	{
		$team_id = $request->input('team_id');
		$functions = \App::make('functions');

		$return = $functions->syncTeam($team_id);

		return response()->json($return);
	}
}