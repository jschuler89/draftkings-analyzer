<?php

namespace App\DK;

use App\PlayerPool;
use App\Players;
use App\PlayerGameLogs;
use App\Teams;
use App\TeamGameLogs;
use App\Objects\Player;
use Illuminate\Support\Facades\DB;

class Functions
{
	public function __contstuct()
	{
		
	}

	public function rosterUpload($csv)
	{
		// "Position","Name","Salary","GameInfo","AvgPointsPerGame","teamAbbrev"
		$csvArray = explode("\n", $csv);

		foreach($csvArray as $index => $_csvRow) {
			if($index == 0) {
				continue;
			}

			$playerInfo = explode(",", $_csvRow);

			if(!isset($playerInfo[1])) {
				continue;
			}

			$playerData['position'] = str_replace('"', '', $playerInfo[0]);
			$playerData['name'] = str_replace('"', '', $playerInfo[1]);
			$playerData['team'] = str_replace(array('"', "\r", "\n"), '', $playerInfo[5]);

			$playerInDB = Players::select('players.*')
				   ->where('position', $playerData['position'])
				   ->where('name', $playerData['name'])
				   ->where('team', $playerData['team'])
				   ->get()
				   ->first();

			if(!$playerInDB) {
				$player = Players::create($playerData);
			} else {
				$player = $playerInDB;
			}

			$position = $playerData['position'];
			$name = $playerData['name'];
			$salary = $playerInfo[2];
			$gameInfo = $playerInfo[3];

			// [0] => Team@Opp, [1] => 01:00PM [2] => ET
			$gameInfoArray = explode(" ", $gameInfo);
			// [0] => Team, [1] => Opp
			$gameArray = explode("@", $gameInfoArray[0]);
			$game_time = str_replace(array('AM', 'PM'), '', $gameInfoArray[1]);
			$opp = trim(str_replace('"', '', strtoupper($gameArray[1])));
			$opp2 = trim(str_replace('"', '', strtoupper($gameArray[0])));
			$fppg = number_format($playerInfo[4], 2);
			$team = trim(str_replace('"', '', $playerData['team']));

			if(strtolower($team) == strtolower($opp)) {
				$opp = $opp2;
			}

			$playerPoolData['player_id'] = $player['player_id'];
			$playerPoolData['fppg'] = $fppg;
			$playerPoolData['salary'] = $salary;
			$playerPoolData['opp'] = $opp;
			$playerPoolData['game_date'] = date('Y-m-d');
			$playerPoolData['game_time'] = $game_time;

			PlayerPool::create($playerPoolData);
		}

		return true;
	}

	public function getPlayerIds()
	{
		$player_ids_results = PlayerPool::select('player_pool.player_id')->where('fppg', '>', 5)->where('game_date', date('Y-m-d'))->get();
		$player_ids = array();

		foreach($player_ids_results as $player_id) {
			$player_ids[] = $player_id['player_id'];
		}

		return $player_ids;
	}

	public function getTeamIds()
	{
		$team_ids_results = Teams::select('teams.*')->get();
		$team_ids = array();

		foreach($team_ids_results as $team_id) {
			$team_ids[] = $team_id['team_id'];
		}

		return $team_ids;
	}

	public function getPlayers($player_ids)
	{
		$players = array();

		foreach($player_ids as $player_id) {
			$players[] = new Player($player_id);
		}

		return $players;
	}

	public function getPitchers($player_ids)
	{
		$players = Players::getPlayersByPosition(array('SP', 'RP'));
		foreach($players as $_player) {
			if(in_array($_player->player_id, $player_ids)) {
				$player = new Player($_player->player_id);
				$playersArray[$_player->player_id] = $player;
			}
		}

		return $playersArray;
	}

	public function getBatters($player_ids)
	{
		$players = Players::getPlayersByPosition(array('C', '1B', '2B', '3B', 'SS', 'OF', '1B/OF', '2B/OF', '3B/OF', '1B/C', '1B/2B', '1B/3B', '2B/3B', '2B/SS', '3B/C', '3B/SS', 'OF/SS'));
		foreach($players as $_player) {
			if(in_array($_player->player_id, $player_ids)) {
				$player = new Player($_player->player_id);
				$playersArray[$_player->player_id] = $player;
			}
		}

		return $playersArray;
	}

	public function syncPlayer($player_id)
	{
		$player = Players::getPlayerByPlayerId($player_id);
		$basicInfo = array();

		if($player['url_param'] == null) {
			$basicInfo = $this->getBasicInfo($player);
			$this->insertUrlParam($player_id, $basicInfo['url_param']);
			$urlparam = $basicInfo['url_param'];
		} else {
			$urlparam = $player['url_param'];
		}

		if(isset($basicInfo['img']) && $basicInfo['img'] != '') {
			$this->insertPlayerImg($player_id, $basicInfo['img']);
		}

		$position = strtolower($player['position']);

		if(in_array($position, array('sp', 'rp'))) {
			$position = 'p';
		} else {
			$position = 'b';
		}

		$gameLogs = $this->getPlayerGameLogs($urlparam, $position);

		$this->insertGameLog($player_id, $gameLogs);

		return array('name' => $player['name']);
	}

	public function syncTeam($team_id)
	{
		$team = Teams::getTeamByTeamId($team_id);
		$teamGameLogs = $this->getTeamGameLogs($team['url_param']);
		$this->insertTeamGameLog($team, $teamGameLogs);

		return array('name' => $team['team']);
	}

	public function insertTeamGameLog($team, $teamLog = array())
	{
		if(empty($teamLog)) {
			return false;
		}

		//delete old records of team game log
		TeamGameLogs::where('team_id', $team['team_id'])->delete();

		$columns = DB::select("SHOW COLUMNS FROM player_game_logs");
		$fields = array();

		foreach($columns as $_column) {
			$_column = (array)$_column;
			$fields[$_column['Field']] = $_column['Field'];
		}

		$logs = $teamLog['gamelogs'];
		$games = 1;

		foreach($logs as $_log) {
			$keys = array();
			$values = array();


			foreach($_log as $index => $_logRow) {
				$column = $index;

				// Get team name 'New England Patriots' -> 'Patriots'
				if($column == 'opp') {
					if($_logRow == 'Bye Week') {
						$_logRow = 'Bye';
					} else {
						$_logRow = explode(' ', $_logRow);
						$_logRow = $_logRow[count($_logRow) - 1];
					}
				}

				if(in_array($column, $fields)) {
					// $keys[] = $fields[$column];
					$values[$fields[$column]] = $_logRow;
					$values['game_num'] = $games;
					$values['team_id'] = $team['team_id'];
				}
				
			}

			TeamGameLogs::create($values);
			$games++;
		}


		return true;
	}

	public function getTeamGameLogs($urlparam)
	{
		$baseballUrl = "http://www.baseball-reference.com/teams/$urlparam/2017/gamelog";
		$gameArray = array();
		
		try {
			$results = file_get_contents($baseballUrl);
		} catch(\Exception $e) {
			return array();
		}

		$doc = new \DOMDocument();
		@$doc->loadHTML($results);

		$xpath = new \DOMXpath($doc);
		$week = 1;

		$elements = $xpath->query("//table[contains(@id, \"tgl_basic\")]//tbody//tr[contains(@id, \"tgl_basic\")]");

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$localArray['week_num'] = $week;

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;
		  		}

		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}

				$week++;
		  	}
	  	}

		return $gameArray;
	}

	/**
	 * Get basic player info
	 * 
	 **/
	public function getBasicInfo($player)
	{
		$max = 10;
		$index = 0;
		$position = $player['position'];
		$urlparam = false;
		$jsonName  = '';
		$img = '';

		while($index < $max) {
			$fullname = str_replace(array(' Jr.', ' III', ' Sr.', '.', "'"), '', $player['name']);
			$name = explode(' ', $fullname);
			$first_name = $name[0];
			$last_name = $name[1];
			$playerValidated = false;

			$first_name_formatted = substr($first_name, 0, 2);
			$last_name_formatted = substr($last_name,0, 5);
			$category = $last_name[0];
			$indexNumber = '0'.$index;

			$urlparam = strtolower("$category/".$last_name_formatted.$first_name_formatted.$indexNumber);
			$baseballUrl = "http://www.baseball-reference.com/players/$urlparam.shtml";

			try {
				$results = file_get_contents($baseballUrl);
			} catch(\Exception $e) {
				$index++;
				continue;
			}

			$doc = new \DOMDocument();
			@$doc->loadHTML($results);

			$xpath = new \DOMXpath($doc);

			$elements = $xpath->query("//div[contains(@id, \"meta\")]//h1");

			if (!is_null($elements)) {
			  	foreach ($elements as $element) {
			  		$jsonName = $element->textContent;
		  		}
		  	}

		  	if($jsonName == $fullname) {
	  			$playerValidated = true;
	  		}

		  	if($playerValidated) {
		  		//get image
		  		$elements = $xpath->query("//div[contains(@class, \"media-item\")]//img");

				if (!is_null($elements)) {
				  	foreach ($elements as $element) {
				  		foreach($element->attributes as $_attribute) {
				  			if($_attribute->name == 'src') {
				  				$img = $_attribute->value;
				  			}
				  		}
				  	}
			  	}
		  		break;
		  	}


			$index++;
		}

		return array('url_param' => $urlparam, 'img' => $img);
	}

	/**
	 * Save player url param 
	 * 
	 */
	public function insertUrlParam($player_id, $url_param)
	{
		if(empty($player_id) || empty($url_param)) {
			return false;
		}

		$player = Players::find($player_id);

		$player->url_param = $url_param;

		$player->save();

		return true;
	}

	public function insertPlayerImg($player_id, $img)
	{
		if(empty($player_id)) {
			return false;
		}

		$player = Players::find($player_id);

		$player->img = $img;

		$player->save();

		return true;
	}

	/**
	 * Get player game logs 
	 * 
	 */
	public function getPlayerGameLogs($urlparam, $position)
	{
		$urlparam = preg_replace('/\w+\//', '', $urlparam); // replace category letter s/salech01 -> salech01
		$urlParamString = "gl.fcgi?id=$urlparam&t=$position&year=2017";
		$baseballUrl = "http://www.baseball-reference.com/players/$urlParamString";
		$gameArray = array();

		try {
			$results = file_get_contents($baseballUrl);
		} catch(\Exception $e) {

		}

		$doc = new \DOMDocument();
		@$doc->loadHTML($results);

		$xpath = new \DOMXpath($doc);

		if($position == 'p') {
			$elements = $xpath->query("//table[contains(@id, \"pitching_gamelogs\")]//tbody//tr[contains(@id, \"pitching_gamelogs\")]");
		} else {
			$elements = $xpath->query("//table[contains(@id, \"batting_gamelogs\")]//tbody//tr[contains(@id, \"batting_gamelogs\")]");
		}

		if (!is_null($elements)) {
	  		$games = 1;
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = strtolower($_attribute->value);
		  					break;
		  				}
		  			}

		  			if($data_stat == 'opp') {
		  				$localArray['opp_stats'] = $this->getTeamStats(strtolower($_childNode->nodeValue));
		  			}


		  			if($data_stat == 'date_game') {
		  				$formatString = preg_replace('/[[:^print:]]/', ' ', $_childNode->nodeValue); // weird characters we need to remove
		  				$dateFormat = explode('  ', $formatString);
		  				if(count($dateFormat) == 1) {
		  					$dateFormat = explode(' ', $formatString);
		  				}

		  				$localArray['date_game'] = date('Y-m-d', strtotime($dateFormat[1].'-'.$dateFormat[0]));
		  				continue;
		  			}

		  			if($_childNode->nodeValue == '' || $_childNode->nodeValue == 'inf' || $_childNode->nodeValue == '---') {
		  				$_childNode->nodeValue = 0;
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;

		  		}
  				$localArray['game_num'] = $games;
	  			$games++;

		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}
		  	}
		}

		return $gameArray;
	}

	public function getTeamStats($urlparam)
	{
		$baseballUrl = "http://www.baseball-reference.com/teams/$urlparam/2017.html";
		
		try {
			$results = file_get_contents($baseballUrl);
		} catch(\Exception $e) {

		}

		$doc = new \DOMDocument();
		$doc->loadHTML(str_replace(array('<!--', '-->'), '',$results));

		$xpath = new \DOMXpath($doc);
		$gamesPlayed = false;

		$elements = $xpath->query("//table[contains(@id, \"team_stats\")]//tbody//tr");
		$gamesPlayedElement = $xpath->query("//table[contains(@id, \"passing\")]//tfoot//tr");

		if (!is_null($gamesPlayedElement)) {
		  	foreach ($gamesPlayedElement as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			if($data_stat == 'g') {
		  				$gamesPlayed = $_childNode->nodeValue;
		  			}

		  		}

		  		if($gamesPlayed) {
		  			$gameArray['games_played'] = $gamesPlayed;
		  			break;
		  		}
		  	}
		}

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;
		  		}

		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}
		  	}
	  	}

		return $gameArray;
	}

	/**
	 * Insert each game log into database
	 * 
	 */
	public function insertGameLog($player_id, $gamelog) 
	{
		$columns = DB::select("SHOW COLUMNS FROM player_game_logs");

		if(empty($gamelog)) {
			return false;
		}

		foreach($gamelog['gamelogs'] as $_game) {
			$values = array();
			$keys = array();
			$alreadyLoaded = false;

			foreach($columns as $_column) {
				$_column = (array)$_column;
				if(isset($_game[$_column['Field']])) {

					if($_column['Field'] == 'game_num') {
						$game_num = $_game[$_column['Field']];
						$count = PlayerGameLogs::select(DB::raw('count(*) as count'))
						                       ->where('player_id', $player_id)
						                       ->where('game_num', $game_num)
						                       ->get()
						                       ->first();

						if($count['count'] > 0) {
							$alreadyLoaded = true;
							break;
						}
					}
					$values['player_id'] = $player_id;
					$values[$_column['Field']] = $_game[$_column['Field']];
					// $keys[] = $_column['Field'];
				}
				
			}

			if($alreadyLoaded) {
				continue;
			}

			$insertPlayer = PlayerGameLogs::create($values);
		}

		return true;
	}
}