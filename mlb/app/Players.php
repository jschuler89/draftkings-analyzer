<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Players extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'players';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'player_id';

    public $timestamps  = false;

    protected $fillable = ['player_id', 'name', 'team', 'position', 'url_param', 'img'];

    public static function getPlayerByPlayerId($player_id)
    {
        $player = Players::select('players.*', 'player_pool.fppg', 'player_pool.salary', DB::raw('avg(player_game_logs.earned_run_avg) as era'), DB::raw('avg(player_game_logs.so) as so'), DB::raw('avg(player_game_logs.ip) as ip'), DB::raw('avg(player_game_logs.batters_faced) as bf'), DB::raw('sum(player_game_logs.h) / sum(player_game_logs.ab) as avg'), DB::raw('sum(player_game_logs.hr) as hr'), DB::raw('sum(player_game_logs.rbi) as rbi'))->leftJoin('player_pool', 'player_pool.player_id', '=', 'players.player_id')->leftJoin('player_game_logs', 'player_game_logs.player_id', '=', 'players.player_id')->where('players.player_id', $player_id)->groupBy(['player_id', 'name', 'team', 'position', 'url_param', 'img', 'fppg', 'salary'])->get()->first();

        return $player;
    }

    public static function getPlayersByPosition($positions = array())
    {
        $players = Players::select('players.*')->leftJoin('player_pool', 'player_pool.player_id', '=', 'players.player_id')->whereRaw('players.position in (\'' . implode("','", $positions) .'\')' )->get();

        return $players;
    }
}
