<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerGameLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_game_logs';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'game_id';

    public $timestamps  = false;

    protected $fillable = ['game_id',  'player_id',   'date_game',   'game_num',    'team_id', 'opp_id',  'game_result', 'team_homeoraway', 'pa',  'ab',  'r',   'h',   '2b',  '3b',  'hr',  'rbi', 'bb',  'ibb', 'so',  'hbp', 'sh',  'sf',  'roe', 'gidp',    'sb',  'cs',  'er',  'days_rest',   'ip',  'pitches', 'batters_faced',   'earned_run_avg',  'batting_avg', 'onbase_perc', 'slugging_perc',   'onbase_plus_slugging',    'batting_order_position',  'leverage_index_avg',  'wpa_bat', 're24_bat',    'draftkings_points'];
}
