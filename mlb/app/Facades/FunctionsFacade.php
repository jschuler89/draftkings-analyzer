<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FunctionsFacade extends Facade{
    protected static function getFunctionsAccessor() { return 'functions'; }
}