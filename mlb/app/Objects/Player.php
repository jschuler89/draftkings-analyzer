<?php

namespace App\Objects;

use App\Players;

class Player
{
	public $playerData;

	public function __construct($player_id = null)
	{
		if($player_id != null) {
			$this->playerData = Players::getPlayerByPlayerId($player_id);
		}
	}
}