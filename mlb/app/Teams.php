<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teams';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'team_id';

    public static function getTeamByTeamId($team_id)
    {
        $team = Teams::select('teams.*')->where('team_id', $team_id)->get()->first();

        return $team;
    }
}
