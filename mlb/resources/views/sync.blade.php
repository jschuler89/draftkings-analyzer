@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer - Sync')
@section('height', 'half-height')
@section('content')
    @include('layouts.nav')
    	<div class="sync-container">
			<div class="sync-teams">
				<h3>Sync Teams</h3>
				<span style="margin-left:10px;"><button class="sync_teams">Sync</button><img src="{{ asset('img/ajax-loader.gif') }}" ></span></span>
				<div class="progress">
				  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    0%
				  </div>
				</div>
			</div>
			<div class="sync-players">
				<h3>Sync Players</h3> 
				<span style="margin-left:10px;"><button class="sync">Sync</button><span><img src="{{ asset('img/ajax-loader.gif') }}" ></span></span>
				<div class="progress">
				  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    0%
				  </div>
				</div>
			</div>
			<div class="sync-both">
				<span style="margin-left:10px;"><button class="sync">Sync Both</button></span>
			</div>
		</div>
		<script type="text/javascript">
			var $sync_players = jQuery('.sync-players .progress-bar');
			var $sync_teams = jQuery('.sync-teams .progress-bar');
			var players = [{{ implode(',', $player_ids) }}];
			var teams = [{{ implode(',', $team_ids) }}];
			var count = 0;
			var teamCount = 0;
			var idx = 0;
			var teamIdx = 0;
			var intervalId;
			var isWaiting = false;
			var isWaitingTeam = false;
			var syncPlayersRunning = false;
			var syncTeamsRunning = false;

			jQuery(document).ready(function() {
				jQuery(document).on('click', '.sync-players button', function() {
					jQuery(this).parent().find('img').addClass('active');
					syncPlayersRunning = true;
					intervalId = setInterval(syncPlayer, 6000);
				});

				jQuery(document).on('click', '.sync-teams button', function() {
					jQuery(this).parent().find('img').addClass('active');
					syncTeamsRunning = true;
					intervalId = setInterval(syncTeam, 6000);
				});

				jQuery(document).on('click', '.sync-both button', function() {
					if(!syncPlayersRunning) {
						jQuery('.sync-players button').click();
					} else {
						alert('Players sync already running');
					}

					if(!syncTeamsRunning) {
						jQuery('.sync-teams button').click();
					} else {
						alert('Teams sync already running');
					}
				});
			});

			function syncPlayer() {
				if(count >= 100) {
					jQuery('.sync-players img').removeClass('active');
					clearInterval(intervalId);
					syncPlayersRunning = false;
				}	

				if(!isWaiting) {
					isWaiting = true;
					count = Math.round( (idx / players.length) * 100 );
					$sync_players.attr('aria-valuenow', count);
					$sync_players.css({width: count + '%'});
					$sync_players.text(count + '%');
					jQuery.ajax({
					  url: 'sync_player?player_id='+players[idx],
					  type: 'GET',
					  success: function(data, textStatus, xhr) {
						idx++;
					  	isWaiting = false;
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					  }
					});
				}
			}

			function syncTeam() {
				if(teamCount >= 100) {
					jQuery('.sync-teams img').removeClass('active');
					syncTeamsRunning = false;
					clearInterval(intervalId);
				}	

				if(!isWaitingTeam) {
					isWaitingTeam = true;
					teamCount = Math.round( (teamIdx / teams.length) * 100 );
					$sync_teams.attr('aria-valuenow', teamCount);
					$sync_teams.css({width: teamCount + '%'});
					$sync_teams.text(teamCount + '%');
					jQuery.ajax({
					  url: 'sync_team?team_id='+teams[teamIdx],
					  type: 'GET',
					  success: function(data, textStatus, xhr) {
						teamIdx++;
					  	isWaitingTeam = false;
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					  }
					});
				}
			}
		</script>
@endsection