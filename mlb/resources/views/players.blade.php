@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer')
@section('css')
    <link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('scripts')
        @parent
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="{{ asset('js/players.js') }}"></script>
@endsection
@section('header_title')
<div class="title m-b-md">
    Draftkings Roster Analyzer
</div>
@endsection
@section('content')
    @include('layouts.nav')
    <div>
        <button class="btn btn-sm active">Pitchers</button>
        <button class="btn btn-sm">Batters</button>
    </div>
    <div id="pitchers" class="player-table active">
        <table class="players-table">
            <thead>
                <tr>
                    <th data-sortable="true">Player Name</th>
                    <th data-sortable="true">Position</th>
                    <th data-sortable="true">Team</th>
                    <th data-sortable="true">Salary</th>
                    <th data-sortable="true">FPPG</th>
                    <th data-sortable="true">IP</th>
                    <th data-sortable="true">ERA</th>
                    <th data-sortable="true">K</th>
                    <th data-sortable="true">Batters Faced</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pitchers as $player)
                <tr>
                    <td><b>{{ $player->playerData['name'] }}</b></td>
                    <td>{{ $player->playerData['position'] }}</td>
                    <td>{{ $player->playerData['team'] }}</td>
                    <td>${{ $player->playerData['salary'] }}</td>
                    <td>{{ $player->playerData['fppg'] }}</td>
                    <td>{{ number_format($player->playerData['ip'], 2) }}</td>
                    <td>{{ number_format($player->playerData['era'], 2) }}</td>
                    <td>{{ number_format($player->playerData['so'], 2) }}</td>
                    <td>{{ number_format($player->playerData['bf'], 2) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div id="batters" class="player-table">
        <table class="players-table">
            <thead>
                <tr>
                    <th data-sortable="true">Player Name</th>
                    <th data-sortable="true">Position</th>
                    <th data-sortable="true">Team</th>
                    <th data-sortable="true">Salary</th>
                    <th data-sortable="true">FPPG</th>
                    <th data-sortable="true">Avg</th>
                    <th data-sortable="true">HR</th>
                    <th data-sortable="true">RBI</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($batters as $player)
                <tr>
                    <td><b>{{ $player->playerData['name'] }}</b></td>
                    <td>{{ $player->playerData['position'] }}</td>
                    <td>{{ $player->playerData['team'] }}</td>
                    <td>${{ $player->playerData['salary'] }}</td>
                    <td>{{ $player->playerData['fppg'] }}</td>
                    <td>{{ number_format($player->playerData['avg'], 3) }}</td>
                    <td>{{ $player->playerData['hr'] }}</td>
                    <td>{{ $player->playerData['rbi'] }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@endsection