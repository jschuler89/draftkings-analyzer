<?php
include_once 'config.php';
require_once 'classes/DK_DB.php';

$user = 'root';
$password = 'root';
$db = 'dk_fb';
$host = 'localhost';
$port = 3306;

$link = mysqli_init();
$success = mysqli_real_connect(
   $link, 
   $host, 
   $user, 
   $password, 
   $db,
   $port
);


ini_set('max_execution_time', -1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$dk_db = new DK_DB();

$allResult = mysqli_query($link, "SELECT pp.*, p.team, p.position, p.name FROM player_pool pp left join players p on p.player_id = pp.player_id where pp.game_date = '$gameDate'");
$qbResult = mysqli_query($link, "SELECT pp.*, p.team, p.position, p.name FROM player_pool pp left join players p on p.player_id = pp.player_id where position like '%QB%' AND game_date = '$gameDate'");
$rbResult = mysqli_query($link, "SELECT pp.*, p.team, p.position, p.name FROM player_pool pp left join players p on p.player_id = pp.player_id where position like '%RB%' AND game_date = '$gameDate'");
$wrResult = mysqli_query($link, "SELECT pp.*, p.team, p.position, p.name FROM player_pool pp left join players p on p.player_id = pp.player_id where position like '%WR%' AND game_date = '$gameDate'");
$teResult = mysqli_query($link, "SELECT pp.*, p.team, p.position, p.name FROM player_pool pp left join players p on p.player_id = pp.player_id where position like '%TE%' AND game_date = '$gameDate'");
$dstResult = mysqli_query($link, "SELECT pp.*, p.team, p.position, p.name FROM player_pool pp left join players p on p.player_id = pp.player_id where position like '%DST%' AND game_date = '$gameDate'");

// Fetch all
$all = mysqli_fetch_all($allResult,MYSQLI_ASSOC);
$qbs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'QB');
$rbs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'RB');
$wrs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'WR');
$tes = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'TE');
// $qbs = mysqli_fetch_all($qbResult,MYSQLI_ASSOC);
// $rbs = mysqli_fetch_all($rbResult,MYSQLI_ASSOC);
// $wrs = mysqli_fetch_all($wrResult,MYSQLI_ASSOC);
// $tes = mysqli_fetch_all($teResult,MYSQLI_ASSOC);
$dsts = mysqli_fetch_all($dstResult,MYSQLI_ASSOC);

$salary_cap = 50000;
$salary_min = 48000;
$fppg_min = 4;
$uploadCSV = array();
$locked = array();
$exclude = array();


$pgs_count = count($qbs);
$sgs_count = count($rbs);
$sfs_count = count($wrs);
$pfs_count = count($tes);
$cs_count = count($dsts);

$positions = array('QB', 'RB', 'RB', 'WR', 'WR', 'WR', 'TE', 'FLEX', 'DST');
$simulations = 50;
$fptsMin = 50;
$rosters = array();

for($i = 0; $i < $simulations; $i++) {

$copy_positions = array('QB', 'RB1', 'RB2', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
$position_count = count($positions);
$roster_filled = 0;
$postionFilled = array();
$playersPicked = array();
$roster = array();
$index = 0;
$salary_cap = 50000;
$salary_total = 0;
$fptsTotal = 0;

while(($roster_filled < $position_count) && count($copy_positions) > 0) {
	$randPosIndex = array_rand($copy_positions, 1);
	$randPos = $copy_positions[$randPosIndex];
	$index++;

	// var_dump($copy_positions);
	// var_dump($randPos);

	if($index >= 40) {
		$copy_positions = array('QB', 'RB1', 'RB2', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
		$position_count = count($positions);
		$roster_filled = 0;
		$postionFilled = array();
		$playersPicked = array();
		$roster = array();
		$index = 0;
		$salary_cap = 50000;
		$salary_total = 0;
		$fptsTotal = 0;
		continue;
	}

	if(!in_array($randPos, $postionFilled)) {
		$players = array();

		switch($randPos) {
			case 'QB':
				$players = $qbs;
				break;
			case 'RB1':
			case 'RB2':
				$players = $rbs;
				break;
			case 'WR1':
			case 'WR2':
			case 'WR3':
				$players = $wrs;
				break;
			case 'TE':
				$players = $tes;
				break;
			case 'FLEX':
				$players = array_merge($rbs, $wrs, $tes);
				break;
			case 'DST':
				$players = $dsts;
				break;
			default:
				$players = array();
				break;
		}

		if(!empty($players)) {
			$randPlayerIndex = array_rand($players, 1);
			$randPlayer = $players[$randPlayerIndex];
			
			if(((isset($randPlayer['proj_fp']) && $randPlayer['proj_fp'] < $fppg_min) || $randPlayer['fppg'] < $fppg_min) || in_array($randPlayer['name'], $playersPicked) || ($salary_cap - $randPlayer['salary']) < 0) {
				continue;
			}

			// if($randPos == 'QB' && $randPlayer['player_id'] != $locked[0]) {
			// 	$index = 39;
			// 	continue;
			// }

			unset($copy_positions[$randPosIndex]);
			$copy_positions = array_values($copy_positions);

			$postionFilled[] = $randPos;
			$playersPicked[] = $randPlayer['name'];
			$randPlayer['position'] = $randPos;
			$roster['players'][] = $randPlayer;
			$salary_cap -= $randPlayer['salary'];
			$salary_total += $randPlayer['salary'];
			$fptsTotal += isset($randPlayer['proj_fp']) ? $randPlayer['proj_fp'] : $randPlayer['fppg'];

			$roster_filled++;
		}

	} else {
		continue;
	}


	// if($roster_filled == 2) {
	// 	break;
	// }
}

if($salary_min <= $salary_total && $fptsTotal > $fptsMin) {
	$roster['fpts'] = $fptsTotal;
	// var_dump($roster); die;
	usort($roster['players'], "compare");
	// foreach($roster as $_csvRoster) {
	// 	$csv[] = $_csvRoster['dk_player_id'];
	// }
	// $uploadCSV[] = implode(',', $csv);
	$rosters[] = $roster;

	usort($rosters, "compareFPTS");

	// var_dump($rosters); die;
}
else {
	$i--;
}

}

function compareFPTS($a, $b) {
	return $a['fpts'] < $b['fpts'];
}

function compare($a, $b) {

	switch($a['position']) {
		case 'QB':
			$points = 1;
			break;
		case 'RB1':
			$points = 2;
			break;
		case 'RB2':
			$points = 3;
			break;
		case 'WR1':
			$points = 4;
			break;
		case 'WR2':
			$points = 5;
			break;
		case 'WR3':
			$points = 6;
			break;
		case 'TE':
			$points = 7;
			break;
		case 'FLEX':
			$points = 8;
			break;
		case 'DST':
			$points = 9;
			break;
		default:
			$points = -1;
			break;
	}

	switch($b['position']) {
		case 'QB':
			$bpoints = 1;
			break;
		case 'RB1':
			$bpoints = 2;
			break;
		case 'RB2':
			$bpoints = 3;
			break;
		case 'WR1':
			$bpoints = 4;
			break;
		case 'WR2':
			$bpoints = 5;
			break;
		case 'WR3':
			$bpoints = 6;
			break;
		case 'TE':
			$bpoints = 7;
			break;
		case 'FLEX':
			$bpoints = 8;
			break;
		case 'DST':
			$bpoints = 9;
			break;
		default:
			$bpoints = -1;
			break;
	}

	return $points > $bpoints;
}
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<div class="col-md-12">
<?php foreach($rosters as $_roster): ?>
	<?php 
		$totalSalary = 0; 
		$totalPoints = 0;
	?>
	<div class="col-md-6 row" style="margin-top:20px">
	<?php foreach($_roster['players'] as $_player): ?>
		<?php 
			$totalSalary += $_player['salary'];
			$totalPoints += isset($_player['proj_fp']) ? number_format($_player['proj_fp'], 2) : number_format($_player['fppg'], 2);
		?>
		<div class="col-md-12">
			<div class="col-md-1"><?php echo $_player['position'] ?></div>
			<div class="col-md-5"><?php echo $_player['name'] ?></div>
			<div class="col-md-1"><?php echo strtoupper($_player['team']) ?></div>
			<div class="col-md-2"><?php echo isset($_player['proj_fp']) ? number_format($_player['proj_fp'], 2) : number_format($_player['fppg'], 2) ?></div>
			<div class="col-md-2">$<?php echo $_player['salary'] ?></div>
		</div>
	<?php endforeach ?>
		<div class="col-md-12">
			<div class="col-md-1">&nbsp;</div>
			<div class="col-md-5">&nbsp;</div>
			<div class="col-md-1">&nbsp;</div>
			<div class="col-md-2"><?php echo $totalPoints ?></div>
			<div class="col-md-2">$<?php echo $totalSalary ?></div>
		</div>
	</div>
<?php endforeach ?>
</div>
