<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
include_once 'config.php';
include_once 'functions/get_player_functions.php';

$params = $_GET;
// $params['player_id'] = 1;

$dk_db = new DK_DB();
$_player = $dk_db->getPlayer($params['player_id']);
$_player = $_player[0];
$player_id = $_player['player_id'];
$position = $_player['position'];
$team = $_player['team'];

$teamInfo = $dk_db->getTeam($team);
$teamInfo = $teamInfo[0];
$team_url_param = $teamInfo['url_param'];

if($position == 'DST') {
	if($team_url_param == null) {
		$return = array('error' => "Couldn't get team info");

		echo json_encode($return);
		die;
	}

	$teamStats = getTeamStats($team_url_param);

	$dk_db->insertTeamStats($player_id, $teamStats);

	$teamLog = getTeamGameLogs($team_url_param);

	$dk_db->insertTeamGameLog($player_id, $teamLog);

	echo json_encode(array('name' => $_player['name']));

}
