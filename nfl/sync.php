<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
include_once 'config.php';
include_once 'functions/get_player_functions.php';


$dk_db = new DK_DB();

$allPlayers = $dk_db->getPlayerPool($gameDate);
foreach($allPlayers as $_player) {
	if($_player['position'] != 'DST') {
		$players[] = $_player['player_id'];
	}
}

$dst = $dk_db->getAllTeams();
foreach($dst as $_dst) {
	$dstplayers[] = $_dst['player_id'];
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Draftkings Roster Sync</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	</head>
	<script type="text/javascript">
		var players = [<?php echo implode(',', $players) ?>];
		var dstplayers = [<?php echo implode(',', $dstplayers) ?>];
		var idx = 0;
		var dstidx = 0;
		var intervalId;
		var isWaiting = false;
		jQuery(document).ready(function() {
			jQuery(document).on('click', '.sync', function() {
				intervalId = setInterval(syncPlayer, 6000);
			});

			jQuery(document).on('click', '.sync_teams', function() {
				intervalId = setInterval(syncTeam, 6000);
			});
		});

		function syncTeam() {		
			if(dstidx >= dstplayers.length) {
				clearInterval(intervalId);
				var div = jQuery('<div>');
			    div.text('Sync Done');
			    jQuery('.players_done').append(div);
			}	

			if(!isWaiting) {
				isWaiting = true;
				jQuery.ajax({
				  url: '/nfl/syncteam.php?player_id='+dstplayers[dstidx],
				  type: 'GET',
				  success: function(data, textStatus, xhr) {
				  	data = JSON.parse(data);
				  	console.log(data);
				  	isWaiting = false;
				    var div = jQuery('<div>');
				    div.text(data.name + ' synced');
				    jQuery('.players_done').append(div);
				    dstidx++;
				  },
				  error: function(xhr, textStatus, errorThrown) {
				    //called when there is an error
				  }
				});
			}
				
		}

		function syncPlayer() {		
			if(idx >= players.length) {
				clearInterval(intervalId);
				var div = jQuery('<div>');
			    div.text('Sync Done');
			    jQuery('.players_done').append(div);
			}	

			if(!isWaiting) {
				isWaiting = true;
				jQuery.ajax({
				  url: '/nfl/syncplayer.php?player_id='+players[idx],
				  type: 'GET',
				  success: function(data, textStatus, xhr) {
				  	data = JSON.parse(data);
				  	console.log(data);
				  	isWaiting = false;
				    var div = jQuery('<div>');
				    div.text(data.name + ' synced');
				    jQuery('.players_done').append(div);
				    idx++;
				  },
				  error: function(xhr, textStatus, errorThrown) {
				    //called when there is an error
				  }
				});
			}
				
		}
	</script>
	<style type="text/css">
		.loading {
			background-image: url('/images/reload.gif');
			display: none;
			float: left;
			width: 30px;
			height: 30px;
		}
		div {
		    width: 100%;
    		float: left;
		}
	</style>
	<body>
		<div>
			<h3 style="float:left;margin:0;">Sync Teams</h3> <span style="margin-left:10px;"><button class="sync_teams">Sync</button></span>
		</div>
		<div>
			<h3 style="float:left;margin:0;">Sync Players</h3> <span style="margin-left:10px;"><button class="sync">Sync</button></span>
		</div>
		<div>
			<h3 style="float:left;margin:0;">Sync Depth Chart</h3> <span style="margin-left:10px;"><button class="sync">Sync</button></span>
		</div>
		<div class="players_done">
			<span class="loading">&nbsp;</span>
		</div>
	</body>
</html>