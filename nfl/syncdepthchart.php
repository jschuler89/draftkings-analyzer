<?php

<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
include_once 'config.php';
include_once 'functions/get_player_functions.php';

$params = $_GET;
// $params['player_id'] = 1;

$dk_db = new DK_DB();
$_player = $dk_db->getPlayer($params['player_id']);
$_player = $_player[0];

$player_id = $_player['player_id'];
$position = $_player['position'];
$team = $_player['team'];
$player_url_param = $_player['url_param'];

// $teamInfo = $dk_db->getTeam($team);

if($position != 'DST') {
	if($player_url_param == null) {
		$urlparam = validatePlayerUrl($_player);

		if(!$urlparam) {
			$return = array('error' => "Couldn't get player info");

			echo json_encode($return);
			die;
		}
		$dk_db->insertUrlParam($player_id, $urlparam);
	} else {
		$urlparam = $player_url_param;
	}

	$gameLogs = getPlayerGameLogs($urlparam);

	if($gameLogs['img'] != null) {
		$dk_db->insertPlayerImg($player_id, $gameLogs['img']);
	}

	$dk_db->insertGameLog($player_id, $gameLogs);

	echo json_encode(array('name' => $_player['name']));

} else {

}
