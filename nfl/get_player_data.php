<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
require_once 'config.php';
include_once 'functions/get_player_functions.php';

$params = $_GET;
// $params['player_id'] = 1;

$dk_db = new DK_DB();
$playerInfo = $dk_db->getPlayer($params['player_id']);
$teamInfo = $dk_db->getTeam($playerInfo[0]['team']);
$urlparam = $playerInfo[0]['url_param'];

if($playerInfo[0]['position'] != 'DST') {
	if(!$urlparam) {
		$urlparam = validatePlayerUrl($playerInfo[0]);
	}
	
	$gameLogs['player'] = getPlayerGameLogs($urlparam);
	$gameLogs['team'] = getTeamGameLogs($teamInfo[0]['url_param']);
	$gameLogs['team_stats'] = getTeamStats($teamInfo[0]['url_param']);

	echo json_encode($gameLogs);
} else {
	$gameLogs['player'] = getTeamGameLogs($teamInfo[0]['url_param']);
	$gameLogs['player']['img'] = getTeamImage($teamInfo[0]['url_param']);
	$gameLogs['team_stats'] = getTeamStats($teamInfo[0]['url_param']);

	echo json_encode($gameLogs);
}


