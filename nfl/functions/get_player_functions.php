<?php
include_once 'config.php';

function getTeamImage($urlparam)
{
	global $useYahoo;
	$footballUrl = "http://www.pro-football-reference.com/teams/$urlparam/2016.htm";

	if($useYahoo) {
		$yqlURL = "https://query.yahooapis.com/v1/public/yql?q=";
		$selectParam = "select * from html where url='' and xpath='//div[contains(@class, \"media-item\")]//img'";
		$completeUrl = $yqlURL.str_replace('+', '%20', rawurlencode($selectParam))."&format=json";

		$results = file_get_contents($completeUrl);
		$json = json_decode($results);

		return $json->query->results->img->src;
	} else {
		$results = file_get_contents($footballUrl);
		$doc = new DOMDocument();
		$doc->loadHTML($results);

		$xpath = new DOMXpath($doc);

		$elements = $xpath->query("//div[contains(@class, \"media-item\")]//img");

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		foreach($element->attributes as $_attribute) {
		  			if($_attribute->name == 'src') {
		  				return $_attribute->value;
		  			}
		  		}
		  	}
	  	}
	}

	return '';
}

function convertTeamToUrl($urlparam)
{
	switch(strtolower($urlparam)) {
		case 'ari':
			$urlparam = 'crd';
			break;
		case 'bal':
			$urlparam = 'rav';
			break;
		case 'ind':
			$urlparam = 'clt';
			break;
		case 'oak':
			$urlparam = 'rai';
			break;
		case 'ten':
			$urlparam = 'oti';
			break;
		case 'hou':
			$urlparam = 'htx';
			break;
		case 'lar':
			$urlparam = 'ram';
			break;
		default:
			$urlparam = $urlparam;
			break;
	}

	return $urlparam;
}

function getTeamStats($urlparam)
{
	global $useYahoo;
	$footballUrl = "http://www.pro-football-reference.com/teams/$urlparam/2016.htm";

	if($useYahoo) {
		$yqlURL = "https://query.yahooapis.com/v1/public/yql?q=";
		$selectParam = "select * from html where url='' and xpath='//table[contains(@id, \"team_stats\")]//tbody'";
		$completeUrl = $yqlURL.str_replace('+', '%20', rawurlencode($selectParam))."&format=json";

		// $image = getTeamImage($urlparam);

		$results = file_get_contents($completeUrl);
		$json = json_decode($results);

		// var_dump($json->query->results); die;

		$gameLogs = $json->query->results->tbody->tr;
		$gameArray = array();
		// $gameArray['img'] = $image;
		// $week = 1;

		foreach($gameLogs as $_gamelog) {
			$localArray = array();

			// var_dump($_gamelog); die;


			foreach($_gamelog->td as $index => $_info) {

				$_info = (array)$_info;

				if(isset($_info->a)) {
					$_info = array_merge($_info, (array)$_info->a);
				}

				if(!isset($_info['data-stat'])) {
					continue;
				}

				// $localArray['week_num'] = $week;
				// var_dump($_info);

				if(isset($_info['content'])) {
					if(isset($_info['a'])) {
						$localArray[$_info['data-stat']] = $_info['a']->content;
					} else {
						$localArray[$_info['data-stat']] = $_info['content'];
					}
				}
			}

			// $week++;

			if(!empty($localArray)) {
				$gameArray['gamelogs'][] = $localArray;
			}
		}
	} else {
		$results = file_get_contents($footballUrl);
		$doc = new DOMDocument();
		$doc->loadHTML(str_replace(array('<!--', '-->'), '',$results));

		$xpath = new DOMXpath($doc);
		$gamesPlayed = false;
		// $week = 1;

		$elements = $xpath->query("//table[contains(@id, \"team_stats\")]//tbody//tr");
		$gamesPlayedElement = $xpath->query("//table[contains(@id, \"passing\")]//tfoot//tr");

		// var_dump($footballUrl);
		// var_dump($gamesPlayedElement); die;

		if (!is_null($gamesPlayedElement)) {
		  	foreach ($gamesPlayedElement as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			// $localArray['week_num'] = $week;

		  			$attributes = $_childNode->attributes;


		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			if($data_stat == 'g') {
		  				$gamesPlayed = $_childNode->nodeValue;
		  			}

		  		}

		  		if($gamesPlayed) {
		  			$gameArray['games_played'] = $gamesPlayed;
		  			break;
		  		}
		  	}
		}

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			// $localArray['week_num'] = $week;

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;
		  		}

		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}

				// $week++;
		  	}
	  	}
	}

	return $gameArray;
}

function getTeamGameLogs($urlparam)
{
	global $useYahoo;
	$footballUrl = "http://www.pro-football-reference.com/teams/$urlparam/2016_games.htm";
	$gameArray = array();

	if($useYahoo) {
		$yqlURL = "https://query.yahooapis.com/v1/public/yql?q=";
		$selectParam = "select * from html where url='' and xpath='//table[contains(@class, \"stats_table\")]//tbody'";
		$completeUrl = $yqlURL.str_replace('+', '%20', rawurlencode($selectParam))."&format=json";


		$results = file_get_contents($completeUrl);
		$json = json_decode($results);

		// var_dump($json->query->results); die;

		$gameLogs = $json->query->results->tbody->tr;
		$week = 1;

		foreach($gameLogs as $_gamelog) {
			$localArray = array();

			// var_dump($_gamelog); die;


			foreach($_gamelog->td as $index => $_info) {

				$_info = (array)$_info;

				if(isset($_info->a)) {
					$_info = array_merge($_info, (array)$_info->a);
				}

				if(!isset($_info['data-stat'])) {
					continue;
				}

				$localArray['week_num'] = $week;
				// var_dump($_info);

				if(isset($_info['content'])) {
					if(isset($_info['a'])) {
						$localArray[$_info['data-stat']] = $_info['a']->content;
					} else {
						$localArray[$_info['data-stat']] = $_info['content'];
					}
				}
			}

			$week++;

			if(!empty($localArray)) {
				$gameArray['gamelogs'][] = $localArray;
			}
		}
	} else {
		$results = file_get_contents($footballUrl);
		$doc = new DOMDocument();
		$doc->loadHTML($results);

		$xpath = new DOMXpath($doc);
		$week = 1;

		$elements = $xpath->query("//table[contains(@id, \"games\")]//tbody//tr");

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$localArray['week_num'] = $week;

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;
		  		}

		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}

				$week++;
		  	}
	  	}
	}

	return $gameArray;
}

function getPlayerImage($urlparam)
{
	global $useYahoo;

	$footballUrl = "http://www.pro-football-reference.com/players/$urlparam.htm";

	if($useYahoo) {
		$yqlURL = "https://query.yahooapis.com/v1/public/yql?q=";
		$selectParam = "select * from html where url='' and xpath='//div[contains(@class, \"media-item\")]//img'";
		$completeUrl = $yqlURL.str_replace('+', '%20', rawurlencode($selectParam))."&format=json";

		$results = file_get_contents($completeUrl);
		$json = json_decode($results);

		return $json->query->results->img->src;
	} else {
		$results = file_get_contents($footballUrl);
		$doc = new DOMDocument();
		$doc->loadHTML($results);

		$xpath = new DOMXpath($doc);

		$elements = $xpath->query("//div[contains(@class, \"media-item\")]//img");

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		foreach($element->attributes as $_attribute) {
		  			if($_attribute->name == 'src') {
		  				return $_attribute->value;
		  			}
		  		}
		  	}
	  	}
	}

	return '';
}

function getDepthCharts()
{
	
}

function getPlayerGameLogs($urlparam)
{
	global $useYahoo;
	$footballUrl = "http://www.pro-football-reference.com/players/$urlparam.htm";
	$gameArray = array();
	$image = getPlayerImage($urlparam);
	$gameArray['img'] = $image;

	if($useYahoo) {
		$yqlURL = "https://query.yahooapis.com/v1/public/yql?q=";
		$selectParam = "select * from html where url='$footballUrl' and xpath='//table[contains(@class, \"row_summable\")]//tbody'";
		$completeUrl = $yqlURL.str_replace('+', '%20', rawurlencode($selectParam))."&format=json";


		$results = file_get_contents($completeUrl);
		$json = json_decode($results);
		$gameLogs = $json->query->results->tbody[0]->tr;


		foreach($gameLogs as $_gamelog) {
			$localArray = array();

			foreach($_gamelog->td as $_info) {

				$_info = (array)$_info;

				if(isset($_info->a)) {
					$_info = array_merge($_info, (array)$_info->a);
				}

				if(!isset($_info['data-stat'])) {
					continue;
				}
				// var_dump($_info);

				if(isset($_info['content'])) {
					if(isset($_info['a'])) {
						$localArray[$_info['data-stat']] = $_info['a']->content;
					} else {
						$localArray[$_info['data-stat']] = $_info['content'];
					}
				}
			}

			if(!empty($localArray)) {
				$gameArray['gamelogs'][] = $localArray;
			}
		}
	} else {
		$results = file_get_contents($footballUrl);
		$doc = new DOMDocument();
		@$doc->loadHTML($results);

		$xpath = new DOMXpath($doc);

		$elements = $xpath->query("//table[contains(@id, \"stats\")]//tbody//tr[contains(@id, \"stats\")]");

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			if($data_stat == 'opp') {
		  				$localArray['opp_stats'] = getTeamStats(convertTeamToUrl(strtolower($_childNode->nodeValue)));
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;
		  		}

		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}
		  	}
		}
	}

	return $gameArray;
}

function validatePlayerUrl($playerInfo)
{
	global $useYahoo;
	$max = 10;
	$index = 0;
	$position = $playerInfo['position'];
	$urlparam = false;

	while($index < $max) {
		$fullname = str_replace(array(' Jr.', ' III', ' Sr.'), '', $playerInfo['name']);
		$name = explode(' ', $fullname);
		$first_name = $name[0];
		$last_name = $name[1];
		$playerValidated = false;

		$first_name_formatted = substr($first_name, 0, 2);
		$last_name_formatted = substr($last_name,0, 4);
		$category = $last_name[0];
		$indexNumber = '0'.$index;

		$urlparam = "$category/".$last_name_formatted.$first_name_formatted.$indexNumber;
		$footballUrl = "http://www.pro-football-reference.com/players/$urlparam.htm";

		if($useYahoo) {
			$yqlURL = "https://query.yahooapis.com/v1/public/yql?q=";
			$selectParam = "select * from html where url='$footballUrl' and xpath='//div//h1'";

			$completeUrl = $yqlURL.str_replace('+', '%20', rawurlencode($selectParam))."&format=json";
			$results = file_get_contents($completeUrl);
			$jsonName = json_decode($results);
			
			$yqlURL = "https://query.yahooapis.com/v1/public/yql?q=";
			$selectParam = "select * from html where url='$footballUrl' and xpath='//div[contains(@id, \"meta\")]//div'";

			$completeUrl = $yqlURL.str_replace('+', '%20', rawurlencode($selectParam))."&format=json";

			$results = file_get_contents($completeUrl);
			$json = json_decode($results);

			// var_dump($json->query->results->div[1]->p[3]->strong); die;

			if(trim($jsonName->query->results->h1->content) == $fullname && !is_array($json->query->results->div) && $json->query->results->div->p[3]->strong == 'Team' && trim(str_replace(': ', '', $json->query->results->div->p[1]->content)) == $position) {
				break;

			} else if(trim($jsonName->query->results->h1->content) == $fullname && is_array($json->query->results->div) && $json->query->results->div[1]->p[3]->strong == 'Team') {
				break;

			} else if(trim($jsonName->query->results->h1->content) == $fullname && is_array($json->query->results->div) && $json->query->results->div[1]->p[3]->strong == 'Team' && trim(str_replace(array(': ', 'Right', 'Left'), '', $json->query->results->div[1]->p[1]->content)) == $position) {
				break;
			}
		} else {
			$results = file_get_contents($footballUrl);
			$doc = new DOMDocument();
			@$doc->loadHTML($results);

			$xpath = new DOMXpath($doc);

			$elements = $xpath->query("//div//h1");

			if (!is_null($elements)) {
			  	foreach ($elements as $element) {
			  		$jsonName = $element->textContent;
		  		}
		  	}

		  	$results = file_get_contents($footballUrl);
			$doc = new DOMDocument();
			@$doc->loadHTML($results);

			$xpath = new DOMXpath($doc);

			$elements = $xpath->query("//div[contains(@id, \"meta\")]//div");

		  	if (!is_null($elements)) {
			  	foreach ($elements as $element) {
			  		if($element->textContent == '') {
			  			continue;
			  		}

			  		$textArray = explode(' ', trim($element->textContent));

			  		$jsonName = trim($textArray[0] . ' ' . $textArray[1]);

			  		if($jsonName == $fullname) {
			  			$playerValidated = true;
			  			break;
			  		}
		  		}
		  	}

		  	if($playerValidated) {
		  		break;
		  	}
		}


		$index++;
	}

	return $urlparam;
}