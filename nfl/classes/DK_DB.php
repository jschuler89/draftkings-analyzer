<?php
class DK_DB 
{
	public $conn;

	public function __construct()
	{
		$DBServer = 'localhost'; // e.g 'localhost' or '192.168.1.100'
		$DBUser   = 'root';
		$DBPass   = 'root';
		$DBName   = 'dk_fb';

		$this->conn = mysqli_connect($DBServer, $DBUser, $DBPass, $DBName);

		if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}

	public function query($sql)
	{
		$result = mysqli_query($this->conn, $sql);

		if(is_object($result)) {
			$row = mysqli_fetch_all($result, MYSQLI_ASSOC);
		} else {
			$row = null;
		}

		return $row;
	}

	public function getPlayer($player_id)
	{
		$selectSmt = "SELECT * FROM players where player_id= $player_id";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getAllTeams()
	{
		$selectSmt = "SELECT * FROM teams";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getTeam($team)
	{
		$selectSmt = "SELECT * FROM teams where team = '$team'";

		$rs = $this->query($selectSmt);

		return $rs;
	}


	public function getAllPlayers($somePlayers = false)
	{
		$selectSmt = "SELECT * FROM players";

		if($somePlayers) {
			$selectSmt = "SELECT * FROM players where player_id not in (select distinct player_id from player_game_logs) and position <> 'DST'";
		}

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function setDKPlayerId($name, $dk_player_id)
	{
		$selectSmt = "SELECT * FROM players where name = '$name'";
		$rs = $this->query($selectSmt);
		$rs = reset($rs);
		$player_id = $rs['player_id'];

		$updateSmt = "UPDATE player_pool set dk_player_id = $dk_player_id where player_id = $player_id";
		$rs = $this->query($updateSmt);

		return $rs;
	}

	public function getPlayerPool($gameDate)
	{
		$selectSmt = "SELECT * FROM player_pool pp left join players p on p.player_id = pp.player_id where pp.game_date = '$gameDate'";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getPlayerPoolByPosition($gameDate, $position)
	{
		$selectSmt = "SELECT * FROM player_pool pp left join players p on p.player_id = pp.player_id where pp.game_date = '$gameDate' and p.position = '$position'";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getPlayerPoolTeamStats($gameDate)
	{
		$selectSmt = "SELECT (ts.off_rush_td / ts.games_played) as off_rush_td, (ts.def_rush_td / ts.games_played) as def_rush_td, (ts.off_pass_td / ts.games_played) as off_pass_td, (ts.def_pass_td / ts.games_played) as def_pass_td, (ts.def_points / ts.games_played) as def_points, (ts.off_points / ts.games_played) as off_points, (ts.def_rush_yds / ts.games_played) as def_rush_yds, (ts.def_pass_yds / ts.games_played) as def_pass_yds, (ts.def_pass_yds + ts.def_rush_yds) / ts.games_played as def_total_yds, (ts.off_pass_yds + ts.off_rush_yds) / ts.games_played as total_yds, pp.*, p.position, p.team, p.name FROM team_stats ts left join players p on p.player_id = ts.player_id left join player_pool pp on pp.player_id = ts.player_id where pp.game_date = '$gameDate'";

		$rs = $this->query($selectSmt);

		foreach($rs as $index => $_result) {
			$player_id = $_result['player_id'];

			$selectSmt = "SELECT ts.* FROM team_game_logs ts left join players p on p.player_id = ts.player_id left join player_pool pp on pp.player_id = ts.player_id where pp.game_date = '$gameDate' and pp.player_id = $player_id and ts.opp != 'Bye' order by ts.week_num desc limit 5";

			$result = $this->query($selectSmt);
			$pass_yds_def = 0;
			$rush_yds_def = 0;
			$pts_def = 0;
			$pts_off = 0;

			foreach($result as $_rs) {
				$rush_yds_def += $_rs['rush_yds_def'];
				$pass_yds_def += $_rs['pass_yds_def'];
				$pts_off += $_rs['pts_off'];
				$pts_def += $_rs['pts_def'];
			}


			$rs[$index]['last_5_def_rush_yds'] = $rush_yds_def / 5;
			$rs[$index]['last_5_def_pass_yds'] = $pass_yds_def / 5;
			$rs[$index]['last_5_def_points'] = $pts_def / 5;
			$rs[$index]['last_5_off_points'] = $pts_off / 5;
		}

		return $rs;
	}

	public function calculateFP($data)
	{
		/*
		((pgl.pass_td * 4) + (pgl.pass_yds * 0.04) - (pgl.pass_int * 1) + (if(pgl.pass_yds >= 300, 3, 0)) + (pgl.rush_td * 6) + (pgl.rush_yds * 0.1) + (pgl.rec * 1) + (pgl.rec_yds * 0.1) + (pgl.rec_td * 6) + (if(pgl.rush_yds >= 100, 3, 0)) + (if(pgl.rec_yds >= 100, 3, 0)))*/
		$fp = 0;
		$fp += 	$data['pass_td'] * 4;
		$fp += 	$data['pass_yds'] * 0.04;
		$fp -= 	$data['pass_int'] * 1;
		$fp += 	($data['pass_yds'] >= 300) ? 3 : 0;
		$fp += 	($data['rush_td'] < 0) ? 0 : $data['rush_td'] * 6;
		$fp += 	$data['rush_yds'] * 0.1;
		$fp += 	$data['rec'] * 1;
		$fp += 	$data['rec_yds'] * 0.1;
		$fp += 	$data['rec_td'] * 6;
		$fp += 	($data['rush_yds'] >= 100) ? 3 : 0;
		$fp += 	($data['rec_yds'] >= 100) ? 3 : 0;

		return $fp;
	}

	public function getLeagueAvg($stat = '')
	{
		if($stat == '') {
			return 0;
		}

		$selectSmt = "SELECT avg($stat / games_played) as league_avg FROM team_stats";

		$rs = $this->query($selectSmt);

		return $rs[0]['league_avg'];

		// $avg = 0;

		// $gamesPlayed = 0;

		// $teams = count($rs);

		// foreach($rs as $_result) {
		// 	$gamesPlayed += $_result['games_played'];

		// 	$avg += ($_result[$stat] / $_result['games_played']);
		// }

		// return $avg / $teams;
	}

	public function getPlayerPoolStatsByPosition($gameDate, $position)
	{
		$selectSmt = "SELECT pp.*, p.*, min((pgl.pass_td * 4) - (pgl.pass_int * 1) + (pgl.pass_yds * 0.04) + (if(pgl.pass_yds >= 300, 3, 0)) + (pgl.rush_td * 6) + (pgl.rush_yds * 0.1) + (pgl.rec * 1) + (pgl.rec_yds * 0.1) + (pgl.rec_td * 6) + (if(pgl.rush_yds >= 100, 3, 0)) + (if(pgl.rec_yds >= 100, 3, 0))) as min_fppg, max((pgl.pass_td * 4) + (pgl.pass_yds * 0.04) - (pgl.pass_int * 1) + (if(pgl.pass_yds >= 300, 3, 0)) + (pgl.rush_td * 6) + (pgl.rush_yds * 0.1) + (pgl.rec * 1) + (pgl.rec_yds * 0.1) + (pgl.rec_td * 6) + (if(pgl.rush_yds >= 100, 3, 0)) + (if(pgl.rec_yds >= 100, 3, 0))) as max_fppg, min(pgl.rush_yds) as min_rush_yds, max(pgl.rush_yds) as max_rush_yds, avg(pgl.pass_cmp) as pass_cmp, avg(pgl.pass_yds) as pass_yds, avg(pgl.pass_att) as pass_att, avg(pgl.pass_td) as pass_td, avg(pgl.pass_rating) as pass_rating, avg(pgl.pass_int) as pass_int, avg(pgl.pass_sacked) as pass_sacked, avg(pgl.pass_sacked_yds) as pass_sacked_yds, avg(pgl.rush_att) as rush_att, avg(pgl.rush_yds) as rush_yds, avg(pgl.rush_td) as rush_td, avg(pgl.targets) as targets, avg(pgl.rec) as rec, avg(pgl.rec_yds) as rec_yds, avg(pgl.rec_td) as rec_td, avg(pgl.all_td) as all_td FROM player_pool pp left join players p on p.player_id = pp.player_id left join player_game_logs pgl on pgl.player_id = p.player_id where pp.game_date = '$gameDate' and p.position = '$position' group by pp.player_id";

		$rs = $this->query($selectSmt);

		foreach($rs as $index => $_rs) {
			$player_id = $_rs['player_id'];
			$pass_yds = $_rs['pass_yds'];
			$fppg = $_rs['fppg'];

			if($position == 'QB') {
				$pass_yds_consist = "select pass_yds from player_game_logs where player_id = $player_id";
				$results = $this->query($pass_yds_consist);

				if(count($results) > 0) {
					$pass_yds_count = 0;

					foreach($results as $_result) {
						if($pass_yds <= $_result['pass_yds']) {
							$pass_yds_count++;
						}
					}

					$rs[$index]['pass_yds_consist'] = number_format( ($pass_yds_count / count($results)) * 100 , 2);
				} else {
					$rs[$index]['pass_yds_consist'] = 0;
				}
			}

			// use opp yds for fantasy projections
			// $dstPlayers = $dk_db->getPlayerPoolTeamStats($gameDate);

			$rs[$index]['proj_fp'] = number_format($this->calculateFP($_rs), 2);

			$fppg_consist_count = 0;
			$fppg_total = "select ((pgl.pass_td * 4) + (pgl.pass_yds * 0.04) - (pgl.pass_int * 1) + (if(pgl.pass_yds >= 300, 3, 0)) + (pgl.rush_td * 6) + (pgl.rush_yds * 0.1) + (pgl.rec * 1) + (pgl.rec_yds * 0.1) + (pgl.rec_td * 6) + (if(pgl.rush_yds >= 100, 3, 0)) + (if(pgl.rec_yds >= 100, 3, 0))) as fppg_total from player_game_logs pgl where player_id = $player_id";
			$results = $this->query($fppg_total);

			if(count($results) > 0) {
				foreach($results as $_result) {
					if($fppg <= $_result['fppg_total']) {
						$fppg_consist_count++;
					}
				}

				$rs[$index]['fppg_consist'] = number_format(($fppg_consist_count / count($results) * 100) ,2);
			} else {
				$rs[$index]['fppg_consist'] = 0;
			}

			$pass_yds_last_5 = "select avg(pgl.pass_yds) as pass_yds_last_5 from (select pass_yds from player_game_logs where player_id = $player_id order by week_num desc limit 5) pgl ";
			$results = $this->query($pass_yds_last_5);
			$rs[$index]['pass_yds_last_5'] = $results[0]['pass_yds_last_5'];

			$rec_yds_last_3 = "select avg(pgl.rec_yds) as rec_yds_last_3 from (select rec_yds from player_game_logs where player_id = $player_id order by week_num desc limit 3) pgl ";
			$results = $this->query($rec_yds_last_3);
			$rs[$index]['rec_yds_last_3'] = $results[0]['rec_yds_last_3'];

			$rec_yds_last_5 = "select avg(pgl.rec_yds) as rec_yds_last_5 from (select rec_yds from player_game_logs where player_id = $player_id order by week_num desc limit 5) pgl ";
			$results = $this->query($rec_yds_last_5);
			$rs[$index]['rec_yds_last_5'] = $results[0]['rec_yds_last_5'];

			$rush_yds_last_3 = "select avg(pgl.rush_yds) as rush_yds_last_3 from (select rush_yds from player_game_logs where player_id = $player_id order by week_num desc limit 3) pgl ";
			$results = $this->query($rush_yds_last_3);
			$rs[$index]['rush_yds_last_3'] = $results[0]['rush_yds_last_3'];

			$rush_yds_last_5 = "select avg(pgl.rush_yds) as rush_yds_last_5 from (select rush_yds from player_game_logs where player_id = $player_id order by week_num desc limit 5) pgl ";
			$results = $this->query($rush_yds_last_5);
			$rs[$index]['rush_yds_last_5'] = $results[0]['rush_yds_last_5'];

			$fppg_last_3 = "select avg(pglogs.fppg_last) as fppg_last_3 from (select ((pgl.pass_td * 4) + (pgl.pass_yds * 0.04) - (pgl.pass_int * 1) + (if(pgl.pass_yds >= 300, 3, 0)) + (pgl.rush_td * 6) + (pgl.rush_yds * 0.1) + (pgl.rec * 1) + (pgl.rec_yds * 0.1) + (pgl.rec_td * 6) + (if(pgl.rush_yds >= 100, 3, 0)) + (if(pgl.rec_yds >= 100, 3, 0))) as fppg_last from player_game_logs pgl where player_id = $player_id order by week_num desc limit 3) pglogs ";
			$results = $this->query($fppg_last_3);
			$rs[$index]['fppg_last_3'] = $results[0]['fppg_last_3'];

			$rec_last_5 = "select avg(pgl.rec) as rec_last_5 from (select rec from player_game_logs where player_id = $player_id order by week_num desc limit 5) pgl ";
			$results = $this->query($rec_last_5);
			$rs[$index]['rec_last_5'] = $results[0]['rec_last_5'];

			$rec_last_3 = "select avg(pgl.rec) as rec_last_3 from (select rec from player_game_logs where player_id = $player_id order by week_num desc limit 3) pgl ";
			$results = $this->query($rec_last_3);
			$rs[$index]['rec_last_3'] = $results[0]['rec_last_3'];

			$targets_last_5 = "select avg(pgl.targets) as targets_last_5 from (select targets from player_game_logs where player_id = $player_id order by week_num desc limit 5) pgl ";
			$results = $this->query($targets_last_5);
			$rs[$index]['targets_last_5'] = $results[0]['targets_last_5'];

			$targets_last_3 = "select avg(pgl.targets) as targets_last_3 from (select targets from player_game_logs where player_id = $player_id order by week_num desc limit 3) pgl ";
			$results = $this->query($targets_last_3);
			$rs[$index]['targets_last_3'] = $results[0]['targets_last_3'];
		}

		return $rs;
	}

	public function getGameTimes($gameDate)
	{
		$selectSmt = "SELECT DISTINCT game_time FROM player_pool where game_date = '$gameDate' order by game_time";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function insertUrlParam($player_id, $url_param)
	{
		if(empty($player_id) || empty($url_param)) {
			return false;
		}

		$selectSmt = "SELECT url_param from players where player_id = $player_id";

		$rs = $this->query($selectSmt);

		if($rs[0]['url_param'] != null) {
			return false;
		}

		$updateSmt = "UPDATE players set url_param = '$url_param' where player_id = $player_id";

		$rs = $this->query($updateSmt);

		return $rs;
	}

	public function insertPlayerImg($player_id, $img)
	{
		if(empty($player_id) || empty($img)) {
			return false;
		}

		$selectSmt = "SELECT img from players where player_id = $player_id";

		$rs = $this->query($selectSmt);

		if($rs[0]['img'] != null) {
			return false;
		}

		$updateSmt = "UPDATE players set img = '$img' where player_id = $player_id";

		$rs = $this->query($updateSmt);

		return $rs;
	}

	public function insertPlayer($playerInfo = array())
	{
		if(empty($playerInfo)) {
			return false;
		}

		$position = $playerInfo[0];
		$name = $playerInfo[1];
		$salary = $playerInfo[2];
		$gameInfo = $playerInfo[3];
		$fppg = $playerInfo[4];
		$team = $playerInfo[5];

		$selectSmt = "SELECT * FROM players where name = $name and team = $team and position = $position";

		$rs = $this->query($selectSmt);

		if(empty($rs)) {
			$insertSmt = "INSERT INTO players(name, team, position) VALUES($name, $team, $position)";
			$rs = $this->query($insertSmt);
		}

		$selectSmt = "SELECT * FROM players where name = $name and team = $team and position = $position";

		$rs = $this->query($selectSmt);

		return $rs[0]['player_id'];
	}

	public function insertGameLog($player_id, $gamelog) 
	{
		$columns = "SHOW COLUMNS FROM player_game_logs";
		$result = $this->query($columns);

		foreach($gamelog['gamelogs'] as $_game) {
			$values = array();
			$keys = array();
			$alreadyLoaded = false;

			foreach($result as $_column) {
				if(isset($_game[$_column['Field']])) {

					if($_column['Field'] == 'week_num') {
						$week_num = $_game[$_column['Field']];
						$selectSmt = "SELECT count(*) as count from player_game_logs where player_id = $player_id and week_num = $week_num";
						$rs = $this->query($selectSmt);

						if(isset($rs[0]) && isset($rs[0]['count']) && $rs[0]['count'] > 0) {
							$alreadyLoaded = true;
							break;
						}
					}
					$values[] = $_game[$_column['Field']];
					$keys[] = $_column['Field'];
				}
				
			}

			if($alreadyLoaded) {
				continue;
			}

			$keysString = implode(',', $keys);
			$valuesString = implode("','", $values);

			$insertSmt = "INSERT INTO player_game_logs(player_id,$keysString) values($player_id,'$valuesString')";

			$rs = $this->query($insertSmt);
		}

		return true;
	}

	public function getRushYdsBufferLast5($player_data)
	{
		$player_id = $player_data['player_id'];

		$selectSmt = "SELECT pgl.* FROM player_game_logs pgl where pgl.player_id = $player_id order by week_num asc limit 5";

		$rs = $this->query($selectSmt);
		$buffer = 0;
		$count = 0;
		$rush_yds = 0;

		if($rs) {
			foreach($rs as $_gamelog) {
				$opp_player_id = $_gamelog['opp_player_id'];
				$week_num = $_gamelog['week_num'];

				$selectSmt = "SELECT * FROM team_game_logs where player_id = $opp_player_id and week_num = $week_num";

				$result = $this->query($selectSmt);


				if($result) {
					$rush_yds += $_gamelog['rush_yds'];
					$count++;
					$rush_yds_per_game = $rush_yds / $count;
					$result = $result[0];
					$buffer += $rush_yds_per_game - $result['rush_yds_def'];
					// if($player_id == 4) {
					// 	var_dump($rush_yds_per_game);
					// 	var_dump($rush_yds);
					// 	var_dump($result);
					// 	var_dump($buffer);
					// }
				} else {
					continue;
				}
			}
		} else {
			return 0;
		}

		if($buffer == 0) {
			return 0;
		}

		return ($buffer / $count);
	}

	public function getRushYdsBuffer($player_data)
	{
		$player_id = $player_data['player_id'];

		$selectSmt = "SELECT pgl.* FROM player_game_logs pgl where pgl.player_id = $player_id";

		$rs = $this->query($selectSmt);
		$buffer = 0;
		$count = 0;
		$rush_yds = 0;

		if($rs) {
			foreach($rs as $_gamelog) {
				$opp_player_id = $_gamelog['opp_player_id'];
				$week_num = $_gamelog['week_num'];

				$selectSmt = "SELECT * FROM team_game_logs where player_id = $opp_player_id and week_num = $week_num";

				$result = $this->query($selectSmt);

				if($result) {
					$rush_yds += $_gamelog['rush_yds'];
					$count++;
					$rush_yds_per_game = $rush_yds / $count;
					$result = $result[0];
					$buffer += $rush_yds_per_game - $result['rush_yds_def'];
					// if($player_id == 4) {
					// 	var_dump($rush_yds_per_game);
					// 	var_dump($rush_yds);
					// 	var_dump($result);
					// 	var_dump($buffer);
					// }
				} else {
					continue;
				}
			}
		} else {
			return 0;
		}

		if($buffer == 0) {
			return 0;
		}

		return ($buffer / $count);
	}

	public function getPassYdsBufferLast5($player_data)
	{
		$player_id = $player_data['player_id'];

		$selectSmt = "SELECT pgl.* FROM player_game_logs pgl where pgl.player_id = $player_id order by week_num asc limit 5";

		$rs = $this->query($selectSmt);
		$buffer = 0;
		$count = 0;
		$pass_yds = 0;

		if($rs) {
			foreach($rs as $_gamelog) {
				$opp_player_id = $_gamelog['opp_player_id'];
				$week_num = $_gamelog['week_num'];

				$selectSmt = "SELECT * FROM team_game_logs where player_id = $opp_player_id and week_num = $week_num";

				$result = $this->query($selectSmt);


				if($result) {
					$pass_yds += $_gamelog['pass_yds'];
					$count++;
					$pass_yds_per_game = $pass_yds / $count;
					$result = $result[0];
					$buffer += $pass_yds_per_game - $result['pass_yds_def'];
				} else {
					continue;
				}
			}
		} else {
			return 0;
		}

		if($buffer == 0) {
			return 0;
		}

		return ($buffer / $count);
	}

	public function getPassYdsBuffer($player_data)
	{
		$player_id = $player_data['player_id'];

		$selectSmt = "SELECT pgl.* FROM player_game_logs pgl where pgl.player_id = $player_id";

		$rs = $this->query($selectSmt);
		$buffer = 0;
		$count = 0;
		$pass_yds = 0;

		if($rs) {
			foreach($rs as $_gamelog) {
				$opp_player_id = $_gamelog['opp_player_id'];
				$week_num = $_gamelog['week_num'];

				$selectSmt = "SELECT * FROM team_game_logs where player_id = $opp_player_id and week_num = $week_num";

				$result = $this->query($selectSmt);

				if($result) {
					$pass_yds += $_gamelog['pass_yds'];
					$count++;
					$pass_yds_per_game = $pass_yds / $count;
					$result = $result[0];
					$buffer += $pass_yds_per_game - $result['pass_yds_def'];
				} else {
					continue;
				}
			}
		} else {
			return 0;
		}

		if($buffer == 0) {
			return 0;
		}

		return ($buffer / $count);
	}

	public function insertTeamGameLog($player_id, $teamLog = array())
	{
		if(empty($teamLog) || !$player_id) {
			return false;
		}

		$selectSmt = "SELECT * FROM team_game_logs where player_id = $player_id";
		$result = $this->query($selectSmt);

		if(count($result) > 0) {
			$deleteSmt = "DELETE FROM team_game_logs where player_id = $player_id";
			$this->query($deleteSmt);
		}

		$columns = "SHOW COLUMNS FROM team_game_logs";
		$result = $this->query($columns);

		$fields = array();

		foreach($result as $_result) {
			$fields[$_result['Field']] = $_result['Field'];
		}

		$logs = $teamLog['gamelogs'];


		foreach($logs as $_log) {
			$keys = array();
			$values = array();

			foreach($_log as $index => $_logRow) {
				$column = $index;

				if(empty($_log['game_outcome'])) {
					if($_log['opp'] != 'Bye Week') {
						continue;
					}
				}

				// Get team name 'New England Patriots' -> 'Patriots'
				if($column == 'opp') {
					if($_logRow == 'Bye Week') {
						$_logRow = 'Bye';
					} else {
						$_logRow = explode(' ', $_logRow);
						$_logRow = $_logRow[count($_logRow) - 1];
					}
				}

				if(in_array($column, $fields)) {
					$keys[] = $fields[$column];
					$values[] = $_logRow;
				}
				
			}

			$keysString = implode(',', $keys);
			$valuesString = implode("','", $values);

			$insertSmt = "INSERT INTO team_game_logs(player_id,$keysString) values($player_id,'$valuesString')";

			// var_dump($insertSmt);

			$rs = $this->query($insertSmt);
		}


		return true;
	}

	public function insertTeamStats($player_id, $teamStats = array())
	{
		if(empty($teamStats) || !$player_id) {
			return false;
		}

		$selectSmt = "SELECT * FROM team_stats where player_id = $player_id";
		$result = $this->query($selectSmt);

		if(count($result) > 0) {
			$deleteSmt = "DELETE FROM team_stats where player_id = $player_id";
			$this->query($deleteSmt);
		}

		$columns = "SHOW COLUMNS FROM team_stats";
		$result = $this->query($columns);

		$fields = array();

		foreach($result as $_result) {
			$fields[$_result['Field']] = $_result['Field'];
		}

		// var_dump($fields);

		$gamesPlayed = $teamStats['games_played'];
		$off_stats = $teamStats['gamelogs'][0];
		$def_stats = $teamStats['gamelogs'][1];

		$off_prefix = 'off_';
		$def_prefix = 'def_';

		// var_dump($off_stats);

		$keys = array();
		$values = array();

		foreach($off_stats as $index => $_stat) {
			$column = $off_prefix.$index;

			if(in_array($column, $fields)) {
				$keys[] = $fields[$column];
				$values[] = $_stat;
			}
		}

		foreach($def_stats as $index => $_stat) {
			$column = $def_prefix.$index;

			if(in_array($column, $fields)) {
				$keys[] = $fields[$column];
				$values[] = $_stat;
			}
		}

		$keysString = implode(',', $keys);
		$valuesString = implode("','", $values);

		$insertSmt = "INSERT INTO team_stats(player_id,games_played,$keysString) values($player_id,$gamesPlayed,'$valuesString')";

		$rs = $this->query($insertSmt);

		return true;
	}

	public function insertIntoPlayerPool($player_id, $playerInfo = array())
	{
		if(empty($playerInfo) || !$player_id) {
			return false;
		}

		$gameDate = '2017-01-07';

		$position = $playerInfo[0];
		$name = $playerInfo[1];
		$salary = $playerInfo[2];
		$gameInfo = $playerInfo[3];

		// [0] => Team@Opp, [1] => 01:00PM [2] => ET
		$gameInfoArray = explode(" ", $gameInfo);
		// [0] => Team, [1] => Opp
		$gameArray = explode("@", $gameInfoArray[0]);
		$game_time = str_replace(array('AM', 'PM'), '', $gameInfoArray[1]);
		$opp = trim(str_replace('"', '', strtoupper($gameArray[1])));
		$opp2 = trim(str_replace('"', '', strtoupper($gameArray[0])));
		$fppg = number_format($playerInfo[4], 2);
		$team = trim(str_replace('"', '', $playerInfo[5]));

		if(strtolower($team) == strtolower($opp)) {
			$opp = $opp2;
		}

		$insertSmt = "INSERT INTO player_pool(player_id, fppg, salary, opp, game_date, game_time) VALUES($player_id, $fppg, $salary, '$opp', '$gameDate', '$game_time')";	
		$rs = $this->query($insertSmt);

		return true;
	}
}
?>
