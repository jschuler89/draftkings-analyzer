<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';

$dk_db = new DK_DB();
$pass_td_league_avg = $dk_db->getLeagueAvg('off_pass_td');
$rush_td_league_avg = $dk_db->getLeagueAvg('off_rush_td');
$qbPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'QB');
$rbPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'RB');
$wrPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'WR');
$tePlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'TE');
$dstPlayers = $dk_db->getPlayerPoolTeamStats($gameDate);
$gameTimes = $dk_db->getGameTimes($gameDate);
$teamsArray = array();

foreach($dstPlayers as $_dstplayer) {
	$teamsArray[strtolower($_dstplayer['team'])] = $_dstplayer;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Draftkings Roster Analyzer</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.2.0/css/mdb.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
		<script src="//rawgit.com/wenzhixin/bootstrap-table-fixed-columns/master/bootstrap-table-fixed-columns.js"></script>
		<script src="js/mdb.js"></script>
		<style type="text/css">
			.bootstrap-table {
				width: 100% !important;
				float: left;
			}
			.red {
				color: red;
				background-color: transparent !important;
			}
			.clearfix {
				display: none;
			}
			.setup.active {
				display: block;
			}
			.center {
				text-align: center;
			}
			tr.disabled {
			    opacity: 0.5;
			}
			.bootstrap-table tbody td {
				cursor: pointer;
			}
			.player-table {
				display: none;
			}
			.player-table.active {
				display: initial;
			}
			.table-hover>tbody>tr:hover {
			    background-color: #aaa;
			}
			.injury {
				color: red;
				display: inline-block;
				margin-left: 3px;
				font-size: 11px;
			}
		</style>
		<script type="text/javascript">
			jQuery('table').bootstrapTable({
				fixedColumns: true,
            	fixedNumber:1
        	});

			jQuery(document).ready(function() {

				var filterTime;
				var filterPosition;

				jQuery(document).on('click', '.reset-filter', function() {
					filterTime = undefined;
					filterPosition = undefined;

					jQuery('.player-table tbody tr').show();
				});

				jQuery(document).on('click', '.time-filter', function() {
					filterTime = jQuery(this).text();

					jQuery('.player-table tbody tr').hide();

					if(filterPosition !== undefined) {
						jQuery('.player-table tbody tr[data-filter-position="'+filterPosition+'"][data-filter-gametime="'+filterTime+'"]').show();
					} else {
						jQuery('.player-table tbody tr[data-filter-gametime="'+filterTime+'"]').show();
					}
				});

				jQuery(document).on('click', '.position-filter', function() {
					filterPosition = jQuery(this).text();

					jQuery('.player-table').hide();

					jQuery('.'+filterPosition+'-table').show();

				});				
			});
		</script>
		<script src="https://use.fontawesome.com/b1353e11e4.js"></script>
	</head>
	<body>
		<div>
			<nav class="navbar navbar-default">
			  	<div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="">DK Roster Analyzer</a>
				    </div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="/home.php" class="menu-click" id="roster-setup">Lineups</a></li>
				        <li><a href="/home.php" class="menu-click" id="data-setup">Data Analysis</a></li>
				        <li class="active"><a href="/lineups_detail.php">Detail Lineups</a></li>
				        <li><a href="/generatelineups.php" id="lineups-setup">Generate Lineups</a></li>
				        <li><a href="/sync.php" id="sync-setup">Sync</a></li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
		    	</div>
		    </nav>
		</div>
		<div>
			<div>
				<span><button class="btn btn-primary btn-sm reset-filter">Reset</button></span>
			</div>
			<div>
				<?php foreach($gameTimes as $_gametime): ?>
					<span><button class="btn btn-primary btn-sm time-filter"><?php echo date('H:i', strtotime($_gametime['game_time'])) ?></button></span>
				<?php endforeach ?>
			</div>
			<div>
				<span><button class="btn btn-primary btn-sm position-filter">QB</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">RB</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">WR</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">TE</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">DST</button></span>
			</div>
			<div class="player-table QB-table active">
				<table class="table table-hover" data-toggle="table">
					<thead>
						<tr>
							<th data-sortable="true">Player Name</th>
							<th data-sortable="true">Position</th>
							<th data-sortable="true">Team</th>
							<th data-sortable="true">Opp</th>
							<th data-sortable="true">Time</th>
							<th data-sortable="true">Salary</th>
							<th data-sortable="true">Pt / $</th>
							<th data-sortable="true">Last 5 Pass Yds/G</th>
							<th data-sortable="true">Pass Yds/G</th>
							<th data-sortable="true">Pass Yds Consist</th>
							<th data-sortable="true">Opp Yds Allowed/G</th>
							<th data-sortable="true">Pass Yds Buffer Last 5</th>
							<th data-sortable="true">Pass Yds Buffer</th>
							<th data-sortable="true">Proj Pass Yds(Last 5)</th>
							<th data-sortable="true">Proj Pass Yds</th>
							<th data-sortable="true">Pass Cmp/G</th>
							<th data-sortable="true">Pass Att/G</th>
							<th data-sortable="true">Pass Cmp%/G</th>
							<th data-sortable="true">Pass Tds/G</th>
							<th data-sortable="true">Proj Pass Tds</th>
							<th data-sortable="true">Opp Pass Tds Allowed/G</th>
							<th data-sortable="true">Ints/G</th>
							<th data-sortable="true">Pass Rtg/G</th>
							<th data-sortable="true">Floor</th>
							<th data-sortable="true">Ceiling</th>
							<th data-sortable="true">Last 3 FPPG</th>
							<th data-sortable="true">Proj FP</th>
							<th data-sortable="true">FPPG</th>
							<th data-sortable="true">Upside</th>
							<th data-sortable="true">Consistency</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($qbPlayers as $player): ?>
							<?php 
								$pass_yds_buffer = $dk_db->getPassYdsBuffer($player); 
								$pass_yds_buffer_last5 = $dk_db->getPassYdsBufferLast5($player); 
							  	$player_data = $player;
							  	$off_factor_pass_tds = ($teamsArray[strtolower($player['team'])]['off_pass_td'] - $pass_td_league_avg ) / 0.5;
							  	$def_factor_pass_tds = ($teamsArray[strtolower($player['opp'])]['def_pass_td'] - $pass_td_league_avg ) / 0.5;
							  	$proj_pass_tds = $pass_td_league_avg + $off_factor_pass_tds + $def_factor_pass_tds;
							  	$player_data['pass_td'] = $proj_pass_tds;
							  	$player_data['pass_yds'] = $teamsArray[strtolower($player['opp'])]['def_pass_yds'] + $pass_yds_buffer;
							  	$proj_fp = $dk_db->calculateFP($player_data);
							?>
							<tr data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
								<td class="name"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
								<td class="position"><?php echo $player['position'] ?></td>
								<td class="team"><?php echo $player['team'] ?></td>
								<td class="opp"><?php echo $player['opp'] ?></td>
								<td class="time"><?php echo date('H:i', strtotime($player['game_time'])) ?>PM</td>
								<td class="salary"><?php echo $player['salary'] ?></td>
								<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($player['fppg'] / ($player['salary'] / 1000), 2) ?></td>
								<td class="pass_yds"><?php echo number_format($player['pass_yds_last_5'], 2) ?></td>
								<td class="pass_yds"><?php echo number_format($player['pass_yds'], 2) ?></td>
								<td class="pass_yds_consist"><?php echo number_format($player['pass_yds_consist'], 2) ?>%</td>
								<td class="def_pass_yds"><?php echo number_format($teamsArray[strtolower($player['opp'])]['def_pass_yds'], 2) ?></td>
								<td class="pass_yds_buffer_last5"><?php echo number_format($pass_yds_buffer_last5, 2) ?></td>
								<td class="pass_yds_buffer"><?php echo number_format($pass_yds_buffer, 2) ?></td>
								<td class="proj_pass_yds"><?php echo number_format(($teamsArray[strtolower($player['opp'])]['def_pass_yds'] + $pass_yds_buffer_last5), 2) ?></td>
								<td class="proj_pass_yds"><?php echo number_format(($teamsArray[strtolower($player['opp'])]['def_pass_yds'] + $pass_yds_buffer), 2) ?></td>
								<td class="pass_cmp"><?php echo number_format($player['pass_cmp'], 2) ?></td>
								<td class="pass_att"><?php echo number_format($player['pass_att'], 2) ?></td>
								<td class="pass_pct"><?php echo ($player['pass_att'] > 0) ? number_format(($player['pass_cmp'] / $player['pass_att']) * 100, 2) : 0.00 ?>%</td>
								<td class="pass_td"><?php echo number_format($player['pass_td'], 2) ?></td>
								<td class="pass_td"><?php echo number_format($proj_pass_tds, 2) ?></td>
								<td class="def_pass_td"><?php echo number_format($teamsArray[strtolower($player['opp'])]['def_pass_td'], 2) ?></td>
								<td class="pass_int"><?php echo number_format($player['pass_int'], 2) ?></td>
								<td class="pass_rating"><?php echo number_format($player['pass_rating'], 2) ?></td>
								<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
								<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
								<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
								<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format($proj_fp, 2) : 0 ?></td>
								<td class="fppg"><?php echo $player['fppg'] ?></td>
								<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format((($proj_fp -  $player['fppg']) / $player['fppg']) * 100, 2)  : 0 ?>%</td>
								<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="player-table RB-table">
				<table class="table table-hover" data-toggle="table">
					<thead>
						<tr>
							<th data-sortable="true">Player Name</th>
							<th data-sortable="true">Position</th>
							<th data-sortable="true">Team</th>
							<th data-sortable="true">Opp</th>
							<th data-sortable="true">Time</th>
							<th data-sortable="true">Salary</th>
							<th data-sortable="true">Pt / $</th>
							<th data-sortable="true">Min Rush Yds</th>
							<th data-sortable="true">Max Rush Yds</th>
							<th data-sortable="true">Last 3 Rush Yds/G</th>
							<th data-sortable="true">Last 5 Rush Yds/G</th>
							<th data-sortable="true">Rush Yds/G</th>
							<th data-sortable="true">Opp Allowed Yds/G</th>
							<th data-sortable="true">Rush Yds Buffer Last 5</th>
							<th data-sortable="true">Rush Yds Buffer</th>
							<th data-sortable="true">Proj Rush Yds(Last 5)</th>
							<th data-sortable="true">Proj Rush Yds</th>
							<th data-sortable="true">Rush Att/G</th>
							<th data-sortable="true">Yds/Att</th>
							<th data-sortable="true">Rush Tds/G</th>
							<th data-sortable="true">Opp Allowed Rush Tds/G</th>
							<th data-sortable="true">Proj Rush Tds</th>
							<th data-sortable="true">Last 3 Targets/G</th>
							<th data-sortable="true">Last 5 Targets/G</th>
							<th data-sortable="true">Targets/G</th>
							<th data-sortable="true">Rec/G</th>
							<th data-sortable="true">Rec Yds/G</th>
							<th data-sortable="true">Rec Tds/G</th>
							<th data-sortable="true">Total Tds/G</th>
							<th data-sortable="true">Floor</th>
							<th data-sortable="true">Ceiling</th>
							<th data-sortable="true">Last 3 FPPG</th>
							<th data-sortable="true">Proj FP</th>
							<th data-sortable="true">FPPG</th>
							<th data-sortable="true">Upside</th>
							<th data-sortable="true">Consistency</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($rbPlayers as $player): ?>
							<?php
								$rush_yds_buffer = $dk_db->getRushYdsBuffer($player); 
								$rush_yds_buffer_last5 = $dk_db->getRushYdsBufferLast5($player); 
							  	$player_data = $player;
							  	$off_factor_rush_tds = ($player['rush_td'] - $rush_td_league_avg ) / 0.8;
							  	$def_factor_rush_tds = ($teamsArray[strtolower($player['opp'])]['def_rush_td'] - $rush_td_league_avg ) / 0.8;
							  	$proj_rush_tds = $rush_td_league_avg + $off_factor_rush_tds + $def_factor_rush_tds;
							  	$player_data['rush_td'] = $proj_rush_tds;
							  	$player_data['rush_yds'] = $teamsArray[strtolower($player['opp'])]['def_rush_yds'] + $rush_yds_buffer;
							  	$proj_fp = $dk_db->calculateFP($player_data);
						  	?>
							<tr data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
								<td class="name"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
								<td class="position"><?php echo $player['position'] ?></td>
								<td class="team"><?php echo $player['team'] ?></td>
								<td class="opp"><?php echo $player['opp'] ?></td>
								<td class="time"><?php echo date('H:i', strtotime($player['game_time'])) ?>PM</td>
								<td class="salary"><?php echo $player['salary'] ?></td>
								<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format( $player['fppg'] / ($player['salary'] / 1000), 2) ?></td>
								<td class="min_rush_yds"><?php echo number_format($player['min_rush_yds'], 2) ?></td>
								<td class="max_rush_yds"><?php echo number_format($player['max_rush_yds'], 2) ?></td>
								<td class="rush_yds"><?php echo number_format($player['rush_yds_last_3'], 2) ?></td>
								<td class="rush_yds"><?php echo number_format($player['rush_yds_last_5'], 2) ?></td>
								<td class="rush_yds"><?php echo number_format($player['rush_yds'], 2) ?></td>
								<td class="def_rush_yds"><?php echo number_format($teamsArray[strtolower($player['opp'])]['def_rush_yds'], 2) ?></td>
								<td class="rush_att"><?php echo number_format($rush_yds_buffer_last5, 2) ?></td>
								<td class="rush_att"><?php echo number_format($rush_yds_buffer, 2) ?></td>
								<td class="proj_rush_yds"><?php echo number_format(($teamsArray[strtolower($player['opp'])]['def_rush_yds'] + $rush_yds_buffer_last5), 2) ?></td>
								<td class="proj_rush_yds"><?php echo number_format(($teamsArray[strtolower($player['opp'])]['def_rush_yds'] + $rush_yds_buffer), 2) ?></td>
								<td class="rush_att"><?php echo number_format($player['rush_att'], 2) ?></td>
								<td class="rush_carry"><?php echo ($player['rush_att'] > 0) ? number_format($player['rush_yds'] / $player['rush_att'], 2) : 0 ?></td>
								<td class="rush_td"><?php echo number_format($player['rush_td'], 2) ?></td>
								<td class="rush_td"><?php echo number_format($teamsArray[strtolower($player['opp'])]['def_rush_td'], 2) ?></td>
								<td class="rush_td"><?php echo number_format($proj_rush_tds, 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets_last_3'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets_last_5'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets'], 2) ?></td>
								<td class="rec"><?php echo number_format($player['rec'], 2) ?></td>
								<td class="rec_yds"><?php echo number_format($player['rec_yds'], 2) ?></td>
								<td class="rec_td"><?php echo number_format($player['rec_td'], 2) ?></td>
								<td class="all_td"><?php echo number_format($player['all_td'], 2) ?></td>
								<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
								<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
								<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
								<td class="proj_fp"><?php echo ($player['rush_yds'] > 0) ? number_format($proj_fp, 2) : $player['fppg'] ?></td>
								<td class="fppg"><?php echo $player['fppg'] ?></td>
								<td class="fppg"><?php echo ($player['rush_yds'] > 0) ? number_format((($proj_fp -  $player['fppg']) / $player['fppg']) * 100, 2)  : 0 ?>%</td>
								<td class="fppg"><?php echo $player['fppg_consist'] ?>%</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="player-table WR-table">
				<table class="table table-hover" data-toggle="table">
					<thead>
						<tr>
							<th data-sortable="true">Player Name</th>
							<th data-sortable="true">Position</th>
							<th data-sortable="true">Team</th>
							<th data-sortable="true">Opp</th>
							<th data-sortable="true">Time</th>
							<th data-sortable="true">Salary</th>
							<th data-sortable="true">$ / Pt</th>
							<th data-sortable="true">Last 3 Targets/G</th>
							<th data-sortable="true">Last 5 Targets/G</th>
							<th data-sortable="true">Targets/G</th>
							<th data-sortable="true">Last 3 Rec/G</th>
							<th data-sortable="true">Last 5 Rec/G</th>
							<th data-sortable="true">Rec/G</th>
							<th data-sortable="true">Last 5 Catch %</th>
							<th data-sortable="true">Catch %</th>
							<th data-sortable="true">Last 3 Rec Yds/G</th>
							<th data-sortable="true">Last 5 Rec Yds/G</th>
							<th data-sortable="true">Rec Yds/G</th>
							<th data-sortable="true">Rec Tds/G</th>
							<th data-sortable="true">Rush Yds/G</th>
							<th data-sortable="true">Rush Att/G</th>
							<th data-sortable="true">Yds/Att</th>
							<th data-sortable="true">Rush Tds/G</th>
							<th data-sortable="true">Total Tds/G</th>
							<th data-sortable="true">Floor</th>
							<th data-sortable="true">Ceiling</th>
							<th data-sortable="true">Last 3 FPPG</th>
							<th data-sortable="true">Proj FP</th>
							<th data-sortable="true">FPPG</th>
							<th data-sortable="true">Consistency</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($wrPlayers as $player): ?>
							<tr data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
								<td class="name"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
								<td class="position"><?php echo $player['position'] ?></td>
								<td class="team"><?php echo $player['team'] ?></td>
								<td class="opp"><?php echo $player['opp'] ?></td>
								<td class="time"><?php echo date('H:i', strtotime($player['game_time'])) ?>PM</td>
								<td class="salary"><?php echo $player['salary'] ?></td>
								<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format( ($player['salary'] / 1000) / $player['fppg'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets_last_3'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets_last_5'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets'], 2) ?></td>
								<td class="rec"><?php echo number_format($player['rec_last_3'], 2) ?></td>
								<td class="rec"><?php echo number_format($player['rec_last_5'], 2) ?></td>
								<td class="rec"><?php echo number_format($player['rec'], 2) ?></td>
								<td class="catch_pct"><?php echo ($player['targets_last_5'] > 0) ? number_format(($player['rec_last_5'] / $player['targets_last_5']) * 100, 2) : 0 ?>%</td>
								<td class="catch_pct"><?php echo ($player['targets'] > 0) ? number_format(($player['rec'] / $player['targets']) * 100, 2) : 0 ?>%</td>
								<td class="rec_yds"><?php echo number_format($player['rec_yds_last_3'], 2) ?></td>
								<td class="rec_yds"><?php echo number_format($player['rec_yds_last_5'], 2) ?></td>
								<td class="rec_yds"><?php echo number_format($player['rec_yds'], 2) ?></td>
								<td class="rec_td"><?php echo number_format($player['rec_td'], 2) ?></td>
								<td class="rush_yds"><?php echo number_format($player['rush_yds'], 2) ?></td>
								<td class="rush_att"><?php echo number_format($player['rush_att'], 2) ?></td>
								<td class="rush_carry"><?php echo ($player['rush_att'] > 0) ? number_format($player['rush_yds'] / $player['rush_att'], 2) : 0 ?></td>
								<td class="rush_td"><?php echo number_format($player['rush_td'], 2) ?></td>
								<td class="all_td"><?php echo number_format($player['all_td'], 2) ?></td>
								<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
								<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
								<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
								<td class="proj_fp"><?php echo $player['proj_fp'] ?></td>
								<td class="fppg"><?php echo $player['fppg'] ?></td>
								<td class="fppg"><?php echo $player['fppg_consist'] ?>%</td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="player-table TE-table">
				<table class="table table-hover" data-toggle="table">
					<thead>
						<tr>
							<th data-sortable="true">Player Name</th>
							<th data-sortable="true">Position</th>
							<th data-sortable="true">Team</th>
							<th data-sortable="true">Opp</th>
							<th data-sortable="true">Time</th>
							<th data-sortable="true">Salary</th>
							<th data-sortable="true">$ / Pt</th>
							<th data-sortable="true">Last 3 Targets/G</th>
							<th data-sortable="true">Last 5 Targets/G</th>
							<th data-sortable="true">Targets/G</th>
							<th data-sortable="true">Last 3 Rec/G</th>
							<th data-sortable="true">Last 5 Rec/G</th>
							<th data-sortable="true">Rec/G</th>
							<th data-sortable="true">Last 5 Catch %</th>
							<th data-sortable="true">Catch %</th>
							<th data-sortable="true">Rec Yds/G</th>
							<th data-sortable="true">Rec Tds/G</th>
							<th data-sortable="true">Rush Yds/G</th>
							<th data-sortable="true">Rush Att/G</th>
							<th data-sortable="true">Yds/Att</th>
							<th data-sortable="true">Rush Tds/G</th>
							<th data-sortable="true">Total Tds/G</th>
							<th data-sortable="true">Floor</th>
							<th data-sortable="true">Ceiling</th>
							<th data-sortable="true">Last 3 FPPG</th>
							<th data-sortable="true">FPPG</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($tePlayers as $player): ?>
							<tr data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
								<td class="name"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
								<td class="position"><?php echo $player['position'] ?></td>
								<td class="team"><?php echo $player['team'] ?></td>
								<td class="opp"><?php echo $player['opp'] ?></td>
								<td class="time"><?php echo date('H:i', strtotime($player['game_time'])) ?>PM</td>
								<td class="salary"><?php echo $player['salary'] ?></td>
								<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format( ($player['salary'] / 1000) / $player['fppg'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets_last_3'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets_last_5'], 2) ?></td>
								<td class="targets"><?php echo number_format($player['targets'], 2) ?></td>
								<td class="rec"><?php echo number_format($player['rec_last_3'], 2) ?></td>
								<td class="rec"><?php echo number_format($player['rec_last_5'], 2) ?></td>
								<td class="rec"><?php echo number_format($player['rec'], 2) ?></td>
								<td class="catch_pct"><?php echo ($player['targets_last_5'] > 0) ? number_format(($player['rec'] / $player['targets_last_5']) * 100, 2) : 0 ?>%</td>
								<td class="catch_pct"><?php echo ($player['targets'] > 0) ? number_format(($player['rec'] / $player['targets']) * 100, 2) : 0 ?>%</td>
								<td class="rec_yds"><?php echo number_format($player['rec_yds'], 2) ?></td>
								<td class="rec_td"><?php echo number_format($player['rec_td'], 2) ?></td>
								<td class="rush_yds"><?php echo number_format($player['rush_yds'], 2) ?></td>
								<td class="rush_att"><?php echo number_format($player['rush_att'], 2) ?></td>
								<td class="rush_carry"><?php echo ($player['rush_att'] > 0) ? number_format($player['rush_yds'] / $player['rush_att'], 2) : 0 ?></td>
								<td class="rush_td"><?php echo number_format($player['rush_td'], 2) ?></td>
								<td class="all_td"><?php echo number_format($player['all_td'], 2) ?></td>
								<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
								<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
								<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
								<td class="fppg"><?php echo $player['fppg'] ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="player-table DST-table">
				<table class="table table-hover" data-toggle="table">
					<thead>
						<tr>
							<th data-sortable="true">Player Name</th>
							<th data-sortable="true">Position</th>
							<th data-sortable="true">Team</th>
							<th data-sortable="true">Opp</th>
							<th data-sortable="true">Time</th>
							<th data-sortable="true">Salary</th>
							<th data-sortable="true">Pt / $</th>
							<th data-sortable="true">Last 5 Off Points/G</th>
							<th data-sortable="true">Off Points/G</th>
							<th data-sortable="true">Last 5 Def Points/G</th>
							<th data-sortable="true">Def Points/G</th>
							<th data-sortable="true">Last 5 Def Pass Yds/G</th>
							<th data-sortable="true">Def Pass Yds/G</th>
							<th data-sortable="true">Last 5 Def Rush Yds/G</th>
							<th data-sortable="true">Def Rush Yds/G</th>
							<th data-sortable="true">Def Total Yds/G</th>
							<th data-sortable="true">FPPG</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($dstPlayers as $player): ?>
							<tr data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
								<td class="name"><?php echo $player['name'] ?></td>
								<td class="position"><?php echo $player['position'] ?></td>
								<td class="team"><?php echo $player['team'] ?></td>
								<td class="opp"><?php echo $player['opp'] ?></td>
								<td class="time"><?php echo date('H:i', strtotime($player['game_time'])) ?>PM</td>
								<td class="salary"><?php echo $player['salary'] ?></td>
								<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format( $player['fppg'] / ($player['salary'] / 1000), 2) ?></td>
								<td class="off_points"><?php echo number_format($player['last_5_off_points'], 2) ?></td>
								<td class="off_points"><?php echo number_format($player['off_points'], 2) ?></td>
								<td class="def_points"><?php echo number_format($player['last_5_def_points'], 2) ?></td>
								<td class="def_points"><?php echo number_format($player['def_points'], 2) ?></td>
								<td class="last_5_def_pass_yds"><?php echo number_format($player['last_5_def_pass_yds'], 2) ?></td>
								<td class="def_pass_yds"><?php echo number_format($player['def_pass_yds'], 2) ?></td>
								<td class="def_rush_yds"><?php echo number_format($player['last_5_def_rush_yds'], 2) ?></td>
								<td class="def_rush_yds"><?php echo number_format($player['def_rush_yds'], 2) ?></td>
								<td class="total_yds"><?php echo number_format($player['def_total_yds'], 2) ?></td>
								<td class="fppg"><?php echo $player['fppg'] ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
	</body>
</html>