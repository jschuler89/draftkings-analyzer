<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
require_once 'config.php';
include_once 'functions/get_player_functions.php';


$dk_db = new DK_DB();
$file = 'DKSalaries.csv';

$csv = file_get_contents($file);
$csvFormatted = explode("\n", $csv);

foreach($csvFormatted as $_csvRow) {
	$row = explode(',', $_csvRow);
	$name = $row[2];
	$dk_player_id = $row[3];
	$dk_db->setDKPlayerId($name, $dk_player_id);

	echo "$name dk player id saved <br><br>";
}
