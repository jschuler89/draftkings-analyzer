<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';

$dk_db = new DK_DB();
$players = $dk_db->getPlayerPool($gameDate);
$gameTimes = $dk_db->getGameTimes($gameDate);

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Draftkings Roster Analyzer</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.2.0/css/mdb.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
		<script src="js/mdb.js"></script>
		<style type="text/css">
			.bootstrap-table {
				width: 50% !important;
				float: left;
			}
			.red {
				color: red;
				background-color: transparent !important;
			}
			.clearfix {
				display: none;
			}
			.setup,
			.template {
				display: none;
			}
			.setup.active {
				display: block;
			}
			.center {
				text-align: center;
			}
			tr.disabled {
			    opacity: 0.5;
			}
			.bootstrap-table tbody td {
				cursor: pointer;
			}
			.roster-setup > div > div span {
			    min-width: 100px;
			    display: inline-block;
			}

			.roster-setup > div > div span:first-child,
			.roster-setup > div > div span:nth-child(3) {
			    min-width: 30px;
			}
			.fa-fire {
				display: none !important;
			}
			.fa-fire.active {
				display: initial !important;
			}
			.roster-setup > div > div span:nth-child(4) {
			    min-width: 40px;
			}
			.data-setup {
			    position: relative;
    			height: 600px;
			}
			.loader-gif {
				display: none;
				position: absolute;
				top: 0;
				right: 0;
				left: 0;
				bottom: 0;
				width: 150px;
				height: 0px;
				margin: auto;
			}
			.team_play-container,
			.opp_play-container {
			    font-size: 19px;
				margin-top: 20px;
				float: left;
				width: 100%;
			}
		</style>
		<script type="text/javascript">
			jQuery('table').bootstrapTable();

			jQuery(document).ready(function() {
				jQuery('[data-toggle="tooltip"]').tooltip()

				var totalSalary = 0;
				var totalFppg = 0.0;
				var gamesPlayed = 10;

				jQuery(document).on('click', '.menu-click', function(e) {
					jQuery('.navbar-nav').find('li').removeClass('active');
					jQuery(this).parent().addClass('active');
					jQuery('.bootstrap-table tbody tr').removeClass('disabled');

					var classID = jQuery(this).attr('id');
					jQuery('.setup').removeClass('active');

					jQuery('.'+classID).addClass('active');

					totalSalary = 0;
					totalFppg = 0.0;

					jQuery('.roster-setup div div').removeClass('set');

					jQuery('.roster-setup div div span').each(function() {
						if(jQuery(this).attr('class') != '' && jQuery(this).attr('class') != undefined) {
							jQuery(this).text('');
						}
					});

					e.preventDefault();
					return false;
				});

				function getRandomColor() 
				{
				    var letters = '0123456789ABCDEF';
				    var color = '#';
				    for (var i = 0; i < 6; i++ ) {
				        color += letters[Math.floor(Math.random() * 16)];
				    }
				    return color;
				}

				function setQBData(resp)
				{
					var data = [];
					var yardsData = [];
					var tdData = [];
					var fptsData = [];
					var totalCmp = 0;
					var totalAttempts = 0;
					var totalYards = 0;
					var totalFTPS = 0;
					var totalTd = 0;
					var countTdsIndex = resp.player.gamelogs.length - 3;
					var countTds = 0;
					var yardsArray = [];
					var attArray = [];
					var weeks = [];
					var oppAllowYardsData = [];
					var totalOppAllowYards = 0;
					var overOppYardsAllowed = 0;

					for(var i = 0; i < resp.player.gamelogs.length; i++) {
						var pass_att = resp.player.gamelogs[i].pass_att;
						var pass_yds = resp.player.gamelogs[i].pass_yds;
						var pass_td = resp.player.gamelogs[i].pass_td;
						var rush_yd = resp.player.gamelogs[i].rush_yd;
						var rush_td = resp.player.gamelogs[i].rush_td;
						var pass_cmp = resp.player.gamelogs[i].pass_cmp;
						var pass_cmp_perc = resp.player.gamelogs[i].pass_cmp_perc;
						var pass_rating = resp.player.gamelogs[i].pass_rating;
						var fpts = calculateFPTS(resp.player.gamelogs[i]);
						gamesPlayed = parseInt(resp.player.gamelogs[i].opp_stats.games_played);
						totalFTPS += parseFloat(fpts);
						totalAttempts += parseInt(pass_att);
						totalCmp += parseInt(pass_cmp);
						totalYards += parseInt(pass_yds);
						totalTd += parseInt(pass_td);
						totalOppAllowYards += parseInt(resp.player.gamelogs[i].opp_stats.gamelogs[1].pass_yds /gamesPlayed);

						if(parseInt(pass_yds) > parseInt(resp.player.gamelogs[i].opp_stats.gamelogs[1].pass_yds /gamesPlayed)) {
							overOppYardsAllowed++;
						}

						yardsArray.push(pass_yds);
						attArray.push(pass_att);
						weeks.push('Week '+resp.player.gamelogs[i].week_num);

						oppAllowYardsData.push(parseInt(resp.player.gamelogs[i].opp_stats.gamelogs[1].pass_yds /gamesPlayed));

						if(i >= countTdsIndex && pass_td > 0) {
							countTds++;
						}

						var inputAttemptsData = {value: pass_att, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						data.push(inputAttemptsData);

						var inputYardsData = {value: pass_yds, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						yardsData.push(inputYardsData);

						var inputTdData = {value: pass_td, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						tdData.push(inputTdData);

						// var inputFPTSData = {value: fpts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						fptsData.push(fpts);


					}

					console.log(totalYards);
					console.log(totalOppAllowYards);
					console.log(overOppYardsAllowed / resp.player.gamelogs.length);

					var compiledFPTSData = {
					    labels: weeks,
					    datasets: [
					        {
					            fillColor: "rgba(151,187,205,0.2)",
					            strokeColor: "rgba(151,187,205,1)",
					            pointColor: "rgba(151,187,205,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(151,187,205,1)",
					            data: fptsData
					        }
					    ]
					};

					var compiledData = {
					    labels: weeks,
					    datasets: [
					        {
					            label: "My First dataset",
					            fillColor: "rgba(220,220,220,0.2)",
					            strokeColor: "rgba(220,220,220,1)",
					            pointColor: "rgba(220,220,220,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(220,220,220,1)",
					            data: yardsArray
					        },
					         {
					            fillColor: "rgba(120,140,180,0.2)",
					            strokeColor: "rgba(120,140,180,1)",
					            pointColor: "rgba(120,140,180,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(120,140,180,1)",
					            data: oppAllowYardsData
					        },
					        {
					            label: "My Second dataset",
					            fillColor: "rgba(151,187,205,0.2)",
					            strokeColor: "rgba(151,187,205,1)",
					            pointColor: "rgba(151,187,205,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(151,187,205,1)",
					            data: attArray
					        }
					    ]
					};


					if(countTds == 3) {
						jQuery('.QB .fa-fire').addClass('active');
					}

					// console.log(countTds);
					// console.log(countTdsIndex);
					// console.log(resp.player.gamelogs.length);

					jQuery('.QB .avg_attempts').text(parseInt(totalAttempts / resp.player.gamelogs.length));
					jQuery('.QB .avg_yards').text(parseInt(totalYards / resp.player.gamelogs.length));
					jQuery('.QB .avg_td').text(parseInt(totalTd / resp.player.gamelogs.length));
					jQuery('.QB .avg_fpts').text(parseFloat(totalFTPS / resp.player.gamelogs.length).toFixed(2));

				    jQuery('canvas').remove();

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'attemptChart');
				    jQuery('.QB .avg_attempts_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'attYardsChart');
				    jQuery('.QB .avg_yards_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'yardsChart');
				    jQuery('.QB .avg_yards_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'tdChart');
				    jQuery('.QB .avg_td_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'fptsChart');
				    jQuery('.QB .avg_ftps_container').before(canvas);
					
					var option = {
				    	responsive: true,
				    };

					var ctx = document.getElementById("attemptChart").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(data, option); 

					var ctx = document.getElementById("tdChart").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(tdData, option); 

					var ctx = document.getElementById("yardsChart").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(yardsData, option); 

					var ctx = document.getElementById("fptsChart").getContext('2d');
					var myLineChart = new Chart(ctx).Line(compiledFPTSData, option); 

					var ctx = document.getElementById("attYardsChart").getContext('2d');
					var myLineChart = new Chart(ctx).Line(compiledData, option); 
				}

				function setRBData(resp)
				{
					var data = [];
					var yardsData = [];
					var tdData = [];
					var fptsData = [];
					var totalAttempts = 0;
					var totalYards = 0;
					var totalFTPS = 0;
					var totalTd = 0;
					var oppAllowYardsData = [];
					var weeks = [];
					var yardsArray = [];
					var recYardsArray = [];
					var tgtsArray = [];
					var totalTargets = 0;
					var totalRecYards = 0;
					var totalRec = 0;
					var totalTeamPassAttempts = parseInt(resp.team_stats.gamelogs[0].pass_att);

					for(var i = 0; i < resp.player.gamelogs.length; i++) {
						var rush_att = resp.player.gamelogs[i].rush_att;
						var rush_yds = resp.player.gamelogs[i].rush_yds;
						var rec_yds = resp.player.gamelogs[i].rec_yds;
						var rec = resp.player.gamelogs[i].rec;
						var rush_td = resp.player.gamelogs[i].rush_td;
						var rec_td = resp.player.gamelogs[i].rec_td;
						var tgts = resp.player.gamelogs[i].targets;
						var fpts = calculateFPTS(resp.player.gamelogs[i]);
						gamesPlayed = parseInt(resp.player.gamelogs[i].opp_stats.games_played);
						totalTargets += parseInt(tgts);
						totalFTPS += parseFloat(fpts);
						totalAttempts += parseInt(rush_att);
						totalYards += parseInt(rush_yds);
						totalRec += parseInt(rec);
						totalRecYards += parseInt(rec_yds);
						totalTd += parseInt(rush_td) + parseInt(rec_td);

						var inputData = {value: tgts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						tgtsArray.push(inputData);

						weeks.push('Week '+resp.player.gamelogs[i].week_num);
						yardsArray.push(rush_yds);

						oppAllowYardsData.push(parseInt(resp.player.gamelogs[i].opp_stats.gamelogs[1].rush_yds /gamesPlayed));

						var inputAttemptsData = {value: rush_att, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						data.push(inputAttemptsData);

						// var inputYardsData = {value: rush_yds, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						yardsData.push(rush_yds);

						var inputTdData = {value: parseInt(rush_td) + parseInt(rec_td), label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						tdData.push(inputTdData);

						// var inputFPTSData = {value: fpts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						fptsData.push(fpts);


					}

					var compiledFPTSData = {
					    labels: weeks,
					    datasets: [
					        {
					            fillColor: "rgba(151,187,205,0.2)",
					            strokeColor: "rgba(151,187,205,1)",
					            pointColor: "rgba(151,187,205,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(151,187,205,1)",
					            data: fptsData
					        }
					    ]
					};

					var compiledData = {
					    labels: weeks,
					    datasets: [
					        {
					            label: "My First dataset",
					            fillColor: "rgba(220,220,220,0.2)",
					            strokeColor: "rgba(220,220,220,1)",
					            pointColor: "rgba(220,220,220,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(220,220,220,1)",
					            data: yardsArray
					        },
					         {
					            fillColor: "rgba(120,140,180,0.2)",
					            strokeColor: "rgba(120,140,180,1)",
					            pointColor: "rgba(120,140,180,1)",
					            pointStrokeColor: "#fff",
					            pointHighlightFill: "#fff",
					            pointHighlightStroke: "rgba(120,140,180,1)",
					            data: oppAllowYardsData
					        }
					    ]
					};

					var catch_pct = parseFloat(totalRec / totalTargets) * 100;

					jQuery('.RB .avg_targets').text(parseInt(totalTargets / resp.player.gamelogs.length));
					jQuery('.RB .target_perc').text(parseFloat( (totalTargets / totalTeamPassAttempts) * 100).toFixed(2));
					jQuery('.RB .yards_per_target').text(parseFloat(totalRecYards / totalTargets).toFixed(2));
					jQuery('.RB .avg_attempts').text(parseInt(totalAttempts / resp.player.gamelogs.length));
					jQuery('.RB .avg_yards').text(parseInt(totalYards / resp.player.gamelogs.length));
					jQuery('.RB .avg_td').text(parseInt(totalTd / resp.player.gamelogs.length));
					jQuery('.RB .avg_fpts').text(parseFloat(totalFTPS / resp.player.gamelogs.length).toFixed(2));
					jQuery('.catch_pct').text(parseFloat(catch_pct).toFixed(2));


				    jQuery('canvas').remove();

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'myChartRB');
				    jQuery('.RB .avg_targets_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'attemptChartRB');
				    jQuery('.RB .avg_attempts_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'yardsChartRB');
				    jQuery('.RB .avg_yards_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'tdChartRB');
				    jQuery('.RB .avg_td_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'fptsChartRB');
				    jQuery('.RB .avg_ftps_container').before(canvas);
					
					var option = {
				    	responsive: true,
				    };

				    var ctx = document.getElementById("myChartRB").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(tgtsArray, option); 

					var ctx = document.getElementById("attemptChartRB").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(data, option); 

					var ctx = document.getElementById("tdChartRB").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(tdData, option); 

					var ctx = document.getElementById("yardsChartRB").getContext('2d');
					var myLineChart = new Chart(ctx).Line(compiledData, option); 

					var ctx = document.getElementById("fptsChartRB").getContext('2d');
					var myLineChart = new Chart(ctx).Line(compiledFPTSData, option); 
				}

				function setWRData(resp)
				{
					var data = [];
					var yardsData = [];
					var fptsData = [];
					var totalTargets = 0;
					var totalRec = 0;
					var totalYards = 0;
					var totalFTPS = 0;
					var totalTeamPassYards = parseInt(resp.team_stats.gamelogs[0].pass_yds);
					var totalTeamPassAttempts = parseInt(resp.team_stats.gamelogs[0].pass_att);

					for(var i = 0; i < resp.player.gamelogs.length; i++) {
						var fpts = calculateFPTS(resp.player.gamelogs[i]);
						totalFTPS += parseFloat(fpts);
						var tgts = resp.player.gamelogs[i].targets;
						var rec = resp.player.gamelogs[i].rec;
						var rec_yards = resp.player.gamelogs[i].rec_yds;
						// if(resp.team.gamelogs[i].pts_off != undefined) {
						// 	var team_pass_yards = resp.team.gamelogs[i].pass_yds_off;
						// 	totalTeamPassYards += parseInt(team_pass_yards);
						// }
						totalTargets += parseInt(tgts);
						totalRec += parseInt(rec);
						totalYards += parseInt(rec_yards);
						var inputData = {value: tgts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						data.push(inputData);

						var inputYardsData = {value: rec_yards, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						yardsData.push(inputYardsData);

						var inputFPTSData = {value: fpts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						fptsData.push(inputFPTSData);


					}

					var catch_pct = parseFloat(totalRec / totalTargets) * 100;

					jQuery('.WR .yards_per_target').text(parseFloat(totalYards / totalTargets).toFixed(2));
					jQuery('.WR .avg_targets').text(parseInt(totalTargets / resp.player.gamelogs.length));
					jQuery('.WR .target_perc').text(parseFloat( (totalTargets / totalTeamPassAttempts) * 100).toFixed(2));
					jQuery('.WR .avg_yards').text(parseInt(totalYards / resp.player.gamelogs.length));
					jQuery('.WR .avg_yards_perc').text(parseFloat( (totalYards / totalTeamPassYards) * 100).toFixed(2));
					jQuery('.WR .avg_fpts').text(parseFloat(totalFTPS / resp.player.gamelogs.length).toFixed(2));
					jQuery('.WR .catch_pct').text(parseFloat(catch_pct).toFixed(2));

					var option = {
				    	responsive: true,
				    };

				    jQuery('canvas').remove();
				    
				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'myChartWR');
				    jQuery('.WR .avg_targets_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'yardsChartWR');
				    jQuery('.WR .avg_yards_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'fptsChartWR');
				    jQuery('.WR .avg_ftps_container').before(canvas);

					var ctx = document.getElementById("myChartWR").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(data, option); 

					var ctx = document.getElementById("yardsChartWR").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(yardsData, option); 

					var ctx = document.getElementById("fptsChartWR").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(fptsData, option); 
				}

				function setTEData(resp)
				{
					var data = [];
					var yardsData = [];
					var fptsData = [];
					var totalTargets = 0;
					var totalRec = 0;
					var totalYards = 0;
					var totalFTPS = 0;
					var totalTeamPassYards = 0;
					var totalTeamPassAttempts = parseInt(resp.team_stats.gamelogs[0].pass_att);

					for(var i = 0; i < resp.player.gamelogs.length; i++) {
						var fpts = calculateFPTS(resp.player.gamelogs[i]);
						totalFTPS += parseFloat(fpts);
						var tgts = resp.player.gamelogs[i].targets;
						var rec = resp.player.gamelogs[i].rec;
						var rec_yards = resp.player.gamelogs[i].rec_yds;
						if(resp.team.gamelogs[i].pts_off != undefined) {
							var team_pass_yards = resp.team.gamelogs[i].pass_yds_off;
							totalTeamPassYards += parseInt(team_pass_yards);
						}
						totalTargets += parseInt(tgts);
						totalRec += parseInt(rec);
						totalYards += parseInt(rec_yards);
						var inputData = {value: tgts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						data.push(inputData);

						var inputYardsData = {value: rec_yards, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						yardsData.push(inputYardsData);

						var inputFPTSData = {value: fpts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						fptsData.push(inputFPTSData);
					}

					var catch_pct = parseFloat(totalRec / totalTargets) * 100;

					jQuery('.TE .yards_per_target').text(parseFloat(totalYards / totalTargets).toFixed(2));
					jQuery('.TE .avg_targets').text(parseInt(totalTargets / resp.player.gamelogs.length));
					jQuery('.TE .target_perc').text(parseFloat( (totalTargets / totalTeamPassAttempts) * 100).toFixed(2));
					jQuery('.TE .avg_yards').text(parseInt(totalYards / resp.player.gamelogs.length));
					jQuery('.TE .avg_yards_perc').text(parseFloat( (totalYards / totalTeamPassYards) * 100).toFixed(2));
					jQuery('.TE .avg_fpts').text(parseFloat(totalFTPS / resp.player.gamelogs.length).toFixed(2));
					jQuery('.TE .catch_pct').text(parseFloat(catch_pct).toFixed(2));

					var option = {
				    	responsive: true,
				    };

				    jQuery('canvas').remove();

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'myChartTE');
				    jQuery('.TE .avg_targets_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'yardsChartTE');
				    jQuery('.TE .avg_yards_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'fptsChartTE');
				    jQuery('.TE .avg_ftps_container').before(canvas);

					var ctx = document.getElementById("myChartTE").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(data, option); 

					var ctx = document.getElementById("yardsChartTE").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(yardsData, option); 

					var ctx = document.getElementById("fptsChartTE").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(fptsData, option); 
				}

				function setDSTData(resp)
				{
					var data = [];
					var ptsDefData = [];
					var fptsData = [];
					var totalPtsOff = 0;
					var totalPtsDef = 0;
					var totalRec = 0;
					var totalYards = 0;
					var totalFTPS = 0;
					var games = 1;

					for(var i = 0; i < resp.player.gamelogs.length; i++) {

						if(resp.player.gamelogs[i].pts_off == undefined || resp.player.gamelogs[i].pts_off == '') {
							continue;
						}
						// var fpts = calculateFPTS(resp.player.gamelogs[i]);
						// totalFTPS += parseFloat(fpts);
						var pts_off = resp.player.gamelogs[i].pts_off;
						var pts_def = resp.player.gamelogs[i].pts_def;
						// var rec = resp.player.gamelogs[i].rec;
						// var rec_yards = resp.player.gamelogs[i].rec_yds;
						totalPtsOff += parseInt(pts_off);
						totalPtsDef += parseInt(pts_def);
						// totalYards += parseInt(rec_yards);

						var inputData = {value: pts_off, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						data.push(inputData);

						var inputPtsDefData = {value: pts_def, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						ptsDefData.push(inputPtsDefData);

						// var inputFPTSData = {value: fpts, label: 'Week '+resp.player.gamelogs[i].week_num, color: getRandomColor(), highlight: "#FF5A5E"};
						// fptsData.push(inputFPTSData);

						games++;
					}

					console.log(data);
					console.log(ptsDefData);


					jQuery('.DST .opp_play-container .pass_pct').text(parseInt((resp.team_stats.gamelogs[1].pass_att / resp.team_stats.gamelogs[1].plays_offense * 100)));
					jQuery('.DST .opp_play-container .rush_pct').text(parseInt((resp.team_stats.gamelogs[1].rush_att / resp.team_stats.gamelogs[1].plays_offense * 100)));
					jQuery('.DST .team_play-container .pass_pct').text(parseInt((resp.team_stats.gamelogs[0].pass_att / resp.team_stats.gamelogs[0].plays_offense * 100)));
					jQuery('.DST .team_play-container .rush_pct').text(parseInt((resp.team_stats.gamelogs[0].rush_att / resp.team_stats.gamelogs[0].plays_offense * 100)));
					jQuery('.DST .avg_ptsfor').text(parseFloat(resp.team_stats.gamelogs[0].points / games).toFixed(2));
					jQuery('.DST .avg_ptsdef').text(parseFloat(resp.team_stats.gamelogs[1].points / games).toFixed(2));

					jQuery('canvas').remove();

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'ptsFor');
				    jQuery('.avg_ptsfor_container').before(canvas);

				    var canvas = jQuery('<canvas>');
				    canvas.attr('id', 'ptsDef');
				    jQuery('.avg_ptsdef_container').before(canvas);

				    var option = {
				    	responsive: true,
				    };

					var ctx = document.getElementById("ptsFor").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(data, option); 

					var ctx = document.getElementById("ptsDef").getContext('2d');
					var myLineChart = new Chart(ctx).Doughnut(ptsDefData, option); 
				}

				function calculateFPTS(gameInfo)
				{
					var fpts = 0.00;

					// Passing
					fpts += (gameInfo.pass_td == undefined) ?  0 : parseFloat(gameInfo.pass_td * 3);
					fpts += (gameInfo.pass_yds == undefined) ?  0 : parseFloat(gameInfo.pass_yds * 0.04);
					fpts += (gameInfo.pass_int == undefined) ?  0 : parseFloat(gameInfo.pass_int * -1);
					fpts += (gameInfo.pass_yds == undefined) ?  0 : parseFloat(parseInt(gameInfo.pass_yds / 300) * 3);

					// Rushing
					fpts += (gameInfo.rush_td == undefined) ?  0 : parseFloat(gameInfo.rush_td * 6);
					fpts += (gameInfo.rush_yds == undefined) ?  0 : parseFloat(gameInfo.rush_yds * 0.1);
					fpts += (gameInfo.rush_yds == undefined) ?  0 : parseFloat(parseInt(gameInfo.rush_yds / 100) * 3);

					// Receiving
					fpts += (gameInfo.rec_yds == undefined) ?  0 : parseFloat(gameInfo.rec_yds * 0.1);
					fpts += (gameInfo.rec_yds == undefined) ?  0 : parseFloat(parseInt(gameInfo.rec_yds / 100) * 3);
					fpts += (gameInfo.rec == undefined) ?  0 : parseFloat(gameInfo.rec);
					fpts += (gameInfo.rec_td == undefined) ?  0 : parseFloat(gameInfo.rec_td * 6);

					return fpts.toFixed(2);
				}

				var filterTime;
				var filterPosition;

				jQuery(document).on('click', '.reset-filter', function() {
					filterTime = undefined;
					filterPosition = undefined;

					jQuery('.player-table tbody tr').show();
				});

				jQuery(document).on('click', '.time-filter', function() {
					filterTime = jQuery(this).text();

					jQuery('.player-table tbody tr').hide();

					if(filterPosition !== undefined) {
						jQuery('.player-table tbody tr[data-filter-position="'+filterPosition+'"][data-filter-gametime="'+filterTime+'"]').show();
					} else {
						jQuery('.player-table tbody tr[data-filter-gametime="'+filterTime+'"]').show();
					}
				});

				jQuery(document).on('click', '.position-filter', function() {
					filterPosition = jQuery(this).text();

					jQuery('.player-table tbody tr').hide();
					
					if(filterTime !== undefined) {
						jQuery('.player-table tbody tr[data-filter-gametime="'+filterTime+'"][data-filter-position="'+filterPosition+'"]').show();
					} else {
						jQuery('.player-table tbody tr[data-filter-position="'+filterPosition+'"]').show();
					}

				});

				jQuery(document).on('click', '.bootstrap-table tbody tr', function() {
					var rowParent = jQuery(this);
					var player_id = rowParent.attr('data-playerid');
					var name = rowParent.find('.name').text();
					var team = rowParent.find('.team').text();
					var opp = rowParent.find('.opp').text();
					var position = rowParent.find('.position').text();
					var salary = rowParent.find('.salary').text();
					var fppg = rowParent.find('.fppg').text();

					if(jQuery('.data-setup.active').is('*')) {
						jQuery('.loader-gif').show();
						jQuery('.data-setup h4').hide();

						jQuery.ajax({
							url: 'get_player_data.php',
							data: {player_id: player_id},
							method: 'get',
							success: function(resp) {
								resp = JSON.parse(resp);
								jQuery('.loader-gif').hide();
								console.log(resp);
								jQuery('.template').hide();
								jQuery('.fa-fire').removeClass('active');
								var template = jQuery('.template.'+position);
								template.show();
								template.find('img').attr('src', resp.player.img);
								template.find('.name').text(name);
								template.find('.position').text(position);
								template.find('.team').text(team);

								if(position == 'QB') {
									setQBData(resp);
								}
								if(position == 'RB') {
									setRBData(resp);
								}
								if(position == 'WR') {
									setWRData(resp);
								}
								if(position == 'TE') {
									setTEData(resp);
								}
								if(position == 'DST') {
									setDSTData(resp);
								}
							}
						});
					}


					if(jQuery('.roster-setup.active').is('*')) {
						if(rowParent.hasClass('disabled')) {
							jQuery('#player_id_'+player_id).find('.name').text('');
							jQuery('#player_id_'+player_id).find('.team').text('');
							jQuery('#player_id_'+player_id).find('.salary').text('');
							jQuery('#player_id_'+player_id).find('.fppg').text('');
							jQuery('#player_id_'+player_id).removeClass('set');
							rowParent.removeClass('disabled');

							totalSalary -= parseInt(salary);
							totalFppg -= parseFloat(fppg);

							jQuery('.salary-line .salary').text(totalSalary);
							jQuery('.salary-line .fppg').text(totalFppg.toFixed(2));
							
							return false;
						}

						rowParent.addClass('disabled');

						var lineupPosition = jQuery('.roster-setup [data-position="'+position+'"]');

						var playerPlaced = false;

						lineupPosition.each(function() {
							var element = jQuery(this);

							if(!element.hasClass('set')) {
								element.find('.name').text(name);
								element.find('.team').text(team);
								element.find('.salary').text(salary);
								element.find('.fppg').text(fppg);
								element.addClass('set');
								element.attr('id', 'player_id_' + player_id);

								totalSalary += parseInt(salary);
								totalFppg += parseFloat(fppg);

								jQuery('.salary-line .salary').text(totalSalary);
								jQuery('.salary-line .fppg').text(totalFppg.toFixed(2));
								playerPlaced = true;

								return false;
							}
						});

						if(!playerPlaced && (position == 'WR' || position == 'RB' || position == 'TE')) {
							var lineupPosition = jQuery('.roster-setup [data-position="FLEX"]');
							var element = lineupPosition;

							if(!element.hasClass('set')) {
								element.find('.name').text(name);
								element.find('.team').text(team);
								element.find('.salary').text(salary);
								element.find('.fppg').text(fppg);
								element.addClass('set');
								element.attr('id', 'player_id_' + player_id);

								totalSalary += parseInt(salary);
								totalFppg += parseFloat(fppg);

								jQuery('.salary-line .salary').text(totalSalary);
								jQuery('.salary-line .fppg').text(totalFppg.toFixed(2));
							}
						}
					}
				});
			});
		</script>
		<script src="https://use.fontawesome.com/b1353e11e4.js"></script>
	</head>
	<body>
		<div>
			<nav class="navbar navbar-default">
			  	<div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="">DK Roster Analyzer</a>
				    </div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="#" class="menu-click" id="roster-setup">Lineups</a></li>
				        <li><a href="#" class="menu-click" id="data-setup">Data Analysis</a></li>
				        <li><a href="/lineups_detail.php">Detail Lineups</a></li>
				        <li><a href="/generatelineups.php" id="lineups-setup">Generate Lineups</a></li>
				        <li><a href="/sync.php" id="sync-setup">Sync</a></li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
		    	</div>
		    </nav>
		</div>
		<div>
			<div>
				<span><button class="btn btn-primary btn-sm reset-filter">Reset</button></span>
			</div>
			<div>
				<?php foreach($gameTimes as $_gametime): ?>
					<span><button class="btn btn-primary btn-sm time-filter"><?php echo date('H:i', strtotime($_gametime['game_time'])) ?></button></span>
				<?php endforeach ?>
			</div>
			<div>
				<span><button class="btn btn-primary btn-sm position-filter">QB</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">RB</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">WR</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">TE</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">DST</button></span>
			</div>
			<table class="table table-hover player-table" data-toggle="table">
				<thead>
					<tr>
						<th data-sortable="true">Player Name</th>
						<th data-sortable="true">Position</th>
						<th data-sortable="true">Team</th>
						<th data-sortable="true">Time</th>
						<th data-sortable="true">Opp</th>
						<th data-sortable="true">Salary</th>
						<th data-sortable="true">$ / Pt</th>
						<th data-sortable="true">FPPG</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($players as $player): ?>
						<tr data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
							<td class="name"><?php echo $player['name'] ?></td>
							<td class="position"><?php echo $player['position'] ?></td>
							<td class="team"><?php echo $player['team'] ?></td>
							<td class="time"><?php echo date('H:i', strtotime($player['game_time'])) ?>PM</td>
							<td class="opp"><?php echo $player['opp'] ?></td>
							<td class="salary"><?php echo $player['salary'] ?></td>
							<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format( ($player['salary'] / 1000) / $player['fppg'], 2) ?></td>
							<td class="fppg"><?php echo $player['fppg'] ?></td>
						</tr>
					<?php endforeach ?>
				</tbody>
			</table>
			<div class="roster-setup active col-md-4 setup">
				<div>
					<h4>Create Lineup</h4>
					<div data-position="QB">
						<span>QB</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="RB">
						<span>RB</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="RB">
						<span>RB</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="WR">
						<span>WR</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="WR">
						<span>WR</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="WR">
						<span>WR</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="TE">
						<span>TE</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="FLEX">
						<span>FLEX</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div data-position="DST">
						<span>DST</span>
						<span class="name"></span>
						<span class="team"></span>
						<span class="salary"></span>
						<span class="fppg"></span>
					</div>
					<div class="salary-line">
						<span><b>Salary</b></span>
						<span class="name">&nbsp;</span>
						<span class="team">&nbsp;</span>
						<span class="salary">0</span>
						<span class="fppg">0.00</span>
					</div>
				</div>
			</div>
			<div class="data-setup setup col-md-6">
				<h4>Click any player to get data analysis</h4>
				<div class="loader-gif">
					<img src="images/reload.gif">
				</div>
				<div class="template QB center">
					<div>
						<img src="">
						<h5><span class="name"></span> - <span class="position"></span> - <span class="team"></span> <i class="fa fa-fire red" data-toggle="tooltip" data-placement="top" title="TD in last 3 games" aria-hidden="true"></i></h5>
					</div>
					<div class="gamelogs">
						<div class="targets-container">
							<h5>Attempts</h5>
							<canvas id="attemptChart"></canvas>
							<div class="avg_attempts_container">Avg Attempts: <span class="avg_attempts"></span></div>
						</div>
						<div class="yards-container">
							<h5>Yards</h5>
							<canvas id="yardsChart"></canvas>
							<div class="avg_yards_container">Avg Yards: <span class="avg_yards"></span></div>
						</div>
						<div class="td-container">
							<h5>TDs</h5>
							<canvas id="tdChart"></canvas>
							<div class="avg_td_container">Avg TDs: <span class="avg_td"></span></div>
						</div>
						<div class="fpts-container">
							<h5>FPTS</h5>
							<canvas id="fptsChart"></canvas>
							<div class="avg_ftps_container">Avg FPTS: <span class="avg_fpts"></span></div>
						</div>
					</div>
				</div>
				<div class="template RB center">
					<div>
						<img src="">
						<h5><span class="name"></span> - <span class="position"></span> - <span class="team"></span></h5>
					</div>
					<div class="gamelogs">
						<div class="targets-container">
							<h5>Attempts</h5>
							<canvas id="attemptChartRB"></canvas>
							<div class="avg_attempts_container">Avg Attempts: <span class="avg_attempts"></span></div>
							<h5>Targets</h5>
							<canvas id="myChartRB"></canvas>
							<div class="avg_targets_container">Avg Targets: <span class="avg_targets"></span></div>
							<div class="">Yards Per Target: <span class="yards_per_target"></span></div>
							<div class="">Catch Pct%: <span class="catch_pct"></span>%</div>
							<div class="">Target Pct%: <span class="target_perc"></span>%</div>
						</div>
						<div class="yards-container">
							<h5>Yards</h5>
							<canvas id="yardsChartRB"></canvas>
							<div class="avg_yards_container">Avg Yards: <span class="avg_yards"></span></div>
						</div>
						<div class="td-container">
							<h5>TDs</h5>
							<canvas id="tdChartRB"></canvas>
							<div class="avg_td_container">Avg TDs: <span class="avg_td"></span></div>
						</div>
						<div class="fpts-container">
							<h5>FPTS</h5>
							<canvas id="fptsChartRB"></canvas>
							<div class="avg_ftps_container">Avg FPTS: <span class="avg_fpts"></span></div>
						</div>
					</div>
				</div>
				<div class="template WR center">
					<div>
						<img src="">
						<h5><span class="name"></span> - <span class="position"></span> - <span class="team"></span></h5>
					</div>
					<div class="gamelogs">
						<div class="targets-container">
							<h5>Targets</h5>
							<canvas id="myChartWR"></canvas>
							<div class="avg_targets_container">Avg Targets: <span class="avg_targets"></span></div>
							<div class="">Yards Per Target: <span class="yards_per_target"></span></div>
							<div class="">Catch Pct%: <span class="catch_pct"></span>%</div>
							<div class="">Target Pct%: <span class="target_perc"></span>%</div>
						</div>
						<div class="yards-container">
							<h5>Yards</h5>
							<canvas id="yardsChartWR"></canvas>
							<div class="avg_yards_container">Avg Yards: <span class="avg_yards"></span></div>
							<div class="avg_yards_perc_container">Yards %: <span class="avg_yards_perc"></span>%</div>
						</div>
						<div class="fpts-container">
							<h5>FPTS</h5>
							<canvas id="fptsChartWR"></canvas>
							<div class="avg_ftps_container">Avg FPTS: <span class="avg_fpts"></span></div>
						</div>
					</div>
				</div>
				<div class="template TE center">
					<div>
						<img src="">
						<h5><span class="name"></span> - <span class="position"></span> - <span class="team"></span></h5>
					</div>
					<div class="gamelogs">
						<div class="targets-container">
							<h5>Targets</h5>
							<canvas id="myChartTE"></canvas>
							<div class="avg_targets_container">Avg Targets: <span class="avg_targets"></span></div>
							<div class="">Yards Per Target: <span class="yards_per_target"></span></div>
							<div class="">Catch Pct%: <span class="catch_pct"></span>%</div>
							<div class="">Target Pct%: <span class="target_perc"></span>%</div>
						</div>
						<div class="yards-container">
							<h5>Yards</h5>
							<canvas id="yardsChartTE"></canvas>
							<div class="avg_yards_container">Avg Yards: <span class="avg_yards"></span></div>
							<div class="avg_yards_perc_container">Yards %: <span class="avg_yards_perc"></span>%</div>
						</div>
						<div class="fpts-container">
							<h5>FPTS</h5>
							<canvas id="fptsChartTE"></canvas>
							<div class="avg_ftps_container">Avg FPTS: <span class="avg_fpts"></span></div>
						</div>
					</div>
				</div>
				<div class="template DST center">
					<div>
						<img src="">
						<h5><span class="team"></span></h5>
					</div>
					<div class="gamelogs col-md-12">
						<div class="offense col-md-6">
							<h5><b>Offense</b></h5>
							<div class="ptsfor-container">
								<h5>Points</h5>
								<canvas id="ptsFor"></canvas>
								<div class="avg_ptsfor_container">Avg Points: <span class="avg_ptsfor"></span></div>
							</div>
							<div class="team_play-container">
								<div class="col-md-12">
									<div class="col-md-6">
										<div><span class="pass_pct"></span>%</div>
										<div>Pass %</div>
									</div>
									<div class="col-md-6">
										<div><span class="rush_pct"></span>%</div>
										<div>Rush %</div>
									</div>
								</div>
							</div>
						</div>
						<div class="defense col-md-6">
							<h5><b>Defense</b></h5>
							<div class="ptsfor-container">
								<h5>Points</h5>
								<canvas id="ptsDef"></canvas>
								<div class="avg_ptsdef_container">Avg Points: <span class="avg_ptsdef"></span></div>
							</div>
							<div class="opp_play-container">
								<div class="col-md-12">
									<div class="col-md-6">
										<div><span class="pass_pct"></span>%</div>
										<div>Pass %</div>
									</div>
									<div class="col-md-6">
										<div><span class="rush_pct"></span>%</div>
										<div>Rush %</div>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="targets-container">
							<h5>Targets</h5>
							<canvas id="myChartTE"></canvas>
							<div class="avg_targets_container">Avg Targets: <span class="avg_targets"></span></div>
							<div class="">Yards Per Target: <span class="yards_per_target"></span></div>
							<div class="">Catch Pct%: <span class="catch_pct"></span>%</div>
						</div>
						<div class="yards-container">
							<h5>Yards</h5>
							<canvas id="yardsChartTE"></canvas>
							<div class="avg_yards_container">Avg Yards: <span class="avg_yards"></span></div>
						</div>
						<div class="fpts-container">
							<h5>FPTS</h5>
							<canvas id="fptsChartTE"></canvas>
							<div class="avg_ftps_container">Avg FPTS: <span class="avg_fpts"></span></div>
						</div> -->
					</div>
				</div>
			</div>
		</div>
	</body>
</html>