<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';

$dk_db = new DK_DB();
$upload = $_FILES;
$alreadyLoaded = false;

if(empty($_FILES['uploadfile']['tmp_name'])) {
	header('Location: home.php');
}

$csv = file_get_contents($_FILES['uploadfile']['tmp_name']);

// "Position","Name","Salary","GameInfo","AvgPointsPerGame","teamAbbrev"
$csvArray = explode("\n", $csv);

$selectSmt = "SELECT * FROM player_pool where game_date = '$gameDate'";

$rs = $dk_db->query($selectSmt);

if(isset($rs[0]['player_id'])) {
	$alreadyLoaded = true;
}

if(!$alreadyLoaded) {
	foreach($csvArray as $index => $_csvRow) {
		if($index == 0) {
			continue;
		}

		$playerInfo = explode(",", $_csvRow);

		if(!isset($playerInfo[1])) {
			continue;
		}

		$position = $playerInfo[0];
		$name = $playerInfo[1];
		$salary = $playerInfo[2];
		$gameInfo = $playerInfo[3];

		// [0] => Team@Opp, [1] => 01:00PM [2] => ET
		$gameInfoArray = explode(" ", $gameInfo);
		// [0] => Team, [1] => Opp
		$gameArray = explode("@", $gameInfoArray[0]);
		$gameDate = $gameInfoArray[1];
		$opp = $gameArray[1];
		$fppg = $playerInfo[4];
		$team = $playerInfo[5];

		$player_id = $dk_db->insertPlayer($playerInfo);

		$dk_db->insertIntoPlayerPool($player_id, $playerInfo);
	}
}

header('Location: home.php');
