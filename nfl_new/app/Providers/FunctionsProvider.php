<?php

namespace App\Providers;

use App\DK\Functions;
use Illuminate\Support\ServiceProvider;

class FunctionsProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
         $this->app->singleton('functions', function ($app) {
            return new Functions(/*$app['config']['dk']*/);
        });
    }
}
