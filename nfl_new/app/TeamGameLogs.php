<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TeamGameLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'team_game_logs';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'game_id';

    public $timestamps  = false;

    protected $fillable = ['game_id','player_id','week_num','team','opp','game_outcome','pts_off','pts_def','yards_off','pass_yds_off','rush_yds_off','to_off','yards_def','pass_yds_def','rush_yds_def','to_def'];

    public static function leagueAverages()
    {
        $avgs = TeamGameLogs::select(DB::raw('avg(team_game_logs.pass_yds_off) as pass_yds'), DB::raw('avg(team_game_logs.rush_yds_off) as rush_yds'))->get()->first();
        
        return $avgs;
    }

    public static function leagueMinMaxOffAverages()
    {
        $avgs = TeamGameLogs::select(DB::raw('avg(team_game_logs.pass_yds_off) as pass_yds_off'), DB::raw('avg(team_game_logs.rush_yds_off) as rush_yds_off'), DB::raw('avg(team_game_logs.to_off) as to_off'), DB::raw('avg(team_game_logs.pts_off) as pts_off'))
            ->groupBy(['player_id'])->toSql();

        $minMax = DB::table(DB::raw("({$avgs}) s"))->select(DB::raw('min(pass_yds_off) as min_pass_yds_off'), DB::raw('max(pass_yds_off) as max_pass_yds_off'), DB::raw('min(rush_yds_off) as min_rush_yds_off'), DB::raw('max(rush_yds_off) as max_rush_yds_off'), DB::raw('min(pts_off) as min_pts_off'), DB::raw('max(pts_off) as max_pts_off'))->get()->toArray();
        
        return $minMax;
    }

    public static function leagueMinMaxDefAverages()
    {
        $avgs = TeamGameLogs::select(DB::raw('avg(team_game_logs.pass_yds_def) as pass_yds_def'), DB::raw('avg(team_game_logs.rush_yds_def) as rush_yds_def'), DB::raw('avg(team_game_logs.to_def) as to_def'), DB::raw('avg(team_game_logs.pts_def) as pts_def'))->groupBy(['player_id'])->toSql();

        $minMax = DB::table(DB::raw("({$avgs}) s"))->select(DB::raw('min(pass_yds_def) as min_pass_yds_def'), DB::raw('max(pass_yds_def) as max_pass_yds_def'), DB::raw('min(rush_yds_def) as min_rush_yds_def'), DB::raw('max(rush_yds_def) as max_rush_yds_def'), DB::raw('min(pts_def) as min_pts_def'), DB::raw('max(pts_def) as max_pts_def'))->get()->toArray();
        
        return $minMax;
    }
}
