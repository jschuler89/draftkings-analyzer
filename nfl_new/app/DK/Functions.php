<?php

namespace App\DK;

use App\PlayerPool;
use App\Players;
use App\PlayerGameLogs;
use App\Teams;
use App\DepthChart;
use App\TeamGameLogs;
use App\Objects\Player;
use Illuminate\Support\Facades\DB;

class Functions
{
	public $gameDate;

	public function __construct()
	{
		$this->gameDate = '2019-11-03';	
	}

	public function rosterUpload($csv)
	{
		// Position,Name + ID,Name,ID,Roster Position,Salary,Game Info,TeamAbbrev,AvgPointsPerGame
		$csvArray = explode("\n", $csv);
		$headers = array();

		foreach($csvArray as $index => $_csvRow) {
			$playerInfo = explode(",", trim($_csvRow));

			if($index == 0) {
				$headers = array_flip($playerInfo);
				continue;
			}

			if(!isset($playerInfo[$headers['Name']])) {
				continue;
			}

			$position = explode('/', $playerInfo[$headers['Position']]);

			$playerData['position'] = str_replace('"', '', $position[0]);
			$playerData['name'] = str_replace('"', '', $playerInfo[$headers['Name']]);
			$playerData['team'] = str_replace(array('"', "\r", "\n"), '', $playerInfo[$headers['TeamAbbrev']]);

			$playerInDB = Players::select('players.*')
				   ->where('position', $position[0])
				   ->where('name', $playerData['name'])
				   ->where('team', $playerData['team'])
				   ->get()
				   ->first();

			if(!$playerInDB) {
				$player = Players::create($playerData);
			} else {
				$player = $playerInDB;
			}

			$salary = $playerInfo[$headers['Salary']];

			// [0] => Team@Opp, [1] => 09/09/2019 [2] => 01:00PM [3] => ET
			// [0] => Team, [1] => Opp
			$gameInfoArray = explode(' ', $playerInfo[$headers['Game Info']]);
			$gameArray = explode("@", $gameInfoArray[0]);
			$game_time = str_replace(array('AM', 'PM'), '', $gameInfoArray[2]);
			$opp = trim(str_replace('"', '', strtoupper($gameArray[1])));
			$opp2 = trim(str_replace('"', '', strtoupper($gameArray[0])));
			$fppg = number_format($playerInfo[$headers['AvgPointsPerGame']], 2);
			$team = trim(str_replace('"', '', $playerData['team']));
			$gameDate = explode('/', $gameInfoArray[1]);

			if(strtolower($team) == strtolower($opp)) {
				$opp = $opp2;
			}

			$playerPoolData['player_id'] = $player['player_id'];
			$playerPoolData['fppg'] = $fppg;
			$playerPoolData['salary'] = $salary;
			$playerPoolData['opp'] = $opp;
			$playerPoolData['game_date'] = $gameDate[2] . '-' . $gameDate[0] . '-' . $gameDate[1];
			$playerPoolData['game_time'] = $game_time;
			$playerPoolData['dk_player_id'] = $playerInfo[$headers['ID']];

			PlayerPool::create($playerPoolData);
		}

		return true;
	}

	public function getPlayerIds()
	{
		$player_ids_results = PlayerPool::select('player_pool.player_id')->where('game_date', $this->gameDate)->get();
		$player_ids = array();

		foreach($player_ids_results as $player_id) {
			$player_ids[] = $player_id['player_id'];
		}

		return $player_ids;
	}

	public function getTeamIds()
	{
		$team_ids_results = Teams::select('teams.*')->get();
		$team_ids = array();

		foreach($team_ids_results as $team_id) {
			$team_ids[] = $team_id['team_id'];
		}

		return $team_ids;
	}

	public function getPlayers($player_ids)
	{
		$players = array();

		foreach($player_ids as $player_id) {
			$players[] = new Player($player_id);
		}

		return $players;
	}

	public function getGames()
	{
		$games = DB::select("select p.team, pp.opp, t.team_id, t2.team_id as opp_id, t2.team as opp_team from player_pool pp left join players p on p.player_id = pp.player_id left join teams t on t.team = p.team left join teams t2 on t2.team = pp.opp where pp.game_date = '{$this->gameDate}' and p.team is not null group by p.team, pp.opp, t2.team_id, t.team_id, t2.team");

		$gamesArray = array();

		foreach($games as $_game) {
			$_game = (array)$_game;
			$key = "{$_game['team_id']}:{$_game['opp_id']}";
			$value = "{$_game['team']} vs {$_game['opp_team']}";
			$gamesArray[] = array('key' => $key, 'value' => $value);
		}

		return $gamesArray;
	}

	public function getGamesDepthChart()
	{
		$games = DB::select("select p.team, pp.opp, t.team_id, t2.team_id as opp_id from player_pool pp left join players p on p.player_id = pp.player_id left join teams t on t.team = p.team left join teams t2 on t2.depth_url_param = pp.opp where pp.game_date = '{$this->gameDate}' and p.team is not null group by p.team, pp.opp, t2.team_id, t.team_id");

		$depthChartForGames = array();

		foreach($games as $_game) {
			$_game = (array)$_game;
			$teamDepth = DepthChart::select('position', 'name', 'team_id', 'player_id', 'number', 'depth_position')
									->where('team_id', $_game['team_id'])
									->where('depth_position', '<=', 2)
									->groupBy('position', 'name', 'team_id', 'player_id', 'number', 'depth_position')
									->orderBy('depth_position', 'asc')
			                        ->get();
			foreach($teamDepth as $_depth) {
				$depthChartForGames[$_game['team_id']]['depth'][$_depth['position']][] = $_depth;
				
			}
			$depthChartForGames[$_game['team_id']]['opp_id'] = $_game['opp_id'];
		}

		return $depthChartForGames;
	}

	public function getLeagueAvgs()
	{
		$avgs = array();
		$avgs['QB'] = PlayerGameLogs::leagueAverages('QB');
		$positionFPAvg = $this->calculateFPAvg($avgs['QB']);
		$avgs['QB_FP'] = $positionFPAvg;
		$avgs['RB'] = PlayerGameLogs::leagueAverages('RB');
		$positionFPAvg = $this->calculateFPAvg($avgs['RB']);
		$avgs['RB_FP'] = $positionFPAvg;
		$avgs['WR'] = PlayerGameLogs::leagueAverages('WR');
		$positionFPAvg = $this->calculateFPAvg($avgs['WR']);
		$avgs['WR_FP'] = $positionFPAvg;
		$avgs['TE'] = PlayerGameLogs::leagueAverages('TE');
		$positionFPAvg = $this->calculateFPAvg($avgs['TE']);
		$avgs['TE_FP'] = $positionFPAvg;

		return $avgs;
	}

	public function leagueMinMaxDefAverages()
	{
		$avgs = TeamGameLogs::leagueMinMaxDefAverages();

		return $avgs;
	}

	public function leagueMinMaxOffAverages()
	{
		$avgs = TeamGameLogs::leagueMinMaxOffAverages();

		return $avgs;
	}

	/**
	 * Get players by position by searching player_ids
	 * 
	 */
	public function getPlayersByPosition($position, $player_ids = array(), $criteria = array())
	{
		if(empty($player_ids)) {
			$player_ids = $this->getPlayerIds();
		}

		if(!empty($criteria) && $criteria['enabled']) {
			$players = Players::getPlayersByPositionAndCriteria($position, $criteria);
		} else {
			$players = Players::getPlayersByPosition(array(strtoupper($position)));
		}
		$playersArray = array();
		foreach($players as $_player) {
			if(in_array($_player->player_id, $player_ids)) {
				$player = $this->getPlayer($_player->player_id);

				$positionAvg = PlayerGameLogs::leagueAverages($position);
				// if($position == 'QB') {
				// 	var_dump($positionAvg); die;
				// }
				$positionFPAvg = $this->calculateFPAvg($positionAvg);

				if($player['fppg'] >= $positionFPAvg) {
					$player['above_avg_fppg'] = true;
				} else {
					$player['above_avg_fppg'] = false;
				}

				if(!empty($player)) {
					$playersArray[$_player->player_id] = $player;
				}
			}
		}

		return $playersArray;
	}

	public function getQBs($player_ids)
	{
		$players = Players::getPlayersByPosition(array('QB'));
		$playersArray = array();
		foreach($players as $_player) {
			if(in_array($_player->player_id, $player_ids)) {
				$player = $this->getPlayer($_player->player_id);
				$playersArray[$_player->player_id] = $player;
			}
		}

		return $playersArray;
	}

	public function getRBs($player_ids)
	{
		$players = Players::getPlayersByPosition(array('RB'));
		$playersArray = array();
		foreach($players as $_player) {
			if(in_array($_player->player_id, $player_ids)) {
				$player = new Player($_player->player_id);
				if($player->playerData['fppg'] > 0) {
					$playersArray[$_player->player_id] = $player;
				}
			}
		}

		return $playersArray;
	}

	public function getWRs($player_ids)
	{
		$players = Players::getPlayersByPosition(array('WR'));
		$playersArray = array();
		foreach($players as $_player) {
			if(in_array($_player->player_id, $player_ids)) {
				$player = $this->getPlayer($_player->player_id);
				if($player->playerData['fppg'] > 0) {
					$playersArray[$_player->player_id] = $player;
				}
			}
		}

		return $playersArray;
	}

	public function getTEs($player_ids)
	{
		$players = Players::getPlayersByPosition(array('TE'));
		$playersArray = array();
		foreach($players as $_player) {
			if(in_array($_player->player_id, $player_ids)) {
				$player = new Player($_player->player_id);
				if($player->playerData['fppg'] > 0) {
					$playersArray[$_player->player_id] = $player;
				}
			}
		}

		return $playersArray;
	}

	public function getDSTs()
	{
		$leagueMinMaxDefAverages = $this->leagueMinMaxDefAverages(); // generate defensive score based on min/max league avgs
		$leagueMinMaxDefAverages = (array)reset($leagueMinMaxDefAverages);
		$leagueMinMaxOffAverages = $this->leagueMinMaxOffAverages();
		$leagueMinMaxOffAverages = (array)reset($leagueMinMaxOffAverages);
		$teams = Teams::getTeams();
		$teamsArray = array();
		foreach($teams as $_team) {
			if($_team['game_date'] == $this->gameDate) {
				$_teamLast5 = Teams::allowedYardsLast5($_team['team_id']);
				$pass_yds_off = $_team['pass_yds_off'];
				$rush_yds_off = $_team['rush_yds_off'];
				$pts_off = $_team['pts_off'];
				$pass_yds_off_score = ($pass_yds_off / $leagueMinMaxOffAverages['max_pass_yds_off']) * 100;
				$rush_yds_off_score = ($rush_yds_off / $leagueMinMaxOffAverages['max_rush_yds_off']) * 100;
				$pts_off_score = ($pts_off / $leagueMinMaxOffAverages['max_pts_off']) * 100;
				$off_score = ($pass_yds_off_score + $rush_yds_off_score + $pts_off_score) / 3;

				$pass_yds_def = $_team['pass_yds_def'];
				$rush_yds_def = $_team['rush_yds_def'];
				$pts_def = $_team['pts_def'];
				$pass_yds_def_score = ($pass_yds_def == 0) ? 0 : ($leagueMinMaxDefAverages['min_pass_yds_def'] / $pass_yds_def) * 100;
				$rush_yds_def_score = ($rush_yds_def == 0) ? 0 : ($leagueMinMaxDefAverages['min_rush_yds_def'] / $rush_yds_def) * 100;
				$pts_def_score = ($pts_def == 0) ? 0 : ($leagueMinMaxDefAverages['min_pts_def'] / $pts_def) * 100;
				$def_score = ($pass_yds_def_score + $rush_yds_def_score + $pts_def_score) / 3;

				$teamsArray[strtoupper($_team['team'])] = $_team;
				$teamsArray[strtoupper($_team['team'])]['team'] = strtoupper($_team['team']);
				$teamsArray[strtoupper($_team['team'])]['opp'] = strtoupper($_team['opp']);
				$teamsArray[strtoupper($_team['team'])]['pass_yds_off_score'] = $pass_yds_off_score;
				$teamsArray[strtoupper($_team['team'])]['rush_yds_off_score'] = $rush_yds_off_score;
				$teamsArray[strtoupper($_team['team'])]['off_score'] = $off_score;
				$teamsArray[strtoupper($_team['team'])]['def_score'] = $def_score;
				$teamsArray[strtoupper($_team['team'])]['pass_yds_def_score'] = $pass_yds_def_score;
				$teamsArray[strtoupper($_team['team'])]['rush_yds_def_score'] = $rush_yds_def_score;
				$teamsArray[strtoupper($_team['team'])]['pass_yds_def_last_5'] = $_teamLast5['pass_yds_def_last_5'];
				$teamsArray[strtoupper($_team['team'])]['rush_yds_def_last_5'] = $_teamLast5['rush_yds_def_last_5'];
				$teamsArray[strtoupper($_team['team'])]['pts_def_last_5'] = $_teamLast5['pts_def_last_5'];
			}
		}

		return $teamsArray;
	}

	public function getPlayer($player_id)
	{
		$cache = DB::select("SELECT * FROM player_cache where player_id = $player_id");

		if(!$cache) {
			$player = Players::getPlayerByPlayerId($player_id);
			// var_dump($player); die;
			$gamelogs = DB::select("SELECT * FROM player_game_logs where player_id = $player_id");

			foreach($gamelogs as $key => $_gamelog) {
				$_gamelog = (array)$_gamelog;
				$fp = $this->calculateFp($_gamelog);
				$gamelogs[$key]->fp  = $fp;
			}

			if($gamelogs) {
				$player['game_logs'] = $gamelogs;
			}

			DB::table('player_cache')->insert(array('player_id' => $player_id, 'data' => serialize($player)));
		} else {
			$player = unserialize($cache[0]->data);
		}

		return $player;
	}

	public function calculateFP($data)
	{
		$fp = 0;
		$fp += 	$data['pass_td'] * 4;
		$fp += 	$data['pass_yds'] * 0.04;
		$fp -= 	$data['pass_int'] * 1;
		$fp += 	($data['pass_yds'] >= 300) ? 3 : 0;
		$fp += 	($data['rush_td'] < 0) ? 0 : $data['rush_td'] * 6;
		$fp += 	$data['rush_yds'] * 0.1;
		$fp += 	$data['rec'] * 1;
		$fp += 	$data['rec_yds'] * 0.1;
		$fp += 	$data['rec_td'] * 6;
		$fp += 	($data['rush_yds'] >= 100) ? 3 : 0;
		$fp += 	($data['rec_yds'] >= 100) ? 3 : 0;

		return $fp;
	}

	public function calculateFPAvg($data)
	{
		$fp = 0;
		$fp += 	$data['pass_td_avg'] * 4;
		$fp += 	$data['pass_yds_avg'] * 0.04;
		$fp -= 	$data['pass_int_avg'] * 1;
		$fp += 	($data['pass_yds_avg'] >= 300) ? 3 : 0;
		$fp += 	($data['rush_td_avg'] < 0) ? 0 : $data['rush_td_avg'] * 6;
		$fp += 	$data['rush_yds_avg'] * 0.1;
		$fp += 	$data['rec_avg'] * 1;
		$fp += 	$data['rec_yds_avg'] * 0.1;
		$fp += 	$data['rec_td_avg'] * 6;
		$fp += 	($data['rush_yds_avg'] >= 100) ? 3 : 0;
		$fp += 	($data['rec_yds_avg'] >= 100) ? 3 : 0;

		return $fp;
	}

	public function cmpTargets($a, $b)
	{
		if($a['targets'] == $b['targets']) {
			return 0;
		}

		return ($a['targets'] < $b['targets']) ? 1 : -1;
	}

	public function syncPlayer($player_id)
	{
		$player = Players::getPlayerByPlayerId($player_id);
		$basicInfo = array();

		$position = strtolower($player['position']);

		if($position == 'dst') {
			return array('name' => $player['name']);
		}

		if($player['url_param'] == null) {
			$basicInfo = $this->getBasicInfo($player);
			$this->insertUrlParam($player_id, $basicInfo['url_param']);
			$urlparam = $basicInfo['url_param'];
		} else {
			$urlparam = $player['url_param'];
		}

		if(isset($basicInfo['img']) && $basicInfo['img'] != '') {
			$this->insertPlayerImg($player_id, $basicInfo['img']);
		}


		$gameLogs = $this->getPlayerGameLogs($urlparam, $position);

		$this->insertGameLog($player_id, $gameLogs);

		return array('name' => $player['name']);
	}

	public function syncTeam($team_id)
	{
		$team = Teams::getTeamByTeamId($team_id);
		$teamGameLogs = $this->getTeamGameLogs($team['url_param']);
		$this->insertTeamGameLog($team, $teamGameLogs);

		return array('name' => $team['team']);
	}

	public function syncDepthChart($team_id)
	{
		$team = Teams::getTeamByTeamId($team_id);
		$this->getDepthChart($team['depth_url_param'], $team_id);

		return array('name' => $team['team']);
	}

	public function insertTeamGameLog($team, $teamLog = array())
	{
		if(empty($teamLog)) {
			return false;
		}

		//delete old records of team game log
		// TeamGameLogs::where('team_id', $team['team_id'])->delete();

		$columns = DB::select("SHOW COLUMNS FROM team_game_logs");
		$fields = array();

		foreach($columns as $_column) {
			$_column = (array)$_column;
			$fields[$_column['Field']] = $_column['Field'];
		}

		$logs = $teamLog['gamelogs'];
		$games = 1;

		foreach($logs as $_log) {
			$keys = array();
			$values = array();


			foreach($_log as $index => $_logRow) {
				$column = $index;

				// Get team name 'New England Patriots' -> 'Patriots'
				if($column == 'opp') {
					if($_logRow == 'Bye Week') {
						$_logRow = 'Bye';
					} else {
						$_logRow = explode(' ', $_logRow);
						$_logRow = $_logRow[count($_logRow) - 1];
					}
				}

				if(in_array($column, $fields)) {
					// $keys[] = $fields[$column];
					$values[$fields[$column]] = $_logRow;
					$values['game_num'] = $games;
					$values['player_id'] = $team['team_id'];
				}
				
			}

			TeamGameLogs::create($values);
			$games++;
		}


		return true;
	}

	public function getTeamGameLogs($urlparam)
	{
		$footballUrl = "http://www.pro-football-reference.com/teams/$urlparam/2019.htm";
		$gameArray = array();
		
		try {
			$results = file_get_contents($footballUrl);
		} catch(\Exception $e) {
			return array();
		}

		$doc = new \DOMDocument();
		@$doc->loadHTML($results);

		$xpath = new \DOMXpath($doc);
		$week = 1;

		$elements = $xpath->query("//table[contains(@id, \"games\")]//tbody//tr");

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		$continue = false;
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$localArray['week_num'] = $week;

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			if($_childNode->nodeValue == '' || $_childNode->nodeValue == 'inf' || $_childNode->nodeValue == '---') {
		  				$_childNode->nodeValue = 0;
		  			}

		  			if($data_stat == 'yards_off' && $_childNode->nodeValue == 0) {
		  				$continue = true;
		  				break;
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;
		  		}

		  		if(!empty($localArray) && !$continue) {
					$gameArray['gamelogs'][] = $localArray;
				}

				$week++;
		  	}
	  	}

		return $gameArray;
	}

	public function getDepthChart($urlparam, $team_id)
	{
		$urlparam = strtoupper($urlparam);
		$footballUrl = "http://www.ourlads.com/nfldepthcharts/depthchart/$urlparam";
		$gameArray = array();
		
		try {
			$results = file_get_contents($footballUrl);
		} catch(\Exception $e) {
			return array();
		}

		$doc = new \DOMDocument();
		@$doc->loadHTML($results);

		$xpath = new \DOMXpath($doc);
		$week = 1;

		$elements = $xpath->query("//tbody[contains(@id, \"ctl00_phContent_dcTBody\")]//tr");
		$skip = array('Offense', 'Defense', 'OFF', 'DEF', ' ', 'ST', 'Reserves');
		$offPositions = array('LWR', 'RWR', 'SWR', 'WR', 'LT', 'LG', 'C', 'RG', 'RT', 'TE', 'QB', 'RB', 'FB', 'TE/HB');
		$defPositions = array('LE', 'LDE', 'RDE', 'DT', 'LDT', 'RDT', 'NT', 'DE', 'RE', 'WLB', 'MLB', 'SLB', 'RCB', 'SS', 'FS', 'S', 'LCB', 'LOLB', 'ROLB', 'LILB', 'RILB', 'OLB', 'DE/OB', 'ILB', 'NLB', 'NB', 'CB', 'SAM', 'SILB', 'LB', 'MIKE', 'WILL', 'RUSH', 'JACK');
		$stPositions = array('P', 'PK', 'LS', 'H', 'KO', 'PR', 'KR', 'RES', 'PS', 'SUS', 'RET');

		$allPositions = array_merge($offPositions, $defPositions, $stPositions);

		if (!is_null($elements)) {
	  		$alreadyInputDE = false;
	  		$alreadyInputDT = false;
	  		$alreadyInputS = false;
	  		$alreadyInputMLB = false;
	  		$alreadyInputLB = false;
	  		$alreadyInputILB = false;
	  		$alreadyInputOLB = false;

		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		$continue = false;
		  		$foundPosition = false;
		  		$depth = 1;
		  		$lastPosition = '';
		  		foreach($element->childNodes as $_childNode) {

		  			// we are only looking at table data
		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			// look to skip these fields
		  			if(in_array($_childNode->nodeValue, $skip)) {
		  				continue;
		  			}

		  			// look to skip this field if it has the 'dt-sh' class
		  			if(isset($_childNode->attributes[0]) && $_childNode->attributes[0]->value == "dt-sh") {
		  				continue;
		  			}

		  			// make sure we grab the position
		  			// if we are at a new position restart the depth chart number
		  			if(in_array($_childNode->nodeValue, $allPositions)) {
		  				$position = $_childNode->nodeValue;
		  				$foundPosition = true;
		  				$depth = 1;

		  				// detect if we already entered this position
		  				// if so add a '2' so we can detect if there are multiple of that position
		  				if($position == 'DE' && $alreadyInputDE) {
		  					$position = 'DE2';
		  					$alreadyInputDE = false;
		  				}

		  				if($position == 'DE' && !$alreadyInputDE) {
		  					$alreadyInputDE = true;
		  				}

		  				if($position == 'DT' && $alreadyInputDT) {
		  					$position = 'DT2';
		  					$alreadyInputDT = false;
		  				}

		  				if($position == 'DT' && !$alreadyInputDT) {
		  					$alreadyInputDT = true;
		  				}

		  				if($position == 'MLB' && $alreadyInputMLB) {
		  					$position = 'MLB2';
		  					$alreadyInputMLB = false;
		  				}

		  				if($position == 'MLB' && !$alreadyInputMLB) {
		  					$alreadyInputMLB = true;
		  				}

		  				if($position == 'LB' && $alreadyInputLB) {
		  					$position = 'LB2';
		  					$alreadyInputLB = false;
		  				}

		  				if($position == 'LB' && !$alreadyInputLB) {
		  					$alreadyInputLB = true;
		  				}

		  				if($position == 'S' && $alreadyInputS) {
		  					$position = 'S2';
		  					$alreadyInputS = false;
		  				}

		  				if($position == 'S' && !$alreadyInputS) {
		  					$alreadyInputS = true;
		  				}

		  				if($position == 'ILB' && $alreadyInputILB) {
		  					$position = 'ILB2';
		  					$alreadyInputILB = false;
		  				}

		  				if($position == 'ILB' && !$alreadyInputILB) {
		  					$alreadyInputILB = true;
		  				}

		  				if($position == 'OLB' && $alreadyInputOLB) {
		  					$position = 'OLB2';
		  					$alreadyInputOLB = false;
		  				}

		  				if($position == 'OLB' && !$alreadyInputOLB) {
		  					$alreadyInputOLB = true;
		  				}

		  				continue;
		  			}

		  			// check if value is a number (it has to be jersey number)
		  			if(is_numeric($_childNode->nodeValue)) {
		  				$number = $_childNode->nodeValue;
		  				continue;
		  			}

		  			// any blank values skip it
		  			if($_childNode->nodeValue == '') {
		  				continue;
		  			}

		  			$nameArray = explode(' ', $_childNode->nodeValue);
		  			if(!isset($nameArray[1])) {
		  				var_dump($_childNode->nodeValue); die;
		  			}
		  			$fname = $nameArray[1];
		  			$lname = rtrim($nameArray[0], ',');
		  			$name = $fname . ' ' . $lname;

		  			$playerInfo = Players::getPlayerByName($name);

		  			if($position == 'DE/OB') {
		  				$position = 'DE_OB';
		  			}

		  			if($position == 'LB' || $position == 'MIKE') {
		  				$position = 'MLB';
		  			}

		  			if($position == 'LB2') {
		  				$position = 'SLB';
		  			}

		  			if($position == 'WILL') {
		  				$position = 'WLB';
		  			}

		  			if($position == 'RUSH' || $position == 'JACK') {
		  				$position = 'ROLB';
		  			}

		  			if($position == 'WR') {
		  				$position = 'SWR';
		  			}

		  			$values = array();
		  			$values['depth_position'] = $depth;
		  			$values['name'] = $name;
		  			$values['team_id'] = $team_id;
		  			$values['number'] = $number;
		  			$values['position'] = $position;
		  			$values['player_id'] = ($playerInfo) ? $playerInfo['player_id'] : NULL;

		  			DepthChart::create($values);

		  			$depth++;		  			
		  		}
		  	}
	  	}

		return array('success' => true);
	}

	/**
	 * Get basic player info
	 * 
	 **/
	public function getBasicInfo($player)
	{
		$max = 10;
		$index = 0;
		$position = $player['position'];
		$urlparam = false;
		$jsonName  = '';
		$img = '';

		while($index < $max) {
			$fullname = str_replace(array(' Jr.', ' III', ' Sr.', '.', "'"), '', $player['name']);
			$name = explode(' ', $fullname);
			$first_name = $name[0];
			$last_name = $name[1];
			$playerValidated = false;

			$first_name_formatted = substr($first_name, 0, 2);
			$last_name_formatted = substr($last_name,0, 4);
			$category = $last_name[0];
			$indexNumber = '0'.$index;

			$urlparam = "$category/".$last_name_formatted.$first_name_formatted.$indexNumber;
			$footballUrl = "http://www.pro-football-reference.com/players/$urlparam.htm";

			try {
				$results = file_get_contents($footballUrl);
			} catch(\Exception $e) {
				$index++;
				continue;
			}

			$doc = new \DOMDocument();
			@$doc->loadHTML($results);

			$xpath = new \DOMXpath($doc);

			$elements = $xpath->query("//div//h1");

			if (!is_null($elements)) {
			  	foreach ($elements as $element) {
			  		$jsonName = $element->textContent;
		  		}
		  	}

		  	if(trim($jsonName) == trim($fullname)) {
	  			$playerValidated = true;
	  		}

		  	if($playerValidated) {
		  		//get image
		  		$elements = $xpath->query("//div[contains(@class, \"media-item\")]//img");

				if (!is_null($elements)) {
				  	foreach ($elements as $element) {
				  		foreach($element->attributes as $_attribute) {
				  			if($_attribute->name == 'src') {
				  				$img = $_attribute->value;
				  			}
				  		}
				  	}
			  	}

			  	if($img != '') {
		  			break;
			  	}
		  	}


			$index++;
		}

		return array('url_param' => $urlparam, 'img' => $img);
	}

	/**
	 * Save player url param 
	 * 
	 */
	public function insertUrlParam($player_id, $url_param)
	{
		if(empty($player_id) || empty($url_param)) {
			return false;
		}

		$player = Players::find($player_id);

		$player->url_param = $url_param;

		$player->save();

		return true;
	}

	public function insertPlayerImg($player_id, $img)
	{
		if(empty($player_id)) {
			return false;
		}

		$player = Players::find($player_id);

		$player->img = $img;

		$player->save();

		return true;
	}

	/**
	 * Get player game logs 
	 * 
	 */
	public function getPlayerGameLogs($urlparam, $position)
	{
		// $urlparam = preg_replace('/\w+\//', '', $urlparam); // replace category letter s/salech01 -> salech01
		// $urlParamString = "gl.fcgi?id=$urlparam&t=$position&year=2019";
		$footballUrl = "http://www.pro-football-reference.com/players/$urlparam/gamelog/2019/";
		$gameArray = array();

		try {
			$results = file_get_contents($footballUrl);
		} catch(\Exception $e) {

		}

		$doc = new \DOMDocument();
		@$doc->loadHTML($results);

		$xpath = new \DOMXpath($doc);

		$elements = $xpath->query("//table[contains(@id, \"stats\")]//tbody//tr[contains(@id, \"stats\")]");

		if (!is_null($elements)) {
	  		$games = 1;
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = strtolower($_attribute->value);
		  					break;
		  				}
		  			}

		  			if($data_stat == 'opp') {
		  				$localArray['opp_stats'] = array(); //$this->getTeamStats(strtolower($_childNode->nodeValue));
		  			}


		  			if($data_stat == 'date_game') {
		  				$formatString = preg_replace('/[[:^print:]]/', ' ', $_childNode->nodeValue); // weird characters we need to remove
		  				$dateFormat = explode('  ', $formatString);
		  				if(count($dateFormat) == 1) {
		  					$dateFormat = explode(' ', $formatString);
		  				}

		  				$localArray['date_game'] = date('Y-m-d', strtotime($dateFormat[1].'-'.$dateFormat[0]));
		  				continue;
		  			}

		  			if($_childNode->nodeValue == '' || $_childNode->nodeValue == 'inf' || $_childNode->nodeValue == '---') {
		  				$_childNode->nodeValue = 0;
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;

		  		}
  				$localArray['week_num'] = $games;
	  			$games++;



		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}
		  	}
		}

		return $gameArray;
	}

	public function getTeamStats($urlparam)
	{
		$footballUrl = "http://www.pro-football-reference.com/teams/$urlparam/2019.htm";
		
		try {
			$results = file_get_contents($footballUrl);
		} catch(\Exception $e) {

		}

		$doc = new \DOMDocument();
		$doc->loadHTML(str_replace(array('<!--', '-->'), '',$results));

		$xpath = new \DOMXpath($doc);
		$gamesPlayed = false;

		$elements = $xpath->query("//table[contains(@id, \"team_stats\")]//tbody//tr");
		$gamesPlayedElement = $xpath->query("//table[contains(@id, \"passing\")]//tfoot//tr");

		if (!is_null($gamesPlayedElement)) {
		  	foreach ($gamesPlayedElement as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			if($data_stat == 'g') {
		  				$gamesPlayed = $_childNode->nodeValue;
		  			}

		  		}

		  		if($gamesPlayed) {
		  			$gameArray['games_played'] = $gamesPlayed;
		  			break;
		  		}
		  	}
		}

		if (!is_null($elements)) {
		  	foreach ($elements as $element) {
		  		$localArray = array();
		  		foreach($element->childNodes as $_childNode) {

		  			if($_childNode->tagName != 'td') {
		  				continue;
		  			}

		  			$attributes = $_childNode->attributes;

		  			foreach($attributes as $_attribute) {
		  				if($_attribute->name == 'data-stat') {
		  					$data_stat = $_attribute->value;
		  					break;
		  				}
		  			}

		  			$localArray[$data_stat] = $_childNode->nodeValue;
		  		}

		  		if(!empty($localArray)) {
					$gameArray['gamelogs'][] = $localArray;
				}
		  	}
	  	}

		return $gameArray;
	}

	/**
	 * Insert each game log into database
	 * 
	 */
	public function insertGameLog($player_id, $gamelog) 
	{
		$columns = DB::select("SHOW COLUMNS FROM player_game_logs");

		if(empty($gamelog)) {
			return false;
		}

		foreach($gamelog['gamelogs'] as $_game) {
			$values = array();
			$keys = array();
			$alreadyLoaded = false;

			foreach($columns as $_column) {
				$_column = (array)$_column;
				if(isset($_game[$_column['Field']])) {

					if($_column['Field'] == 'week_num') {
						$week_num = $_game[$_column['Field']];
						$count = PlayerGameLogs::select(DB::raw('count(*) as count'))
						                       ->where('player_id', $player_id)
						                       ->where('week_num', $week_num)
						                       ->get()
						                       ->first();

						if($count['count'] > 0) {
							$alreadyLoaded = true;
							break;
						}
					}
					$values['player_id'] = $player_id;
					$values[$_column['Field']] = $_game[$_column['Field']];
					// $keys[] = $_column['Field'];
				}
				
			}

			if($alreadyLoaded) {
				continue;
			}

			$insertPlayer = PlayerGameLogs::create($values);
		}

		return true;
	}

	public function generateShowdownLineupsByCriteria($request)
	{
		ini_set('max_execution_time', 0);
		$criteria = $request->input('criteria');
		$qb_criteria = $criteria['QB'];
		$rb_criteria = $criteria['RB'];
		$wr_criteria = $criteria['WR'];
		$te_criteria = $criteria['TE'];
		$dst_criteria = $criteria['DST'];
		$methods = array('<'=> 'less_than', '<=' => 'less_than_equal', '>' => 'greater_than', '>=' => 'greater_than_equal', '=' => 'equal_to');

		$qbs = $this->getPlayersByPosition('QB', array(), $qb_criteria);
		$rbs = $this->getPlayersByPosition('RB', array(), $rb_criteria);
		$wrs = $this->getPlayersByPosition('WR', array(), $wr_criteria);
		$tes = $this->getPlayersByPosition('TE', array(), $te_criteria);
		$dsts = $this->getDSTs();

		$positions = array('CPT', 'FLEX1', 'FLEX2', 'FLEX3', 'FLEX4', 'FLEX5');
		$salary_cap = 50000;
		$salary_min = 47000;
		$simulations = 5000;
		$salary_total = 0;
		$fptsTotal = 0;
		$rosters = array();
		$cpts = array();

		for($i = 0; $i < $simulations; $i++) {
			$copy_positions = array('CPT', 'FLEX1', 'FLEX2', 'FLEX3', 'FLEX4', 'FLEX5');
			$position_count = count($positions);
			$roster_filled = 0;
			$postionFilled = array();
			$playersPicked = array();
			$roster = array();
			$index = 0;
			$salary_total = 0;
			$fptsTotal = 0;

			while(($roster_filled < $position_count) && count($copy_positions) > 0) {
				$randPosIndex = array_rand($copy_positions, 1);
				$randPos = $copy_positions[$randPosIndex];
				$index++;

				if($index >= 14) {
					$copy_positions = array('CPT', 'FLEX1', 'FLEX2', 'FLEX3', 'FLEX4', 'FLEX5');
					$position_count = count($positions);
					$roster_filled = 0;
					$postionFilled = array();
					$playersPicked = array();
					$roster = array();
					$index = 0;
					$salary_cap = 50000;
					$salary_total = 0;
					$fptsTotal = 0;
				}

				if(!in_array($randPos, $postionFilled)) {
					$players = array();

					switch($randPos) {
						case 'CPT':
						case 'FLEX1':
						case 'FLEX2':
						case 'FLEX3':
						case 'FLEX4':
						case 'FLEX5':
							$players = array_merge($qbs, $rbs, $wrs, $tes, $dsts);
							break;
						default:
							$players = array();
							break;
					}

					if(!empty($players)) {
						$randPlayerIndex = array_rand($players, 1);
						$randPlayer = $players[$randPlayerIndex]->toArray();
						$salary = $randPlayer['salary'];
						$fppg = $randPlayer['fppg'];

						if($randPos == 'CPT') {
							$salary = $salary * 1.5;
							$randPlayer['cpt_salary'] = $salary;
							$cpts[] = $randPlayer['name'];
							$fppg = $randPlayer['fppg'] * 1.5;
							$randPlayer['fppg'] = $fppg;
						}

						// if($randPos != 'CPT' && in_array($randPlayer['name'], $cpts)) {
						// 	var_dump($randPos);
						// 	var_dump($randPlayer);
						// 	var_dump($roster);
						// 	die;
						// }

						if(in_array($randPlayer['name'], $playersPicked)/* || ($salary_cap - $salary) < 0*/) {
							continue;
						}

						$randPlayer['new_salary'] = $salary;
						$salary = $randPlayer['new_salary'];
						unset($copy_positions[$randPosIndex]);
						$copy_positions = array_values($copy_positions);

						$postionFilled[] = $randPos;
						$playersPicked[] = $randPlayer['name'];
						$randPlayer['new_position'] = $randPos;
						$roster[$randPos] = $randPlayer;
						$salary_cap -= $salary;
						$salary_total += $salary;
						$fptsTotal += $fppg;
						$roster['salary_total'] = $salary_total;
						$roster['fppg_total'] = $fptsTotal;
						$roster_filled++;
					}
				} else {
					continue;
				}
			}

			if($roster['salary_total'] < 50001) {
				$rosters[] = $roster;
			}
		}

		usort($rosters, array($this, "compareSalary"));

		return $rosters;
	}

	public function generateLineupsByCriteria($request)
	{
		ini_set('max_execution_time', 0);
		$criteria = $request->input('criteria');
		$qb_criteria = $criteria['QB'];
		$rb_criteria = $criteria['RB'];
		$wr_criteria = $criteria['WR'];
		$te_criteria = $criteria['TE'];
		$dst_criteria = $criteria['DST'];
		$methods = array('<'=> 'less_than', '<=' => 'less_than_equal', '>' => 'greater_than', '>=' => 'greater_than_equal', '=' => 'equal_to');

		$qbs = $this->getPlayersByPosition('QB', array(), $qb_criteria);
		$rbs = $this->getPlayersByPosition('RB', array(), $rb_criteria);
		$wrs = $this->getPlayersByPosition('WR', array(), $wr_criteria);
		$tes = $this->getPlayersByPosition('TE', array(), $te_criteria);
		$dsts = $this->getDSTs();

		$positions = array('QB', 'RB1', 'RB2', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
		$salary_cap = 50000;
		$salary_min = 48000;
		$simulations = 150;
		$salary_total = 0;
		$fptsTotal = 0;
		$rosters = array();

		for($i = 0; $i < $simulations; $i++) {
			$copy_positions = array('QB', 'RB1', 'RB', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
			$position_count = count($positions);
			$roster_filled = 0;
			$postionFilled = array();
			$playersPicked = array();
			$roster = array();
			$index = 0;
			$fptsTotal = 0;

			while(($roster_filled < $position_count) && count($copy_positions) > 0) {
				$randPosIndex = array_rand($copy_positions, 1);
				$randPos = $copy_positions[$randPosIndex];
				$index++;

				if($index >= 14) {
					$copy_positions = array('QB', 'RB1', 'RB2', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
					$position_count = count($positions);
					$roster_filled = 0;
					$postionFilled = array();
					$playersPicked = array();
					$roster = array();
					$index = 0;
					$salary_cap = 50000;
					$salary_total = 0;
					$fptsTotal = 0;
				}

				if(!in_array($randPos, $postionFilled)) {
					$players = array();

					switch($randPos) {
						case 'QB':
							$players = $qbs;
							break;
						case 'RB1':
						case 'RB2':
							$players = $rbs;
							break;
						case 'WR1':
						case 'WR2':
						case 'WR3':
							$players = $wrs;
							break;
						case 'TE':
							$players = $tes;
							break;
						case 'FLEX':
							$players = array_merge($rbs, $wrs);
							break;
						case 'DST':
							$players = $dsts;
							break;
						default:
							$players = array();
							break;
					}

					if(!empty($players)) {
						$randPlayerIndex = array_rand($players, 1);
						$randPlayer = $players[$randPlayerIndex]->toArray();
						$randPlayer['name'] = !isset($randPlayer['name']) ? $randPlayer['team'] : $randPlayer['name'];

						if(in_array($randPlayer['name'], $playersPicked) || ($salary_cap - $randPlayer['salary']) < 0) {
							continue;
						}

						$criteriaPos = (empty($randPlayer['position'])) ? $randPos : $randPlayer['position'];
						$passedCriteria = false;
						$brokeOut = false;

						if($criteria[$criteriaPos]['enabled']) {
							foreach($criteria[$criteriaPos] as $key => $value) {
								if(empty($value['mode'])) {
									continue;
								}

								// if player doesn't have attribute
								// get out
								if(empty($randPlayer[$key])) {
									$brokeOut = true;
									break;
								}

								$type = $methods[$value['mode']];

								$passedCriteria = $this->$type($randPlayer[$key], $value['value']);
							}

							if(!$passedCriteria) {
								continue;
							}
						}

						if($brokeOut) {
							continue;
						}

						unset($copy_positions[$randPosIndex]);
						$copy_positions = array_values($copy_positions);
						$randPlayer['dk_position'] = $randPos;

						$postionFilled[] = $randPos;
						$playersPicked[] = (isset($randPlayer['name'])) ? $randPlayer['name'] : $randPlayer['team'];
						$roster[$randPos] = $randPlayer;
						$fptsTotal += $randPlayer['fppg'];
						$salary_cap -= $randPlayer['salary'];
						$salary_total += $randPlayer['salary'];
						$roster['salary_total'] = $salary_total;
						$roster['fppg_total'] = $fptsTotal;
						$roster_filled++;
					}
				} else {
					continue;
				}
			}

			// if($salary_total > $salary_min) {
				$rosters[] = $roster;
			// }
		}

		usort($rosters, array($this, "compareSalary"));

		return $rosters;
	}

	public function equal_to($a, $b)
	{
		return $a == $b;
	}

	public function less_than($a, $b)
	{
		return $a < $b;
	}

	public function less_than_equal($a, $b)
	{
		return $a <= $b;
	}

	public function greater_than($a, $b)
	{
		return $a > $b;
	}

	public function greater_than_equal($a, $b)
	{
		return $a >= $b;
	}

	public function compareSalary($a, $b) {
		if ($a['fppg_total'] == $b['fppg_total']) return 0;
		return ($a['fppg_total'] < $b['fppg_total']) ? 1 : -1;
	}
}
