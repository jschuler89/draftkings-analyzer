<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DepthChart extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'depth_chart';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'depth_id';

    public $timestamps  = false;

    protected $fillable = ['depth_id', 'player_id', 'name', 'team_id', 'position', 'depth_position', 'number'];
}
