<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PlayerGameLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_game_logs';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'game_id';

    public $timestamps  = false;

    protected $fillable = ['game_id','player_id','opp_player_id','week_num','team','opp','game_result','pass_cmp','pass_att','pass_yds','pass_td','pass_int','pass_rating','pass_sacked','pass_sacked_yds','rush_att','rush_yds','rush_td','targets','rec','rec_yds','rec_td','all_td'];

    public static function leagueAverages($position = 'QB')
    {
        $pass_cmp_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.pass_cmp) as pass_cmp_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("pass_cmp > 0 and position = '$position'")->get()->first()->toArray();
        $pass_td_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.pass_td) as pass_td_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("pass_td >= 0 and position = '$position'")->get()->first()->toArray();
        $pass_int_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.pass_int) as pass_int_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("pass_int > 0 and position = '$position'")->get()->first()->toArray();
        $pass_att_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.pass_att) as pass_att_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("pass_att > 2 and position = '$position'")->get()->first()->toArray();
        $pass_yds_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.pass_yds) as pass_yds_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("pass_yds > 0 and position = '$position'")->get()->first()->toArray();
        $rush_att_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.rush_att) as rush_att_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("rush_att > 0 and position = '$position'")->get()->first()->toArray();
        $rush_yds_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.rush_yds) as rush_yds_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("rush_yds > 0 and position = '$position'")->get()->first()->toArray();
        $rush_td_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.rush_td) as rush_td_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("rush_td >= 0 and position = '$position'")->get()->first()->toArray();
        $rec_yds_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.rec_yds) as rec_yds_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("rec_yds > 0 and position = '$position'")->get()->first()->toArray();
        $rec_td_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.rec_td) as rec_td_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("rec_td >= 0 and position = '$position'")->get()->first()->toArray();
        $targets_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.targets) as targets_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("targets > 0 and position = '$position'")->get()->first()->toArray();
        $rec_avg = PlayerGameLogs::select(DB::raw('avg(player_game_logs.rec) as rec_avg'))->leftJoin('players', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw("rec > 0 and position = '$position'")->get()->first()->toArray();
        $avgs = array_merge($pass_att_avg, $pass_yds_avg, $rush_att_avg, $rush_yds_avg, $targets_avg, $pass_cmp_avg, $rec_avg, $pass_td_avg, $pass_int_avg, $rec_td_avg, $rush_td_avg, $rec_yds_avg);
        
        return $avgs;
    }
}
