<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Teams extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teams';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'team_id';

    public static function getTeamByTeamId($team_id)
    {
        $team = Teams::select('teams.*')->where('team_id', $team_id)->get()->first();

        return $team;
    }

    public static function getTeams()
    {
        $teams = Teams::select('teams.*', 'player_pool.opp', 'player_pool.salary', 'player_pool.fppg', 'player_pool.game_date', DB::raw('avg(team_game_logs.pass_yds_def) as pass_yds_def'), DB::raw('avg(team_game_logs.pass_yds_off) as pass_yds_off'), DB::raw('avg(team_game_logs.rush_yds_off) as rush_yds_off'), DB::raw('avg(team_game_logs.pts_off) as pts_off'), DB::raw('avg(team_game_logs.rush_yds_def) as rush_yds_def'), DB::raw('avg(team_game_logs.pts_def) as pts_def'))->leftJoin('team_game_logs', 'team_game_logs.player_id', '=', 'teams.team_id')->leftJoin('player_pool', 'player_pool.player_id', '=', 'teams.player_id')->whereRaw('player_pool.game_date = "2019-11-03"')->groupBy(['player_id', 'team_id', 'team', 'url_param', 'opp', 'salary', 'fppg', 'game_date', 'depth_url_param', 'name'])->get();

        return $teams;
    }

    public static function allowedYardsLast5($team_id)
    {
        $team = Teams::select(DB::raw('avg(team_game_logs.pass_yds_def) as pass_yds_def_last_5'), DB::raw('avg(team_game_logs.rush_yds_def) as rush_yds_def_last_5'), DB::raw('avg(team_game_logs.pts_def) as pts_def_last_5'))->leftJoin('team_game_logs', 'team_game_logs.player_id', '=', 'teams.team_id')->leftJoin('player_pool', 'player_pool.player_id', '=', 'teams.player_id')->where('team_id', $team_id)->whereRaw('week_num <= 2')->groupBy(['team_game_logs.player_id', 'teams.team_id', 'teams.team'])->get()->first();

        return $team;
    }
}
