<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Players extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'players';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'player_id';

    public $timestamps  = false;

    protected $fillable = ['player_id', 'name', 'team', 'position', 'url_param', 'img'];

    public static function getPlayerByName($name)
    {
        $player = Players::select('players.player_id', 'players.team')->where('players.name', $name)->get()->first();
        
        return $player;
    }

    public static function getPlayerByPlayerId($player_id)
    {
        $player = Players::select('players.*', 'player_pool.salary', 'player_pool.fppg', 'player_pool.over_under', 'player_pool.spread', 'player_pool.opp', DB::raw('avg(player_game_logs.pass_yds) as pass_yds'), DB::raw('avg(player_game_logs.pass_att) as pass_att'), DB::raw('avg(player_game_logs.pass_cmp) as pass_cmp'), DB::raw('avg(player_game_logs.pass_td) as pass_tds'), DB::raw('sum(player_game_logs.pass_int) as pass_int'), DB::raw('avg(player_game_logs.rush_att) as rush_att'), DB::raw('avg(player_game_logs.rush_yds) as rush_yds'), DB::raw('avg(player_game_logs.rush_td) as rush_tds'), DB::raw('avg(player_game_logs.targets) as targets'), DB::raw('avg(player_game_logs.rec_yds) as rec_yds'), DB::raw('avg(player_game_logs.rec) as rec'), DB::raw('sum(player_game_logs.rec) as rec_sum'), DB::raw('sum(player_game_logs.targets) as targets_sum'), DB::raw('avg(player_game_logs.rec_yds) as rec_yds'), DB::raw('avg(player_game_logs.rec_td) as rec_tds'))->leftJoin('player_pool', 'player_pool.player_id', '=', 'players.player_id')->leftJoin('player_game_logs', 'player_game_logs.player_id', '=', 'players.player_id')->where('players.player_id', $player_id)->whereRaw('player_pool.game_date = "2019-11-03"')->groupBy(['player_id', 'name', 'team', 'opp', 'position', 'url_param', 'img', 'fppg', 'salary', 'over_under', 'spread'])->get()->first();
        
        return $player;
    }

    public static function getPlayersByPositionAndCriteria($position, $criteria)
    {
        $builtCriteria = array();

        foreach($criteria as $key => $_c) {
            if($key == 'enabled' || empty($_c['mode'])) {
                continue;
            }

            if($_c['mode']) {
                $builtCriteria[] = $key.$_c['mode'].$_c['value'];
            }
        }

        $players = Players::select('players.*', 'player_pool.salary', 'player_pool.fppg', 'player_pool.over_under', 'player_pool.spread', 'player_pool.opp', DB::raw('avg(player_game_logs.pass_yds) as pass_yds'), DB::raw('avg(player_game_logs.pass_att) as pass_att'), DB::raw('avg(player_game_logs.pass_cmp) as pass_cmp'), DB::raw('avg(player_game_logs.pass_td) as pass_tds'), DB::raw('sum(player_game_logs.pass_int) as pass_int'), DB::raw('avg(player_game_logs.rush_att) as rush_att'), DB::raw('avg(player_game_logs.rush_yds) as rush_yds'), DB::raw('avg(player_game_logs.rush_td) as rush_tds'), DB::raw('avg(player_game_logs.targets) as targets'), DB::raw('avg(player_game_logs.rec_yds) as rec_yds'), DB::raw('avg(player_game_logs.rec) as rec'), DB::raw('sum(player_game_logs.rec) as rec_sum'), DB::raw('sum(player_game_logs.targets) as targets_sum'), DB::raw('avg(player_game_logs.rec_yds) as rec_yds'), DB::raw('avg(player_game_logs.rec_td) as rec_tds'))->leftJoin('player_pool', 'player_pool.player_id', '=', 'players.player_id')->leftJoin('player_game_logs', 'player_game_logs.player_id', '=', 'players.player_id')->whereRaw('players.position= "' . $position . '"')->whereRaw(implode(' and ', $builtCriteria))->whereRaw('player_pool.game_date = "2019-11-03"')->groupBy(['player_id', 'name', 'team', 'opp', 'position', 'url_param', 'img', 'fppg', 'salary', 'over_under', 'spread'])->get();

        return $players;
    }

    public static function getPlayersByPosition($positions = array())
    {
        $players = Players::select('players.*')->leftJoin('player_pool', 'player_pool.player_id', '=', 'players.player_id')->whereRaw('players.position in (\'' . implode("','", $positions) .'\')' )->get();

        return $players;
    }
}
