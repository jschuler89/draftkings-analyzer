<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class DKController extends Controller
{
	public function rosterUpload(Request $request)
	{
		$csv = file_get_contents($_FILES['file']['tmp_name']);
		$functions = \App::make('functions');
		$functions->rosterUpload($csv);

		return response()->json([
		    'success' => true
		]);
	}

	public function upload()
	{
		$variables = array();
		return view('upload', $variables);
	}

	public function generateLineups(Request $request)
	{
		$variables = array();
		return view('generate_lineups', $variables);
	}

	public function lineupCriteria(Request $request)
	{
		$functions = \App::make('functions');
		$return = $functions->generateLineupsByCriteria($request);

		return response()->json($return);
	}

	public function lineupShowdownCriteria(Request $request)
	{
		$functions = \App::make('functions');
		$return = $functions->generateShowdownLineupsByCriteria($request);

		return response()->json($return);
	}

	public function generateRosters(Request $request)
	{
		$functions = \App::make('functions');
		// $qb_criteria = $request->input('qb');
		// $rb_criteria = $request->input('rb');
		// $wr_criteria = $request->input('wr');
		// $te_criteria = $request->input('te');
		// $dst_criteria = $request->input('dst');
		$qbs = $functions->getPlayersByPosition('QB');
		$rbs = $functions->getPlayersByPosition('RB');
		$wrs = $functions->getPlayersByPosition('WR');
		$tes = $functions->getPlayersByPosition('TE');
		$dsts = $functions->getDSTs();

		$positions = array('QB', 'RB1', 'RB2', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
		$salary_cap = 50000;
		$salary_min = 48000;
		$simulations = 1;
		$salary_total = 0;
		$rosters = array();

		for($i = 0; $i < $simulations; $i++) {
			$copy_positions = array('QB', 'RB1', 'RB', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
			$position_count = count($positions);
			$roster_filled = 0;
			$postionFilled = array();
			$playersPicked = array();
			$roster = array();
			$index = 0;

			while(($roster_filled < $position_count) && count($copy_positions) > 0) {
				$randPosIndex = array_rand($copy_positions, 1);
				$randPos = $copy_positions[$randPosIndex];
				$index++;

				if($index >= 14) {
					$copy_positions = array('QB', 'RB1', 'RB2', 'WR1', 'WR2', 'WR3', 'TE', 'FLEX', 'DST');
					$position_count = count($positions);
					$roster_filled = 0;
					$postionFilled = array();
					$playersPicked = array();
					$roster = array();
					$index = 0;
					$salary_cap = 50000;
					$salary_total = 0;
					$fptsTotal = 0;
				}

				if(!in_array($randPos, $postionFilled)) {
					$players = array();

					switch($randPos) {
						case 'QB':
							$players = $qbs;
							break;
						case 'RB1':
						case 'RB2':
							$players = $rbs;
							break;
						case 'WR1':
						case 'WR2':
						case 'WR3':
							$players = $wrs;
							break;
						case 'TE':
							$players = $tes;
							break;
						case 'FLEX':
							$players = array_merge($rbs, $wrs, $tes);
							break;
						case 'DST':
							$players = $dsts;
							break;
						default:
							$players = array();
							break;
					}

					if(!empty($players)) {
						$randPlayerIndex = array_rand($players, 1);
						$randPlayer = $players[$randPlayerIndex];

						if(in_array($randPlayer['name'], $playersPicked) || ($salary_cap - $randPlayer['salary']) < 0) {
							continue;
						}

						unset($copy_positions[$randPosIndex]);
						$copy_positions = array_values($copy_positions);

						$postionFilled[] = $randPos;
						$playersPicked[] = $randPlayer['name'];
						$roster['players'][] = $randPlayer;
						$salary_cap -= $randPlayer['salary'];
						$salary_total += $randPlayer['salary'];
						$roster_filled++;
					}
				} else {
					continue;
				}
			}

			if($salary_total > $salary_min) {
				$rosters[] = $roster;
			}
		}

		return $rosters;
	}

	public function dataAnalysis()
	{
		$functions = \App::make('functions');
		$variables = array();

		return view('data_analysis', $variables);
	}

	public function players()
	{
		$variables = array();

		return view('players2', $variables);
	}

	public function defense()
	{
		$variables = array();

		return view('defense', $variables);
	}

	public function depth_chart()
	{
		$variables = array();

		return view('depth_chart', $variables);
	}

	public function player_type(Request $request)
	{
		$functions = \App::make('functions');
		$player_type = $request->input('type');
		$player_ids = $functions->getPlayerIds();
		$position = strtoupper($player_type);
		$return = $functions->getPlayersByPosition($position, $player_ids);

		return response()->json($return);
	}

	public function get_defense()
	{
		$functions = \App::make('functions');
		$return = $functions->getDSTs();

		return response()->json($return);
	}

	public function league_avgs()
	{
		$functions = \App::make('functions');
		$return = $functions->getLeagueAvgs();
		
		return response()->json($return);
	}

	public function getPlayer(Request $request)
	{
		$player_id = $request->input('player_id');
		$functions = \App::make('functions');

		$return = $functions->getPlayer($player_id);

		return response()->json($return);
	}

	public function getGamesDepthChart()
	{
		$functions = \App::make('functions');
		$return = $functions->getGamesDepthChart();

		return response()->json($return);
	}

	public function getGames()
	{
		$functions = \App::make('functions');
		$return = $functions->getGames();

		return response()->json($return);
	}

	public function sync()
	{
		$functions = \App::make('functions');
		$player_ids = $functions->getPlayerIds();
		$team_ids = $functions->getTeamIds();
		$variables = array();
		$variables['player_ids'] = $player_ids;
		$variables['team_ids'] = $team_ids;

		return view('sync', $variables);
	}

	public function syncPlayer(Request $request)
	{
		$player_id = $request->input('player_id');
		$functions = \App::make('functions');

		$return = $functions->syncPlayer($player_id);

		return response()->json($return);
	}

	public function syncTeam(Request $request)
	{
		$team_id = $request->input('team_id');
		$functions = \App::make('functions');

		$return = $functions->syncTeam($team_id);

		return response()->json($return);
	}

	public function syncDepthChart(Request $request)
	{
		$team_id = $request->input('team_id');
		$functions = \App::make('functions');

		$return = $functions->syncDepthChart($team_id);

		return response()->json($return);
	}
}