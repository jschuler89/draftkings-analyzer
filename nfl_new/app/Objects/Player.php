<?php

namespace App\Objects;

use App\Players;
use Illuminate\Support\Facades\DB;

class Player
{
	public $playerData;
	public $playerId;

	public function __construct($player_id = null)
	{
		if($player_id != null) {
			$this->playerData = Players::getPlayerByPlayerId($player_id);
			$this->playerId = $player_id;
			$this->exceedValues = $this->getExceedValues();
		}
	}

	public function getExceedValues()
	{
		$player_id = $this->playerId;
		$player_base = $this->playerData['salary'] / 1000;
		$player_2x = $player_base * 2;
		$player_2x_times = 0;
		$player_3x = $player_base * 3;
		$player_3x_times = 0;
		$player_4x = $player_base * 4;
		$player_4x_times = 0;
		$player_2x_pct = 0;
		$player_3x_pct = 0;
		$player_4x_pct = 0;

		$rs = DB::select("SELECT * FROM player_game_logs where player_id = $player_id");

		if($rs) {
			foreach($rs as $_gamelog) {
				$_gamelog = (array)$_gamelog;
				$fp = $this->calculateFP($_gamelog);

				if($fp >= $player_2x) {
					$player_2x_times++;
				}

				if($fp >= $player_3x) {
					$player_3x_times++;
				}

				if($fp >= $player_4x) {
					$player_4x_times++;
				}
			}

			$player_2x_pct = ($player_2x_times / count($rs) * 100);
			$player_3x_pct = ($player_3x_times / count($rs) * 100);
			$player_4x_pct = ($player_4x_times / count($rs) * 100);
		}

		return array('player_2x' => $player_2x, 'player_2x_pct' => $player_2x_pct, 'player_3x' => $player_3x, 'player_3x_pct' => $player_3x_pct, 'player_4x' => $player_4x, 'player_4x_pct' => $player_4x_pct );
	}

	/**
	 * Get player's % of pass yds
	 */
	public function getPctOfPassYds()
	{
		$player_id = $this->playerId;

		$rs = DB::select("SELECT * FROM player_game_logs where player_id = $player_id");
		$buffer = 0;
		$count = 0;
		$pass_yds = 0;
		$pct = array();

		if($rs) {
			foreach($rs as $_gamelog) {
				$_gamelog = (array)$_gamelog;
				$week_num = $_gamelog['week_num'];

				$team = DB::select("SELECT * from teams where url_param = '" . strtolower($_gamelog['team']) . "' OR team = '" . ucwords(strtolower($_gamelog['team'])) . "'");

				if(isset($team[0])) {
					$team_player_id = $team[0]->team_id;

					$result = DB::select("SELECT * FROM team_game_logs where player_id = $team_player_id and week_num = $week_num");

					if($result && $result[0]->pass_yds_off > 0) {
						$pct[] = ($_gamelog['rec_yds'] / $result[0]->pass_yds_off) * 100;
					}
				} else {
					continue;
				}
			}
		} else {
			return 0;
		}

		if(count($pct) == 0) {
			return 0;
		}

		return (array_sum($pct) / count($pct));
	}

	public function getPassYdsBuffer()
	{
		$player_id = $this->playerId;

		$rs = DB::select("SELECT * FROM player_game_logs where player_id = $player_id");
		$buffer = 0;
		$count = 0;
		$pass_yds = 0;

		if($rs) {
			foreach($rs as $_gamelog) {
				$_gamelog = (array)$_gamelog;
				$week_num = $_gamelog['week_num'];

				$team = DB::select("SELECT * from teams where url_param = '" . strtolower($_gamelog['opp']) . "'");

				if(isset($team[0])) {
					$opp_player_id = $team[0]->team_id;

					$result = DB::select("SELECT * FROM team_game_logs where player_id = $opp_player_id and week_num = $week_num");

					if($result) {
						$pass_yds += $_gamelog['pass_yds'];
						$count++;
						$pass_yds_per_game = $pass_yds / $count;
						$result = $result[0];
						$buffer += $pass_yds_per_game - $result->pass_yds_def;
					}
				} else {
					continue;
				}
			}
		} else {
			return 0;
		}

		if($buffer == 0) {
			return 0;
		}

		return ($buffer / $count);
	}

	public function getRushYdsBuffer()
	{
		$player_id = $this->playerId;

		$rs = DB::select("SELECT * FROM player_game_logs where player_id = $player_id");
		$buffer = 0;
		$count = 0;
		$rush_yds = 0;

		if($rs) {
			foreach($rs as $_gamelog) {
				$_gamelog = (array)$_gamelog;
				$week_num = $_gamelog['week_num'];

				$team = DB::select("SELECT * from teams where url_param = '" . strtolower($_gamelog['opp']) . "'");

				if(isset($team[0])) {
					$opp_player_id = $team[0]->team_id;

					$result = DB::select("SELECT * FROM team_game_logs where player_id = $opp_player_id and week_num = $week_num");

					if($result) {
						$rush_yds += $_gamelog['rush_yds'];
						$count++;
						$rush_yds_per_game = $rush_yds / $count;
						$result = $result[0];
						$buffer += $rush_yds_per_game - $result->rush_yds_def;
					}
				} else {
					continue;
				}
			}
		} else {
			return 0;
		}

		if($buffer == 0) {
			return 0;
		}

		return ($buffer / $count);
	}

	public function calculateFP($data)
	{
		$fp = 0;
		$fp += 	$data['pass_td'] * 4;
		$fp += 	$data['pass_yds'] * 0.04;
		$fp -= 	$data['pass_int'] * 1;
		$fp += 	($data['pass_yds'] >= 300) ? 3 : 0;
		$fp += 	($data['rush_td'] < 0) ? 0 : $data['rush_td'] * 6;
		$fp += 	$data['rush_yds'] * 0.1;
		$fp += 	$data['rec'] * 1;
		$fp += 	$data['rec_yds'] * 0.1;
		$fp += 	$data['rec_td'] * 6;
		$fp += 	($data['rush_yds'] >= 100) ? 3 : 0;
		$fp += 	($data['rec_yds'] >= 100) ? 3 : 0;

		return $fp;
	}
}