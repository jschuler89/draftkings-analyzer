<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerCache extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_cache';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'cache_id';
}
