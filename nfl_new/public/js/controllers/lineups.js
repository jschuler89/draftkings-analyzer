angular.module('lineupsApp.controllers', []).
controller('GenerateLineupsController', function($scope, lineupservice){
	$scope.rosters = {};
	$scope.showdownRosters = {};
	$scope.loadingLineups = false;
	$scope.generateLineups = function() {
		var criteria = {
			'QB': $scope.qb,
			'RB': $scope.rb,
			'WR': $scope.wr,
			'TE': $scope.te,
			'DST': $scope.dst
		}
		console.log(criteria);
		$scope.loadingLineups = true;
		lineupservice.lineupCriteria(criteria).then(function(resp) {
			$scope.loadingLineups = false;
			$scope.rosters = resp.data;
		});
	};
	$scope.generateShowdownLineups = function() {
		var criteria = {
			'QB': $scope.qb,
			'RB': $scope.rb,
			'WR': $scope.wr,
			'TE': $scope.te,
			'DST': $scope.dst
		}
		console.log(criteria);
		$scope.loadingLineups = true;
		lineupservice.lineupShowdownCriteria(criteria).then(function(resp) {
			$scope.loadingLineups = false;
			$scope.showdownRosters = resp.data;
		});
	};
	$scope.qb = {
		enabled: false,
		pass_att: {
			mode: '',
			value: ''
		},
		pass_cmp: {
			mode: '',
			value: ''
		},
		pass_yds: {
			mode: '',
			value: ''
		}
	};

	$scope.rb = {
		enabled: false,
		rush_att: {
			mode: '',
			value: ''
		},
		rush_yds: {
			mode: '',
			value: ''
		},
		targets: {
			mode: '',
			value: ''
		}
	};

	$scope.wr = {
		enabled: false,
		rec: {
			mode: '',
			value: ''
		},
		rec_yds: {
			mode: '',
			value: ''
		},
		targets: {
			mode: '',
			value: ''
		}
	};

	$scope.te = {
		enabled: false,
		rec: {
			mode: '',
			value: ''
		},
		rec_yds: {
			mode: '',
			value: ''
		},
		targets: {
			mode: '',
			value: ''
		}
	};

	$scope.dst = {
		enabled: false,
		sacks: {
			mode: '',
			value: ''
		},
		ints: {
			mode: '',
			value: ''
		}
	};
});