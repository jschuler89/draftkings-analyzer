angular.module('playersApp.controllers', []).
controller('PlayersController', function($scope, playerservice){
	$scope.players = {};
	$scope.leagueAverages = {};
	$scope.DSTs = [];
	$scope.DSTObj = {};
	$scope.week_num = 7;
	$scope.showQB = true;
	$scope.showRB = false;
	$scope.showWR = false;
	$scope.showTE = false;
	$scope.selectedPosition = 'QB';
	$scope.selectedPlayerName = 'None';
	$scope.selectedPlayerTeam = 'None';
	$scope.selectedPlayerPosition = 'None';
	$scope.selectedPlayerSalary = 0;
	$scope.selectedPlayerCompletions = 0;
	$scope.selectedPlayerPassAtts = 0;
	$scope.selectedPlayerPassYds = 0;
	$scope.selectedPlayerPassTds = 0;
	$scope.selectedPlayerPassInt = 0;
	$scope.selectedPlayerRushAtts = 0;
	$scope.selectedPlayerRushYds = 0;
	$scope.selectedPlayerTgts = 0;
	$scope.selectedPlayerRecYds = 0;
	$scope.selectedPlayerRecTds = 0;
	$scope.selectedPlayerImage = 'img/placeholder.png';

	$scope.sortType     = 'salary'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order

	$scope.sortBy = function(propertyName) {
	    $scope.sortReverse = ($scope.sortType === propertyName) ? !$scope.sortReverse : false;
	    $scope.sortType = propertyName;
	};

	$scope.selectPlayer = function(Player) {
		console.log(Player);
		$scope.selectedPlayerName = Player.name;
		$scope.selectedPlayerTeam = Player.team;
		$scope.selectedPlayerPosition = Player.position;
		$scope.selectedPlayerImage = Player.img;
		$scope.selectedPlayerSalary = Player.salary;
		$scope.selectedPlayerCompletions = Player.pass_cmp;
		$scope.selectedPlayerPassAtts = Player.pass_att;
		$scope.selectedPlayerPassYds = Player.pass_yds;
		$scope.selectedPlayerPassTds = Player.pass_tds;
		$scope.selectedPlayerPassInt = Player.pass_int;
		$scope.selectedPlayerRushAtts = Player.rush_att;
		$scope.selectedPlayerRushYds = Player.rush_yds;
		$scope.selectedPlayerRushTds = Player.rush_tds;
		$scope.selectedPlayerTgts = Player.targets;
		$scope.selectedPlayerTgtsSum = Player.targets_sum;
		$scope.selectedPlayerRec = Player.rec;
		$scope.selectedPlayerRecSum = Player.rec_sum;
		$scope.selectedPlayerRecYds = Player.rec_yds;
		$scope.selectedPlayerRecTds = Player.rec_tds;
		$scope.selectedPlayerGameLogs = Player.game_logs;

	}

	$scope.compareFPPGQB = function(v1, v2) {
		if($scope.sortType == 'fppg') {
			return (parseFloat(v1.value) < parseFloat(v2.value)) ? -1 : 1;
		}

		if($scope.sortType == 'fp_pt') {
			var player = $scope.players['QB'][v1.index];
			var player2 = $scope.players['QB'][v2.index];
			var fp_pt1 = parseFloat(player.fppg / (player.salary / 1000));
			var fp_pt2 = parseFloat(player2.fppg / (player2.salary / 1000));

			return (fp_pt1 < fp_pt2) ? -1 : 1;
		}

		if($scope.sortType == 'def_score') {
			var player = $scope.players['QB'][v1.index];
			var player2 = $scope.players['QB'][v2.index];
			var dst1 = $scope.DSTs[v1.index];
			var dst2 = $scope.DSTs[v2.index];

			return (parseFloat($scope.DSTObj[player.opp.toUpperCase()].pass_yds_def_score) < parseFloat($scope.DSTObj[player2.opp.toUpperCase()].pass_yds_def_score)) ? -1 : 1;
		}

		return v1.value < v2.value ? -1 : 1;
	}

	$scope.compareFPPGRB = function(v1, v2) {
		if($scope.sortType == 'fppg') {
			return (parseFloat(v1.value) < parseFloat(v2.value)) ? -1 : 1;
		}

		if($scope.sortType == 'fp_pt') {
			var player = $scope.players['RB'][v1.index];
			var player2 = $scope.players['RB'][v2.index];
			var fp_pt1 = parseFloat(player.fppg / (player.salary / 1000));
			var fp_pt2 = parseFloat(player2.fppg / (player2.salary / 1000));

			return (fp_pt1 < fp_pt2) ? -1 : 1;
		}

		if($scope.sortType == 'def_score') {
			var player = $scope.players['RB'][v1.index];
			var player2 = $scope.players['RB'][v2.index];
			var dst1 = $scope.DSTs[v1.index];
			var dst2 = $scope.DSTs[v2.index];

			return (parseFloat($scope.DSTObj[player.opp.toUpperCase()].rush_yds_def_score) < parseFloat($scope.DSTObj[player2.opp.toUpperCase()].rush_yds_def_score)) ? -1 : 1;
		}

		return v1.value < v2.value ? -1 : 1;
	}

	$scope.compareFPPGWR = function(v1, v2) {
		if($scope.sortType == 'fppg') {
			return (parseFloat(v1.value) < parseFloat(v2.value)) ? -1 : 1;
		}

		if($scope.sortType == 'fp_pt') {
			var player = $scope.players['WR'][v1.index];
			var player2 = $scope.players['WR'][v2.index];
			var fp_pt1 = parseFloat(player.fppg / (player.salary / 1000));
			var fp_pt2 = parseFloat(player2.fppg / (player2.salary / 1000));

			return (fp_pt1 < fp_pt2) ? -1 : 1;
		}

		if($scope.sortType == 'def_score') {
			var player = $scope.players['WR'][v1.index];
			var player2 = $scope.players['WR'][v2.index];
			var dst1 = $scope.DSTs[v1.index];
			var dst2 = $scope.DSTs[v2.index];

			return (parseFloat($scope.DSTObj[player.opp.toUpperCase()].pass_yds_def_score) < parseFloat($scope.DSTObj[player2.opp.toUpperCase()].pass_yds_def_score)) ? -1 : 1;
		}

		return v1.value < v2.value ? -1 : 1;
	}

	$scope.compareFPPGTE = function(v1, v2) {
		if($scope.sortType == 'fppg') {
			return (parseFloat(v1.value) < parseFloat(v2.value)) ? -1 : 1;
		}

		if($scope.sortType == 'fp_pt') {
			var player = $scope.players['TE'][v1.index];
			var player2 = $scope.players['TE'][v2.index];
			var fp_pt1 = parseFloat(player.fppg / (player.salary / 1000));
			var fp_pt2 = parseFloat(player2.fppg / (player2.salary / 1000));

			return (fp_pt1 < fp_pt2) ? -1 : 1;
		}

		if($scope.sortType == 'def_score') {
			var player = $scope.players['TE'][v1.index];
			var player2 = $scope.players['TE'][v2.index];
			var dst1 = $scope.DSTs[v1.index];
			var dst2 = $scope.DSTs[v2.index];

			return (parseFloat($scope.DSTObj[player.opp.toUpperCase()].pass_yds_def_score) < parseFloat($scope.DSTObj[player2.opp.toUpperCase()].pass_yds_def_score)) ? -1 : 1;
		}

		return v1.value < v2.value ? -1 : 1;
	}

	playerservice.getDefense().then(function(response){
		$scope.DSTObj = angular.copy(response.data);
		var result = Object.keys(response.data).map(function(key) {
		  return response.data[key];
		});
		console.log(result);
		$scope.DSTs = result;
	});

	playerservice.leagueAverages().then(function(response) {
		$scope.leagueAverages = response.data;
		console.log($scope.leagueAverages);
	});

	playerservice.getPlayersByPosition('QB').then(function(response) {
		var result = Object.keys(response.data).map(function(key) {
		  return response.data[key];
		});
		console.log(result);
		$scope.players['QB'] = result;
	});

	playerservice.getPlayersByPosition('RB').then(function(response) {
		var result = Object.keys(response.data).map(function(key) {
		  return response.data[key];
		});
		console.log(result);
		$scope.players['RB'] = result;
	});

	playerservice.getPlayersByPosition('WR').then(function(response) {
		var result = Object.keys(response.data).map(function(key) {
		  return response.data[key];
		});
		console.log(result);
		$scope.players['WR'] = result;
	});

	playerservice.getPlayersByPosition('TE').then(function(response) {
		var result = Object.keys(response.data).map(function(key) {
		  return response.data[key];
		});
		console.log(result);
		$scope.players['TE'] = result;
	});

	$scope.showPosition = function(positionToShow) {
		$scope.showQB = false;
		$scope.showRB = false;
		$scope.showWR = false;
		$scope.showTE = false;
		$scope.selectedPosition = positionToShow;

		switch(positionToShow) {
			case 'QB':
				$scope.showQB = true;
				break;
			case 'RB':
				$scope.showRB = true;
				break;
			case 'WR':
				$scope.showWR = true;
				break;
			case 'TE':
				$scope.showTE = true;
				break;
			default:
				break;
		}
	}

})
.controller('DepthChartController', function($scope, playerservice){
	$scope.teamDepthCharts = [];
	$scope.games = [];
	$scope.off_team_id = 3;
	$scope.def_team_id = 6;
	$scope.game = {selected: ''};

	playerservice.getThisWeekGamesDepthChart().then(function(response) {
		$scope.teamDepthCharts = angular.copy(response.data);
		console.log($scope.teamDepthCharts);
	});
	playerservice.getThisWeekGames().then(function(response) {
		$scope.games = angular.copy(response.data);
		var key = $scope.games[0].key;
		$scope.game.selected = key;
		var gamesArray = key.split(':');
		$scope.off_team_id = gamesArray[0];
		$scope.def_team_id = gamesArray[1];
		console.log($scope.games);
	});

	$scope.changeGame = function(game) {
		var gamesArray = game.selected.split(':');
		$scope.off_team_id = gamesArray[0];
		$scope.def_team_id = gamesArray[1];
	}
})
.controller('DefenseController', function($scope, playerservice){
	$scope.DSTs = [];
	$scope.DSTObj = {};
	$scope.week_num = 5;
	$scope.sortType     = 'salary'; // set the default sort type
	$scope.sortReverse  = true;  // set the default sort order

	$scope.sortBy = function(propertyName) {
	    $scope.sortReverse = ($scope.sortType === propertyName) ? !$scope.sortReverse : false;
	    $scope.sortType = propertyName;
	};

	$scope.compareFPPG = function(v1, v2) {
		if($scope.sortType == 'fppg') {
			return (parseFloat(v1.value) < parseFloat(v2.value)) ? -1 : 1;
		}

		if($scope.sortType == 'pass_yds_off_score') {
			var dst1 = $scope.DSTs[v1.index];
			var dst2 = $scope.DSTs[v2.index];
			return (parseFloat($scope.DSTObj[dst1.opp].pass_yds_off_score) < parseFloat($scope.DSTObj[dst2.opp].pass_yds_off_score)) ? -1 : 1;
		}

		if($scope.sortType == 'rush_yds_off_score') {
			var dst1 = $scope.DSTs[v1.index];
			var dst2 = $scope.DSTs[v2.index];
			return (parseFloat($scope.DSTObj[dst1.opp].rush_yds_off_score) < parseFloat($scope.DSTObj[dst2.opp].rush_yds_off_score)) ? -1 : 1;
		}

		if($scope.sortType == 'score_diff') {
			var dst1 = $scope.DSTs[v1.index];
			var dst2 = $scope.DSTs[v2.index];
			var diff1 = parseFloat(dst1.def_score - $scope.DSTObj[dst1.opp].off_score);
			var diff2 = parseFloat(dst2.def_score - $scope.DSTObj[dst2.opp].off_score);
			return (diff1 < diff2) ? -1 : 1;
		}


		return (parseFloat(v1.value) < parseFloat(v2.value)) ? -1 : 1;
	};

	playerservice.getDefense().then(function(response){
		$scope.DSTObj = angular.copy(response.data);
		var result = Object.keys(response.data).map(function(key) {
		  return response.data[key];
		});
		console.log(result);
		$scope.DSTs = result;
	});
});
