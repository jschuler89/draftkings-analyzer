$(document).ready(function() {
    $('table').DataTable( {
        "scrollX": true,
        "pageLength": 50,
        "order": [[ 4, 'desc' ]]
    } );
    $('.btn').click(function() {
    	var id = $(this).attr('data-table');
    	$('.btn').removeClass('active');
    	$(this).addClass('active');
    	$('.card-content').removeClass('active')
    	$('#'+id).addClass('active')
    });
    $(document).on('click', '.card-content tbody tr', function() {
        $('.card-content tbody tr').removeClass('active');
        $(this).addClass('active');
        var player_id = $(this).attr('data-player-id');
        $.ajax({
            url: 'get_player?player_id=' + player_id,
            method: 'GET',
            dataType: 'json',
            success: function(resp) {
                var tr = $('<tr>');
                var classPosition = resp.position;
                var modalClass = '#playerModal' + '.' + classPosition;
                $(modalClass + ' img').attr('src', resp.img);
                $(modalClass + ' .player-name').text(resp.name);
                var tbody = $(modalClass + ' tbody');
                tbody.html('');

                if(resp.position == 'QB') {
                    for(var i = 0; i < resp.game_logs.length; i++) {
                        var td = $('<td>');
                        td.text(resp.game_logs[i].week_num);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].opp);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].game_result);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].pass_cmp);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].pass_att);
                        tr.append(td);
                        var td = $('<td>');
                        td.text( parseFloat((resp.game_logs[i].pass_cmp / resp.game_logs[i].pass_att) * 100).toFixed(2) + '%' );
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].pass_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].pass_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].pass_int);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_att);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].fp);
                        tr.append(td);
                        tbody.prepend(tr);
                        tr = $('<tr>');
                    }
                }

                if(resp.position == 'RB') {
                    for(var i = 0; i < resp.game_logs.length; i++) {
                        var td = $('<td>');
                        td.text(resp.game_logs[i].week_num);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].opp);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].game_result);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_att);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(parseFloat(resp.game_logs[i].rush_yds / resp.game_logs[i].rush_att).toFixed(2));
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].targets);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].fp);
                        tr.append(td);
                        tbody.prepend(tr);
                        tr = $('<tr>');
                    }
                }

                if(resp.position == 'WR') {
                    for(var i = 0; i < resp.game_logs.length; i++) {
                        var td = $('<td>');
                        td.text(resp.game_logs[i].week_num);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].opp);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].game_result);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].targets);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(parseFloat( (resp.game_logs[i].rec / resp.game_logs[i].targets ) * 100 ).toFixed(0) + '%');
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_att);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].fp);
                        tr.append(td);
                        tbody.prepend(tr);
                        tr = $('<tr>');
                    }
                }

                if(resp.position == 'TE') {
                    for(var i = 0; i < resp.game_logs.length; i++) {
                        var td = $('<td>');
                        td.text(resp.game_logs[i].week_num);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].opp);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].game_result);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].targets);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(parseFloat( (resp.game_logs[i].rec / resp.game_logs[i].targets ) * 100 ).toFixed(0) + '%');
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rec_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_att);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_yds);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].rush_td);
                        tr.append(td);
                        var td = $('<td>');
                        td.text(resp.game_logs[i].fp);
                        tr.append(td);
                        tbody.prepend(tr);
                        tr = $('<tr>');
                    }
                }
                $(modalClass).modal('show');
            }
        })
        console.log(player_id);
    });
} );