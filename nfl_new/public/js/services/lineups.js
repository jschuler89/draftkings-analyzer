angular.module('lineupsApp.services', []).
  factory('lineupservice', function($http) {

  	var lineup = {};

  	lineup.lineupCriteria = function(criteria) {
      return $http({
            method: 'POST',
            data: {criteria},
            url: 'http://dk.local/nfl_new/public/lineup_criteria'
        });
    };

    lineup.lineupShowdownCriteria = function(criteria) {
      return $http({
            method: 'POST',
            data: {criteria},
            url: 'http://dk.local/nfl_new/public/lineup_showdown_criteria'
        });
    };

  	return lineup;
});