angular.module('playersApp.services', []).
  factory('playerservice', function($http) {

  	var player = {};

  	player.getPlayersByPosition = function(position) {
      return $http({
            method: 'GET',
            url: 'http://dk.local/nfl_new/public/player_type?type='+position
        });
    };

    player.leagueAverages = function() {
    	return $http({
    		method: 'GET',
    		url: 'http://dk.local/nfl_new/public/leagueavgs'
    	})
    };

    player.getDefense = function() {
    	return $http({
    		method: 'GET',
    		url: 'http://dk.local/nfl_new/public/get_defense'
    	})
    };

    player.getThisWeekGamesDepthChart = function() {
      return $http({
        method: 'GET',
        url: 'http://dk.local/nfl_new/public/get_games_depth_chart'
      })
    };

    player.getThisWeekGames = function() {
      return $http({
        method: 'GET',
        url: 'http://dk.local/nfl_new/public/get_games'
      })
    };

  	return player;
});