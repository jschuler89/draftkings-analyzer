$(document).ready(function() {
    $('table').DataTable( {
        "scrollX": true,
        "pageLength": 50
    } );
    $('.btn').click(function() {
    	var id = $(this).attr('data-table');
    	$('.btn').removeClass('active');
    	$(this).addClass('active');
    	$('.player-table').removeClass('active')
        $('#'+id).addClass('active')
    	$('.'+id).addClass('active')
    })
    $('.players-table tbody tr').click(function() {
        if($(this).hasClass('active')) {
            $('.players-table tbody tr').removeClass('active');
            return false;
        }
        var player_id = $(this).attr('data-player-id');

        $.ajax({
            url: 'get_player?player_id=' + player_id,
            method: 'GET',
            dataType: 'json',
            success: function(resp) {
                $('.qbs img').attr('src', resp.img);
                $('.qbs .name').text(resp.name);
                $('.qbs .pass_yds').text(parseFloat(resp.pass_yds).toFixed(2));
                $('.qbs .pass_att').text(parseFloat(resp.pass_att).toFixed(2));
                $('.qbs .pass_cmp').text(parseFloat(resp.pass_cmp).toFixed(2));
                $('.qbs .pass_cmp_perc').text(parseFloat(resp.pass_cmp / resp.pass_att * 100).toFixed(2) + '%');
                $('.qbs .target1_name').text(resp.tgts[0].name);
                $('.qbs .target1_tgts').text(resp.tgts[0].targets);
                $('.qbs .target2_name').text(resp.tgts[1].name);
                $('.qbs .target2_tgts').text(resp.tgts[1].targets);
                $('.qbs .target3_name').text(resp.tgts[2].name);
                $('.qbs .target3_tgts').text(resp.tgts[2].targets);
            }
        })
        $('.players-table tbody tr').removeClass('active');
        $(this).addClass('active');
    });
} );