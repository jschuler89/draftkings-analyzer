angular.module('playersApp', [
    'playersApp.controllers',
    'playersApp.services'
], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});

angular.module('lineupsApp', [
    'lineupsApp.controllers',
    'lineupsApp.services'
], function($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});