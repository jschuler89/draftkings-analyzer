@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer')
@section('height', 'full-height')
@include('layouts.head')
@section('css')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/dropzone.css') }}">
@endsection
@section('scripts')
        <script src="{{ asset('js/dropzone.js') }}"></script>
        <script src="{{ asset('js/main.js') }}"></script>
@endsection
@section('height', 'full-height')
@section('content')
    @include('layouts.sidebar')
    <form action="roster_upload" id="dz-container" class="panel panel-info dropzone" method="post" enctype="multipart/form-data" style="margin: 18px;width: 640px;margin: 0 auto;margin-top: 60px;">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
        <div class="dz-message needsclick">
            Drop files here or click to upload.<br>
          </div>
    </form>
@endsection
@include('layouts.corejs')