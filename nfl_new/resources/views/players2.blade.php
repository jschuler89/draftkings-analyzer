<!doctype html>
<html lang="en">
@include('layouts.head')
<body>
    <div class="">
        @include('layouts.sidebar')
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> Players </a>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12 player-table" ng-app="playersApp" ng-controller="PlayersController">
                            <div>
                                <button type="button" ng-class="{active: showQB}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('QB')">QB</button>
                                <button type="button" ng-class="{active: showRB}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('RB')">RB</button>
                                <button type="button" ng-class="{active: showWR}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('WR')">WR</button>
                                <button type="button" ng-class="{active: showTE}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('TE')">TE</button>
                            </div>
                            <div class="card">
                                <div class="card-header" data-background-color="blue">
                                    <h4 class="title"><% selectedPosition %></h4>
                                    <p class="category">Week <% week_num %></p>
                                </div>
                                <div class="card-content table-responsive" ng-show="showQB">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('spread')">Spread <span ng-show="sortType == 'spread'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('over_under')">O/U <span ng-show="sortType == 'over_under'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="Player in players.QB | orderBy:sortType:sortReverse:compareFPPG">
                                                <td><% Player.name %></td>
                                                <td><% Player.team %></td>
                                                <td><% Player.salary %></td>
                                                <td><% Player.fppg %></td>
                                                <td><% Player.spread %></td>
                                                <td><% Player.over_under %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-content table-responsive" ng-show="showRB">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('spread')">Spread <span ng-show="sortType == 'spread'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('over_under')">O/U <span ng-show="sortType == 'over_under'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="Player in players.RB | orderBy:sortType:sortReverse:compareFPPG">
                                                <td><% Player.name %></td>
                                                <td><% Player.team %></td>
                                                <td><% Player.salary %></td>
                                                <td><% Player.fppg %></td>
                                                <td><% Player.spread %></td>
                                                <td><% Player.over_under %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-content table-responsive" ng-show="showWR">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('spread')">Spread <span ng-show="sortType == 'spread'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('over_under')">O/U <span ng-show="sortType == 'over_under'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="Player in players.WR | orderBy:sortType:sortReverse:compareFPPG">
                                                <td><% Player.name %></td>
                                                <td><% Player.team %></td>
                                                <td><% Player.salary %></td>
                                                <td><% Player.fppg %></td>
                                                <td><% Player.spread %></td>
                                                <td><% Player.over_under %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="card-content table-responsive" ng-show="showTE">
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('spread')">Spread <span ng-show="sortType == 'spread'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                            <th ng-click="sortBy('over_under')">O/U <span ng-show="sortType == 'over_under'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <tbody>
                                            <tr ng-repeat="Player in players.TE | orderBy:sortType:sortReverse:compareFPPG">
                                                <td><% Player.name %></td>
                                                <td><% Player.team %></td>
                                                <td><% Player.salary %></td>
                                                <td><% Player.fppg %></td>
                                                <td><% Player.spread %></td>
                                                <td><% Player.over_under %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
@include('layouts.corejs')

</html>