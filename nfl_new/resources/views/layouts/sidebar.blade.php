<div class="sidebar" data-color="blue" data-image="../assets/img/sidebar-1.jpg">
    <div class="logo">
        <a href="/nfl_new/public/players" class="simple-text">
            Draftkings Analyzer
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="{{ Request::is('players') ? 'active' : ''}}">
                <a href="players">
                    <i class="material-icons">person</i>
                    <p>Players</p>
                </a>
            </li>
            <li class="{{ Request::is('defense') ? 'active' : ''}}">
                <a href="defense">
                    <i class="material-icons">security</i>
                    <p>Defense</p>
                </a>
            </li>
            <li class="{{ Request::is('depth_chart') ? 'active' : ''}}">
                <a href="depth_chart">
                    <i class="material-icons">list</i>
                    <p>Depth Chart</p>
                </a>
            </li>
            <li class="{{ Request::is('data_analysis') ? 'active' : ''}}">
                <a href="data_analysis">
                    <i class="material-icons">show_chart</i>
                    <p>Data Analysis</p>
                </a>
            </li>
            <li class="{{ Request::is('build_lineup') ? 'active' : ''}}">
                <a href="generate_lineups">
                    <i class="material-icons">build</i>
                    <p>Build Lineup</p>
                </a>
            </li>
            <li class="{{ Request::is('generate_lineups') ? 'active' : ''}}">
                <a href="generate_lineups">
                    <i class="material-icons">subject</i>
                    <p>Generate Lineups</p>
                </a>
            </li>
            <li class="{{ Request::is('sync') ? 'active' : ''}}">
                <a href="sync">
                    <i class="material-icons">cached</i>
                    <p>Sync</p>
                </a>
            </li>
            <li class="{{ Request::is('upload') ? 'active' : ''}}">
                <a href="upload">
                    <i class="material-icons">cloud_upload</i>
                    <p>Upload</p>
                </a>
            </li>
        </ul>
    </div>
</div>