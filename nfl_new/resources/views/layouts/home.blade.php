<html>
    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png" />
        <link rel="icon" type="image/png" href="../assets/img/favicon.png" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>Draftkings Analyzer</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <!-- Bootstrap core CSS     -->
        <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
        <!--  Material Dashboard CSS    -->
        <link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet" />
        <!--  CSS for Demo Purpose, don't include it in your project     -->
        <link href="{{ asset('css/demo.css') }}" rel="stylesheet" />
        <!--     Fonts and icons     -->
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
        <script src="{{ asset('bower_components/angular/angular.min.js') }}"></script>
        <script src="{{ asset('js/angular-app.js') }}"></script>
        <script src="{{ asset('js/services/players.js') }}"></script>
        <script src="{{ asset('js/controllers/players.js') }}"></script>
        <link href="{{ asset('css/styles.css') }}" rel="stylesheet" />
        @yield('css')
        @yield('scripts')
    </head>
    <body>
        <div class="flex-center position-ref @yield('height')">
            <div class="content">
                @yield('header_title')
                @yield('content')
            </div>
        </div>
    </body>
</html>