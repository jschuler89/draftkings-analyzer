@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer - Data Analysis')
@section('css')
    <link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- <link href="{{ asset('css/dynatable.css') }}" rel="stylesheet"> -->
@endsection
@section('scripts')
        <!-- <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script> -->
        <!-- <script src="{{ asset('js/data-analysis.js') }}"></script> -->
        <!-- <script src="{{ asset('js/dynatable.js') }}"></script> -->
@endsection
@section('header_title')
<div class="title m-b-md">
    Draftkings Roster Analyzer
</div>
@endsection
@section('content')
    @include('layouts.sidebar')
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> Teams </a>
                </div>
            </div>
        </nav>
        <div class="content" ng-app="playersApp" ng-controller="DefenseController">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 player-table">
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="title"><% selectedPosition %></h4>
                                <p class="category">Week <% week_num %></p>
                            </div>
                            <div class="card-content table-responsive">
                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('opp')">Opp <span ng-show="sortType == 'opp'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('pass_yds_def')">Pass Yds Allowed <span ng-show="sortType == 'pass_yds_def'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('rush_yds_def')">Rush Yds Allowed <span ng-show="sortType == 'rush_yds_def'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('pts_def')">Points Allowed <span ng-show="sortType == 'pts_def'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('pass_yds_off_score')">Opp Pass Yds Score <span ng-show="sortType == 'pass_yds_off_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('rush_yds_off_score')">Opp Rush Yds Score <span ng-show="sortType == 'rush_yds_off_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('off_score')">Opp Off Score <span ng-show="sortType == 'off_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('pass_yds_def_score')">Pass Def Score <span ng-show="sortType == 'pass_yds_def_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('rush_yds_def_score')">Rush Def Score <span ng-show="sortType == 'rush_yds_def_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('def_score')">Def Score <span ng-show="sortType == 'def_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('score_diff')">Score Diff <span ng-show="sortType == 'score_diff'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="def in DSTs | orderBy:sortType:sortReverse:compareFPPG">
                                            <td><% def.team %></td>
                                            <td><% def.opp %></td>
                                            <td><% def.salary | currency:"$":0 %></td>
                                            <td><% def.pass_yds_def | number:2 %></td>
                                            <td><% def.rush_yds_def | number:2 %></td>
                                            <td><% def.pts_def | number:2 %></td>
                                            <td><% DSTObj[def.opp].pass_yds_off_score | number:2 %></td>
                                            <td><% DSTObj[def.opp].rush_yds_off_score | number:2 %></td>
                                            <td><% DSTObj[def.opp].off_score | number:2 %></td>
                                            <td><% def.pass_yds_def_score | number:2 %></td>
                                            <td><% def.rush_yds_def_score | number:2 %></td>
                                            <td><% def.def_score | number:2 %></td>
                                            <td ng-class="{'lime-green-bkg': def.def_score - DSTObj[def.opp].off_score <= 8, 'light-green-bkg': def.def_score - DSTObj[def.opp].off_score > 8, 'red-bkg': def.def_score - DSTObj[def.opp].off_score < 0, 'dark-red-bkg': def.def_score - DSTObj[def.opp].off_score <= -15, 'green-bkg': def.def_score - DSTObj[def.opp].off_score > 15 && def.fppg >= 10 }"><% def.def_score - DSTObj[def.opp].off_score | number:2 %></td>
                                            <td><% def.fppg | number:2 %></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-5" style="margin-top: 74px;position: fixed;right: 10px;">
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="title"><% selectedPlayerName %> - <% selectedPlayerTeam %></h4>
                                <p class="category"><% selectedPlayerPosition %></p>
                            </div>
                            <div class="card-content" ng-show="showQB">
                                <div class="inline-block">
                                    <div>
                                        <span class="stat-label">Pass Cmp</span>
                                        <span><% selectedPlayerCompletions | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerCompletions/leagueAverages.pass_cmp_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages.pass_cmp_avg < selectedPlayerCompletions && (((selectedPlayerCompletions/leagueAverages.pass_cmp_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages.pass_cmp_avg > selectedPlayerCompletions && selectedPlayerCompletions > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Pass Atts</span>
                                        <span><% selectedPlayerPassAtts | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerPassAtts/leagueAverages.pass_att_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages.pass_att_avg < selectedPlayerPassAtts && (((selectedPlayerPassAtts/leagueAverages.pass_att_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages.pass_att_avg > selectedPlayerPassAtts && selectedPlayerPassAtts > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Cmp %</span>
                                        <span><% (selectedPlayerCompletions / selectedPlayerPassAtts) * 100 | number:2 %>%</span>
                                    </div>
                                </div>
                                <div class="inline-block ml10">
                                    <div>
                                        <span class="stat-label">Pass Yds</span>
                                        <span><% selectedPlayerPassYds | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerPassYds/leagueAverages.pass_yds_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages.pass_yds_avg < selectedPlayerPassYds && (((selectedPlayerPassYds/leagueAverages.pass_yds_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages.pass_yds_avg > selectedPlayerPassYds && selectedPlayerPassYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Pass TDs</span>
                                        <span><% selectedPlayerPassTds | number:2 %></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Ints</span>
                                        <span><% selectedPlayerPassInt | number:2 %></span>
                                    </div>
                                </div>
                                <div class="mt10">
                                    <table class="game-log-table" ng-show="selectedPlayerGameLogs.length > 0">
                                        <thead>
                                            <th>Week #</th>
                                            <th>Opp</th>
                                            <th>Result</th>
                                            <th>Pass Cmp</th>
                                            <th>Pass Atts</th>
                                            <th>Pass Yds</th>
                                            <th>Pass TDs</th>
                                            <th>FPs</th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="game in selectedPlayerGameLogs">
                                                <td class="text-center"><% game.week_num %></td>
                                                <td class="text-center"><% game.opp %></td>
                                                <td class="text-center"><% game.game_result %></td>
                                                <td class="text-center"><% game.pass_cmp %></td>
                                                <td class="text-center"><% game.pass_att %></td>
                                                <td class="text-center"><% game.pass_yds %></td>
                                                <td class="text-center"><% game.pass_td %></td>
                                                <td class="text-center"><% game.fp %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-content" ng-show="showRB">
                                <div class="inline-block">
                                    <div>
                                        <span class="stat-label">Rush Atts</span>
                                        <span><% selectedPlayerRushAtts | number:2 %></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts.corejs')