@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer - Sync')
@section('height', 'full-height')
@include('layouts.head')
@section('content')
    @include('layouts.sidebar')
    	<div class="sync-container main-panel">
			<div class="sync-teams">
				<h3>Sync Teams</h3>
				<span style="margin-left:10px;"><button class="sync_teams">Sync</button><img src="{{ asset('img/ajax-loader.gif') }}" ></span></span>
				<div class="progress">
				  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    0%
				  </div>
				</div>
			</div>
			<div class="sync-players">
				<h3>Sync Players</h3> 
				<span style="margin-left:10px;"><button class="sync">Sync</button><span><img src="{{ asset('img/ajax-loader.gif') }}" ></span></span>
				<div class="progress">
				  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    0%
				  </div>
				</div>
			</div>
			<div class="sync-depth">
				<h3>Sync Depth Chart</h3> 
				<span style="margin-left:10px;"><button class="sync">Sync</button><span><img src="{{ asset('img/ajax-loader.gif') }}" ></span></span>
				<div class="progress">
				  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    0%
				  </div>
				</div>
			</div>
			<div class="sync-player-cache" style="margin-top:40px;">
				<h3>Sync Player Cache</h3> 
				<span style="margin-left:10px;"><button class="sync">Sync</button><span><img src="{{ asset('img/ajax-loader.gif') }}" ></span></span>
				<div class="progress">
				  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    0%
				  </div>
				</div>
			</div>
			<div class="sync-vegas" style="margin-top:40px;">
				<h3>Sync Vegas</h3> 
				<span style="margin-left:10px;"><button class="sync">Sync</button><span><img src="{{ asset('img/ajax-loader.gif') }}" ></span></span>
				<div class="progress">
				  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
				    0%
				  </div>
				</div>
			</div>
		</div>
		<style type="text/css">
			.sync, 
			.sync_teams {
				margin-bottom: 10px;
			}
			.sync-container img {
				display: none;
			}

			.sync-container img.active {
				display: block;
			}
			.sync-container {
			    padding: 50px;
			}
		</style>
		<script type="text/javascript">
			var $sync_players = jQuery('.sync-players .progress-bar');
			var $sync_player_cache = jQuery('.sync-player-cache .progress-bar');
			var $sync_depth = jQuery('.sync-depth .progress-bar');
			var $sync_teams = jQuery('.sync-teams .progress-bar');
			var players = [{{ implode(',', $player_ids) }}];
			var teams = [{{ implode(',', $team_ids) }}];
			var count = 0;
			var teamCount = 0;
			var idx = 0;
			var teamIdx = 0;
			var teamDepthIdx = 0;
			var intervalId;
			var isWaiting = false;
			var isWaitingTeam = false;
			var isWaitingTeamDepth = false;
			var syncPlayersRunning = false;
			var syncPlayersDepth = false;
			var syncPlayerCacheRunning = false;
			var syncVegasRunning = false;
			var syncTeamsRunning = false;

			jQuery(document).ready(function() {
				jQuery(document).on('click', '.sync-players button', function() {
					jQuery(this).parent().find('img').addClass('active');
					syncPlayersRunning = true;
					intervalId = setInterval(syncPlayer, 6000);
				});

				jQuery(document).on('click', '.sync-teams button', function() {
					jQuery(this).parent().find('img').addClass('active');
					syncTeamsRunning = true;
					intervalId = setInterval(syncTeam, 6000);
				});

				jQuery(document).on('click', '.sync-player-cache button', function() {
					jQuery(this).parent().find('img').addClass('active');
					syncPlayerCacheRunning = true;
					intervalId = setInterval(syncPlayerCache, 6000);
				});

				jQuery(document).on('click', '.sync-vegas button', function() {
					jQuery(this).parent().find('img').addClass('active');
					syncVegasRunning = true;
					intervalId = setInterval(syncVegas, 6000);
				});

				jQuery(document).on('click', '.sync-depth button', function() {
					jQuery(this).parent().find('img').addClass('active');
					syncPlayersDepth = true;
					intervalId = setInterval(syncPlayerDepth, 3000);
				});

				jQuery(document).on('click', '.sync-both button', function() {
					if(!syncPlayersRunning) {
						jQuery('.sync-players button').click();
					} else {
						alert('Players sync already running');
					}

					if(!syncTeamsRunning) {
						jQuery('.sync-teams button').click();
					} else {
						alert('Teams sync already running');
					}
				});
			});

			function syncPlayer() {
				if(count >= 100) {
					jQuery('.sync-players img').removeClass('active');
					clearInterval(intervalId);
					syncPlayersRunning = false;
				}	

				if(!isWaiting) {
					isWaiting = true;
					count = Math.round( (idx / players.length) * 100 );
					$sync_players.attr('aria-valuenow', count);
					$sync_players.css({width: count + '%'});
					$sync_players.text(count + '%');
					jQuery.ajax({
					  url: 'sync_player?player_id='+players[idx],
					  type: 'GET',
					  success: function(data, textStatus, xhr) {
						idx++;
					  	isWaiting = false;
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					    idx++;
					  	isWaiting = false;
					  }
					});
				}
			}

			function syncPlayerCache() {
				if(count >= 100) {
					jQuery('.sync-player-cache img').removeClass('active');
					clearInterval(intervalId);
					syncPlayersRunning = false;
				}	

				if(!isWaiting) {
					isWaiting = true;
					count = Math.round( (idx / players.length) * 100 );
					$sync_player_cache.attr('aria-valuenow', count);
					$sync_player_cache.css({width: count + '%'});
					$sync_player_cache.text(count + '%');
					jQuery.ajax({
					  url: 'get_player?player_id='+players[idx],
					  type: 'GET',
					  success: function(data, textStatus, xhr) {
						idx++;
					  	isWaiting = false;
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					    idx++;
					  	isWaiting = false;
					  }
					});
				}
			}

			function syncVegas() {
				if(count >= 100) {
					jQuery('.sync-vegas img').removeClass('active');
					clearInterval(intervalId);
					syncVegasRunning = false;
				}	

				if(!isWaiting) {
					isWaiting = true;
					count = Math.round( (idx / players.length) * 100 );
					$sync_player_cache.attr('aria-valuenow', count);
					$sync_player_cache.css({width: count + '%'});
					$sync_player_cache.text(count + '%');
					jQuery.ajax({
					  url: 'get_player?player_id='+players[idx],
					  type: 'GET',
					  success: function(data, textStatus, xhr) {
						idx++;
					  	isWaiting = false;
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					    idx++;
					  	isWaiting = false;
					  }
					});
				}
			}

			function syncPlayerDepth() {
				if(count >= 100) {
					jQuery('.sync-depth img').removeClass('active');
					clearInterval(intervalId);
					syncPlayersDepth = false;
				}	

				if(!isWaitingTeamDepth) {
					isWaitingTeamDepth = true;
					count = Math.round( (teamDepthIdx / teams.length) * 100 );
					$sync_depth.attr('aria-valuenow', count);
					$sync_depth.css({width: count + '%'});
					$sync_depth.text(count + '%');
					jQuery.ajax({
					  url: 'sync_depth_chart?team_id='+teams[teamDepthIdx],
					  type: 'GET',
					  success: function(data, textStatus, xhr) {
						teamDepthIdx++;
					  	isWaitingTeamDepth = false;
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					    teamDepthIdx++;
					  	isWaitingTeamDepth = false;
					  }
					});
				}
			}

			function syncTeam() {
				if(teamCount >= 100) {
					jQuery('.sync-teams img').removeClass('active');
					syncTeamsRunning = false;
					clearInterval(intervalId);
				}	

				if(!isWaitingTeam) {
					isWaitingTeam = true;
					teamCount = Math.round( (teamIdx / teams.length) * 100 );
					$sync_teams.attr('aria-valuenow', teamCount);
					$sync_teams.css({width: teamCount + '%'});
					$sync_teams.text(teamCount + '%');
					jQuery.ajax({
					  url: 'sync_team?team_id='+teams[teamIdx],
					  type: 'GET',
					  success: function(data, textStatus, xhr) {
						teamIdx++;
					  	isWaitingTeam = false;
					  },
					  error: function(xhr, textStatus, errorThrown) {
					    //called when there is an error
					  }
					});
				}
			}
		</script>
@endsection
@include('layouts.corejs')