@extends('layouts.home')
@include('layouts.head')
@section('title', 'Draftkings Roster Analyzer')
@section('content')
    @include('layouts.sidebar')
    <div class="criteria-container main-panel" ng-app="lineupsApp" ng-controller="GenerateLineupsController">
        <form class="col-md-12">
            <div class="criteria-holder">
                <div class="criteria qb-criteria col-md-2">
                    <h4>QB Criteria <input type="checkbox" ng-model="qb.enabled"></h4>
                    <div class="fields">
                        <div>
                            <span class="criteria-label">Pass Att</span>
                            <select name="pass_att_compare" ng-model="qb.pass_att.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input name="pass_att" ng-model="qb.pass_att.value">
                        </div>
                        <div>
                            <span class="criteria-label">Pass Cmp</span>
                            <select ng-model="qb.pass_cmp.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select >
                            <input ng-model="qb.pass_cmp.value">
                        </div>
                        <div>
                            <span class="criteria-label">Pass Yds</span>
                            <select ng-model="qb.pass_yds.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="qb.pass_yds.value">
                        </div>
                    </div>
                </div>
                <div class="criteria rb-criteria col-md-2">
                    <h4>RB Criteria <input type="checkbox" ng-model="rb.enabled"></h4>
                    <div class="fields">
                        <div>
                            <span class="criteria-label">Rush Att</span>
                            <select ng-model="rb.rush_att.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="rb.rush_att.value">
                        </div>
                        <div>
                            <span class="criteria-label">Rush Yds</span>
                            <select ng-model="rb.rush_yds.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="rb.rush_yds.value">
                        </div>
                        <div>
                            <span class="criteria-label">Targets</span>
                            <select ng-model="rb.targets.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="rb.targets.value">
                        </div>
                    </div>
                </div>
                <div class="criteria wr-criteria col-md-2">
                    <h4>WR Criteria <input type="checkbox" ng-model="wr.enabled"></h4>
                    <div class="fields">
                        <div>
                            <span class="criteria-label">Targets</span>
                            <select ng-model="wr.targets.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="wr.targets.value">
                        </div>
                        <div>
                            <span class="criteria-label">Rec</span>
                            <select ng-model="wr.rec.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="wr.rec.value">
                        </div>
                        <div>
                            <span class="criteria-label">Rec Yds</span>
                            <select ng-model="wr.rec_yds.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="wr.rec_yds.value">
                        </div>
                    </div>
                </div>
                <div class="criteria te-criteria col-md-2">
                    <h4>TE Criteria <input type="checkbox" ng-model="te.enabled"></h4>
                    <div class="fields">
                        <div>
                            <span class="criteria-label">Targets</span>
                            <select ng-model="te.targets.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="te.targets.value">
                        </div>
                        <div>
                            <span class="criteria-label">Rec</span>
                            <select  ng-model="te.rec.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input  ng-model="te.rec.value">
                        </div>
                        <div>
                            <span class="criteria-label">Rec Yds</span>
                            <select ng-model="te.rec_yds.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="te.rec_yds.value">
                        </div>
                    </div>
                </div>
                <div class="criteria dst-criteria col-md-2">
                    <h4>DST Criteria <input type="checkbox" ng-model="dst.enabled"></h4>
                    <div class="fields">
                        <div>
                            <span class="criteria-label">Sacks</span>
                            <select ng-model="dst.sacks.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="dst.sacks.value">
                        </div>
                        <div>
                            <span class="criteria-label">INTs</span>
                            <select ng-model="dst.int.mode">
                                <option></option>
                                <option>></option>
                                <option>>=</option>
                                <option><</option>
                                <option><=</option>
                            </select>
                            <input ng-model="dst.int.value">
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-5 col-md-3">
                    <button class="btn btn-primary" ng-click="generateShowdownLineups()" ng-disabled="loadingLineups"><span ng-show="loadingLineups"><img src="{{ asset('img/ajax-loader.gif') }}" ></span> Generate Showdown Lineups</button>
                </div>
                <div class="col-md-2">
                    <button class="btn btn-success" ng-click="generateLineups()" ng-disabled="loadingLineups"><span ng-show="loadingLineups"><img src="{{ asset('img/ajax-loader.gif') }}" ></span> Generate Lineups</button>
                </div>
            </div>
        </form>
        <style>
            .criteria-container .lineups td {
                border-top: 1px solid black;
            }
        </style>
        <div class="lineups col-md-12">
            <table ng-repeat="roster in showdownRosters" class="col-md-4 table table-hover" style="width: 47%;border: 1px solid black;margin-right: 10px;">
                <thead>
                    <th>Position</th>
                    <th>Name</th>
                    <th>Team</th>
                    <th>Opp</th>
                    <th>Salary</th>
                    <th>FP</th>
                </thead>
                <tr>
                    <td>CPT</td>
                    <td><% roster.CPT.name %></td>
                    <td><% roster.CPT.team %></td>
                    <td><% roster.CPT.opp %></td>
                    <td><% roster.CPT.new_salary %></td>
                    <td><% roster.CPT.fppg %></td>
                </tr>
                <tr>
                    <td>FLEX</td>
                    <td><% roster.FLEX1.name %></td>
                    <td><% roster.FLEX1.team %></td>
                    <td><% roster.FLEX1.opp %></td>
                    <td><% roster.FLEX1.new_salary %></td>
                    <td><% roster.FLEX1.fppg %></td>
                </tr>
                <tr>
                    <td>FLEX</td>
                    <td><% roster.FLEX2.name %></td>
                    <td><% roster.FLEX2.team %></td>
                    <td><% roster.FLEX2.opp %></td>
                    <td><% roster.FLEX2.new_salary %></td>
                    <td><% roster.FLEX2.fppg %></td>
                </tr>
                <tr>
                    <td>FLEX</td>
                    <td><% roster.FLEX3.name %></td>
                    <td><% roster.FLEX3.team %></td>
                    <td><% roster.FLEX3.opp %></td>
                    <td><% roster.FLEX3.new_salary %></td>
                    <td><% roster.FLEX3.fppg %></td>
                </tr>
                <tr>
                    <td>FLEX</td>
                    <td><% roster.FLEX4.name %></td>
                    <td><% roster.FLEX4.team %></td>
                    <td><% roster.FLEX4.opp %></td>
                    <td><% roster.FLEX4.new_salary %></td>
                    <td><% roster.FLEX4.fppg %></td>
                </tr>
                <tr>
                    <td>FLEX</td>
                    <td><% roster.FLEX5.name %></td>
                    <td><% roster.FLEX5.team %></td>
                    <td><% roster.FLEX5.opp %></td>
                    <td><% roster.FLEX5.new_salary %></td>
                    <td><% roster.FLEX5.fppg %></td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><% roster.salary_total %></td>
                    <td><% roster.fppg_total %></td>
                </tr>
            </table>
        </div>
        <div class="lineups col-md-12">
            <table ng-repeat="roster in rosters" class="col-md-4 table table-hover" style="width: 47%;border: 1px solid black;margin-right: 10px;">
                <thead>
                    <th>Position</th>
                    <th>Name</th>
                    <th>Team</th>
                    <th>Opp</th>
                    <th>Salary</th>
                    <th>FP</th>
                </thead>
                <tr>
                    <td><% roster.QB.position %></td>
                    <td><% roster.QB.name %></td>
                    <td><% roster.QB.team %></td>
                    <td><% roster.QB.opp %></td>
                    <td><% roster.QB.salary %></td>
                    <td><% roster.QB.fppg %></td>
                </tr>
                <tr>
                    <td><% roster.RB1.position %></td>
                    <td><% roster.RB1.name %></td>
                    <td><% roster.RB1.team %></td>
                    <td><% roster.RB1.opp %></td>
                    <td><% roster.RB1.salary %></td>
                    <td><% roster.RB1.fppg %></td>
                </tr>
                <tr>
                    <td><% roster.RB2.position %></td>
                    <td><% roster.RB2.name %></td>
                    <td><% roster.RB2.team %></td>
                    <td><% roster.RB2.opp %></td>
                    <td><% roster.RB2.salary %></td>
                    <td><% roster.RB2.fppg %></td>
                </tr>
                <tr>
                    <td><% roster.WR1.position %></td>
                    <td><% roster.WR1.name %></td>
                    <td><% roster.WR1.team %></td>
                    <td><% roster.WR1.opp %></td>
                    <td><% roster.WR1.salary %></td>
                    <td><% roster.WR1.fppg %></td>
                </tr>
                <tr>
                    <td><% roster.WR2.position %></td>
                    <td><% roster.WR2.name %></td>
                    <td><% roster.WR2.team %></td>
                    <td><% roster.WR2.opp %></td>
                    <td><% roster.WR2.salary %></td>
                    <td><% roster.WR2.fppg %></td>
                </tr>
                <tr>
                    <td><% roster.WR3.position %></td>
                    <td><% roster.WR3.name %></td>
                    <td><% roster.WR3.team %></td>
                    <td><% roster.WR3.opp %></td>
                    <td><% roster.WR3.salary %></td>
                    <td><% roster.WR3.fppg %></td>
                </tr>
                <tr>
                    <td><% roster.TE.position %></td>
                    <td><% roster.TE.name %></td>
                    <td><% roster.TE.team %></td>
                    <td><% roster.TE.opp %></td>
                    <td><% roster.TE.salary %></td>
                    <td><% roster.TE.fppg %></td>
                </tr>
                <tr>
                    <td>FLEX</td>
                    <td><% roster.FLEX.name %></td>
                    <td><% roster.FLEX.team %></td>
                    <td><% roster.FLEX.opp %></td>
                    <td><% roster.FLEX.salary %></td>
                    <td><% roster.FLEX.fppg %></td>
                </tr>
                <tr>
                    <td>DST</td>
                    <td><% roster.DST.name %></td>
                    <td><% roster.DST.team %></td>
                    <td><% roster.DST.opp %></td>
                    <td><% roster.DST.salary %></td>
                    <td><% roster.DST.fppg %></td>
                </tr>
                <tr>
                    <td>Total</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td><% roster.salary_total %></td>
                    <td><% roster.fppg_total %></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="modal fade QB" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Cmp</th>
                    <th>Att</th>
                    <th>%</th>
                    <th>Yds</th>
                    <th>TDs</th>
                    <th>INTs</th>
                    <th>Rush Atts</th>
                    <th>Rush Yds</th>
                    <th>Rush TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade RB" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Att</th>
                    <th>Yds</th>
                    <th>Avg</th>
                    <th>TDs</th>
                    <th>Rec</th>
                    <th>Targets</th>
                    <th>Rec Yds</th>
                    <th>Rec TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade WR" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Rec</th>
                    <th>Tgts</th>
                    <th>Catch %</th>
                    <th>Yds</th>
                    <th>TDs</th>
                    <th>Rush Atts</th>
                    <th>Rush Yds</th>
                    <th>Rush TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade TE" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Rec</th>
                    <th>Tgts</th>
                    <th>Catch %</th>
                    <th>Yds</th>
                    <th>TDs</th>
                    <th>Rush Atts</th>
                    <th>Rush Yds</th>
                    <th>Rush TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@include('layouts.corejs')