@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer - Sync')
@section('height', 'full-height')
@include('layouts.head')
@section('content')
    @include('layouts.sidebar')
    <style type="text/css">
    	.field {
		    width: 1350px;
		    height: 1000px;
		    position: relative;
	        font-size: 12px;
    	}
    	.defense > div {
		    position: absolute;
		    border: 2px solid black;
    		padding: 10px;
		}
		.offense > div {
		    position: absolute;
		    border: 2px solid green;
    		padding: 10px;
		}
		.teams-option {
			width: 1270px;
			margin-top: 30px;
		}
		.lcb {
			top: 150px;
    		right: 0;
		}
		.rcb {
			top: 150px;
    		left: 50px;
		}
		.nb {
			top: 170px;
    		left: 200px;
		}
		.nt {
		    top: 170px;
    		left: 580px;
		}
		.ntdt {
			top: 170px;
    		left: 660px;
		}
		.dt1 {
		    top: 170px;
    		left: 520px;
		}
		.dt2 {
		    top: 170px;
    		left: 660px;
		}
		.de1 {
		    top: 170px;
    		left: 400px;
		}
		.ntde {
			top: 170px;
    		left: 460px;
		}
		.de2 {
		    top: 170px;
    		left: 800px;
		}
		.ntde2 {
			top: 170px;
    		left: 700px;
		}
		.mlb {
		    top: 50px;
    		left: 610px;
		}
		.mlb2 {
			top: 50px;
    		left: 480px;
		}
		.wlb {
		    top: 50px;
    		left: 480px;
		}
		.wlbmlb {
			top: 50px;
    		left: 350px;
		}
		.slb {
		    top: 50px;
    		left: 740px;
		}
		.fs {
		    top: 50px;
    		left: 190px;
		}
		.ss {
		    top: 50px;
    		left: 900px;
		}
		.qb {
			top: 420px;
    		left: 620px;
		}
		.rb {
			top: 550px;
    		left: 670px;
		}
		.swr {
			top: 290px;
    		left: 180px;
		}
		.fb {
		    top: 550px;
    		left: 520px;
		}
		.lwr {
		    top: 290px;
    		right: 0;
		}
		.rwr {
		    top: 290px;
    		left: 50px;
		}
		.te {
		    top: 290px;
    		left: 980px;
		}
		.lt {
		    top: 290px;
    		left: 340px;
		}
		.lg {
		    top: 290px;
    		left: 470px;
		}
		.c {
		    top: 290px;
    		left: 580px;
		}
		.rt {
		    top: 290px;
    		left: 830px;
		}
		.rg {
		    top: 290px;
    		left: 710px;
		}
		.bold {
			font-weight: bold;
		}
    </style>
    <div class="main-panel" ng-app="playersApp" ng-controller="DepthChartController">
    	<div class="text-center teams-option">
    		<select ng-model="game.selected" ng-change="changeGame(game)" ng-options="game.key as game.value for game in games"></select>
    	</div>
     	<div class="field">
	     	<div class="defense">
	     		<div class="lcb">
	     			<h5>Left CB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.LCB"><% depth.name %></div>
	     		</div>
	     		<div class="rcb">
	     			<h5>Right CB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.RCB"><% depth.name %></div>
	     		</div>
	     		<div class="nb" ng-show="teamDepthCharts[def_team_id].depth.NB">
	     			<h5>Nickel CB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.NB"><% depth.name %></div>
	     		</div>
	     		<div class="nb" ng-show="teamDepthCharts[def_team_id].depth.S && teamDepthCharts[def_team_id].depth.SS">
	     			<h5>Nickel CB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.S"><% depth.name %></div>
	     		</div>
	     		<div class="nb" ng-show="teamDepthCharts[def_team_id].depth.CB">
	     			<h5>Nickel CB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.CB"><% depth.name %></div>
	     		</div>
	     		<div class="dt1" ng-show="teamDepthCharts[def_team_id].depth.DT">
	     			<h5>D Tackle</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.DT"><% depth.name %></div>
	     		</div>
	     		<div class="dt1" ng-show="teamDepthCharts[def_team_id].depth.LDT">
	     			<h5>D Tackle</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.LDT"><% depth.name %></div>
	     		</div>
	     		<div class="nt" ng-show="teamDepthCharts[def_team_id].depth.NT" ng-class="{ntdt: teamDepthCharts[def_team_id].depth.DT}">
	     			<h5>Nose Tackle</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.NT"><% depth.name %></div>
	     		</div>
	     		<div class="dt2" ng-show="teamDepthCharts[def_team_id].depth.RDT">
	     			<h5>D Tackle</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.RDT"><% depth.name %></div>
	     		</div>
	     		<div class="dt2" ng-show="teamDepthCharts[def_team_id].depth.DT2">
	     			<h5>D Tackle</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.DT2"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.DE" ng-class="{ntde: teamDepthCharts[def_team_id].depth.NT}" class="de1">
	     			<h5>D End</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.DE"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.RDE" ng-class="{ntde: teamDepthCharts[def_team_id].depth.NT && !teamDepthCharts[def_team_id].depth.DT}" class="de1">
	     			<h5>D End</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.RDE"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.RE" ng-class="{ntde: teamDepthCharts[def_team_id].depth.NT && !teamDepthCharts[def_team_id].depth.DT}" class="de1">
	     			<h5>D End</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.RE"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.DE_OB" ng-class="{ntde: teamDepthCharts[def_team_id].depth.NT && !teamDepthCharts[def_team_id].depth.DT}" class="de1">
	     			<h5>D End</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.DE_OB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.LDE" ng-class="{ntde2: teamDepthCharts[def_team_id].depth.NT && !teamDepthCharts[def_team_id].depth.DT}" class="de2">
	     			<h5>D End</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.LDE"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.LE" ng-class="{ntde2: teamDepthCharts[def_team_id].depth.NT && !teamDepthCharts[def_team_id].depth.DT}" class="de2">
	     			<h5>D End</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.LE"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.DE2" ng-class="{ntde2: teamDepthCharts[def_team_id].depth.NT}" class="de2">
	     			<h5>D End</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.DE2"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.MLB" class="mlb">
	     			<h5>Middle LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.MLB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.ILB" class="mlb">
	     			<h5>Inside LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.ILB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.ILB2" class="mlb2">
	     			<h5>Inside LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.ILB2"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.MLB2" class="mlb2">
	     			<h5>Middle LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.MLB2"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.WLB" ng-class="{wlbmlb: teamDepthCharts[def_team_id].depth.MLB2 || teamDepthCharts[def_team_id].depth.ILB2 || teamDepthCharts[def_team_id].depth.RILB}" class="wlb">
	     			<h5>Wheel LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.WLB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.OLB" class="wlb">
	     			<h5>Outside LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.OLB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.OLB2" class="slb">
	     			<h5>Outside LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.OLB2"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.ROLB" ng-class="{wlbmlb: teamDepthCharts[def_team_id].depth.ROLB}" class="wlb">
	     			<h5>Right OLB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.ROLB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.RILB" class="mlb2">
	     			<h5>Right ILB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.RILB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.LILB" class="mlb">
	     			<h5>Left ILB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.LILB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.SILB" class="mlb">
	     			<h5>Left ILB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.SILB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.LOLB" class="slb">
	     			<h5>Left OLB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.LOLB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.SLB" class="slb">
	     			<h5>Sam LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.SLB"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.SAM" class="slb">
	     			<h5>Sam LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.SAM"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[def_team_id].depth.OLB2" class="slb">
	     			<h5>Outside LB</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.OLB2"><% depth.name %></div>
	     		</div>
	     		<div class="fs" ng-show="teamDepthCharts[def_team_id].depth.FS">
	     			<h5>Free Safety</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.FS"><% depth.name %></div>
	     		</div>
	     		<div class="fs" ng-show="teamDepthCharts[def_team_id].depth.S && !teamDepthCharts[def_team_id].depth.SS">
	     			<h5>Free Safety</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.S"><% depth.name %></div>
	     		</div>
	     		<div class="ss" ng-show="teamDepthCharts[def_team_id].depth.SS">
	     			<h5>Strong Safety</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.SS"><% depth.name %></div>
	     		</div>
	     		<div class="ss" ng-show="teamDepthCharts[def_team_id].depth.S2">
	     			<h5>Strong Safety</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[def_team_id].depth.S2"><% depth.name %></div>
	     		</div>
	     	</div>
	     	<div class="offense">
	     		<div class="qb">
	     			<h5>Quarterback</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.QB"><% depth.name %></div>
	     		</div>
	     		<div class="rb">
	     			<h5>Running Backs</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.RB"><% depth.name %></div>
	     		</div>
	     		<div class="fb">
	     			<h5>Full Backs</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.FB"><% depth.name %></div>
	     		</div>
	     		<div class="lwr">
	     			<h5>Left WR</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.LWR"><% depth.name %></div>
	     		</div>
	     		<div class="rwr">
	     			<h5>Right WR</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.RWR"><% depth.name %></div>
	     		</div>
	     		<div ng-show="teamDepthCharts[off_team_id].depth.SWR" class="swr">
	     			<h5>Slot WR</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.SWR"><% depth.name %></div>
	     		</div>
	     		<div class="te">
	     			<h5>Tight Ends</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.TE"><% depth.name %></div>
	     		</div>
	     		<div class="lt">
	     			<h5>Left Tackle</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.LT"><% depth.name %></div>
	     		</div>
	     		<div class="lg">
	     			<h5>Left Guard</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.LG"><% depth.name %></div>
	     		</div>
	     		<div class="c">
	     			<h5>Centers</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.C"><% depth.name %></div>
	     		</div>
	     		<div class="rt">
	     			<h5>Right Tackle</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.RT"><% depth.name %></div>
	     		</div>
	     		<div class="rg">
	     			<h5>Right Guard</h5>
	     			<div ng-class="{bold: $index == 0}" ng-repeat="depth in teamDepthCharts[off_team_id].depth.RG"><% depth.name %></div>
	     		</div>
	     	</div>
     	</div>
    </div>
@endsection
@include('layouts.corejs')