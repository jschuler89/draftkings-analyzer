@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer')
@section('header_title')
<div class="title m-b-md">
    Draftkings Roster Analyzer
</div>
@endsection
@section('content')
    @include('layouts.nav')
    <div class="player-nav">
        <button data-table="qbs" class="btn btn-sm active">QB</button>
        <button data-table="rbs" class="btn btn-sm">RB</button>
        <button data-table="wrs" class="btn btn-sm">WR</button>
        <button data-table="tes" class="btn btn-sm">TE</button>
        <button data-table="dsts" class="btn btn-sm">DST</button>
    </div>
    <div class="card">
        <div class="card-header" data-background-color="orange">
            <h4 class="title">Player Stats</h4>
        </div>
        <div id="qbs" class="card-content table-responsive active">
            <table class="table table-hover">
                <thead class="text-warning">
                    <th>Player Name</th>
                    <th>Position</th>
                    <th>Team</th>
                    <th>Opp</th>
                    <th>Salary</th>
                    <th>FPPG</th>
                    <th>Pass Cmp</th>
                    <th>Pass Atts</th>
                    <th>Cmp %</th>
                    <th>Opp Allowed</th>
                    <!-- <th>Opp Allowed (5)</th> -->
                    <th>Pass Yds</th>
                    <th>Proj Pass Yds</th>
                    <!-- <th>Proj Pass Yds (5)</th> -->
                    <th>Pass TDs</th>
                    <th>INTs</th>
                    <th>Value 2x</th>
                    <th>Value 2x%</th>
                    <th>Value 3x</th>
                    <th>Value 3x%</th>
                    <th>Value 4x</th>
                    <th>Value 4x%</th>
                </thead>
                <tbody>
                    @foreach ($qbs as $player)
                    <tr data-player-id="{{ $player->playerData['player_id'] }}">
                        <td><b>{{ $player->playerData['name'] }}</b></td>
                        <td>{{ $player->playerData['position'] }}</td>
                        <td>{{ $player->playerData['team'] }}</td>
                        <td>{{ $player->playerData['opp'] }}</td>
                        <td>${{ $player->playerData['salary'] }}</td>
                        <td>{{ $player->playerData['fppg'] }}</td>
                        <td>{{ number_format($player->playerData['pass_cmp'], 2) }}</td>
                        <td>{{ number_format($player->playerData['pass_att'], 2) }}</td>
                        <td>{{ $player->playerData['pass_att'] > 0 ? number_format(($player->playerData['pass_cmp'] / $player->playerData['pass_att']) * 100, 2) : 0 }}%</td>
                        <td>{{ number_format($dsts[strtolower($player->playerData['opp'])]['pass_yds_def'], 2) }}</td>
                        <!-- <td>{{ number_format($dsts[strtolower($player->playerData['opp'])]['pass_yds_def_last_5'], 2) }}</td> -->
                        <td>{{ number_format($player->playerData['pass_yds'], 2) }}</td>
                        <td>{{ number_format($dsts[strtolower($player->playerData['opp'])]['pass_yds_def'] + $player->getPassYdsBuffer(), 2) }}</td>
                        <!-- <td>{{ number_format($dsts[strtolower($player->playerData['opp'])]['pass_yds_def_last_5'] + $player->getPassYdsBuffer(), 2) }}</td> -->
                        <td>{{ number_format($player->playerData['pass_tds'], 2) }}</td>
                        <td>{{ $player->playerData['pass_int'] > 0 ? $player->playerData['pass_int'] : 0 }}</td>
                        <td>{{ $player->exceedValues['player_2x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_2x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_3x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_3x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_4x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_4x_pct'], 2) }}%</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="rbs" class="card-content table-responsive">
            <table class="table table-hover">
                <thead class="text-warning">
                    <th>Player Name</th>
                    <th>Position</th>
                    <th>Team</th>
                    <th>Opp</th>
                    <th>Salary</th>
                    <th>FPPG</th>
                    <!-- <th>Pt / $</th> -->
                    <th>Rush Atts</th>
                    <th>Rush Yds</th>
                    <th>Rush TDs</th>
                    <th>Targets</th>
                    <th>Rec</th>
                    <th>Rec Yds</th>
                    <th>Rec TDs</th>
                    <th>Value 2x</th>
                    <th>Value 2x%</th>
                    <th>Value 3x</th>
                    <th>Value 3x%</th>
                    <th>Value 4x</th>
                    <th>Value 4x%</th>
                </thead>
                <tbody>
                    @foreach ($rbs as $player)
                    <tr data-player-id="{{ $player->playerData['player_id'] }}">
                        <td><b>{{ $player->playerData['name'] }}</b></td>
                        <td>{{ $player->playerData['position'] }}</td>
                        <td>{{ $player->playerData['team'] }}</td>
                        <td>{{ $player->playerData['opp'] }}</td>
                        <td>${{ $player->playerData['salary'] }}</td>
                        <td>{{ $player->playerData['fppg'] }}</td>
                        <!-- <td>{{ number_format($player->playerData['fppg'] / ($player->playerData['salary'] / 1000) , 2) }}</td> -->
                        <td>{{ $player->playerData['rush_att'] != '' ? number_format($player->playerData['rush_att'], 2) : 0 }}</td>
                        <td>{{ number_format($player->playerData['rush_yds'], 2) }}</td>
                        <td>{{ number_format($player->playerData['rush_tds'], 2) }}</td>
                        <td>{{ $player->playerData['targets'] > 0 ? number_format($player->playerData['targets'], 2) : 0 }}</td>
                        <td>{{ number_format($player->playerData['rec'], 2) }}</td>
                        <td>{{ number_format($player->playerData['rec_yds'], 2) }}</td>
                        <td>{{ number_format($player->playerData['rec_tds'], 2) }}</td>
                        <td>{{ $player->exceedValues['player_2x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_2x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_3x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_3x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_4x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_4x_pct'], 2) }}%</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="wrs" class="card-content table-responsive">
            <table class="table table-hover">
                <thead class="text-warning">
                    <th>Player Name</th>
                    <th>Position</th>
                    <th>Team</th>
                    <th>Opp</th>
                    <th>Salary</th>
                    <th>FPPG</th>
                    <th>Targets</th>
                    <th>Rec</th>
                    <th>Rec %</th>
                    <th>Rec Yds</th>
                    <th>% of Pass Yds</th>
                    <th>Rec TDs</th>
                    <th>Value 2x</th>
                    <th>Value 2x%</th>
                    <th>Value 3x</th>
                    <th>Value 3x%</th>
                    <th>Value 4x</th>
                    <th>Value 4x%</th>
                </thead>
                <tbody>
                    @foreach ($wrs as $player)
                    <tr data-player-id="{{ $player->playerData['player_id'] }}">
                        <td><b>{{ $player->playerData['name'] }}</b></td>
                        <td>{{ $player->playerData['position'] }}</td>
                        <td>{{ $player->playerData['team'] }}</td>
                        <td>{{ $player->playerData['opp'] }}</td>
                        <td>${{ $player->playerData['salary'] }}</td>
                        <td>{{ $player->playerData['fppg'] }}</td>
                        <td>{{ $player->playerData['targets'] > 0 ? number_format($player->playerData['targets'], 2) : 0 }}</td>
                        <td>{{ number_format($player->playerData['rec'], 2) }}</td>
                        <td>{{ $player->playerData['targets'] > 0 ? number_format(($player->playerData['rec'] / $player->playerData['targets']) * 100, 2) : 0 }}%</td>
                        <td>{{ number_format($player->playerData['rec_yds'], 2) }}</td>
                        <td>{{ number_format($player->getPctOfPassYds(), 2) }}%</td>
                        <td>{{ number_format($player->playerData['rec_tds'], 2) }}</td>
                        <td>{{ $player->exceedValues['player_2x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_2x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_3x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_3x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_4x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_4x_pct'], 2) }}%</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="tes" class="card-content table-responsive">
            <table class="table table-hover">
                <thead class="text-warning">
                    <th>Player Name</th>
                    <th>Position</th>
                    <th>Team</th>
                    <th>Opp</th>
                    <th>Salary</th>
                    <th>FPPG</th>
                    <th>Targets</th>
                    <th>Rec</th>
                    <th>Rec %</th>
                    <th>Rec Yds</th>
                    <th>% of Pass Yds</th>
                    <th>Rec TDs</th>
                    <th>Value 2x</th>
                    <th>Value 2x%</th>
                    <th>Value 3x</th>
                    <th>Value 3x%</th>
                    <th>Value 4x</th>
                    <th>Value 4x%</th>
                </thead>
                <tbody>
                    @foreach ($tes as $player)
                    <tr data-player-id="{{ $player->playerData['player_id'] }}">
                        <td><b>{{ $player->playerData['name'] }}</b></td>
                        <td>{{ $player->playerData['position'] }}</td>
                        <td>{{ $player->playerData['team'] }}</td>
                        <td>{{ $player->playerData['opp'] }}</td>
                        <td>${{ $player->playerData['salary'] }}</td>
                        <td>{{ $player->playerData['fppg'] }}</td>
                        <td>{{ $player->playerData['targets'] > 0 ? number_format($player->playerData['targets'], 2) : 0 }}</td>
                        <td>{{ number_format($player->playerData['rec'], 2) }}</td>
                        <td>{{ $player->playerData['targets'] > 0 ? number_format(($player->playerData['rec'] / $player->playerData['targets']) * 100, 2) : 0 }}%</td>
                        <td>{{ number_format($player->playerData['rec_yds'], 2) }}</td>
                        <td>{{ number_format($player->getPctOfPassYds(), 2) }}%</td>
                        <td>{{ number_format($player->playerData['rec_tds'], 2) }}</td>
                        <td>{{ $player->exceedValues['player_2x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_2x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_3x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_3x_pct'], 2) }}%</td>
                        <td>{{ $player->exceedValues['player_4x'] }}</td>
                        <td>{{ number_format($player->exceedValues['player_4x_pct'], 2) }}%</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="dsts" class="card-content table-responsive">
            <table class="table table-hover">
                <thead class="text-warning">
                    <th>Team</th>
                    <th>Opp</th>
                    <th>Salary</th>
                    <th>Pass Yds Allowed</th>
                    <th>Pass Yds Allowed (5)</th>
                    <th>Rush Yds Allowed</th>
                    <th>Rush Yds Allowed (5)</th>
                    <th>Points Allowed</th>
                    <th>Points Allowed (5)</th>
                </thead>
                <tbody>
                    @foreach ($dsts as $team)
                    <tr>
                        <td>{{ $team['team'] }}</td>
                        <td>{{ $team['opp'] }}</td>
                        <td>${{ $team['salary'] }}</td>
                        <td>{{ $team['pass_yds_def'] > 0 ? number_format($team['pass_yds_def'], 2) : 0 }}</td>
                        <td>{{ $team['pass_yds_def_last_5'] > 0 ? number_format($team['pass_yds_def_last_5'], 2) : 0 }}</td>
                        <td>{{ $team['rush_yds_def'] > 0 ? number_format($team['rush_yds_def'], 2) : 0 }}</td>
                        <td>{{ $team['rush_yds_def_last_5'] > 0 ? number_format($team['rush_yds_def_last_5'], 2) : 0 }}</td>
                        <td>{{ $team['pts_def'] > 0 ? number_format($team['pts_def'], 2) : 0 }}</td>
                        <td>{{ $team['pts_def_last_5'] > 0 ? number_format($team['pts_def_last_5'], 2) : 0 }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div class="modal fade QB" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Cmp</th>
                    <th>Att</th>
                    <th>%</th>
                    <th>Yds</th>
                    <th>TDs</th>
                    <th>INTs</th>
                    <th>Rush Atts</th>
                    <th>Rush Yds</th>
                    <th>Rush TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade RB" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Att</th>
                    <th>Yds</th>
                    <th>Avg</th>
                    <th>TDs</th>
                    <th>Rec</th>
                    <th>Targets</th>
                    <th>Rec Yds</th>
                    <th>Rec TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade WR" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Rec</th>
                    <th>Tgts</th>
                    <th>Catch %</th>
                    <th>Yds</th>
                    <th>TDs</th>
                    <th>Rush Atts</th>
                    <th>Rush Yds</th>
                    <th>Rush TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="modal fade TE" id="playerModal" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <img src="">
            <h4 class="player-name"></h4>
            <div class="player-salary"></div>
          </div>
          <div class="modal-body">
            <table>
                <thead>
                    <th>Week</th>
                    <th>Opp</th>
                    <th>Result</th>
                    <th>Rec</th>
                    <th>Tgts</th>
                    <th>Catch %</th>
                    <th>Yds</th>
                    <th>TDs</th>
                    <th>Rush Atts</th>
                    <th>Rush Yds</th>
                    <th>Rush TDs</th>
                    <th>FPTS</th>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
          </div>
          <!-- <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection