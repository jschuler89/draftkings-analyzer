@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer - Data Analysis')
@section('css')
    <link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- <link href="{{ asset('css/dynatable.css') }}" rel="stylesheet"> -->
@endsection
@section('scripts')
        <!-- <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script> -->
        <!-- <script src="{{ asset('js/data-analysis.js') }}"></script> -->
        <!-- <script src="{{ asset('js/dynatable.js') }}"></script> -->
@endsection
@section('header_title')
<div class="title m-b-md">
    Draftkings Roster Analyzer
</div>
@endsection
@section('content')
    @include('layouts.sidebar')
    <div class="main-panel">
        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"> Players </a>
                </div>
            </div>
        </nav>
        <div class="content" ng-app="playersApp" ng-controller="PlayersController">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 player-table">
                        <div>
                            <button type="button" ng-class="{active: showQB}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('QB')">QB - Avg FP: <% leagueAverages['QB_FP'] | number:2 %></button>
                            <button type="button" ng-class="{active: showRB}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('RB')">RB - Avg FP: <% leagueAverages['RB_FP'] | number:2 %></button>
                            <button type="button" ng-class="{active: showWR}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('WR')">WR - Avg FP: <% leagueAverages['WR_FP'] | number:2 %></button>
                            <button type="button" ng-class="{active: showTE}" class="btn player-tab btn-success btn-simple btn-lg" ng-click="showPosition('TE')">TE - Avg FP: <% leagueAverages['TE_FP'] | number:2 %></button>
                        </div>
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="title"><% selectedPosition %></h4>
                                <p class="category">Week <% week_num %></p>
                            </div>
                            <div class="card-content table-responsive" ng-show="showQB">
                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('opp')">Opp <span ng-show="sortType == 'opp'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('def_score')">Opp Def <span ng-show="sortType == 'def_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fp_pt')">$ / pt <span ng-show="sortType == 'fp_pt'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                    </thead>
                                    <tbody>
                                        <tr ng-click="selectPlayer(Player)" ng-repeat="Player in players.QB | orderBy:sortType:sortReverse:compareFPPGQB">
                                            <td><% Player.name %></td>
                                            <td><% Player.salary | currency:"$":0 %></td>
                                            <td><% Player.team %></td>
                                            <td><% Player.opp %></td>
                                            <td><% DSTObj[Player.opp].pass_yds_def_score | number:2 %></td>
                                            <td><% Player.fppg %></td>
                                            <td><% Player.fppg / (Player.salary / 1000) | number:2 %></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-content table-responsive" ng-show="showRB">
                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('opp')">Opp <span ng-show="sortType == 'opp'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('def_score')">Opp Def <span ng-show="sortType == 'def_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fp_pt')">$ / pt <span ng-show="sortType == 'fp_pt'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                    </thead>
                                    <tbody>
                                        <tr ng-click="selectPlayer(Player)" ng-repeat="Player in players.RB | orderBy:sortType:sortReverse:compareFPPGRB">
                                            <td><% Player.name %></td>
                                            <td><% Player.salary | currency:"$":0 %></td>
                                            <td><% Player.team %></td>
                                            <td><% Player.opp %></td>
                                            <td><% DSTObj[Player.opp].rush_yds_def_score | number:2 %></td>
                                            <td><% Player.fppg %></td>
                                            <td><% Player.fppg / (Player.salary / 1000) | number:2 %></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-content table-responsive" ng-show="showWR">
                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('opp')">Opp <span ng-show="sortType == 'opp'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('def_score')">Opp Def <span ng-show="sortType == 'def_score'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fp_pt')">$ / pt <span ng-show="sortType == 'fp_pt'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                    </thead>
                                    <tbody>
                                        <tr ng-click="selectPlayer(Player)" ng-repeat="Player in players.WR | orderBy:sortType:sortReverse:compareFPPGWR">
                                            <td><% Player.name %></td>
                                            <td><% Player.salary | currency:"$":0 %></td>
                                            <td><% Player.team %></td>
                                            <td><% Player.opp %></td>
                                            <td><% DSTObj[Player.opp].pass_yds_def_score | number:2 %></td>
                                            <td><% Player.fppg %></td>
                                            <td><% Player.fppg / (Player.salary / 1000) | number:2 %></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="card-content table-responsive" ng-show="showTE">
                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th ng-click="sortBy('name')">Name <span ng-show="sortType == 'name'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('salary')">Salary <span ng-show="sortType == 'salary'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('team')">Team <span ng-show="sortType == 'team'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('opp')">Opp <span ng-show="sortType == 'opp'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                        <th ng-click="sortBy('fppg')">FPPG <span ng-show="sortType == 'fppg'"><i class="material-icons" ng-show=!sortReverse>arrow_drop_up</i><i class="material-icons" ng-show=sortReverse>arrow_drop_down</i></span></th>
                                    <tbody>
                                        <tr ng-click="selectPlayer(Player)" ng-repeat="Player in players.TE | orderBy:sortType:sortReverse:compareFPPGTE">
                                            <td><% Player.name %></td>
                                            <td><% Player.salary | currency:"$":0 %></td>
                                            <td><% Player.team %></td>
                                            <td><% Player.opp %></td>
                                            <td><% Player.fppg %></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5" style="margin-top: 74px;position: fixed;right: 10px;">
                        <div class="card">
                            <div class="card-header" data-background-color="blue">
                                <h4 class="title"><% selectedPlayerName %> - <% selectedPlayerTeam %></h4>
                                <p class="category"><% selectedPlayerPosition %></p>
                            </div>
                            <div class="card-content" ng-show="showQB">
                                <div class="inline-block">
                                    <div>
                                        <span class="stat-label">Pass Cmp</span>
                                        <span><% selectedPlayerCompletions | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerCompletions/leagueAverages['QB'].pass_cmp_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['QB'].pass_cmp_avg < selectedPlayerCompletions && (((selectedPlayerCompletions/leagueAverages['QB'].pass_cmp_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['QB'].pass_cmp_avg > selectedPlayerCompletions && selectedPlayerCompletions > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Pass Atts</span>
                                        <span><% selectedPlayerPassAtts | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerPassAtts/leagueAverages['QB'].pass_att_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['QB'].pass_att_avg < selectedPlayerPassAtts && (((selectedPlayerPassAtts/leagueAverages['QB'].pass_att_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['QB'].pass_att_avg > selectedPlayerPassAtts && selectedPlayerPassAtts > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Cmp %</span>
                                        <span><% (selectedPlayerCompletions / selectedPlayerPassAtts) * 100 | number:2 %>%</span>
                                    </div>
                                </div>
                                <div class="inline-block ml10">
                                    <div>
                                        <span class="stat-label">Pass Yds</span>
                                        <span><% selectedPlayerPassYds | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerPassYds/leagueAverages['QB'].pass_yds_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['QB'].pass_yds_avg < selectedPlayerPassYds && (((selectedPlayerPassYds/leagueAverages['QB'].pass_yds_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['QB'].pass_yds_avg > selectedPlayerPassYds && selectedPlayerPassYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Pass TDs</span>
                                        <span><% selectedPlayerPassTds | number:2 %></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Ints</span>
                                        <span><% selectedPlayerPassInt | number:2 %></span>
                                    </div>
                                </div>
                                <div class="mt10">
                                    <table class="game-log-table" ng-show="selectedPlayerGameLogs.length > 0">
                                        <thead>
                                            <th>Week #</th>
                                            <th>Opp</th>
                                            <th>Result</th>
                                            <th>Pass Cmp</th>
                                            <th>Pass Atts</th>
                                            <th>Pass Yds</th>
                                            <th>Pass TDs</th>
                                            <th>FPs</th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="game in selectedPlayerGameLogs">
                                                <td class="text-center"><% game.week_num %></td>
                                                <td class="text-center"><% game.opp %></td>
                                                <td class="text-center"><% game.game_result %></td>
                                                <td class="text-center"><% game.pass_cmp %></td>
                                                <td class="text-center"><% game.pass_att %></td>
                                                <td class="text-center"><% game.pass_yds %></td>
                                                <td class="text-center"><% game.pass_td %></td>
                                                <td class="text-center"><% game.fp | number:2 %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-content" ng-show="showRB">
                                <div class="inline-block">
                                    <div>
                                        <span class="stat-label">Rush Att</span>
                                        <span><% selectedPlayerRushAtts | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerRushAtts/leagueAverages['RB'].rush_att_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['RB'].rush_att_avg < selectedPlayerRushAtts && (((selectedPlayerRushAtts/leagueAverages['RB'].rush_att_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="parseFloat(leagueAverages['RB'].rush_att_avg) > selectedPlayerRushAtts && selectedPlayerRushAtts > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Rush Yds</span>
                                        <span><% selectedPlayerRushYds | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerRushYds/leagueAverages['RB'].rush_yds_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['RB'].rush_yds_avg < selectedPlayerRushYds && (((selectedPlayerRushYds/leagueAverages['RB'].rush_yds_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['RB'].rush_yds_avg > selectedPlayerRushYds && selectedPlayerRushYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">YPC</span>
                                        <span><% (selectedPlayerRushYds / selectedPlayerRushAtts) | number:2 %></span>
                                    </div>
                                </div>
                                <div class="inline-block ml10">
                                    <div>
                                        <span class="stat-label">Targets</span>
                                        <span><% selectedPlayerTgts | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerTgts/leagueAverages['RB'].targets_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['RB'].targets_avg < selectedPlayerTgts && (((selectedPlayerTgts/leagueAverages['RB'].targets_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['RB'].targets_avg > selectedPlayerTgts && selectedPlayerRushYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Rec Yds</span>
                                        <span><% selectedPlayerRecYds | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerRecYds/leagueAverages['RB'].pass_yds_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['RB'].pass_yds_avg < selectedPlayerRecYds && (((selectedPlayerRecYds/leagueAverages['RB'].pass_yds_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['RB'].pass_yds_avg > selectedPlayerRecYds && selectedPlayerRecYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Rush TDs</span>
                                        <span><% selectedPlayerRushTds | number:2 %></span>
                                    </div>
                                </div>
                                <div class="mt10">
                                    <table class="game-log-table" ng-show="selectedPlayerGameLogs.length > 0">
                                        <thead>
                                            <th>Week #</th>
                                            <th>Opp</th>
                                            <th>Result</th>
                                            <th>Atts</th>
                                            <th>Yds</th>
                                            <th>TDs</th>
                                            <th>Targets</th>
                                            <th>Rec Yds</th>
                                            <th>Rec TDs</th>
                                            <th>FPs</th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="game in selectedPlayerGameLogs">
                                                <td class="text-center"><% game.week_num %></td>
                                                <td class="text-center"><% game.opp %></td>
                                                <td class="text-center"><% game.game_result %></td>
                                                <td class="text-center"><% game.rush_att %></td>
                                                <td class="text-center"><% game.rush_yds %></td>
                                                <td class="text-center"><% game.rush_td %></td>
                                                <td class="text-center"><% game.targets %></td>
                                                <td class="text-center"><% game.rec_yds %></td>
                                                <td class="text-center"><% game.rec_td %></td>
                                                <td class="text-center"><% game.fp | number:2 %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-content" ng-show="showWR">
                                <div class="inline-block ml10">
                                    <div>
                                        <span class="stat-label">Targets</span>
                                        <span><% selectedPlayerTgts | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerTgts/leagueAverages['WR'].targets_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['WR'].targets_avg < selectedPlayerTgts && (((selectedPlayerTgts/leagueAverages['WR'].targets_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['WR'].targets_avg > selectedPlayerTgts && selectedPlayerRushYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Rec Yds</span>
                                        <span><% selectedPlayerRecYds | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerRecYds/leagueAverages['WR'].pass_yds_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['WR'].pass_yds_avg < selectedPlayerRecYds && (((selectedPlayerRecYds/leagueAverages['WR'].pass_yds_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['WR'].pass_yds_avg > selectedPlayerRecYds && selectedPlayerRecYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Rec TDs</span>
                                        <span><% selectedPlayerRecTds | number:2 %></span>
                                    </div>
                                </div>
                                <div class="inline-block">
                                    <div>
                                        <span class="stat-label">Recptions</span>
                                        <span><% selectedPlayerRec | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerRec/leagueAverages['WR'].rec_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['WR'].rec_avg < selectedPlayerRec && (((selectedPlayerRec/leagueAverages['WR'].rec_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['WR'].rec_avg > selectedPlayerRec && selectedPlayerRec > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Catch %</span>
                                        <span><% (selectedPlayerRec / selectedPlayerTgts) * 100 | number:2 %>%</span>
                                    </div>
                                    <div>
                                        <span class="stat-label">YPC</span>
                                        <span><% (selectedPlayerRecYds / selectedPlayerRec) | number:2 %></span>
                                    </div>
                                </div>
                                <div class="mt10">
                                    <table class="game-log-table" ng-show="selectedPlayerGameLogs.length > 0">
                                        <thead>
                                            <th>Week #</th>
                                            <th>Opp</th>
                                            <th>Result</th>
                                            <th>Targets</th>
                                            <th>Rec</th>
                                            <th>Rec Yds</th>
                                            <th>Catch %</th>
                                            <th>Rec TDs</th>
                                            <th>FPs</th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="game in selectedPlayerGameLogs">
                                                <td class="text-center"><% game.week_num %></td>
                                                <td class="text-center"><% game.opp %></td>
                                                <td class="text-center"><% game.game_result %></td>
                                                <td class="text-center"><% game.targets %></td>
                                                <td class="text-center"><% game.rec %></td>
                                                <td class="text-center"><% game.rec_yds %></td>
                                                <td class="text-center"><% game.targets > 0 ? (game.rec / game.targets) * 100 : 0 | number:2 %>%</td>
                                                <td class="text-center"><% game.rec_td %></td>
                                                <td class="text-center"><% game.fp | number:2 %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="card-content" ng-show="showTE">
                                <div class="inline-block ml10">
                                    <div>
                                        <span class="stat-label">Targets</span>
                                        <span><% selectedPlayerTgts | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerTgts/leagueAverages['TE'].targets_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['TE'].targets_avg < selectedPlayerTgts && (((selectedPlayerTgts/leagueAverages['TE'].targets_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['TE'].targets_avg > selectedPlayerTgts && selectedPlayerRushYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Rec Yds</span>
                                        <span><% selectedPlayerRecYds | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerRecYds/leagueAverages['TE'].pass_yds_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['TE'].pass_yds_avg < selectedPlayerRecYds && (((selectedPlayerRecYds/leagueAverages['TE'].pass_yds_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['TE'].pass_yds_avg > selectedPlayerRecYds && selectedPlayerRecYds > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Rec TDs</span>
                                        <span><% selectedPlayerRecTds | number:2 %></span>
                                    </div>
                                </div>
                                <div class="inline-block">
                                    <div>
                                        <span class="stat-label">Recptions</span>
                                        <span><% selectedPlayerRec | number:2 %></span>
                                        <span class="darkorange stat-level" title="Greatly Exceeds League Avg" ng-show="(((selectedPlayerRec/leagueAverages['TE'].rec_avg) * 100) - 100) > 20"><i class="material-icons">grade</i></span>
                                        <span class="green stat-level" title="Above League Avg" ng-show="leagueAverages['TE'].rec_avg < selectedPlayerRec && (((selectedPlayerRec/leagueAverages['TE'].rec_avg) * 100) - 100) < 20"><i class="material-icons">arrow_upward</i></span>
                                        <span class="red stat-level" title="Below League Avg" ng-show="leagueAverages['TE'].rec_avg > selectedPlayerRec && selectedPlayerRec > 0"><i class="material-icons">arrow_downward</i></span>
                                    </div>
                                    <div>
                                        <span class="stat-label">Catch %</span>
                                        <span><% (selectedPlayerRec / selectedPlayerTgts) * 100 | number:2 %>%</span>
                                    </div>
                                    <div>
                                        <span class="stat-label">YPC</span>
                                        <span><% (selectedPlayerRecYds / selectedPlayerRec) | number:2 %></span>
                                    </div>
                                </div>
                                <div class="mt10">
                                    <table class="game-log-table" ng-show="selectedPlayerGameLogs.length > 0">
                                        <thead>
                                            <th>Week #</th>
                                            <th>Opp</th>
                                            <th>Result</th>
                                            <th>Targets</th>
                                            <th>Rec</th>
                                            <th>Rec Yds</th>
                                            <th>Catch %</th>
                                            <th>Rec TDs</th>
                                            <th>FPs</th>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="game in selectedPlayerGameLogs">
                                                <td class="text-center"><% game.week_num %></td>
                                                <td class="text-center"><% game.opp %></td>
                                                <td class="text-center"><% game.game_result %></td>
                                                <td class="text-center"><% game.targets %></td>
                                                <td class="text-center"><% game.rec %></td>
                                                <td class="text-center"><% game.rec_yds %></td>
                                                <td class="text-center"><% game.targets > 0 ? (game.rec / game.targets) * 100 : 0 | number:2 %>%</td>
                                                <td class="text-center"><% game.rec_td %></td>
                                                <td class="text-center"><% game.fp | number:2 %></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts.corejs')