<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/roster_upload', 'DKController@rosterUpload');

Route::get('/players', 'DKController@players');

Route::get('/defense', 'DKController@defense');

Route::get('/depth_chart', 'DKController@depth_chart');

Route::get('/get_defense', 'DKController@get_defense');

Route::get('/upload', 'DKController@upload');

Route::get('/leagueavgs', 'DKController@league_avgs');

Route::get('/get_games_depth_chart', 'DKController@getGamesDepthChart');

Route::get('/get_games', 'DKController@getGames');

Route::get('/player_type', 'DKController@player_type');

Route::get('/sync', 'DKController@sync');

Route::get('/get_player', 'DKController@getPlayer');

Route::get('/sync_player', 'DKController@syncPlayer');

Route::get('/sync_team', 'DKController@syncTeam');

Route::get('/sync_depth_chart', 'DKController@syncDepthChart');

Route::get('/data_analysis', 'DKController@dataAnalysis');

Route::get('/generate_lineups', 'DKController@generateLineups');

Route::post('/lineup_criteria', 'DKController@lineupCriteria');

Route::post('/lineup_showdown_criteria', 'DKController@lineupShowdownCriteria');

Route::get('/generate_rosters', 'DKController@generateRosters');