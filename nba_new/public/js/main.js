// This example uses jQuery so it creates the Dropzone, only when the DOM has
// loaded.

// Disabling autoDiscover, otherwise Dropzone will try to attach twice.
Dropzone.autoDiscover = false;
// or disable for specific dropzone:
Dropzone.options.dzContainer = false;


$(function() {
	var dzContainer = new Dropzone("#dz-container", { url: "/roster_upload"});

	dzContainer.on("complete", function(file) {
		// alert('success');
	});

	dzContainer.on("error", function(file) {
	});
})