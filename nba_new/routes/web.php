<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/roster_upload', 'DKController@rosterUpload');

Route::get('/players', 'DKController@players');

Route::get('/sync', 'DKController@sync');

Route::get('/sync_player', 'DKController@syncPlayer');

Route::get('/sync_team', 'DKController@syncTeam');