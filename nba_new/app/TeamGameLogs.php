<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamGameLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'team_game_logs';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'game_id';

    public $timestamps  = false;

    protected $fillable = ['team_id', 'game_num', 'opp_id', 'game_result', 'game_location', 'pts', 'opp_pts', 'fg', 'fga', 'fg3', 'fg3a', 'ft', 'fta', 'orb', 'trb', 'ast', 'stl', 'blk', 'tov', 'pf', 'opp_fg', 'opp_fga', 'opp_fg3', 'opp_fg3a', 'opp_ft', 'opp_fta', 'opp_orb', 'opp_trb', 'opp_ast', 'opp_stl', 'opp_blk', 'opp_tov', 'opp_pf'];
}
