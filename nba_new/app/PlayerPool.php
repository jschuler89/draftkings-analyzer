<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerPool extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_pool';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'player_pool_id';

    public $timestamps  = false;

    protected $fillable = ['player_pool_id', 'player_id', 'fppg', 'salary', 'opp', 'game_date', 'game_time', 'dk_player_id', 'injury', 'mp'];
}
