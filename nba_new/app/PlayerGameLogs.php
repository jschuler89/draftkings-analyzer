<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlayerGameLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'player_game_logs';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'game_id';

    public $timestamps  = false;

    protected $fillable = ['player_id', 'date_game', 'game_num', 'team_id', 'opp_id', 'game_result', 'game_location', 'mp', 'fg', 'fga', 'fg3', 'fg3a', 'ft', 'fta', 'orb', 'drb', 'trb', 'ast', 'stl', 'blk', 'tov', 'pf', 'pts'];
}
