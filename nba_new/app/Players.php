<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Players extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'players';

    /**
     * Primary key.
     *
     * @var string
     */
    protected $primaryKey = 'player_id';

    public $timestamps  = false;

    protected $fillable = ['player_id', 'name', 'team', 'position', 'url_param', 'img', 'dfg'];

    public static function getPlayerByPlayerId($player_id)
    {
        $player = Players::select('players.*')->where('player_id', $player_id)->get()->first();

        return $player;
    }
}
