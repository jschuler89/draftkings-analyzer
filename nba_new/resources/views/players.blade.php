@extends('layouts.home')
@section('title', 'Draftkings Roster Analyzer')
@section('css')
    <link href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" rel="stylesheet">
@endsection
@section('scripts')
        @parent
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="{{ asset('js/players.js') }}"></script>
@endsection
@section('header_title')
<div class="title m-b-md">
    Draftkings Roster Analyzer
</div>
@endsection
@section('content')
    @include('layouts.nav')
    <table class="players-table">
        <thead>
            <tr>
                <th data-sortable="true">Player Name</th>
                <th data-sortable="true">Position</th>
                <th data-sortable="true">Team</th>
                <th data-sortable="true">Opp</th>
                <th data-sortable="true">Time</th>
                <th data-sortable="true">Salary</th>
                <th data-sortable="true">Pt / $</th>
                <th data-sortable="true">Last 5 MP / G</th>
                <th data-sortable="true">MP / G</th>
                <th data-sortable="true">Last 8 Team Pts / G</th>
                <th data-sortable="true">Last 8 Opp Pts / G</th>
                <th data-sortable="true">Away Pts / G</th>
                <th data-sortable="true">Home Pts / G</th>
                <th data-sortable="true">Pts / G</th>
                <th data-sortable="true">Min Proj Pts</th>
                <th data-sortable="true">Max Proj Pts</th>
                <th data-sortable="true">Proj Pts</th>
                <th data-sortable="true">Proj Pts (FG %)</th>
                <th data-sortable="true">FG %</th>
                <th data-sortable="true">FGA / G</th>
                <th data-sortable="true">FGA % (Team)</th>
                <th data-sortable="true">Last 3 Proj FGA</th>
                <th data-sortable="true">Proj FGA</th>
                <th data-sortable="true">3PM / G</th>
                <th data-sortable="true">3PA / G</th>
                <th data-sortable="true">3P % / G</th>
                <th data-sortable="true">Ast / G</th>
                <th data-sortable="true">Proj Ast</th>
                <th data-sortable="true">Max Proj Ast</th>
                <th data-sortable="true">Reb / G</th>
                <th data-sortable="true">Proj Reb</th>
                <th data-sortable="true">Max Proj Reb</th>
                <th data-sortable="true">Stl / G</th>
                <th data-sortable="true">Blk / G</th>
                <th data-sortable="true">Tov / G</th>
                <th data-sortable="true">Floor</th>
                <th data-sortable="true">Ceiling</th>
                <th data-sortable="true">Last 3 FPPG</th>
                <th data-sortable="true">Last 5 FPPG</th>
                <th data-sortable="true">FPPG</th>
                <th data-sortable="true">Proj FP</th>
                <th data-sortable="true">Last 5 Consistency</th>
                <th data-sortable="true">Consistency</th>
                <th data-sortable="true">%2x</th>
                <th data-sortable="true">%3x</th>
                <th data-sortable="true">%4x</th>
                <th data-sortable="true">%5x</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($players as $player)
            <tr>
                <td><b>{{ $player->playerData['name'] }}</b></td>
                <td>{{ $player->playerData['position'] }}</td>
                <td>{{ $player->playerData['team'] }}</td>
                <td>{{ $player->playerData['opp'] }}</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
                <td>1</td>
            </tr>
            @endforeach
        </tbody>
    </table>

@endsection