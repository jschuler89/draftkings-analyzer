<?php

$user = 'root';
$password = 'root';
$db = 'dk_nba';
$host = 'localhost';
$port = 3306;

$link = mysqli_init();
$success = mysqli_real_connect(
   $link, 
   $host, 
   $user, 
   $password, 
   $db,
   $port
);


ini_set('max_execution_time', -1);
ini_set('display_errors', 1);  
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once 'classes/DK_DB.php';
require_once 'config.php';

$slates = array();

$dk_db = new DK_DB();
$players = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'UTIL', $slates);
$teams = $dk_db->getCompiledTeamStats($gameDate);
$teamsArray = array();

foreach($teams as $_team) {
	$teamsArray[strtolower($_team['team'])] = $_team;
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Draftkings Lineups Builder</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.js"></script>
	</head>

	<style type="text/css">
		.mb10 {
			margin-bottom: 10px;
		}
		.lineup-template span {
		    display: inline-block;
		}
		.lineup-template .position {
			width: 30px;
		}
		.lineup-template .fppg {
			width: 36px;
		}
		.lineup-template .salary {
			width: 48px;
		}
		.lineup-template .name {
			width: 170px;
		}
		.lineup-template .totals .salary-container {
			position: relative;
		}
		.salary-left {
		    position: absolute;
		    left: -18px;
		    top: 15px;
		}
	</style>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('table').bootstrapTable();
			var lineup_salary = 0;
			var lineup_fp = 0;
			var players = [];

			jQuery(document).on('click', '.clear', function() {
				players = [];
				lineup_fp = 0;
				lineup_salary = 0;
				jQuery('.lineup-template').find('.name').text('');
				jQuery('.lineup-template').find('.salary').text('');
				jQuery('.lineup-template').find('.fppg').text('');

				jQuery('.totals .salary').text('0');
				jQuery('.totals .fppg').text('0.00');
				jQuery('.salary-left').text('($6250)');
			});

			jQuery(document).on('click', '.position-filter', function() {
				filterPosition = jQuery(this).text().toLowerCase();

				jQuery('table tbody tr').hide();

				jQuery('.position-'+filterPosition).show();

				if(filterPosition == 'util') {
					jQuery('table tbody tr').show();
				}

				if(filterPosition == 'pg') {
					jQuery('.position-pg_sg').show();
					jQuery('.position-pg_sf').show();
				}

				if(filterPosition == 'sg') {
					jQuery('.position-pg_sg').show();
					jQuery('.position-sg_sf').show();
				}

				if(filterPosition == 'sf') {
					jQuery('.position-pg_sf').show();
					jQuery('.position-sg_sf').show();
					jQuery('.position-sf_pf').show();
				}

				if(filterPosition == 'pf') {
					jQuery('.position-sf_pf').show();
					jQuery('.position-pf_c').show();
				}

				if(filterPosition == 'c') {
					jQuery('.position-pf_c').show();
				}

				if(filterPosition == 'g') {
					jQuery('.position-pg').show();
					jQuery('.position-sg').show();
					jQuery('.position-sg_sf').show();
					jQuery('.position-pg_sg').show();
					jQuery('.position-pg_sf').show();
				}

				if(filterPosition == 'f') {
					jQuery('.position-sg_sf').show();
					jQuery('.position-sf').show();
					jQuery('.position-pf').show();
					jQuery('.position-sf_pf').show();
					jQuery('.position-pf_c').show();
				}

			});	

			jQuery(document).on('click', 'table tr', function() {
				var player_id = jQuery(this).attr('data-playerid')
				var position = jQuery(this).attr('data-position');
				var player_name = jQuery(this).find('.name').text();
				var player_salary = jQuery(this).find('.salary').text();
				var player_fp = jQuery(this).find('.fppg').text();
				var position_array = position.split('/');
				var allgood = false;

				if(players.indexOf(player_id) >= 0) {
					return false;
				} 

				for(var i in position_array) {
					var tmpposition = position_array[i];

					if(jQuery('.'+tmpposition).find('.name').text() == '') {
						position = tmpposition;
						allgood = true;
						break;
					}

					if(tmpposition == 'c' && jQuery('.util').find('.name').text() == '') {
						position = 'util';
						allgood = true;
						break;
					}
				}

				if(!allgood){
					if(jQuery('.g').find('.name').text() == '' && position.includes('g')) {
						position = 'g';
					} else if(jQuery('.f').find('.name').text() == '' && position.includes('f')) {
						position = 'f';
					} else if(jQuery('.f').find('.name').text() != '' && position.includes('f') && jQuery('.util').find('.name').text() == '') {
						position = 'util';
					} else if(jQuery('.g').find('.name').text() != '' && position.includes('g') && jQuery('.util').find('.name').text() == '') {
						position = 'util';
					} else {
						return false;
					}
				}

				players.push(player_id);

				lineup_salary += parseInt(player_salary.replace('$', ''));
				lineup_fp += parseFloat(player_fp);

				var salary_left = 50000 - lineup_salary;

				if(players.length < 8) {
					var salary_per_player = salary_left / (8 - players.length);
				} else {
					var salary_per_player = salary_left;
				}

				jQuery('.totals .salary').text(lineup_salary);
				jQuery('.totals .fppg').text(lineup_fp.toFixed(2));
				jQuery('.salary-left').text('($'+parseFloat(salary_per_player).toFixed(2)+')');
				jQuery('.'+position).find('.name').text(player_name);
				jQuery('.'+position).find('.salary').text(player_salary);
				jQuery('.'+position).find('.fppg').text(player_fp);
			});
		});
	</script>

	<body>
		<div>
			<nav class="navbar navbar-default">
			  	<div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="">DK Roster Analyzer</a>
				    </div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="/nba/data_analysis.php">Data Analysis</a></li>
				        <li><a href="/nba/lineups_detail.php">Detail Lineups</a></li>
				        <li class="active"><a href="/nba/createlineups.php">Create Lineups</a></li>
				        <li><a href="/nba/generatelineups.php">Generate Lineups</a></li>
				        <li><a href="/nba/sync.php">Sync</a></li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
		    	</div>
		    </nav>
		    <div class="col-md-12 mb10">
			    <div>
					<span><button class="btn btn-primary btn-sm position-filter">PG</button></span>
					<span><button class="btn btn-primary btn-sm position-filter">SG</button></span>
					<span><button class="btn btn-primary btn-sm position-filter">SF</button></span>
					<span><button class="btn btn-primary btn-sm position-filter">PF</button></span>
					<span><button class="btn btn-primary btn-sm position-filter">C</button></span>
					<span><button class="btn btn-primary btn-sm position-filter">G</button></span>
					<span><button class="btn btn-primary btn-sm position-filter">F</button></span>
					<span><button class="btn btn-primary btn-sm position-filter">UTIL</button></span>
				</div>
			</div>
			<div class="col-md-5">
				<table class="table table-hover table-responsive">
					<thead>
						<tr>
							<th data-sortable="true">Name</th>
							<th data-sortable="true">Team</th>
							<th data-sortable="true">Position</th>
							<th data-sortable="true">Salary</th>
							<th data-sortable="true">FP</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($players as $player): ?>
							<?php 
									$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_pg']) + $player['pts_intercept']['b'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];

								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['player_pool_mp'];
								  	$manuallyUpdated = false;

								  	if($player['player_pool_mp'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	if(($player['mp_5'] > $player['player_pool_mp']) && $manuallyUpdated) {
								  		$mins_to_use = $player['mp_5'];
								  	}

								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);
							?>
							<tr class="position-<?php echo strtolower(str_replace('/', '_', $player['position'])) ?>" data-position="<?php echo strtolower($player['position']) ?>" data-playerid="<?php echo $player['player_id']?>">
								<td class="name"><?php echo $player['name'] ?></td>
								<td><?php echo $player['team'] ?></td>
								<td><?php echo $player['position'] ?></td>
								<td class="salary">$<?php echo $player['salary'] ?></td>
								<td class="fppg"><?php echo number_format($proj_fp, 2) ?></td>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
			<div class="col-md-5 lineup-template">
				<div class="pg">
					<span class="position">PG</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="sg">
					<span class="position">SG</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="sf">
					<span class="position">SF</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="pf">
					<span class="position">PF</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="c">
					<span class="position">C</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="g">
					<span class="position">G</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="f">
					<span class="position">F</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="util">
					<span class="position">UTIL</span>
					<span class="name"></span>
					<span class="salary"></span>
					<span class="fppg"></span>
				</div>
				<div class="totals">
					<span class="position"><button class="clear btn btn-sm btn-primary">Clear</button></span>
					<span class="name">&nbsp;</span>
					<span class="salary-container"><span class="salary">0</span> <span class="salary-left">($6250)</span></span>
					<span class="fppg">0.00</span>
				</div>
			</div>
		</div>
	</body>
</html>