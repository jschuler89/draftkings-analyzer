<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
require_once 'config.php';
include_once 'functions/get_player_functions.php';

$params = $_GET;
// $params['player_id'] = 1;

$dk_db = new DK_DB();
$json = array();

switch($params['type']) {
	case 'high_fga':
		$json = $dk_db->getHighFGA($gameDate, $params['position']);
		break;
	case 'high_pt':
		$json = $dk_db->getHighPtSalary($gameDate, $params['position']);
		break;
	case 'high_consist':
		$json = $dk_db->getHighConsist($gameDate, $params['position']);
		break;
	case 'fp_increase':
		$json = $dk_db->getFPIncrease($gameDate, $params['position']);
		break;
	case 'min_increase':
		$json = $dk_db->getMinsIncrease($gameDate, $params['position']);
		break;
}

echo json_encode($json);