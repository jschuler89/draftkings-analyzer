<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';
$dk_db = new DK_DB();
$teams = $dk_db->getCompiledTeamStats($gameDate);
foreach($teams as $_team) {
	$teamsArray[strtolower($_team['team'])] = $_team;
}

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Draftkings Roster Analyzer</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.2.0/css/mdb.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.js"></script>
		<script src="js/mdb.js"></script>
		<script src="https://use.fontawesome.com/b1353e11e4.js"></script>
	</head>
	<body>
		<div>
			<nav class="navbar navbar-default">
			  	<div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="">DK Roster Analyzer</a>
				    </div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="/nba/data_analysis.php">Data Analysis</a></li>
				        <li><a href="/nba/lineups_detail.php">Detail Lineups</a></li>
				        <li><a href="/nba/createlineups.php">Create Lineups</a></li>
				        <li><a href="/nba/generatelineups.php">Generate Lineups</a></li>
				        <li><a href="/nba/sync.php">Sync</a></li>
				        <li class="active"><a href="/nba/sim.php">Simulation</a></li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
		    	</div>
		    </nav>
		</div>
		<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery(document).on('click', '.run-sim', function() {
					var team = $(this).attr('team');
					// var opp = $(this).attr('opp');

					jQuery.ajax({
						url: '/nba/get_sim_data.php?team=' + team,
						type: 'GET'
						// dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
					})
					.done(function() {
						console.log("success");
					})
					.fail(function() {
						console.log("error");
					})
					.always(function() {
						console.log("complete");
					});
					
				})

			});
		</script>
		<div class="col-md-12">
			<?php foreach($teamsArray as $key => $_team): ?>
				<div class="col-md-3">
					<span><?php echo strtoupper($key) ?> vs <?php echo $_team['opp'] ?></span>
					<button class="btn btn-success run-sim" team="<?php echo $key ?>" opp="<?php echo $_team['opp'] ?>" type="button">Run</button>
				</div>
			<?php endforeach ?>
		</div>
		<div>
			
		</div>
	</body>
</html>