<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
include_once 'config.php';
include_once 'functions/get_player_functions.php';

$params = $_GET;
// $params['player_id'] = 1;

$dk_db = new DK_DB();
$_team = $dk_db->getTeamById($params['team_id']);
$_team = $_team[0];
$team_id = $_team['team_id'];
$position = $_team['position'];
$team = $_team['team'];

$team_url_param = $_team['url_param'];

if($team_url_param == null) {
	$return = array('error' => "Couldn't get team info");

	echo json_encode($return);
	die;
}

// $teamStats = getTeamStats($team_url_param);

// $dk_db->insertTeamStats($team_id, $teamStats);

$teamLog = getTeamGameLogs($team_url_param);

$dk_db->insertTeamGameLog($team_id, $teamLog);

echo json_encode(array('name' => $_team['team']));