<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';

$dk_db = new DK_DB();
$upload = $_FILES;
$alreadyLoaded = false;

if(empty($_FILES['uploadfile']['tmp_name'])) {
	header('Location: home.php');
}

$csv = file_get_contents($_FILES['uploadfile']['tmp_name']);

// "Position","Name","Salary","GameInfo","AvgPointsPerGame","teamAbbrev"
$csvArray = explode("\n", $csv);

$selectSmt = "SELECT * FROM player_pool where game_date = '$gameDate'";

$rs = $dk_db->query($selectSmt);

if(isset($rs[0]['player_id'])) {
	$alreadyLoaded = true;
}


//Position,Name + ID,Name,ID,Roster Position,Salary,Game Info,TeamAbbrev,AvgPointsPerGame
//SF/PF,Giannis Antetokounmpo (11532260),Giannis Antetokounmpo,11532260,SF/PF/F/UTIL,11600,TOR@MIL 10/29/2018 08:00PM ET,MIL,55.21
if(!$alreadyLoaded) {
	foreach($csvArray as $index => $_csvRow) {
		if($index == 0) {
			continue;
		}

		$playerInfo = explode(",", $_csvRow);

		if(!isset($playerInfo[1])) {
			continue;
		}

		$player_id = $dk_db->insertPlayer($playerInfo);

		$dk_db->insertIntoPlayerPool($player_id, $playerInfo, $gameDate);
	}
}

header('Location: lineups_detail.php');
