<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';

$dk_db = new DK_DB();
$fgaArray = array();
$gameScoreArray = array();
$valueArray = array();
$position = $argv[1];

if(isset($argv[2])) {
	$salary = $argv[2];
} else {
	$salary = false;
}
// var_dump($argv); // arguments
// 
$teamSql = "SELECT * FROM teams WHERE url_param = '$position'";
$team = $dk_db->query($teamSql);

if($team) {
	// get home stats
	$teamOppStatsSql = "SELECT avg(opp_pts) as avg_opp_pts from team_game_logs WHERE game_location = '' and team_id = " . $team[0]['team_id'];
	$teamOppStats = $dk_db->query($teamOppStatsSql);
	$teamStatsSql = "SELECT avg(pts) as avg_pts from team_game_logs WHERE game_location = '' and team_id = " . $team[0]['team_id'];
	$teamStats = $dk_db->query($teamStatsSql);
	echo "Teams avg " . $teamOppStats[0]['avg_opp_pts'] . " while at home" . PHP_EOL;
	echo "{$team[0]['team']} avgs " . $teamStats[0]['avg_pts'] . " while at home" . PHP_EOL . PHP_EOL;

	// get away stats
	$teamOppStatsSql = "SELECT avg(opp_pts) as avg_opp_pts from team_game_logs WHERE game_location = '@' and team_id = " . $team[0]['team_id'];
	$teamOppStats = $dk_db->query($teamOppStatsSql);
	$teamStatsSql = "SELECT avg(pts) as avg_pts from team_game_logs WHERE game_location = '@' and team_id = " . $team[0]['team_id'];
	$teamStats = $dk_db->query($teamStatsSql);
	echo "Teams avg " . $teamOppStats[0]['avg_opp_pts'] . " while away" . PHP_EOL;
	echo "{$team[0]['team']} avgs " . $teamStats[0]['avg_pts'] . " while away" . PHP_EOL;

	exit();
}


// search for best player via position

if($salary) {
	if($position == 'ALL') {
		$playerPoolSql = "SELECT * FROM player_pool pp LEFT JOIN players p on p.player_id = pp.player_id WHERE pp.game_date = '$gameDate' and injury is null and salary <= $salary";
	} else {
		$playerPoolSql = "SELECT * FROM player_pool pp LEFT JOIN players p on p.player_id = pp.player_id WHERE pp.game_date = '$gameDate' and p.position like '%$position%' and injury is null and salary <= $salary";
	}
} else {
	if($position == 'ALL') {
		$playerPoolSql = "SELECT * FROM player_pool pp LEFT JOIN players p on p.player_id = pp.player_id WHERE pp.game_date = '$gameDate' and injury is null";
	} else {
		$playerPoolSql = "SELECT * FROM player_pool pp LEFT JOIN players p on p.player_id = pp.player_id WHERE pp.game_date = '$gameDate' and p.position like '%$position%' and injury is null";
	}
}
$playerPool = $dk_db->query($playerPoolSql);

foreach($playerPool as $_player) {
	// echo PHP_EOL;
	$player_id = $_player['player_id'];
	$player_name = $_player['name'];
	$playerGameLogSql = "SELECT * FROM player_game_logs WHERE player_id = $player_id";
	$playerGameLog = $dk_db->query($playerGameLogSql);
	// echo "Looking at $player_name" . PHP_EOL; 
	// echo "SHOOTING:" . PHP_EOL; 
	getShootingData($playerGameLog, $player_name, $fgaArray);
	// echo "BEST GAME: ";
	getGameScoreData($playerGameLog, $player_name, $gameScoreArray);
	$valueArray[$player_name] = ($_player['salary'] / 1000) * 5;
	// echo PHP_EOL;
}

arsort($fgaArray);
arsort($gameScoreArray);
arsort($valueArray);

// echo "Sorted Avg FGA:" . PHP_EOL;
// foreach($fgaArray as $key => $_fg) {
// 	echo "$key {$_fg}" . PHP_EOL;
// }
// echo PHP_EOL;

echo "Sorted Avg Game Scores:" . PHP_EOL;
foreach($gameScoreArray as $key => $_fg) {
	echo "$key {$_fg}" . PHP_EOL;
}
echo PHP_EOL;

// echo "FP Needed to Hit 5x Value:" . PHP_EOL;
// foreach($valueArray as $key => $_fg) {
// 	echo "$key {$_fg}" . PHP_EOL;
// }


function getShootingData($gameLog, $player_name, &$playerArray)
{
	$bestGame = 0;
	$bestGameFg = 0;
	$bestGameFgA = 0;
	$worstGame = 1;
	$worstGameFg = 0;
	$worstGameFgA = 0;
	$bestOpp = '';
	$worstOpp = '';
	$counter = 1;
	$totalFg = 0;
	$totalFgA = 0;
	foreach($gameLog as $_game) {
		$fg = $_game['fg'];
		$fga = $_game['fga'];
		$totalFg += $_game['fg'];
		$totalFgA += $_game['fga'];

		if($fga > 0) {
			$fgPct = $fg/$fga;
		} else {
			$fgPct = 0;
		}

		if($fgPct > $bestGame) {
			$bestGameFg = $fg;
			$bestGameFgA = $fga;
			$bestGame = $fgPct;
			$bestOpp = $_game['opp_id'];
		}

		if($fgPct < $worstGame) {
			$worstGameFg = $fg;
			$worstGameFgA = $fga;
			$worstGame = $fgPct;
			$worstOpp = $_game['opp_id'];
		}

		$counter++;
	}


	if($totalFgA > 0) {
		$totalFgPct = $totalFg/$totalFgA;
	} else {
		$totalFgPct = 0;
	}

	$avgFgA = round($totalFgA/$counter, 2);
	$playerArray[$player_name] = $avgFgA;

	// echo 'Best shooting game was ' . round($bestGame * 100) . "% against $bestOpp he shot $bestGameFg for $bestGameFgA" . PHP_EOL;
	// echo 'Worst shooting game was ' . round($worstGame * 100) . "% against $worstOpp he shot $worstGameFg for $worstGameFgA" . PHP_EOL;
	// echo 'Average shooting % is ' . round($totalFgPct * 100) . "%" . PHP_EOL;
	// echo 'He takes around ' . $avgFgA . ' shots per game ' . PHP_EOL;
}

function getGameScoreData($gameLog, $player_name, &$playerArray)
{
	/**
 * Game Score = Points Scored + (0.4 x Field Goals) – (0.7 x Field Goal Attempts) – (0.4 x (Free Throw Attempts – Free Throws)) + (0.7 x Offensive Rebounds) + (0.3 x Defensive Rebounds) + Steals + (0.7 x Assists) + (0.7 x Blocks) – (0.4 x Personal Fouls) – Turnovers
 **/

	$bestGameScore = 0;
	$bestPts = 0;
	$bestAst = 0;
	$bestReb = 0;
	$bestStl = 0;
	$bestBlk = 0;
	$counter = 1;
	$totalGameScore = 0;
	foreach($gameLog as $_game) {
		$pts = $_game['pts'];
		$fg = $_game['fg'];
		$fga = $_game['fga'];
		$ft = $_game['ft'];
		$fta = $_game['fta'];
		$orb = $_game['orb'];
		$drb = $_game['drb'];
		$stl = $_game['stl'];
		$blk = $_game['blk'];
		$pf = $_game['pf'];
		$tov = $_game['tov'];
		$ast = $_game['ast'];

		$gameScore = $pts + (0.4 * $fg) - (0.7 * $fga) - (0.4 * ($fta - $ft)) + (0.7 * $orb) + (0.3 * $drb) + $stl + (0.7 * $ast) + (0.7 * $blk) - (0.4 * $pf) - $tov;

		if($gameScore > $bestGameScore) {
			$bestPts = $_game['pts'];
			$bestAst = $_game['ast'];
			$bestReb = $_game['orb'] + $_game['drb'];
			$bestStl = $_game['stl'];
			$bestBlk = $_game['stl'];
			$pts = $_game['pts'];
			$bestGameScore = $gameScore;
			$opp = $_game['opp_id'];
		}

		$totalGameScore += $gameScore;

		$counter++;
	}

	$playerArray[$player_name] = round($totalGameScore / $counter, 2);

	// echo "His best game was against $opp with $bestPts pts, $bestAst ast, $bestReb reb, $bestStl stl, and $bestBlk blk" . PHP_EOL;
}