import { Component, OnInit } from '@angular/core';
import { NBA } from '../classes/nba';
import { PLAYERS } from '../mocks/mock-players';

@Component({
  selector: 'app-dk',
  templateUrl: './dk.component.html',
  styleUrls: ['./dk.component.css']
})
export class DkComponent implements OnInit {
  nba: NBA = {
  	id: 1,
  	name: 'James Harden'
  };
  players = PLAYERS;
  constructor() { }

  ngOnInit() {
  }

}
