<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';

$dk_db = new DK_DB();
$players = $dk_db->getPlayerPool($gameDate);
$gameTimes = $dk_db->getGameTimes($gameDate);

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Draftkings Roster Analyzer</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.2.0/css/mdb.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>
		<script src="js/mdb.js"></script>
		<style type="text/css">
			.bootstrap-table {
				width: 100% !important;
				float: left;
				padding: 20px
			}
			.red {
				color: red;
				background-color: transparent !important;
			}
			.clearfix {
				display: none;
			}
			.setup,
			.template {
				display: none;
			}
			.setup.active {
				display: block;
			}
			.center {
				text-align: center;
			}
			tr.disabled {
			    opacity: 0.5;
			}
			.bootstrap-table tbody td {
				cursor: pointer;
			}
			.roster-setup > div > div span {
			    min-width: 100px;
			    display: inline-block;
			}

			.roster-setup > div > div span:first-child,
			.roster-setup > div > div span:nth-child(3) {
			    min-width: 30px;
			}
			.fa-fire {
				display: none !important;
			}
			.fa-fire.active {
				display: initial !important;
			}
			.roster-setup > div > div span:nth-child(4) {
			    min-width: 40px;
			}
			.data-setup {
			    position: relative;
    			height: 600px;
			}
			.loader-gif {
				display: none;
				position: absolute;
				top: 0;
				right: 0;
				left: 0;
				bottom: 0;
				width: 150px;
				height: 0px;
				margin: auto;
			}
			.team_play-container,
			.opp_play-container {
			    font-size: 19px;
				margin-top: 20px;
				float: left;
				width: 100%;
			}
			.player-table {
				display: none;
			}
			.player-table.active {
				display: block;
			}
		</style>
		<script type="text/javascript">
			

			jQuery(document).ready(function() {
				var data_trend;
				var filterPosition;

				jQuery(document).on('click', '.trend', function() {
					data_trend = jQuery(this).attr('data-trend');
					jQuery('.trend').removeClass('active');
					jQuery(this).addClass('active');
					jQuery('.player-table').removeClass('active');
					jQuery('.'+data_trend).addClass('active');
				});

				jQuery(document).on('click', '.position-filter', function() {
					filterPosition = jQuery(this).text().toLowerCase();

					if(data_trend) {
						jQuery.ajax({
							url: '/nba/get_data.php',
							type: 'GET',
							dataType: 'json',
							data: {type: data_trend, position: filterPosition.toLowerCase()},
						})
						.done(function(resp) {
							jQuery('table').bootstrapTable('load', getData(resp));
						})
						.fail(function() {
							console.log("error");
						})
						.always(function() {
						});
					} else {
						alert('Please select trend.')
					}
					
				})

			});

			function getData(data) {
				var array = $.map(data, function(value, index) {
				    return [value];
				});

				return array;
			}
		</script>
		<script src="https://use.fontawesome.com/b1353e11e4.js"></script>
	</head>
	<body>
		<div>
			<nav class="navbar navbar-default">
			  	<div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="">DK Roster Analyzer</a>
				    </div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li class="active"><a href="#">Data Analysis</a></li>
				        <li><a href="/nba/lineups_detail.php">Detail Lineups</a></li>
				        <li><a href="/nba/generatelineups.php" id="lineups-setup">Generate Lineups</a></li>
				        <li><a href="/nba/sync.php" id="sync-setup">Sync</a></li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
		    	</div>
		    </nav>
		</div>
		<div>
			<div>
				<span><button class="btn btn-primary btn-sm trend" data-trend="high_fga">High FGA</button></span>
				<span><button class="btn btn-primary btn-sm trend" data-trend="min_increase">Mins Increase</button></span>
				<!-- <span><button class="btn btn-primary btn-sm trend" data-trend="fp_increase">FP Increase</button></span> -->
				<span><button class="btn btn-primary btn-sm trend" data-trend="high_pt">High Pt / $</button></span>
				<span><button class="btn btn-primary btn-sm trend" data-trend="high_consist">High Consistency</button></span>
			</div>
			<div>
				<span><button class="btn btn-primary btn-sm position-filter">PG</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">SG</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">SF</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">PF</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">C</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">G</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">F</button></span>
			</div>
			<div class="player-table active high_fga">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table"  data-sort-name="proj_fga" data-sort-order="desc">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col" data-field="name">Player Name</th>
								<th data-sortable="true" data-field="position">Position</th>
								<th data-sortable="true" data-field="salary">Salary</th>
								<th data-sortable="true" data-field="team">Team</th>
								<th data-sortable="true" data-field="last3_fga">FGA Last 3 Games</th>
								<th data-sortable="true" data-field="fga">FGA / G</th>
								<th data-sortable="true" data-field="proj_fga">Proj FGA</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="player-table high_pt">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table"  data-sort-name="pts_salary" data-sort-order="desc">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col" data-field="name">Player Name</th>
								<th data-sortable="true" data-field="position">Position</th>
								<th data-sortable="true" data-field="salary">Salary</th>
								<th data-sortable="true" data-field="team">Team</th>
								<th data-sortable="true" data-field="proj_fp">Proj FP</th>
								<th data-sortable="true" data-field="pts_salary">Pt / $</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="player-table min_increase">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table"  data-sort-name="proj_mins" data-sort-order="desc">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col" data-field="name">Player Name</th>
								<th data-sortable="true" data-field="position">Position</th>
								<th data-sortable="true" data-field="salary">Salary</th>
								<th data-sortable="true" data-field="team">Team</th>
								<th data-sortable="true" data-field="mp_5">Last 5 MP / G</th>
								<th data-sortable="true" data-field="mp_g">MP / G</th>
								<th data-sortable="true" data-field="proj_mins">Proj Mins</th>
								<th data-sortable="true" data-field="proj_fp">Proj FP</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
			<div class="player-table high_consist">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table"  data-sort-name="value_4" data-sort-order="desc">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col" data-field="name">Player Name</th>
								<th data-sortable="true" data-field="position">Position</th>
								<th data-sortable="true" data-field="salary">Salary</th>
								<th data-sortable="true" data-field="team">Team</th>
								<th data-sortable="true" data-field="value_3">Value 3x%</th>
								<th data-sortable="true" data-field="value3">3x</th>
								<th data-sortable="true" data-field="value_4">Value 4x%</th>
								<th data-sortable="true" data-field="value">4x</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</body>
</html>