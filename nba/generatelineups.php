<?php
include_once 'config.php';

$user = 'root';
$password = 'root';
$db = 'dk_nba';
$host = 'localhost';
$port = 3306;

$link = mysqli_init();
$success = mysqli_real_connect(
   $link, 
   $host, 
   $user, 
   $password, 
   $db,
   $port
);


ini_set('max_execution_time', -1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once 'classes/DK_DB.php';
require_once 'config.php';

$slates = array();

$dk_db = new DK_DB();
$pgs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'PG', $slates);
$sgs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'SG', $slates);
$sfs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'SF', $slates);
$pfs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'PF', $slates);
$cs = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'C', $slates);
$teams = $dk_db->getCompiledTeamStats($gameDate);
$teamsArray = array();

foreach($teams as $_team) {
	$teamsArray[strtolower($_team['team'])] = $_team;
}

$salary_cap = 50000;
$salary_min = 48000;
$fppg_min = 17;
$gm_score_min = 4;
$uploadCSV = array();
$uploadCSV = "PG,SG,SF,PF,C,G,F,UTIL\n";
$locked = array();
$exclude = array();

$positions = array('PG', 'SG', 'SF', 'PF', 'C', 'G', 'F', 'UTIL');
$simulations = 150;
$fptsMin = 220;
$rosters = array();

for($i = 0; $i < $simulations; $i++) {

$copy_positions = array('PG', 'SG', 'SF', 'PF', 'C', 'G', 'F', 'UTIL');
$position_count = count($positions);
$roster_filled = 0;
$postionFilled = array();
$playersPicked = array();
$roster = array();
$index = 0;
$salary_cap = 50000;
$salary_total = 0;
$fptsTotal = 0;
$pts_salary_filter = false;
$pts_salary_min = 5.2;

while(($roster_filled < $position_count) && count($copy_positions) > 0) {
	$randPosIndex = array_rand($copy_positions, 1);
	$randPos = $copy_positions[$randPosIndex];
	$index++;

	if($index >= 14) {
		$copy_positions = array('PG', 'SG', 'SF', 'PF', 'C', 'G', 'F', 'UTIL');
		$position_count = count($positions);
		$roster_filled = 0;
		$postionFilled = array();
		$playersPicked = array();
		$roster = array();
		$index = 0;
		$salary_cap = 50000;
		$salary_total = 0;
		$fptsTotal = 0;
		continue;
	}

	if(!in_array($randPos, $postionFilled)) {
		$players = array();

		switch($randPos) {
			case 'PG':
				$players = $pgs;
				break;
			case 'SG':
				$players = $sgs;
				break;
			case 'SF':
				$players = $sfs;
				break;
			case 'PF':
				$players = $pfs;
				break;
			case 'C':
				$players = $cs;
				break;
			case 'G':
				$players = array_merge($pgs, $sgs);
				break;
			case 'F':
				$players = array_merge($sfs, $pfs);
				break;
			case 'UTIL':
				$players = array_merge($pgs, $sgs, $sfs, $pfs, $cs);
				break;
			default:
				$players = array();
				break;
		}

		if(!empty($players)) {
			$randPlayerIndex = array_rand($players, 1);
			$randPlayer = $players[$randPlayerIndex];

			if($randPlayer['injury'] == 'O' || in_array($randPlayer['player_id'], $exclude)) {
				continue;
			}


			$player_data = $randPlayer;
			$randPlayer['orig_position'] = $randPlayer['position'];
			$position_string = explode('/', $randPlayer['orig_position']);
			$position_string = strtolower($position_string[0]);
			$fga_g = number_format($teamsArray[strtolower($randPlayer['team'])]['fga'] * ($randPlayer['fga'] / $teamsArray[strtolower($randPlayer['team'])]['fga']), 2);

			// $player_data['pts'] = ($randPlayer['intercept']['m'] * ($teamsArray[strtolower($randPlayer['opp'])]['min_fga'] * ($randPlayer['fga'] / $teamsArray[strtolower($randPlayer['team'])]['fga']))) + $randPlayer['intercept']['b'] + $randPlayer['ft'];

			$player_data['pts'] = ($randPlayer['intercept']['m'] * ($teamsArray[strtolower($randPlayer['opp'])]['opp_fga'] * ($randPlayer['fga'] / $teamsArray[strtolower($randPlayer['team'])]['fga']))) + $randPlayer['intercept']['b'] + $randPlayer['ft'];
		  	// $player_data['pts'] = $randPlayer['pts_intercept']['m'] * ($teamsArray[strtolower($randPlayer['opp'])]['dfg_'.$position_string]) + $randPlayer['pts_intercept']['b'] + $randPlayer['ft'];
		  	// $player_data['trb'] = ($randPlayer['reb_intercept']['m'] * $teamsArray[strtolower($randPlayer['opp'])]['opp_trb']) + $randPlayer['reb_intercept']['b'];

		  	$proj_fp = $dk_db->calculateFP($player_data);

		  	$mins_to_use = $randPlayer['player_pool_mp'];
		  	$manuallyUpdated = false;

		  	if($randPlayer['player_pool_mp'] > $randPlayer['mp_g']) {
		  		$manuallyUpdated = true;
		  	}

		  	if(($randPlayer['mp_5'] > $randPlayer['player_pool_mp']) && $manuallyUpdated) {
		  		$mins_to_use = $randPlayer['mp_5'];
		  	}

		  	$trb_g = number_format($randPlayer['trb'], 2);

		 //  	if(in_array($position_string, array('PG', 'SG', 'SF')) && $fga_g < 9) {
			// 	continue;
			// }

			// if(in_array($position_string, array('PF', 'C')) && $trb_g < 7) {
			// 	continue;
			// }

		  	$proj_fp = $mins_to_use * ($proj_fp / $randPlayer['mp_g']);

		  	$pts_salary = $proj_fp / ($randPlayer['salary'] / 1000);
			
			if($pts_salary_filter) {
				if($pts_salary_min > $pts_salary) {
					continue;
				}
			}

			if($gm_score_min > $randPlayer['gm_score']) {
				continue;
			}

			if(/*($proj_fp < $fppg_min) || */in_array($randPlayer['name'], $playersPicked) || ($salary_cap - $randPlayer['salary']) < 0) {
				continue;
			}

			unset($copy_positions[$randPosIndex]);
			$copy_positions = array_values($copy_positions);

			$postionFilled[] = $randPos;
			$playersPicked[] = $randPlayer['name'];
			$randPlayer['orig_position'] = $randPlayer['position'];
			$randPlayer['position'] = $randPos;
			$randPlayer['proj_fp'] = $proj_fp;
			$roster['players'][] = $randPlayer;
			$salary_cap -= $randPlayer['salary'];
			$salary_total += $randPlayer['salary'];

			$fptsTotal += $randPlayer['gm_score'];

			$roster_filled++;
		}

	} else {
		continue;
	}

}

if(/*$fptsTotal > $fptsMin && */$salary_total > $salary_min) {
	// var_dump($fptsTotal);
	// var_dump($salary_total);
	$roster['fpts'] = $fptsTotal;
	// var_dump($roster); die;
	usort($roster['players'], "compare");

	
	$rosters[] = $roster;


	// var_dump($rosters); die;
}
else {
	$i--;
}

}

usort($rosters, "compareFPTS");
if(isset($_GET['generate'])) {
	foreach($rosters as $roster) {
		foreach($roster as $_csvRoster) {
			if(is_array($_csvRoster)) {
				$csv = array();
				foreach($_csvRoster as $_rst) {
					if(empty($_rst['dk_player_id'])) {
						$csv[] = $_rst['name'] . ' - ' . $_rst['player_id'];
					} else {
						$csv[] = $_rst['dk_player_id'];
					}
				}
				$uploadCSV .= implode(',', $csv) . "\n";
			}
		}
	}
}

function compareFPTS($a, $b) {
	if ((float)$a['fpts'] == (float)$b['fpts']) return 0;
	return ((float)$a['fpts'] < (float)$b['fpts']) ? 1 : -1;
}

function compare($a, $b) {

	switch($a['position']) {
		case 'PG':
			$points = 1;
			break;
		case 'SG':
			$points = 2;
			break;
		case 'SF':
			$points = 3;
			break;
		case 'PF':
			$points = 4;
			break;
		case 'C':
			$points = 5;
			break;
		case 'G':
			$points = 6;
			break;
		case 'F':
			$points = 7;
			break;
		case 'UTIL':
			$points = 8;
			break;
		default:
			$points = -1;
			break;
	}

	switch($b['position']) {
		case 'PG':
			$bpoints = 1;
			break;
		case 'SG':
			$bpoints = 2;
			break;
		case 'SF':
			$bpoints = 3;
			break;
		case 'PF':
			$bpoints = 4;
			break;
		case 'C':
			$bpoints = 5;
			break;
		case 'G':
			$bpoints = 6;
			break;
		case 'F':
			$bpoints = 7;
			break;
		case 'UTIL':
			$bpoints = 8;
			break;
		default:
			$bpoints = -1;
			break;
	}

	return $points > $bpoints;
}
?>

<?php

if(isset($_GET['generate'])) {

file_put_contents('./dk_export.csv', $uploadCSV);
}


?>



<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
	body {
		font-size: 12px;
	}
</style>
<?php 
usort($rosters, "compareFPTS");

foreach($rosters as $_roster): ?>
	<?php 
		$totalSalary = 0; 
		$totalPoints = 0;
	?>
	<div class="col-md-3 row" style="margin-top:20px">
	<?php foreach($_roster['players'] as $_player): ?>
		<?php
			$null_value = 'a_null';
			$player_data = $_player;
			$position_string = explode('/', $_player['orig_position']);
			$position_string = strtolower($position_string[0]);
			$player_data['pts'] = ($_player['intercept']['m'] * ($teamsArray[strtolower($_player['opp'])]['opp_fga'] * ($_player['fga'] / $teamsArray[strtolower($_player['team'])]['fga']))) + $_player['intercept']['b'] + $_player['ft'];
		  	// $player_data['pts'] = $_player['pts_intercept']['m'] * ($teamsArray[strtolower($_player['opp'])]['dfg_'.$position_string]) + $_player['pts_intercept']['b'];

		  	$proj_fp = $dk_db->calculateFP($player_data);
		  	$proj_fp = $_player['player_pool_mp'] * ($proj_fp / $_player['mp_g'] );
			$totalSalary += $_player['salary'];
			$totalPoints += number_format($_player['gm_score'], 2);
		?>
		<div class="col-md-12">
			<?php echo $_player['position'] . "	" . $_player['name'] . "	" . strtoupper($_player['team']). "	" . $_player['salary'] . "	" . number_format($_player['gm_score'], 2) . "<br>" ?>
		</div>
	<?php endforeach ?>
		<div class="col-md-12">
			<?php echo $null_value . "	" .  $null_value . "	" .  $null_value . "	" . $totalSalary . "	" . $totalPoints  . "<br><br>"?></div>
		</div>
	</div>
<?php endforeach ?>
