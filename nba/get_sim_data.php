<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
require_once 'config.php';
include_once 'functions/get_player_functions.php';

$params = $_GET;

$dk_db = new DK_DB();
$results = $dk_db->runSim($gameDate, $params['team']);
echo json_encode($results);