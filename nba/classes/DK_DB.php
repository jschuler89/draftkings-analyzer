<?php
class DK_DB 
{
	public $conn;

	public function __construct()
	{
		$DBServer = '127.0.0.1'; // e.g 'localhost' or '192.168.1.100'
		$DBUser   = 'root';
		$DBPass   = 'root';
		$DBName   = 'dk_nba';

		$this->conn = mysqli_connect($DBServer, $DBUser, $DBPass, $DBName);

		if (mysqli_connect_errno())
		{
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
		}
	}

	public function query($sql)
	{
		$result = mysqli_query($this->conn, $sql);

		if(is_object($result)) {
			$row = mysqli_fetch_all($result, MYSQLI_ASSOC);
		} else {
			$row = null;
		}

		return $row;
	}

	public function getPlayer($player_id)
	{
		$selectSmt = "SELECT * FROM players where player_id= $player_id";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getPlayerGameLogs($player_id)
	{
		$selectSmt = "SELECT * FROM player_game_logs where player_id= $player_id order by game_num desc";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getAllTeams()
	{
		$selectSmt = "SELECT * FROM teams";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getTeam($team)
	{
		$selectSmt = "SELECT * FROM teams where team = '$team'";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getTeamById($team_id)
	{
		$selectSmt = "SELECT * FROM teams where team_id = $team_id";

		$rs = $this->query($selectSmt);

		return $rs;
	}


	public function getAllPlayers($somePlayers = false)
	{
		$selectSmt = "SELECT * FROM players";

		if($somePlayers) {
			$selectSmt = "SELECT * FROM players where player_id not in (select distinct player_id from player_game_logs)";
		}

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function setDKPlayerId($name, $dk_player_id)
	{
		$selectSmt = "SELECT * FROM players where name = '$name'";
		$rs = $this->query($selectSmt);
		$rs = reset($rs);
		$player_id = $rs['player_id'];

		$updateSmt = "UPDATE player_pool set dk_player_id = $dk_player_id where player_id = $player_id";
		$rs = $this->query($updateSmt);

		return $rs;
	}

	public function getPlayerPool($gameDate)
	{
		$selectSmt = "SELECT * FROM player_pool pp left join players p on p.player_id = pp.player_id where pp.game_date = '$gameDate' order by salary desc";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function getPlayerPoolByPosition($gameDate, $position)
	{
		$selectSmt = "SELECT * FROM player_pool pp left join players p on p.player_id = pp.player_id where pp.game_date = '$gameDate' and p.position = '$position'";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function runSim($gameDate, $team)
	{
		$timesToRun = 5;
		for($i = 0; $i < $timesToRun; $i++) {
			$results = $this->getSimData($gameDate, $team, $i);
			// var_dump($results);
		}

		return array();
	}

	public function getSimData($gameDate, $team, $count = -1)
	{
		$selectSmt = "SELECT min(ts.opp_ast) as min_ast, max(ts.fga) as max_fga, min(ts.fga) as min_fga, avg(ts.pts) as pts, min(ts.fg) as min_fgp, max(ts.fg_pct) as max_fgp, avg(ts.fg_pct) as fgp, avg(ts.fg) as fg, avg(ts.fga) as fga, avg(ts.orb) as orb, min(ts.trb) as min_trb, max(ts.trb) as max_trb, avg(ts.trb) as trb, avg(ts.ast) as ast, avg(ts.stl) as stl, avg(ts.blk) as blk, avg(ts.tov) as tov, avg(ts.opp_pts) as opp_pts, avg(ts.opp_fg) as opp_fg, avg(ts.opp_fga) as opp_fga, avg(ts.opp_orb) as opp_orb, avg(ts.opp_trb) as opp_trb, avg(ts.opp_ast) as opp_ast, avg(ts.opp_stl) as opp_stl, avg(ts.opp_stl) as opp_stl, avg(ts.blk) as opp_blk, avg(ts.opp_tov) as opp_tov, t.team, t.team_id, pp.opp FROM team_game_logs ts left join teams t on t.team_id = ts.team_id left join players p on p.team = t.team left join player_pool pp on pp.player_id = p.player_id  where pp.game_date = '$gameDate' and t.team = '$team' group by ts.team_id, pp.opp";

		$rs = $this->query($selectSmt);

		// query to get min/max of what opponent allows
		$selectSmt = "SELECT max(opp_fg_pct) as max_opp_fgp, min(opp_fg_pct) as min_opp_fgp, max(opp_fga) as max_opp_fga, min(opp_fga) as min_opp_fga, max(opp_trb) as max_opp_trb, min(opp_trb) as min_opp_trb, max(opp_ast) as max_opp_ast, min(opp_ast) as min_opp_ast, max(opp_fta) as max_opp_fta, min(opp_fta) as min_opp_fta, max(opp_stl) as max_opp_stl, min(opp_stl) as min_opp_stl, max(opp_tov) as max_opp_tov, min(opp_tov) as min_opp_tov FROM team_game_logs ts left join teams t on t.team_id = ts.team_id left join players p on p.team = t.team left join player_pool pp on pp.player_id = p.player_id  where pp.game_date = '$gameDate' and t.team = '" . $rs[0]['opp'] ."' group by ts.team_id";

		$opp_rs = $this->query($selectSmt);
		$opp_rs = reset($opp_rs);

		foreach($rs as $index => $_result) {
			$team_max_fgp = $opp_rs['max_opp_fgp'];
			$team_min_fgp = $opp_rs['min_opp_fgp'];

			$team_max_fga = $opp_rs['max_opp_fga'];
			$team_min_fga = $opp_rs['min_opp_fga'];

			$team_max_trb = $opp_rs['max_opp_trb'];
			$team_min_trb = $opp_rs['min_opp_trb'];

			$team_max_ast = $opp_rs['max_opp_ast'];
			$team_min_ast = $opp_rs['min_opp_ast'];

			$team_max_fta = $opp_rs['max_opp_fta'];
			$team_min_fta = $opp_rs['min_opp_fta'];

			$team_max_stl = $opp_rs['max_opp_stl'];
			$team_min_stl = $opp_rs['min_opp_stl'];

			$team_max_tov = $opp_rs['max_opp_tov'];
			$team_min_tov = $opp_rs['min_opp_tov'];

			$team_fga_rand = 0;
			$team_fga_rand = rand($team_min_fga, $team_max_fga);
			$team_fgp_rand = 0;
			$team_fgp_rand = rand($team_min_fgp, $team_max_fgp);
			$team_trb_rand = 0;
			$team_trb_rand = rand($team_min_trb, $team_max_trb);
			$team_ast_rand = 0;
			$team_ast_rand = rand($team_min_ast, $team_max_ast);
			$team_fta_rand = 0;
			$team_fta_rand = rand($team_min_fta, $team_max_fta);
			$team_stl_rand = 0;
			$team_stl_rand = rand($team_min_stl, $team_max_stl);
			$team_tov_rand = 0;
			$team_tov_rand = rand($team_min_tov, $team_max_tov);
			


			// get player data
			$selectSmt = "SELECT * FROM player_pool pp left join players p on p.player_id = pp.player_id where pp.game_date = '$gameDate' and p.team = '$team' order by salary desc";

			$player_rs = $this->query($selectSmt);

			$team_pts = 0;

			if($count > -1) {
				$count++;
				echo "<h4>Game $count</h4> <br/> <br/>";
			}

			foreach($player_rs as $_player) {
				$player_id = $_player['player_id'];
				$player_name = $_player['name'];
				
				$selectSmt = "SELECT (pgl.fg / pgl.fga) as fg_pct, pgl.ft, avg(tgl.fg_pct) as team_fgp, avg(tgl.fga) as team_fga, pgl.fga, pgl.pts, pgl.trb, avg(tgl.trb) as team_trb, avg(tgl.ast) as team_ast, pgl.ast, avg(tgl.fta) as team_fta, avg(tgl.ft) as team_ft, avg(tgl.stl) as team_stl, pgl.stl, avg(tgl.tov) as team_tov, pgl.tov from player_game_logs pgl left join teams t on t.url_param = pgl.opp_id left join team_game_logs tgl on tgl.team_id = t.team_id where pgl.player_id = $player_id and pgl.game_num > 0 group by pgl.game_id order by pgl.game_num desc limit 20";
				$player_data_rs = $this->query($selectSmt);

				$a = array();
				$b = array();
				$c = array();
				$d = array();
				$e = array();
				$f = array();
				$g = array();
				$h = array();
				$i = array();
				$j = array();

				foreach($player_data_rs as $_xy) {
					$a[] = $_xy['team_trb'];
					$b[] = $_xy['trb'];
					$c[] = $_xy['team_ast'];
					$d[] = $_xy['ast'];
					$e[] = $_xy['team_fgp'];
					$f[] = $_xy['pts'];
					$g[] = $_xy['team_fga'];
					$h[] = $_xy['fga'];
					$i[] = $_xy['team_fga'];
					$j[] = $_xy['pts'];
					$k[] = $_xy['team_fta'];
					$l[] = $_xy['ft'];
					$m[] = $_xy['team_stl'];
					$n[] = $_xy['stl'];
					$o[] = $_xy['team_tov'];
					$p[] = $_xy['tov'];
				}

				$fga_pts_interceptInfo = $this->linear_regression($i, $j);
				$rebInterceptInfo = $this->linear_regression($a, $b);
				$astInterceptInfo = $this->linear_regression($c, $d);
				$fga_interceptInfo = $this->linear_regression($g, $h);
				$fgp_pts_interceptInfo = $this->linear_regression($e, $f);
				$ftInterceptInfo = $this->linear_regression($k, $l);
				$stlInterceptInfo = $this->linear_regression($m, $n);
				$tovInterceptInfo = $this->linear_regression($o, $p);

				$fga_player_pts = number_format($fga_pts_interceptInfo['m'] * $team_fga_rand + $fga_pts_interceptInfo['b'], 2);
				$fgp_player_pts = number_format($fgp_pts_interceptInfo['m'] * $team_fgp_rand + $fgp_pts_interceptInfo['b'], 2);
				$player_ast = number_format($astInterceptInfo['m'] * $team_ast_rand + $astInterceptInfo['b'], 2);
				$player_fga = number_format($fga_interceptInfo['m'] * $team_fga_rand + $fga_interceptInfo['b'], 2);
				$player_trb = number_format($rebInterceptInfo['m'] * $team_trb_rand + $rebInterceptInfo['b'], 2);
				$player_ft = number_format($ftInterceptInfo['m'] * $team_fta_rand + $ftInterceptInfo['b'], 2);
				$player_stl = number_format($stlInterceptInfo['m'] * $team_stl_rand + $stlInterceptInfo['b'], 2);
				$player_tov = number_format($tovInterceptInfo['m'] * $team_tov_rand + $tovInterceptInfo['b'], 2);
				$pts_avg = (($fga_player_pts + $fgp_player_pts) / 2) + $player_ft;
				$fpts_data = array(
					'pts' => $pts_avg,
					'trb' => $player_trb,
					'ast' => $player_ast,
					'stl' => $player_stl,
					'blk' => 0,
					'tov' => $player_tov,
					'fg3' => 0
				);
				$fpts = $this->calculateFP($fpts_data);
				$players[$player_name] = array('pts' => $pts_avg);

				echo "$player_name : $player_fga FGA $pts_avg PTS $player_trb REB $player_ast AST $player_stl STL $player_tov TO $fpts FPTS<br/><br/>";
			}

		}

		echo "--------------- <br/><br/>";

		return $players;
	}

	public function getCompiledTeamStats($gameDate)
	{
		$selectSmt = "SELECT min(ts.opp_ast) as min_ast, max(ts.fga) as max_fga, min(ts.fga) as min_fga, avg(ts.pts) as pts, avg(ts.fg) as fg, avg(ts.fga) as fga, avg(ts.orb) as orb, avg(ts.trb) as trb, avg(ts.ast) as ast, avg(ts.stl) as stl, avg(ts.blk) as blk, avg(ts.tov) as tov, avg(ts.opp_pts) as opp_pts, avg(ts.opp_fg) as opp_fg, avg(ts.opp_fga) as opp_fga, avg(ts.opp_orb) as opp_orb, avg(ts.opp_trb) as opp_trb, avg(ts.opp_ast) as opp_ast, avg(ts.opp_stl) as opp_stl, avg(ts.opp_stl) as opp_stl, avg(ts.blk) as opp_blk, avg(ts.opp_tov) as opp_tov, t.team, t.team_id, pp.opp FROM team_game_logs ts left join teams t on t.team_id = ts.team_id left join players p on p.team = t.team left join player_pool pp on pp.player_id = p.player_id  where pp.game_date = '$gameDate' group by pp.player_id, t.team_id, pp.opp";

		$rs = $this->query($selectSmt);

		$homePts = 0;
		$awayPts = 0;

		foreach($rs as $index => $_result) {
			$team = $_result['team'];

			$selectSmt = "select avg(p.dfg) as dfg from players p left join player_pool pp on p.player_id = pp.player_id where pp.mp >= 20 and p.dfg > 0 and pp.injury is null and team = '$team' and position in ('PG', 'PG/SG', 'PG/SF')";

			$result = $this->query($selectSmt);

			$rs[$index]['dfg_pg'] = $result[0]['dfg'] / 100;

			$selectSmt = "select avg(p.dfg) as dfg from players p left join player_pool pp on p.player_id = pp.player_id where pp.mp >= 20 and pp.injury is null and team = '$team' and position in ('SG', 'PG/SG', 'SG/SF')";

			$result = $this->query($selectSmt);

			$rs[$index]['dfg_sg'] = $result[0]['dfg'] / 100;

			$selectSmt = "select avg(p.dfg) as dfg from players p left join player_pool pp on p.player_id = pp.player_id where pp.mp >= 20 and pp.injury is null and pp.injury is null and team = '$team' and position in ('SF', 'SG/SF', 'SF/PF')";

			$result = $this->query($selectSmt);

			$rs[$index]['dfg_sf'] = $result[0]['dfg'] / 100;

			$selectSmt = "select avg(p.dfg) as dfg from players p left join player_pool pp on p.player_id = pp.player_id where pp.mp >= 20 and pp.injury is null and team = '$team' and position in ('PF', 'SF/PF', 'PF/C')";

			$result = $this->query($selectSmt);

			$rs[$index]['dfg_pf'] = $result[0]['dfg'] / 100;

			$selectSmt = "select avg(p.dfg) as dfg from players p left join player_pool pp on p.player_id = pp.player_id where pp.mp >= 20 and pp.injury is null and team = '$team' and position in ('C', 'PF/C')";

			$result = $this->query($selectSmt);

			$rs[$index]['dfg_c'] = $result[0]['dfg'] / 100;

			$selectSmt = "SELECT avg(pts) as pts_last_8 FROM (SELECT ts.pts FROM team_game_logs ts where team_id = " . $_result['team_id'] . " order by game_id desc limit 8) as ts";

			$result = $this->query($selectSmt);

			$rs[$index]['pts_last_8'] = $result[0]['pts_last_8'];

			$selectSmt = "SELECT avg(opp_pts) as opp_pts_last_8 FROM (SELECT ts.opp_pts FROM team_game_logs ts where team_id = " . $_result['team_id'] . " order by game_id desc limit 8) as ts";

			$result = $this->query($selectSmt);

			$rs[$index]['opp_pts_last_8'] = $result[0]['opp_pts_last_8'];

			$selectSmt = "SELECT avg(ts.pts) as pts FROM team_game_logs ts where game_location = '@' and team_id = " . $_result['team_id'];

			$result = $this->query($selectSmt);

			$rs[$index]['away_pts'] = $result[0]['pts'];

			$selectSmt = "SELECT avg(ts.fga) as team_fga_last_3 FROM team_game_logs ts where team_id = " . $_result['team_id'];

			$result = $this->query($selectSmt);

			$rs[$index]['team_fga_last_3'] = $result[0]['team_fga_last_3'];

			$selectSmt = "SELECT avg(ts.pts) as pts FROM team_game_logs ts where game_location = '' and team_id = " . $_result['team_id'];

			$result = $this->query($selectSmt);

			$rs[$index]['home_pts'] = $result[0]['pts'];

			$selectSmt = "select count(*) as wins from team_game_logs where game_result = 'W' and team_id = " . $_result['team_id'];

			$result = $this->query($selectSmt);

			$rs[$index]['wins'] = $result[0]['wins'];

			$selectSmt = "select count(*) as losses from team_game_logs where game_result = 'L' and team_id = " . $_result['team_id'];

			$result = $this->query($selectSmt);

			$rs[$index]['losses'] = $result[0]['losses'];

			$rs[$index]['win_pct'] = ($rs[$index]['wins'] / ($rs[$index]['wins'] + $rs[$index]['losses'])) * 100;

			$selectSmt = "select avg(pts - opp_pts) as pts_diff from team_game_logs where team_id = " . $_result['team_id']; 

			$result = $this->query($selectSmt);

			$rs[$index]['pts_diff'] = $result[0]['pts_diff'];

			$selectSmt = "select * from team_game_logs where team_id = " . $_result['team_id'] . " order by game_num desc limit 8";

			$result = $this->query($selectSmt);
			$wins_last_8 = 0;

			foreach($result as $_results) {
				if($_results['game_result'] == 'W') {
					$wins_last_8++;
				} 
			}

			$rs[$index]['win_pct_last_8'] = ($wins_last_8 / 8) * 100;

			$selectSmt = "SELECT game_result FROM team_game_logs ts where game_location = '@' and team_id = " . $_result['team_id'];

			$result = $this->query($selectSmt);
			$wins_away = 0;
			$losses_away = 0;

			foreach($result as $_results) {
				if($_results['game_result'] == 'W') {
					$wins_away++;
				} else {
					$losses_away++;
				}
			}

			if(($wins_away + $losses_away) > 0) {
				$rs[$index]['win_pct_away'] = ($wins_away / ($wins_away + $losses_away)) * 100;
			} else {
				$rs[$index]['win_pct_away'] = 0;
			}

			$selectSmt = "SELECT game_result FROM team_game_logs ts where game_location = '' and team_id = " . $_result['team_id'];	

			$result = $this->query($selectSmt);	
			$wins_home = 0;
			$losses_home = 0;

			foreach($result as $_results) {
				if($_results['game_result'] == 'W') {
					$wins_home++;
				} else {
					$losses_home++;
				}
			}

			if(($wins_home + $losses_home) > 0) {
				$rs[$index]['win_pct_home'] = ($wins_home / ($wins_home + $losses_home)) * 100;	
			} else {
				$rs[$index]['win_pct_home'] = 0;
			}

			$rs[$index]['wins'] = ($wins_home + $wins_away);
			$rs[$index]['loss'] = ($losses_home + $losses_away);
		}

		return $rs;
	}

	public function calculateFP($data)
	{
		$dbldblcount = 0;
		$fp = 0;
		$fp += 	$data['pts'] * 1;
		$fp += 	$data['fg3'] * 0.5;
		$fp += 	$data['trb'] * 1.25;
		$fp += 	$data['ast'] * 1.5;
		$fp += 	$data['stl'] * 2;
		$fp += 	$data['blk'] * 2;
		$fp -= 	$data['tov'] * 0.5;

		if($data['pts'] >= 10) {
			$dbldblcount++;
		}

		if($data['trb'] >= 10) {
			$dbldblcount++;
		}

		if($data['ast'] >= 10) {
			$dbldblcount++;
		}

		if($data['stl'] >= 10) {
			$dbldblcount++;
		}

		if($data['blk'] >= 10) {
			$dbldblcount++;
		}

		if($dbldblcount >= 2) {
			$fp += 1.5;
		}

		if($dbldblcount >= 3) {
			$fp += 3;
		}

		return $fp;
	}

	public function getLeagueAvg($stat = '')
	{
		if($stat == '') {
			return 0;
		}

		$selectSmt = "SELECT avg($stat / games_played) as league_avg FROM team_stats";

		$rs = $this->query($selectSmt);

		return $rs[0]['league_avg'];
	}

	public function getPlayerPoolStatsByPosition($gameDate, $position, $slates = array())
	{
		$extra = '';

		if($position == 'PG' || $position == 'SG') {
			$extra .= ' OR p.position = "PG/SG" OR p.position = "PG/SF"';
		}

		if($position == 'SG') {
			$extra .= ' OR p.position = "SG/SF"';
		}

		if($position == 'SF') {
			$extra .= ' OR p.position = "SG/SF"';
		}

		if($position == 'SF' || $position == 'PF') {
			$extra .= ' OR p.position = "SF/PF"';
		}

		if($position == 'PF') {
			$extra .= ' OR p.position = "PF/C"';
		}

		if($position == 'C') {
			$extra .= ' OR p.position = "PF/C"';
		}

		if($position == 'G') {
			$extra .= ' OR p.position = "PG/SG" OR p.position = "PG" OR p.position = "SG" OR p.position = "SG/SF"';
		}

		if($position == 'F') {
			$extra .= ' OR p.position = "SG/SF" OR p.position = "SF" OR p.position = "PF" OR p.position = "SF/PF"  OR p.position = "PF/C"';
		}

		if($position == 'UTIL') {
			$extra .= ' OR p.position = "PG" OR p.position = "SG" OR p.position = "PG/SG" OR p.position = "PG/SF" OR p.position = "SG/SF" OR p.position = "SF" OR p.position = "PF" OR p.position = "SF/PF" OR p.position = "PF/C" OR p.position = "C"';
		}

		if(empty($slates)) {
			$selectSmt = "SELECT pp.*, p.*, avg(pgl.fg) / avg(pgl.fga) as fg_pct, avg(pgl.pts) as pts, avg(pgl.mp) as mpg, avg(pgl.fga) as fga, avg(pgl.ft) as ft, avg(pgl.fg3a) as fg3a, avg(pgl.fg3) as fg3, avg(pgl.ast) as ast, avg(pgl.stl) as stl, avg(pgl.trb) as trb, avg(pgl.tov) as tov, avg(pgl.blk) as blk FROM player_pool pp left join players p on p.player_id = pp.player_id left join player_game_logs pgl on pgl.player_id = p.player_id where pp.game_date = '$gameDate' and (p.position = '$position' $extra) and pgl.mp is not null group by p.name, pp.player_pool_id, pp.player_id order by salary desc";
		} else {
			$selectSmt = "SELECT pp.*, p.*, pp.mp as player_pool_mp, avg(pgl.fg) / avg(pgl.fga) as fg_pct, avg(pgl.pts) as pts, avg(pgl.mp) as mpg, avg(pgl.fga) as fga, avg(pgl.ft) as ft, avg(pgl.fg3a) as fg3a, avg(pgl.fg3) as fg3, avg(pgl.ast) as ast, avg(pgl.stl) as stl, avg(pgl.trb) as trb, avg(pgl.tov) as tov, avg(pgl.blk) as blk FROM player_pool pp left join players p on p.player_id = pp.player_id left join player_game_logs pgl on pgl.player_id = p.player_id where pp.game_date = '$gameDate' and (p.position = '$position' $extra) and pp.game_time in ('" . implode("','", $slates) . "') and pgl.mp is not null group by pp.player_id order by salary desc";
		}

		$rs = $this->query($selectSmt);

		if($rs) {
			$player_id = $rs[0]['player_id'];
		}

		foreach($rs as $index => $_result) {
			$avg_fppg = $this->calculateFP($_result);
			$player_id = $_result['player_id'];
			$selectSmt = "select * from player_cache where player_id = $player_id";
			$result = $this->query($selectSmt);

			if($result) {
				$player_data = unserialize($result[0]['data']);
				$rs[$index] = $player_data;
				continue;
			}

			$salary = $_result['salary'];
			$value = $salary / 1000;
			$value2 = $value * 2;
			$value3 = $value * 3;
			$value4 = $value * 4;
			$value5 = $value * 5;

			$selectSmt = "select (pgl.fg / pgl.fga) as fg_pct, pgl.fga, pgl.pts, pgl.trb, avg(tgl.trb) as team_trb, avg(tgl.ast) as team_ast, pgl.ast from player_game_logs pgl left join teams t on t.url_param = pgl.opp_id left join team_game_logs tgl on tgl.team_id = t.team_id  where pgl.player_id = 2 and pgl.game_num > 0 group by pgl.game_id order by pgl.game_num des";

			$xy = $this->query($selectSmt);
			$a = array();
			$b = array();
			$c = array();
			$d = array();
			$e = array();
			$f = array();
			$x = array();
			$y = array();

			if(!empty($xy)) {
				foreach($xy as $_xy) {
					$a[] = $_xy['team_trb'];
					$b[] = $_xy['trb'];
					$c[] = $_xy['team_ast'];
					$d[] = $_xy['ast'];
					$e[] = $_xy['fg_pct'];
					$f[] = $_xy['pts'];
					$x[] = $_xy['fga'];
					$y[] = $_xy['pts'];
				}
			}

			$interceptInfo = $this->linear_regression($x, $y);
			$rebInterceptInfo = $this->linear_regression($a, $b);
			$astInterceptInfo = $this->linear_regression($c, $d);
			$ptsInterceptInfo = $this->linear_regression($e, $f);

			$rs[$index]['intercept'] = $interceptInfo;
			$rs[$index]['reb_intercept'] = $rebInterceptInfo;
			$rs[$index]['ast_intercept'] = $astInterceptInfo;
			$rs[$index]['pts_intercept'] = $ptsInterceptInfo;

			$selectSmt = "SELECT * FROM player_game_logs where player_id = $player_id and game_num > 0";
			$result = $this->query($selectSmt);
			$consist = 0;
			$consist_last_5 = 0;
			$games = 0;
			$minfp = ($result) ? $this->calculateFP($result[0]) : 0;
			$maxfp = ($result) ? $this->calculateFP($result[0]) : 0;
			$resultCount = count($result) - 1;
			$last3IndexStart = $resultCount - 3;
			$last5IndexStart = $resultCount - 5;
			$last3Fpp = 0;
			$last5Fpp = 0;
			$last3FGA = 0;
			$value_2_hit = 0;
			$value_3_hit = 0;
			$value_4_hit = 0;
			$value_5_hit = 0;
			$mp = 0;
			$mp_5 = 0;

			$rs[$index]['gm_score'] = $this->getGameScoreData($result);

			foreach($result as $_index => $_rs) {
				if(is_null($_rs['mp'])) {
					continue;
				}

				$_mp = str_replace(':', '.', $_rs['mp']);
				$_mp = (float)$_mp;

				$mp += $_mp;

				$fpp = $this->calculateFP($_rs);

				if($avg_fppg < $fpp) {
					$consist++;
				}

				if($fpp > $value2) {
					$value_2_hit++;
				}

				if($fpp > $value3) {
					$value_3_hit++;
				}

				if($fpp > $value4) {
					$value_4_hit++;
				}

				if($fpp > $value5) {
					$value_5_hit++;
				}

				if($minfp > $fpp) {
					$minfp = $fpp;
				}

				if($maxfp < $fpp) {
					$maxfp = $fpp;
				}

				if($last5IndexStart < $_index) {
					$mp_5 += $_mp;
					$last5Fpp += $fpp;

					if($avg_fppg < $fpp) {
						$consist_last_5++;
					}
				}

				if($last3IndexStart < $_index) {
					$last3Fpp += $fpp;
					$last3FGA += $_rs['fga'];
				}


				$games++;
			}

			$selectSmt = "SELECT avg(pgl.pts) as away_pts, avg(pgl.fg3) as away_fg3, avg(pgl.ast) as away_ast, avg(pgl.stl) as away_stl, avg(pgl.trb) as away_trb, avg(pgl.tov) as away_tov, avg(pgl.blk) as away_blk FROM player_game_logs pgl where pgl.player_id = $player_id and pgl.game_location = '@' and game_num > 0";
			$result = $this->query($selectSmt);


			foreach($result as $_result) {
				foreach($_result as $key => $value) {
					$rs[$index][$key] = $value;
				}
			}

			$selectSmt = "SELECT avg(pgl.pts) as home_pts, avg(pgl.fg3) as home_fg3, avg(pgl.ast) as home_ast, avg(pgl.stl) as home_stl, avg(pgl.trb) as home_trb, avg(pgl.tov) as home_tov, avg(pgl.blk) as home_blk FROM player_game_logs pgl where pgl.player_id = $player_id and pgl.game_location = '' and game_num > 0 limit 12";
			$result = $this->query($selectSmt);


			foreach($result as $_result) {
				foreach($_result as $key => $value) {
					$rs[$index][$key] = $value;
				}
			}

			$rs[$index]['value2'] = $value2;
			$rs[$index]['value_2'] = ($value_2_hit / $games) * 100;
			$rs[$index]['value3'] = $value3;
			$rs[$index]['value_3'] = ($value_3_hit / $games) * 100;
			$rs[$index]['value4'] = $value4;
			$rs[$index]['value_4'] = ($value_4_hit / $games) * 100;
			$rs[$index]['value5'] = $value5;
			$rs[$index]['value_5'] = ($value_5_hit / $games) * 100;
			$rs[$index]['mp_g'] = $mp / $games;
			$rs[$index]['mp_5'] = $mp_5 / 5;
			$rs[$index]['fppg_last_5'] = $last5Fpp / 5;
			$rs[$index]['fppg_last_3'] = $last3Fpp / 3;
			$rs[$index]['fga_last_3'] = $last3FGA / 3;
			$rs[$index]['min_fppg'] = $minfp;
			$rs[$index]['max_fppg'] = $maxfp;
			$rs[$index]['avg_fppg'] = $avg_fppg;
			$rs[$index]['fppg_consist_last_5'] = ($games > 0) ? number_format(($consist_last_5 / 5) * 100, 2) : 0;
			$rs[$index]['fppg_consist'] = ($games > 0) ? number_format(($consist / $games) * 100, 2) : 0;

			$selectSmt = "select * from player_cache where player_id = $player_id";
			$result = $this->query($selectSmt);
			if(!$result) {
				$serializedPlayerData = serialize($rs[$index]);
				$insertSmt = "insert into player_cache(player_id, data) values($player_id, '$serializedPlayerData')";
				$this->query($insertSmt);
			}

		}

		return $rs;
	}

	public function getGameScoreData($gameLog)
	{
		/**
	 * Game Score = Points Scored + (0.4 x Field Goals) – (0.7 x Field Goal Attempts) – (0.4 x (Free Throw Attempts – Free Throws)) + (0.7 x Offensive Rebounds) + (0.3 x Defensive Rebounds) + Steals + (0.7 x Assists) + (0.7 x Blocks) – (0.4 x Personal Fouls) – Turnovers
	 **/

		$bestGameScore = 0;
		$bestPts = 0;
		$bestAst = 0;
		$bestReb = 0;
		$bestStl = 0;
		$bestBlk = 0;
		$counter = 1;
		$totalGameScore = 0;
		foreach($gameLog as $_game) {
			$pts = $_game['pts'];
			$fg = $_game['fg'];
			$fga = $_game['fga'];
			$ft = $_game['ft'];
			$fta = $_game['fta'];
			$orb = $_game['orb'];
			$drb = $_game['drb'];
			$stl = $_game['stl'];
			$blk = $_game['blk'];
			$pf = $_game['pf'];
			$tov = $_game['tov'];
			$ast = $_game['ast'];

			$gameScore = $pts + (0.4 * $fg) - (0.7 * $fga) - (0.4 * ($fta - $ft)) + (0.7 * $orb) + (0.3 * $drb) + $stl + (0.7 * $ast) + (0.7 * $blk) - (0.4 * $pf) - $tov;

			if($gameScore > $bestGameScore) {
				$bestPts = $_game['pts'];
				$bestAst = $_game['ast'];
				$bestReb = $_game['orb'] + $_game['drb'];
				$bestStl = $_game['stl'];
				$bestBlk = $_game['stl'];
				$pts = $_game['pts'];
				$bestGameScore = $gameScore;
				$opp = $_game['opp_id'];
			}

			$totalGameScore += $gameScore;

			$counter++;
		}

		return round($totalGameScore / $counter, 2);
	}

	public function getHighFGA($gameDate, $position)
	{
		$position = strtoupper($position);
		$player_pool = $this->getPlayerPoolStatsByPosition($gameDate, $position);
		$teams = $this->getCompiledTeamStats($gameDate);
		$teamsArray = array();

		foreach($teams as $_team) {
			$teamsArray[strtolower($_team['team'])] = $_team;
		}

		foreach($player_pool as $index => $player) {
			$proj_fga = number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2);
			$last_3_games_fga = number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2);
			$player_pool[$index]['proj_fga'] = number_format($proj_fga, 2);
			$player_pool[$index]['fga'] = number_format($player_pool[$index]['fga'], 2);
			$player_pool[$index]['last3_fga'] = $last_3_games_fga;
			if($proj_fga < 10 && $last_3_games_fga < 10) {
				unset($player_pool[$index]);
			}
		}

		return $player_pool;
	}

	public function getHighPtSalary($gameDate, $position)
	{
		$position = strtoupper($position);
		$player_pool = $this->getPlayerPoolStatsByPosition($gameDate, $position);
		$teams = $this->getCompiledTeamStats($gameDate);
		$teamsArray = array();

		foreach($teams as $_team) {
			$teamsArray[strtolower($_team['team'])] = $_team;
		}

		foreach($player_pool as $index => $player) {

			if($player['fppg'] == 0) {
				continue;
			}

			$player_data = $player;
			$position = explode('/', $position);
			$position = strtolower($position[0]);

			if($position == 'f') {
				$position = 'sf';
			} else if($position == 'g') {
				$position = 'pg';
			}

			$player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_'.$position]) + $player['pts_intercept']['b'];
		  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];

		  	$proj_fp = $this->calculateFP($player_data);

		  	$mins_to_use = $player['player_pool_mp'];

		  	if(($mins_to_use - $player['mp_5']) > 0) {
		  		$mins_to_use = $player['mp_5'];
		  	}

		  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);

		  	$pts_salary = $player['player_pool_mp'] * ($proj_fp / $player['mp_g'] ) / ($player['salary'] / 1000);

		  	$player_pool[$index]['pts_salary'] = number_format($pts_salary, 2);

		  	$player_pool[$index]['proj_fp'] = number_format($proj_fp, 2);

		  	if($pts_salary < 5.5) {
		  		unset($player_pool[$index]);
		  	} 
		}

		return $player_pool;
	}

	public function getHighConsist($gameDate, $position)
	{
		$position = strtoupper($position);
		$player_pool = $this->getPlayerPoolStatsByPosition($gameDate, $position);
		$teams = $this->getCompiledTeamStats($gameDate);
		$teamsArray = array();

		foreach($teams as $_team) {
			$teamsArray[strtolower($_team['team'])] = $_team;
		}

		foreach($player_pool as $index => $player) {

			if($player['fppg'] == 0) {
				continue;
			}

			$player_data = $player;
			$position = explode('/', $position);
			$position = strtolower($position[0]);

			if($position == 'f') {
				$position = 'sf';
			} else if($position == 'g') {
				$position = 'pg';
			}

			$player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_'.$position]) + $player['pts_intercept']['b'];
		  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];

		  	$proj_fp = $this->calculateFP($player_data);

		  	$mins_to_use = $player['mp_g'];

		  	// if(($mins_to_use - $player['mp_5']) > 0) {
		  	// 	$mins_to_use = $player['mp_5'];
		  	// }

		  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);

		  	$value_4 = number_format($player['value_4'], 2);

		  	$value_3 = number_format($player['value_3'], 2);

		  	$pts_salary = $player['mp_g'] * ($proj_fp / $player['mp_g'] ) / ($player['salary'] / 1000);

		  	$player_pool[$index]['pts_salary'] = number_format($pts_salary, 2);

		  	$player_pool[$index]['proj_fp'] = number_format($proj_fp, 2);

		  	$player_pool[$index]['value3'] = number_format($player_pool[$index]['value3'], 2);

		  	$player_pool[$index]['value'] = number_format($player_pool[$index]['value4'], 2);

		  	$player_pool[$index]['value_3'] = $value_3;

		  	$player_pool[$index]['value_4'] = $value_4;

		  	if($value_4 < 55) {
		  		unset($player_pool[$index]);
		  	} 
		}

		return $player_pool;
	}

	public function getMinsIncrease($gameDate, $position)
	{
		$position = strtoupper($position);
		$player_pool = $this->getPlayerPoolStatsByPosition($gameDate, $position);
		$teams = $this->getCompiledTeamStats($gameDate);
		$teamsArray = array();

		foreach($teams as $_team) {
			$teamsArray[strtolower($_team['team'])] = $_team;
		}

		foreach($player_pool as $index => $player) {

			if($player['fppg'] == 0) {
				continue;
			}

			$player_pool[$index]['mp_5'] = number_format($player['mp_5'], 2);

			$player_pool[$index]['mp_g'] = number_format($player['mp_g'], 2);

			$mins_5 = $player['mp_5'];

			$player_data = $player;
			$position = explode('/', $position);
			$position = strtolower($position[0]);
			
			if($position == 'f') {
				$position = 'sf';
			} else if($position == 'g') {
				$position = 'pg';
			}

			$player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_'.$position]) + $player['pts_intercept']['b'];
		  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];

		  	$proj_fp = $this->calculateFP($player_data);

		  	$mins_to_use = $player['player_pool_mp'];

		  	if($player['mp_5'] > $player['player_pool_mp']) {
		  		$mins_to_use = $player['mp_5'];
		  	}

		  	$player_pool[$index]['proj_mins'] = number_format($mins_to_use, 2);

		  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);

		  	$value_4 = number_format($player['value_4'], 2);

		  	$pts_salary = $proj_fp / ($player['salary'] / 1000);

		  	$player_pool[$index]['pts_salary'] = number_format($pts_salary, 2);

		  	$player_pool[$index]['proj_fp'] = number_format($proj_fp, 2);

		  	if(($mins_to_use - $player['mp_g']) < 2) {
		  		unset($player_pool[$index]);
		  	} 
		}

		return $player_pool;
	}

	public function getFPIncrease($gameDate, $position)
	{
		$position = strtoupper($position);
		$player_pool = $this->getPlayerPoolStatsByPosition($gameDate, $position);
		$teams = $this->getCompiledTeamStats($gameDate);
		$teamsArray = array();

		foreach($teams as $_team) {
			$teamsArray[strtolower($_team['team'])] = $_team;
		}

		foreach($player_pool as $index => $player) {

			if($player['fppg'] == 0) {
				continue;
			}

			$player_data = $player;
			$position = explode('/', $position);
			$position = strtolower($position[0]);

			if($position == 'f') {
				$position = 'sf';
			} else if($position == 'g') {
				$position = 'pg';
			}

			$player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_'.$position]) + $player['pts_intercept']['b'];
		  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];

		  	$proj_fp = $this->calculateFP($player_data);

		  	$mins_to_use = $player['player_pool_mp'];

		  	if(($mins_to_use - $player['mp_5']) > 0) {
		  		$mins_to_use = $player['mp_5'];
		  	}

		  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);

		  	$value_4 = number_format($player['value_4'], 2);

		  	$pts_salary = $player['player_pool_mp'] * ($proj_fp / $player['mp_g'] ) / ($player['salary'] / 1000);

		  	$player_pool[$index]['pts_salary'] = number_format($pts_salary, 2);

		  	$player_pool[$index]['proj_fp'] = number_format($proj_fp, 2);

		  	$player_pool[$index]['value'] = number_format($player_pool[$index]['value4'], 2);

		  	$player_pool[$index]['value_4'] = $value_4;

		  	if($value_4 < 55) {
		  		unset($player_pool[$index]);
		  	} 
		}

		return $player_pool;
	}

	public function getGameTimes($gameDate)
	{
		$selectSmt = "SELECT DISTINCT game_time FROM player_pool where game_date = '$gameDate' order by game_time";

		$rs = $this->query($selectSmt);

		return $rs;
	}

	public function insertUrlParam($player_id, $url_param)
	{
		if(empty($player_id) || empty($url_param)) {
			return false;
		}

		$selectSmt = "SELECT url_param from players where player_id = $player_id";

		$rs = $this->query($selectSmt);

		if($rs[0]['url_param'] != null) {
			return false;
		}

		$updateSmt = "UPDATE players set url_param = '$url_param' where player_id = $player_id";

		$rs = $this->query($updateSmt);

		return $rs;
	}

	public function insertPlayerImg($player_id, $img)
	{
		if(empty($player_id) || empty($img)) {
			return false;
		}

		$selectSmt = "SELECT img from players where player_id = $player_id";

		$rs = $this->query($selectSmt);

		if($rs[0]['img'] != null) {
			return false;
		}

		$updateSmt = "UPDATE players set img = '$img' where player_id = $player_id";

		$rs = $this->query($updateSmt);

		return $rs;
	}

	public function insertPlayer($playerInfo = array())
	{
		if(empty($playerInfo)) {
			return false;
		}

		$position = $playerInfo[0];
		$name = $playerInfo[2];
		$salary = $playerInfo[5];
		$gameInfo = $playerInfo[6];
		$fppg = $playerInfo[8];
		$team = $playerInfo[7];

		$selectSmt = "SELECT * FROM players where name = '$name' and team = '$team' and position = '$position'";

		$rs = $this->query($selectSmt);

		if(empty($rs)) {
			$insertSmt = "INSERT INTO players(name, team, position) VALUES('$name', '$team', '$position')";
			// var_dump($insertSmt); die;
			$rs = $this->query($insertSmt);
		}

		$selectSmt = "SELECT * FROM players where name = '$name' and team = '$team' and position = '$position'";

		$rs = $this->query($selectSmt);

		return $rs[0]['player_id'];
	}

	public function insertGameLog($player_id, $gamelog) 
	{
		$columns = "SHOW COLUMNS FROM player_game_logs";
		$result = $this->query($columns);

		foreach($gamelog['gamelogs'] as $_game) {
			$values = array();
			$keys = array();
			$alreadyLoaded = false;

			foreach($result as $_column) {
				if(isset($_game[$_column['Field']])) {

					if($_column['Field'] == 'game_num') {
						$game_num = $_game[$_column['Field']];
						$selectSmt = "SELECT count(*) as count from player_game_logs where player_id = $player_id and game_num = $game_num";
						$rs = $this->query($selectSmt);

						if(isset($rs[0]) && isset($rs[0]['count']) && $rs[0]['count'] > 0) {
							$alreadyLoaded = true;
							break;
						}
					}
					$values[] = $_game[$_column['Field']];
					$keys[] = $_column['Field'];
				}
				
			}

			if($alreadyLoaded) {
				continue;
			}

			$keysString = implode(',', $keys);
			$valuesString = implode("','", $values);


			$insertSmt = "INSERT INTO player_game_logs(player_id,$keysString) values($player_id,'$valuesString')";

			$rs = $this->query($insertSmt);
		}

		return true;
	}

	public function insertTeamGameLog($team_id, $teamLog = array())
	{
		if(empty($teamLog) || !$team_id) {
			return false;
		}

		$selectSmt = "SELECT * FROM team_game_logs where team_id = $team_id";
		$result = $this->query($selectSmt);

		if(count($result) > 0) {
			$deleteSmt = "DELETE FROM team_game_logs where team_id = $team_id";
			$this->query($deleteSmt);
		}

		$columns = "SHOW COLUMNS FROM team_game_logs";
		$result = $this->query($columns);

		$fields = array();

		foreach($result as $_result) {
			$fields[$_result['Field']] = $_result['Field'];
		}

		$logs = $teamLog['gamelogs'];
		$games = 1;

		foreach($logs as $_log) {
			$keys = array();
			$values = array();


			foreach($_log as $index => $_logRow) {
				$column = $index;

				// if(empty($_log['game_outcome'])) {
				// 	if($_log['opp'] != 'Bye Week') {
				// 		continue;
				// 	}
				// }

				// Get team name 'New England Patriots' -> 'Patriots'
				if($column == 'opp') {
					if($_logRow == 'Bye Week') {
						$_logRow = 'Bye';
					} else {
						$_logRow = explode(' ', $_logRow);
						$_logRow = $_logRow[count($_logRow) - 1];
					}
				}

				if(in_array($column, $fields)) {
					$keys[] = $fields[$column];
					$values[] = $_logRow;
				}
				
			}

			$keysString = implode(',', $keys);
			$valuesString = implode("','", $values);

			$insertSmt = "INSERT INTO team_game_logs(team_id,game_num,$keysString) values($team_id,$games,'$valuesString')";
			$games++;

			// var_dump($insertSmt); die;

			$rs = $this->query($insertSmt);
		}


		return true;
	}

	public function insertTeamStats($team_id, $teamStats = array())
	{
		if(empty($teamStats) || !$team_id) {
			return false;
		}

		$selectSmt = "SELECT * FROM team_stats where team_id = $team_id";
		$result = $this->query($selectSmt);

		if(count($result) > 0) {
			$deleteSmt = "DELETE FROM team_stats where team_id = $team_id";
			$this->query($deleteSmt);
		}

		$columns = "SHOW COLUMNS FROM team_stats";
		$result = $this->query($columns);

		$fields = array();

		foreach($result as $_result) {
			$fields[$_result['Field']] = $_result['Field'];
		}

		// var_dump($fields);

		$gamesPlayed = $teamStats['games_played'];
		$off_stats = $teamStats['gamelogs'][0];
		$def_stats = $teamStats['gamelogs'][1];

		$off_prefix = 'off_';
		$def_prefix = 'def_';

		// var_dump($off_stats);

		$keys = array();
		$values = array();

		foreach($off_stats as $index => $_stat) {
			$column = $off_prefix.$index;

			if(in_array($column, $fields)) {
				$keys[] = $fields[$column];
				$values[] = $_stat;
			}
		}

		foreach($def_stats as $index => $_stat) {
			$column = $def_prefix.$index;

			if(in_array($column, $fields)) {
				$keys[] = $fields[$column];
				$values[] = $_stat;
			}
		}

		$keysString = implode(',', $keys);
		$valuesString = implode("','", $values);

		$insertSmt = "INSERT INTO team_stats(team_id,games_played,$keysString) values($team_id,$gamesPlayed,'$valuesString')";

		$rs = $this->query($insertSmt);

		return true;
	}

	public function insertIntoPlayerPool($player_id, $playerInfo = array(), $gameDate)
	{
		if(empty($playerInfo) || !$player_id) {
			return false;
		}

		$position = $playerInfo[0];
		$name = $playerInfo[2];
		$salary = $playerInfo[5];
		$gameInfo = $playerInfo[6];

		// [0] => Team@Opp, [1] => 01:00PM [2] => ET
		$gameInfoArray = explode(" ", $gameInfo);
		// [0] => Team, [1] => Opp
		$gameArray = explode("@", $gameInfoArray[0]);
		$game_time = str_replace(array('AM', 'PM'), '', $gameInfoArray[1]);
		$opp = trim(str_replace('"', '', strtoupper($gameArray[1])));
		$opp2 = trim(str_replace('"', '', strtoupper($gameArray[0])));
		$fppg = number_format((float)$playerInfo[8], 2);
		$team = trim(str_replace('"', '', $playerInfo[7]));

		if(strtolower($team) == strtolower($opp)) {
			$opp = $opp2;
		}

		$selectSmt = "SELECT * FROM player_game_logs where player_id = $player_id and game_num > 0";

		$rs = $this->query($selectSmt);

		$mp = 0;
		$games = 0;

		if(!empty($rs)) {
			foreach($rs as $_index => $_rs) {
				if(is_null($_rs['mp'])) {
					continue;
				}

				$_mp = str_replace(':', '.', $_rs['mp']);
				$_mp = (float)$_mp;

				$mp += $_mp;
				$games++;
			}

			$mp = ($games > 0) ? $mp / $games : 0;
		}

		$insertSmt = "INSERT INTO player_pool(player_id, fppg, salary, opp, game_date,  mp) VALUES($player_id, $fppg, $salary, '$opp', '$gameDate', $mp)";

		$this->query($insertSmt);

		return true;
	}

	/**
	 * linear regression function
	 * @param $x array x-coords
	 * @param $y array y-coords
	 * @returns array() m=>slope, b=>intercept
	 */
	public function linear_regression($x, $y) {

		  // calculate number points
		  $n = count($x);
		  
		  // ensure both arrays of points are the same size
		  if ($n != count($y)) {

		    trigger_error("linear_regression(): Number of elements in coordinate arrays do not match.", E_USER_ERROR);
		  
		  }

		  // calculate sums
		  $x_sum = array_sum($x);
		  $y_sum = array_sum($y);

		  $xx_sum = 0;
		  $xy_sum = 0;
		  
		  for($i = 0; $i < $n; $i++) {
		  
		    $xy_sum+=($x[$i]*$y[$i]);
		    $xx_sum+=($x[$i]*$x[$i]);
		    
		  }
		  
		  if((($n * $xx_sum) - ($x_sum * $x_sum)) > 0) {
		  	// calculate slope
		  	$m = (($n * $xy_sum) - ($x_sum * $y_sum)) / (($n * $xx_sum) - ($x_sum * $x_sum));
		  } else {
		  	$m = 0;
		  }
		  
		  if($n == 0) {
		  	$b = 0;
		  } else {
			  // calculate intercept
			  $b = ($y_sum - ($m * $x_sum)) / $n;
			    
		  }
		  // return result
		  return array("m"=>$m, "b"=>$b);

	}

	public function linearreg_slope( $valuesIn, $period )
	{
	  $valuesOut = array();
	  
	  $startIdx = 0;
	  $endIdx = count($valuesIn) - 1;
	  
	  $sumX = $period * ( $period - 1 ) * 0.5;
	  $sumXSqr = $period * ( $period - 1 ) * ( 2 * $period - 1 ) / 6;
	  $divisor = $sumX * $sumX - $period * $sumXSqr;
	  
	  for ( $today = $startIdx, $outIdx = 0; $today <= $endIdx; $today++, $outIdx++ )
	  {
	    $sumXY = 0;
	    $sumY = 0;
	    if ( $today >= $period - 1 ) {
	      for( $aux = $period; $aux-- != 0; )
	      {
	        $sumY += $tempValue = $valuesIn[$today - $aux];
	        $sumXY += (double)$aux * $tempValue;
	      }
	      $valuesOut[$outIdx] = ( $period * $sumXY - $sumX * $sumY) / $divisor;
	    }
	  }
	  
	  return $valuesOut;
	}
}
?>
