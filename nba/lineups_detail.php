<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'classes/DK_DB.php';
require_once 'config.php';

$dk_db = new DK_DB();
$gPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'G');
$fPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'F');
$pgPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'PG');
$sgPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'SG');
$sfPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'SF');
$pfPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'PF');
$cPlayers = $dk_db->getPlayerPoolStatsByPosition($gameDate, 'C');
$teams = $dk_db->getCompiledTeamStats($gameDate);
$gameTimes = $dk_db->getGameTimes($gameDate);
$teamsArray = array();

foreach($teams as $_team) {
	$teamsArray[strtolower($_team['team'])] = $_team;
}

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Draftkings Roster Analyzer</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.2.0/css/mdb.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.js"></script>
		<script src="js/mdb.js"></script>
		<style type="text/css">
			.bootstrap-table {
				width: 100% !important;
				float: left;
			}
			.red {
				color: red;
				background-color: transparent !important;
			}
			.clearfix {
				display: none;
			}
			.setup.active {
				display: block;
			}
			.center {
				text-align: center;
			}
			tr.disabled {
			    opacity: 0.5;
			}
			.bootstrap-table tbody td {
				cursor: pointer;
				/*text-align: center;*/
			}
			.player-table {
				display: none;
			}
			.player-table.active {
				display: initial;
			}
			.table-hover >tbody>tr:hover {
			    background-color: #bbb;
			}
			.injury {
				color: red;
				display: inline-block;
				margin-left: 3px;
				font-size: 11px;
			}
			.zui-table {
			    border: none;
			    border-right: solid 1px #DDEFEF;
			    border-collapse: separate;
			    border-spacing: 0;
			    font: normal 13px Arial, sans-serif;
			}
			.zui-table thead th {
			    background-color: #DDEFEF !important;
			    border: none;
			    color: #336B6B;
			    padding: 10px;
			    text-align: left;
			    text-shadow: 1px 1px 1px #fff;
			    white-space: nowrap;
			}
			.zui-table thead th div {
			    background-color: #DDEFEF !important;
			}
			.zui-table tbody td {
			    border-bottom: solid 1px #DDEFEF;
			    color: #333;
			    padding: 10px;
			    text-shadow: 1px 1px 1px #fff;
			    white-space: nowrap;
			}
			.zui-wrapper {
			    position: relative;
			}
			.zui-scroller {
			    margin-left: 150px;
			    overflow-x: scroll;
			    overflow-y: visible;
			    padding-bottom: 5px;
			    /*width: 300px;*/
			}
			.zui-table .zui-sticky-col {
			    border-left: solid 1px #DDEFEF;
			    border-right: solid 1px #DDEFEF;
			    left: 0;
			    position: absolute;
			    top: auto;
			    width: 150px;
			}
			.fixed-table-container {
				position: inherit;
			}
			.fixed-table-container tbody td {
				border-left: 0;
			}
			.table-holder {
				max-height: 300px;
				overflow-x: scroll;
			}
			.table-holder td {
			    text-align: center;
			}

			.table-holder th {
			    padding: 10px;
			}
			.modal-dialog {
				width: 1000px !important;
			}
		</style>
		<script type="text/javascript">

			function calculateFP($data) {
				var $dbldblcount = 0;
				var $fp = 0;
				$fp += 	$data.pts * 1;
				$fp += 	$data.fg3 * 0.5;
				$fp += 	$data.trb * 1.25;
				$fp += 	$data.ast * 1.5;
				$fp += 	$data.stl * 2;
				$fp += 	$data.blk * 2;
				$fp -= 	$data.tov * 0.5;

				if($data.pts >= 10) {
					$dbldblcount++;
				}

				if($data.trb >= 10) {
					$dbldblcount++;
				}

				if($data.ast >= 10) {
					$dbldblcount++;
				}

				if($data.stl >= 10) {
					$dbldblcount++;
				}

				if($data.blk >= 10) {
					$dbldblcount++;
				}

				if($dbldblcount >= 2) {
					$fp += 1.5;
				}

				if($dbldblcount >= 3) {
					$fp += 3;
				}

				return $fp;
			}

			jQuery(document).ready(function() {
				jQuery('.PG-table table').bootstrapTable();

				var filterTime;
				var filterPosition;

				jQuery(document).on('click', '.player-table tbody tr', function() {
					var playerid = jQuery(this).attr('data-playerid');

					jQuery.ajax({
					  url: '/nba/get_player_data_ajax.php?player_id='+playerid,
					  type: 'GET',
					  dataType: 'json',
					  success: function(data, textStatus, xhr) {
					  	jQuery('.player_card .player-name').text(data.info[0].name);
					  	// jQuery('.player_card .modal-body img').attr('src', data.info[0].img);
					  	var $table = jQuery('.table-holder table');
					  	var $tbody = jQuery('<tbody>');

					  	if($table.find('tbody').is('*')) {
					  		$table.find('tbody').remove();
					  	}

					  	for(var i in data.game_logs) {
					  		if(data.game_logs[i].mp == null) {
					  			continue;
					  		}

					  		var $tr = jQuery('<tr>');
					  		$tr.append('<td>'+ data.game_logs[i].date_game +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].opp_id +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].mp +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].fg + '-' + data.game_logs[i].fga + '</td>');
					  		$tr.append('<td>'+ parseFloat( (data.game_logs[i].fg / data.game_logs[i].fga) * 100).toFixed(2) + '%</td>');
					  		$tr.append('<td>'+ data.game_logs[i].fg3 + '-' + data.game_logs[i].fg3a + '</td>');

					  		if(data.game_logs[i].fg3a > 0) {
					  			$tr.append('<td>'+ parseFloat( (data.game_logs[i].fg3 / data.game_logs[i].fg3a) * 100).toFixed(2) + '%</td>');
					  		} else {
					  			$tr.append('<td>0.00%</td>');
					  		}
				  			$tr.append('<td>'+ data.game_logs[i].ft + '-' + data.game_logs[i].fta + '</td>');

					  		if(data.game_logs[i].fta > 0) {
					  			$tr.append('<td>'+ parseFloat( (data.game_logs[i].ft / data.game_logs[i].fta) * 100).toFixed(2) + '%</td>');
					  		} else {
					  			$tr.append('<td>0.00%</td>');
					  		}

					  		$tr.append('<td>'+ data.game_logs[i].trb +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].ast +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].blk +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].stl +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].pf +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].tov +'</td>');
					  		$tr.append('<td>'+ data.game_logs[i].pts +'</td>');
					  		$tr.append('<td>'+ calculateFP(data.game_logs[i]) +'</td>');
					  		$tbody.append($tr);
					  	}

					  	$table.append($tbody);

					  	// jQuery('.table-holder table').html($table);

					  	$('#myModal').modal('show');
					  }
					});
					
					
				});

				jQuery(document).on('click', '.reset-filter', function() {
					filterTime = undefined;
					filterPosition = undefined;

					jQuery('.player-table tbody tr').show();
				});

				jQuery(document).on('click', '.time-filter', function() {
					filterTime = jQuery(this).text();

					jQuery('.player-table tbody tr').hide();

					if(filterPosition !== undefined) {
						jQuery('.player-table').hide();

						jQuery('.'+filterPosition+'-table').show();
						jQuery('.player-table tbody tr[data-filter-gametime="'+filterTime+'"]').show();
					} else {
						jQuery('.player-table tbody tr[data-filter-gametime="'+filterTime+'"]').show();
					}
				});

				jQuery(document).on('click', '.position-filter', function() {
					filterPosition = jQuery(this).text();

					jQuery('.player-table').hide();

					jQuery('.'+filterPosition+'-table').show();

				});				
			});
		</script>
		<script src="https://use.fontawesome.com/b1353e11e4.js"></script>
	</head>
	<body>
		<div>
			<nav class="navbar navbar-default">
			  	<div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="">DK Roster Analyzer</a>
				    </div>
				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				      <ul class="nav navbar-nav">
				        <li><a href="/nba/data_analysis.php">Data Analysis</a></li>
				        <li class="active"><a href="/nba/lineups_detail.php">Detail Lineups</a></li>
				        <li><a href="/nba/createlineups.php">Create Lineups</a></li>
				        <li><a href="/nba/generatelineups.php">Generate Lineups</a></li>
				        <li><a href="/nba/sync.php">Sync</a></li>
				        <li><a href="/nba/sim.php">Simulation</a></li>
				      </ul>
				    </div><!-- /.navbar-collapse -->
		    	</div>
		    </nav>
		</div>
		<div>
			<div>
				<span><button class="btn btn-primary btn-sm position-filter">PG</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">SG</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">SF</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">PF</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">C</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">G</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">F</button></span>
				<span><button class="btn btn-primary btn-sm position-filter">Teams</button></span>
			</div>
			<div class="player-table PG-table active zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col">Player Name</th>
								<th data-sortable="true">Position</th>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Spread</th>
								<th data-sortable="true">O/U</th>
								<th data-sortable="true">Salary</th>
								<th data-sortable="true">Pt / $</th>
								<th data-sortable="true">Last 5 MP / G</th>
								<th data-sortable="true">MP / G</th>
								<th data-sortable="true">Last 8 Team Pts / G</th>
								<th data-sortable="true">Last 8 Opp Pts / G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">Proj Pts</th>
								<th data-sortable="true">FG %</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">FGA % (Team)</th>
								<th data-sortable="true">Last 3 Proj FGA</th>
								<th data-sortable="true">Proj FGA</th>
								<th data-sortable="true">3PM / G</th>
								<th data-sortable="true">3PA / G</th>
								<th data-sortable="true">3P % / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Proj Ast</th>
								<th data-sortable="true">Reb / G</th>
								<th data-sortable="true">Proj Reb</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Floor</th>
								<th data-sortable="true">Ceiling</th>
								<th data-sortable="true">Last 3 FPPG</th>
								<th data-sortable="true">Last 5 FPPG</th>
								<th data-sortable="true">FPPG</th>
								<th data-sortable="true">Proj FP</th>
								<th data-sortable="true">Game Score</th>
								<th data-sortable="true">Last 5 Consistency</th>
								<th data-sortable="true">Consistency</th>
								<th data-sortable="true">%2x</th>
								<th data-sortable="true">%3x</th>
								<th data-sortable="true">%4x</th>
								<th data-sortable="true">%5x</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($pgPlayers as $index => $player): ?>
								<?php 
								  	$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_pg']) + $player['pts_intercept']['b'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];

								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['mp_g'];
								  	$manuallyUpdated = false;

								  	if($player['mpg'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	// if(($player['mp_5'] > $player['mpg']) && $manuallyUpdated) {
								  	// 	$mins_to_use = $player['mp_5'];
								  	// }
								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);

								?>
								<tr data-index="<?php echo $index ?>" data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
									<td class="name zui-sticky-col"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
									<td class="position"><?php echo $player['position'] ?></td>
									<td class="team"><?php echo $player['team'] ?></td>
									<td class="opp"><?php echo $player['opp'] ?></td>
									<td class="spread"><?php echo $player['spread'] ?></td>
									<td class="over_under"><?php echo $player['over_under'] ?></td>
									<td class="salary"><?php echo $player['salary'] ?></td>
									<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($proj_fp / ($player['salary'] / 1000), 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_5'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_g'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['fg_pct'] * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format(($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']) * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['team_fga_last_3']), 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3'], 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3a'], 2) ?></td>
									<td class="ast"><?php echo ($player['fg3a'] > 0) ? number_format(($player['fg3'] / $player['fg3a']) * 100, 2) : 0.00 ?>%</td>
									<td class="ast"><?php echo number_format($player['ast'], 2) ?></td>
									<td class="ast"><?php echo number_format(($player['ast_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_ast']) + $player['ast_intercept']['b'], 2) ?></td>
									<td class="trb"><?php echo number_format($player['trb'], 2) ?></td>
									<td class="trb"><?php echo number_format(($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'], 2) ?></td>
									<td class="stl"><?php echo number_format($player['stl'], 2) ?></td>
									<td class="blk"><?php echo number_format($player['blk'], 2) ?></td>
									<td class="tov"><?php echo number_format($player['tov'], 2) ?></td>
									<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
									<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
									<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
									<td class="fppg"><?php echo $player['fppg_last_5'] ?></td>
									<td class="fppg"><?php echo $player['fppg'] ?></td>
									<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format( $proj_fp, 2) : 0 ?></td>
									<td class="fppg"><?php echo number_format( $player['gm_score'], 2) ?></td>
									<td class="fppg_consist"><?php echo $player['fppg_consist_last_5'] ?>%</td>
									<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
									<td class="fppg_consist"><?php echo number_format($player['value2'], 2) ?>(<?php echo number_format($player['value_2'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value3'], 2) ?>(<?php echo number_format($player['value_3'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value4'], 2) ?>(<?php echo number_format($player['value_4'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value5'], 2) ?>(<?php echo number_format($player['value_5'], 2) ?>%)</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="player-table SG-table zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col">Player Name</th>
								<th data-sortable="true">Position</th>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Spread</th>
								<th data-sortable="true">O/U</th>
								<th data-sortable="true">Salary</th>
								<th data-sortable="true">Pt / $</th>
								<th data-sortable="true">Last 5 MP / G</th>
								<th data-sortable="true">MP / G</th>
								<th data-sortable="true">Last 8 Team Pts / G</th>
								<th data-sortable="true">Last 8 Opp Pts / G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">Proj Pts</th>
								<th data-sortable="true">FG %</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">FGA % (Team)</th>
								<th data-sortable="true">Last 3 Proj FGA</th>
								<th data-sortable="true">Proj FGA</th>
								<th data-sortable="true">3PM / G</th>
								<th data-sortable="true">3PA / G</th>
								<th data-sortable="true">3P % / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Proj Ast</th>
								<th data-sortable="true">Reb / G</th>
								<th data-sortable="true">Proj Reb</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Floor</th>
								<th data-sortable="true">Ceiling</th>
								<th data-sortable="true">Last 3 FPPG</th>
								<th data-sortable="true">Last 5 FPPG</th>
								<th data-sortable="true">FPPG</th>
								<th data-sortable="true">Proj FP</th>
								<th data-sortable="true">Game Score</th>
								<th data-sortable="true">Last 5 Consistency</th>
								<th data-sortable="true">Consistency</th>
								<th data-sortable="true">%2x</th>
								<th data-sortable="true">%3x</th>
								<th data-sortable="true">%4x</th>
								<th data-sortable="true">%5x</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($sgPlayers as $index => $player): ?>
								<?php 
								  	$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_sg']) + $player['pts_intercept']['b'] + $player['ft'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];
								  	
								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['mpg'];
								  	$manuallyUpdated = false;

								  	if($player['mpg'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	if(($player['mp_5'] > $player['mpg']) && $manuallyUpdated) {
								  		$mins_to_use = $player['mp_5'];
								  	}

								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);
								?>
								<tr data-index="<?php echo $index ?>" data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
									<td class="name zui-sticky-col"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
									<td class="position"><?php echo $player['position'] ?></td>
									<td class="team"><?php echo $player['team'] ?></td>
									<td class="opp"><?php echo $player['opp'] ?></td>
									<td class="spread"><?php echo $player['spread'] ?></td>
									<td class="over_under"><?php echo $player['over_under'] ?></td>
									<td class="salary"><?php echo $player['salary'] ?></td>
									<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($proj_fp / ($player['salary'] / 1000), 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_5'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_g'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['fg_pct'] * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format(($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']) * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['team_fga_last_3']), 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3'], 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3a'], 2) ?></td>
									<td class="ast"><?php echo ($player['fg3a'] > 0) ? number_format(($player['fg3'] / $player['fg3a']) * 100, 2) : 0.00 ?>%</td>
									<td class="ast"><?php echo number_format($player['ast'], 2) ?></td>
									<td class="ast"><?php echo number_format(($player['ast_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_ast']) + $player['ast_intercept']['b'], 2) ?></td>
									<td class="trb"><?php echo number_format($player['trb'], 2) ?></td>
									<td class="trb"><?php echo number_format(($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'], 2) ?></td>
									<td class="stl"><?php echo number_format($player['stl'], 2) ?></td>
									<td class="blk"><?php echo number_format($player['blk'], 2) ?></td>
									<td class="tov"><?php echo number_format($player['tov'], 2) ?></td>
									<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
									<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
									<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
									<td class="fppg"><?php echo $player['fppg_last_5'] ?></td>
									<td class="fppg"><?php echo $player['fppg'] ?></td>
									<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format( $proj_fp, 2) : 0 ?></td>
									<td class="fppg"><?php echo number_format( $player['gm_score'], 2) ?></td>
									<td class="fppg_consist"><?php echo $player['fppg_consist_last_5'] ?>%</td>
									<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
									<td class="fppg_consist"><?php echo number_format($player['value2'], 2) ?>(<?php echo number_format($player['value_2'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value3'], 2) ?>(<?php echo number_format($player['value_3'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value4'], 2) ?>(<?php echo number_format($player['value_4'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value5'], 2) ?>(<?php echo number_format($player['value_5'], 2) ?>%)</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="player-table SF-table zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col">Player Name</th>
								<th data-sortable="true">Position</th>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Spread</th>
								<th data-sortable="true">O/U</th>
								<th data-sortable="true">Salary</th>
								<th data-sortable="true">Pt / $</th>
								<th data-sortable="true">Last 5 MP / G</th>
								<th data-sortable="true">MP / G</th>
								<th data-sortable="true">Last 8 Team Pts / G</th>
								<th data-sortable="true">Last 8 Opp Pts / G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">Proj Pts</th>
								<th data-sortable="true">FG %</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">FGA % (Team)</th>
								<th data-sortable="true">Last 3 Proj FGA</th>
								<th data-sortable="true">Proj FGA</th>
								<th data-sortable="true">3PM / G</th>
								<th data-sortable="true">3PA / G</th>
								<th data-sortable="true">3P % / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Proj Ast</th>
								<th data-sortable="true">Reb / G</th>
								<th data-sortable="true">Proj Reb</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Floor</th>
								<th data-sortable="true">Ceiling</th>
								<th data-sortable="true">Last 3 FPPG</th>
								<th data-sortable="true">Last 5 FPPG</th>
								<th data-sortable="true">FPPG</th>
								<th data-sortable="true">Proj FP</th>
								<th data-sortable="true">Game Score</th>
								<th data-sortable="true">Last 5 Consistency</th>
								<th data-sortable="true">Consistency</th>
								<th data-sortable="true">%2x</th>
								<th data-sortable="true">%3x</th>
								<th data-sortable="true">%4x</th>
								<th data-sortable="true">%5x</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($sfPlayers as $index => $player): ?>
								<?php 
								  	$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_sf']) + $player['pts_intercept']['b'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];
								  	
								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['mpg'];
								  	$manuallyUpdated = false;

								  	if($player['mpg'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	if(($player['mp_5'] > $player['mpg']) && $manuallyUpdated) {
								  		$mins_to_use = $player['mp_5'];
								  	}

								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);
								?>
								<tr data-index="<?php echo $index ?>" data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
									<td class="name zui-sticky-col"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
									<td class="position"><?php echo $player['position'] ?></td>
									<td class="team"><?php echo $player['team'] ?></td>
									<td class="opp"><?php echo $player['opp'] ?></td>
									<td class="spread"><?php echo $player['spread'] ?></td>
									<td class="over_under"><?php echo $player['over_under'] ?></td>
									<td class="salary"><?php echo $player['salary'] ?></td>
									<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($proj_fp / ($player['salary'] / 1000), 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_5'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_g'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['fg_pct'] * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format(($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']) * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['team_fga_last_3']), 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3'], 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3a'], 2) ?></td>
									<td class="ast"><?php echo ($player['fg3a'] > 0) ? number_format(($player['fg3'] / $player['fg3a']) * 100, 2) : 0.00 ?>%</td>
									<td class="ast"><?php echo number_format($player['ast'], 2) ?></td>
									<td class="ast"><?php echo number_format(($player['ast_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_ast']) + $player['ast_intercept']['b'], 2) ?></td>
									<td class="trb"><?php echo number_format($player['trb'], 2) ?></td>
									<td class="trb"><?php echo number_format(($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'], 2) ?></td>
									<td class="stl"><?php echo number_format($player['stl'], 2) ?></td>
									<td class="blk"><?php echo number_format($player['blk'], 2) ?></td>
									<td class="tov"><?php echo number_format($player['tov'], 2) ?></td>
									<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
									<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
									<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
									<td class="fppg"><?php echo $player['fppg_last_5'] ?></td>
									<td class="fppg"><?php echo $player['fppg'] ?></td>
									<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format( $proj_fp, 2) : 0 ?></td>
									<td class="fppg"><?php echo number_format( $player['gm_score'], 2) ?></td>
									<td class="fppg_consist"><?php echo $player['fppg_consist_last_5'] ?>%</td>
									<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
									<td class="fppg_consist"><?php echo number_format($player['value2'], 2) ?>(<?php echo number_format($player['value_2'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value3'], 2) ?>(<?php echo number_format($player['value_3'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value4'], 2) ?>(<?php echo number_format($player['value_4'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value5'], 2) ?>(<?php echo number_format($player['value_5'], 2) ?>%)</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="player-table PF-table zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col">Player Name</th>
								<th data-sortable="true">Position</th>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Spread</th>
								<th data-sortable="true">O/U</th>
								<th data-sortable="true">Salary</th>
								<th data-sortable="true">Pt / $</th>
								<th data-sortable="true">Last 5 MP / G</th>
								<th data-sortable="true">MP / G</th>
								<th data-sortable="true">Last 8 Team Pts / G</th>
								<th data-sortable="true">Last 8 Opp Pts / G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">Proj Pts</th>
								<th data-sortable="true">FG %</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">FGA % (Team)</th>
								<th data-sortable="true">Last 3 Proj FGA</th>
								<th data-sortable="true">Proj FGA</th>
								<th data-sortable="true">3PM / G</th>
								<th data-sortable="true">3PA / G</th>
								<th data-sortable="true">3P % / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Proj Ast</th>
								<th data-sortable="true">Reb / G</th>
								<th data-sortable="true">Proj Reb</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Floor</th>
								<th data-sortable="true">Ceiling</th>
								<th data-sortable="true">Last 3 FPPG</th>
								<th data-sortable="true">Last 5 FPPG</th>
								<th data-sortable="true">FPPG</th>
								<th data-sortable="true">Proj FP</th>
								<th data-sortable="true">Game Score</th>
								<th data-sortable="true">Last 5 Consistency</th>
								<th data-sortable="true">Consistency</th>
								<th data-sortable="true">%2x</th>
								<th data-sortable="true">%3x</th>
								<th data-sortable="true">%4x</th>
								<th data-sortable="true">%5x</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($pfPlayers as $index => $player): ?>
								<?php 
								  	$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_pf']) + $player['pts_intercept']['b'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	// $player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];
								  	
								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['mpg'];
								  	$manuallyUpdated = false;

								  	if($player['mpg'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	if(($player['mp_5'] > $player['mpg']) && $manuallyUpdated) {
								  		$mins_to_use = $player['mp_5'];
								  	}

								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);

								?>
								<tr data-index="<?php echo $index ?>" data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
									<td class="name zui-sticky-col"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
									<td class="position"><?php echo $player['position'] ?></td>
									<td class="team"><?php echo $player['team'] ?></td>
									<td class="opp"><?php echo $player['opp'] ?></td>
									<td class="spread"><?php echo $player['spread'] ?></td>
									<td class="over_under"><?php echo $player['over_under'] ?></td>
									<td class="salary"><?php echo $player['salary'] ?></td>
									<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($proj_fp / ($player['salary'] / 1000), 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_5'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_g'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['fg_pct'] * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format(($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']) * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['team_fga_last_3']), 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3'], 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3a'], 2) ?></td>
									<td class="ast"><?php echo ($player['fg3a'] > 0) ? number_format(($player['fg3'] / $player['fg3a']) * 100, 2) : 0.00 ?>%</td>
									<td class="ast"><?php echo number_format($player['ast'], 2) ?></td>
									<td class="ast"><?php echo number_format(($player['ast_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_ast']) + $player['ast_intercept']['b'], 2) ?></td>
									<td class="trb"><?php echo number_format($player['trb'], 2) ?></td>
									<td class="trb"><?php echo number_format(($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'], 2) ?></td>
									<td class="stl"><?php echo number_format($player['stl'], 2) ?></td>
									<td class="blk"><?php echo number_format($player['blk'], 2) ?></td>
									<td class="tov"><?php echo number_format($player['tov'], 2) ?></td>
									<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
									<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
									<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
									<td class="fppg"><?php echo $player['fppg_last_5'] ?></td>
									<td class="fppg"><?php echo $player['fppg'] ?></td>
									<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format( $proj_fp, 2) : 0 ?></td>
									<td class="fppg"><?php echo number_format( $player['gm_score'], 2) ?></td>
									<td class="fppg_consist"><?php echo $player['fppg_consist_last_5'] ?>%</td>
									<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
									<td class="fppg_consist"><?php echo number_format($player['value2'], 2) ?>(<?php echo number_format($player['value_2'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value3'], 2) ?>(<?php echo number_format($player['value_3'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value4'], 2) ?>(<?php echo number_format($player['value_4'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value5'], 2) ?>(<?php echo number_format($player['value_5'], 2) ?>%)</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="player-table C-table zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col">Player Name</th>
								<th data-sortable="true">Position</th>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Spread</th>
								<th data-sortable="true">O/U</th>
								<th data-sortable="true">Salary</th>
								<th data-sortable="true">Pt / $</th>
								<th data-sortable="true">Last 5 MP / G</th>
								<th data-sortable="true">MP / G</th>
								<th data-sortable="true">Last 8 Team Pts / G</th>
								<th data-sortable="true">Last 8 Opp Pts / G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">Proj Pts</th>
								<th data-sortable="true">FG %</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">FGA % (Team)</th>
								<th data-sortable="true">Last 3 Proj FGA</th>
								<th data-sortable="true">Proj FGA</th>
								<th data-sortable="true">3PM / G</th>
								<th data-sortable="true">3PA / G</th>
								<th data-sortable="true">3P % / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Proj Ast</th>
								<th data-sortable="true">Reb / G</th>
								<th data-sortable="true">Proj Reb</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Floor</th>
								<th data-sortable="true">Ceiling</th>
								<th data-sortable="true">Last 3 FPPG</th>
								<th data-sortable="true">Last 5 FPPG</th>
								<th data-sortable="true">FPPG</th>
								<th data-sortable="true">Proj FP</th>
								<th data-sortable="true">Game Score</th>
								<th data-sortable="true">Last 5 Consistency</th>
								<th data-sortable="true">Consistency</th>
								<th data-sortable="true">%2x</th>
								<th data-sortable="true">%3x</th>
								<th data-sortable="true">%4x</th>
								<th data-sortable="true">%5x</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($cPlayers as $index => $player): ?>
								<?php 
								  	$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_c']) + $player['pts_intercept']['b'] + $player['ft'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];
								  	
								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['mpg'];
								  	$manuallyUpdated = false;

								  	if($player['mpg'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	if(($player['mp_5'] > $player['mpg']) && $manuallyUpdated) {
								  		$mins_to_use = $player['mp_5'];
								  	}

								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);

								?>
								<tr data-index="<?php echo $index ?>" data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
									<td class="name zui-sticky-col"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
									<td class="position"><?php echo $player['position'] ?></td>
									<td class="team"><?php echo $player['team'] ?></td>
									<td class="opp"><?php echo $player['opp'] ?></td>
									<td class="spread"><?php echo $player['spread'] ?></td>
									<td class="over_under"><?php echo $player['over_under'] ?></td>
									<td class="salary"><?php echo $player['salary'] ?></td>
									<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($proj_fp / ($player['salary'] / 1000), 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_5'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_g'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['fg_pct'] * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format(($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']) * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['team_fga_last_3']), 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3'], 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3a'], 2) ?></td>
									<td class="ast"><?php echo ($player['fg3a'] > 0) ? number_format(($player['fg3'] / $player['fg3a']) * 100, 2) : 0.00 ?>%</td>
									<td class="ast"><?php echo number_format($player['ast'], 2) ?></td>
									<td class="ast"><?php echo number_format(($player['ast_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_ast']) + $player['ast_intercept']['b'], 2) ?></td>
									<td class="trb"><?php echo number_format($player['trb'], 2) ?></td>
									<td class="trb"><?php echo number_format(($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'], 2) ?></td>
									<td class="stl"><?php echo number_format($player['stl'], 2) ?></td>
									<td class="blk"><?php echo number_format($player['blk'], 2) ?></td>
									<td class="tov"><?php echo number_format($player['tov'], 2) ?></td>
									<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
									<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
									<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
									<td class="fppg"><?php echo $player['fppg_last_5'] ?></td>
									<td class="fppg"><?php echo $player['fppg'] ?></td>
									<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format( $proj_fp, 2) : 0 ?></td>
									<td class="fppg"><?php echo number_format( $player['gm_score'], 2) ?></td>
									<td class="fppg_consist"><?php echo $player['fppg_consist_last_5'] ?>%</td>
									<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
									<td class="fppg_consist"><?php echo number_format($player['value2'], 2) ?>(<?php echo number_format($player['value_2'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value3'], 2) ?>(<?php echo number_format($player['value_3'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value4'], 2) ?>(<?php echo number_format($player['value_4'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value5'], 2) ?>(<?php echo number_format($player['value_5'], 2) ?>%)</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="player-table G-table zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col">Player Name</th>
								<th data-sortable="true">Position</th>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Spread</th>
								<th data-sortable="true">O/U</th>
								<th data-sortable="true">Salary</th>
								<th data-sortable="true">Pt / $</th>
								<th data-sortable="true">Last 5 MP / G</th>
								<th data-sortable="true">MP / G</th>
								<th data-sortable="true">Last 8 Team Pts / G</th>
								<th data-sortable="true">Last 8 Opp Pts / G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">Proj Pts</th>
								<th data-sortable="true">FG %</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">FGA % (Team)</th>
								<th data-sortable="true">Last 3 Proj FGA</th>
								<th data-sortable="true">Proj FGA</th>
								<th data-sortable="true">3PM / G</th>
								<th data-sortable="true">3PA / G</th>
								<th data-sortable="true">3P % / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Proj Ast</th>
								<th data-sortable="true">Reb / G</th>
								<th data-sortable="true">Proj Reb</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Floor</th>
								<th data-sortable="true">Ceiling</th>
								<th data-sortable="true">Last 3 FPPG</th>
								<th data-sortable="true">Last 5 FPPG</th>
								<th data-sortable="true">FPPG</th>
								<th data-sortable="true">Proj FP</th>
								<th data-sortable="true">Game Score</th>
								<th data-sortable="true">Last 5 Consistency</th>
								<th data-sortable="true">Consistency</th>
								<th data-sortable="true">%2x</th>
								<th data-sortable="true">%3x</th>
								<th data-sortable="true">%4x</th>
								<th data-sortable="true">%5x</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($gPlayers as $index => $player): ?>
								<?php 
								  	$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_pg']) + $player['pts_intercept']['b'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];
								  	
								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['mpg'];
								  	$manuallyUpdated = false;

								  	if($player['mpg'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	if(($player['mp_5'] > $player['mpg']) && $manuallyUpdated) {
								  		$mins_to_use = $player['mp_5'];
								  	}

								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);
								?>
								<tr data-index="<?php echo $index ?>" data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
									<td class="name zui-sticky-col"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
									<td class="position"><?php echo $player['position'] ?></td>
									<td class="team"><?php echo $player['team'] ?></td>
									<td class="opp"><?php echo $player['opp'] ?></td>
									<td class="spread"><?php echo $player['spread'] ?></td>
									<td class="over_under"><?php echo $player['over_under'] ?></td>
									<td class="salary"><?php echo $player['salary'] ?></td>
									<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($proj_fp / ($player['salary'] / 1000), 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_5'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_g'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['fg_pct'] * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format(($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']) * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['team_fga_last_3']), 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3'], 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3a'], 2) ?></td>
									<td class="ast"><?php echo ($player['fg3a'] > 0) ? number_format(($player['fg3'] / $player['fg3a']) * 100, 2) : 0.00 ?>%</td>
									<td class="ast"><?php echo number_format($player['ast'], 2) ?></td>
									<td class="ast"><?php echo number_format(($player['ast_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_ast']) + $player['ast_intercept']['b'], 2) ?></td>
									<td class="trb"><?php echo number_format($player['trb'], 2) ?></td>
									<td class="trb"><?php echo number_format(($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'], 2) ?></td>
									<td class="stl"><?php echo number_format($player['stl'], 2) ?></td>
									<td class="blk"><?php echo number_format($player['blk'], 2) ?></td>
									<td class="tov"><?php echo number_format($player['tov'], 2) ?></td>
									<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
									<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
									<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
									<td class="fppg"><?php echo $player['fppg_last_5'] ?></td>
									<td class="fppg"><?php echo $player['fppg'] ?></td>
									<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format( $proj_fp, 2) : 0 ?></td>
									<td class="fppg"><?php echo number_format( $player['gm_score'], 2) ?></td>
									<td class="fppg_consist"><?php echo $player['fppg_consist_last_5'] ?>%</td>
									<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
									<td class="fppg_consist"><?php echo number_format($player['value2'], 2) ?>(<?php echo number_format($player['value_2'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value3'], 2) ?>(<?php echo number_format($player['value_3'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value4'], 2) ?>(<?php echo number_format($player['value_4'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value5'], 2) ?>(<?php echo number_format($player['value_5'], 2) ?>%)</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="player-table F-table zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive zui-table" data-toggle="table">
						<thead>
							<tr>
								<th data-sortable="true" class="zui-sticky-col">Player Name</th>
								<th data-sortable="true">Position</th>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Spread</th>
								<th data-sortable="true">O/U</th>
								<th data-sortable="true">Salary</th>
								<th data-sortable="true">Pt / $</th>
								<th data-sortable="true">Last 5 MP / G</th>
								<th data-sortable="true">MP / G</th>
								<th data-sortable="true">Last 8 Team Pts / G</th>
								<th data-sortable="true">Last 8 Opp Pts / G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">Min Proj Pts</th>
								<th data-sortable="true">Max Proj Pts</th>
								<th data-sortable="true">Proj Pts</th>
								<th data-sortable="true">FG %</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">FGA % (Team)</th>
								<th data-sortable="true">Last 3 Proj FGA</th>
								<th data-sortable="true">Proj FGA</th>
								<th data-sortable="true">3PM / G</th>
								<th data-sortable="true">3PA / G</th>
								<th data-sortable="true">3P % / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Proj Ast</th>
								<th data-sortable="true">Reb / G</th>
								<th data-sortable="true">Proj Reb</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Floor</th>
								<th data-sortable="true">Ceiling</th>
								<th data-sortable="true">Last 3 FPPG</th>
								<th data-sortable="true">Last 5 FPPG</th>
								<th data-sortable="true">FPPG</th>
								<th data-sortable="true">Proj FP</th>
								<th data-sortable="true">Game Score</th>
								<th data-sortable="true">Last 5 Consistency</th>
								<th data-sortable="true">Consistency</th>
								<th data-sortable="true">%2x</th>
								<th data-sortable="true">%3x</th>
								<th data-sortable="true">%4x</th>
								<th data-sortable="true">%5x</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($fPlayers as $index => $player): ?>
								<?php 
								  	$player_data = $player;
								  	// $player_data['pts'] = $player['pts_intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['dfg_sf']) + $player['pts_intercept']['b'] + $player['ft'];
								  	$player_data['pts'] = ($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'] + $player['ft'];
								  	$player_data['trb'] = ($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'];
								  	
								  	$proj_fp = $dk_db->calculateFP($player_data);

								  	$mins_to_use = $player['mpg'];
								  	$manuallyUpdated = false;

								  	if($player['mpg'] > $player['mp_g']) {
								  		$manuallyUpdated = true;
								  	}

								  	if(($player['mp_5'] > $player['mpg']) && $manuallyUpdated) {
								  		$mins_to_use = $player['mp_5'];
								  	}

								  	$proj_fp = $mins_to_use * ($proj_fp / $player['mp_g']);
								?>
								<tr data-index="<?php echo $index ?>" data-filter-gametime="<?php echo date('H:i', strtotime($player['game_time'])) ?>" data-filter-position="<?php echo $player['position'] ?>" data-playerid="<?php echo $player['player_id'] ?>">
									<td class="name zui-sticky-col"><?php echo $player['name'] ?><span class="injury"><?php echo $player['injury'] ?></span></td>
									<td class="position"><?php echo $player['position'] ?></td>
									<td class="team"><?php echo $player['team'] ?></td>
									<td class="opp"><?php echo $player['opp'] ?></td>
									<td class="spread"><?php echo $player['spread'] ?></td>
									<td class="over_under"><?php echo $player['over_under'] ?></td>
									<td class="salary"><?php echo $player['salary'] ?></td>
									<td class="salary_pts"><?php echo ($player['fppg'] == 0) ? 0.00 : number_format($proj_fp / ($player['salary'] / 1000), 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_5'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['mp_g'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['pts_last_8'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['min_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['max_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format(($player['intercept']['m'] * ($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']))) + $player['intercept']['b'], 2) ?></td>
									<td class="pts"><?php echo number_format($player['fg_pct'] * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['team'])]['fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format(($player['fga'] / $teamsArray[strtolower($player['team'])]['fga']) * 100, 2) ?>%</td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga_last_3'] / $teamsArray[strtolower($player['team'])]['fga']), 2) ?></td>
									<td class="pts"><?php echo number_format($teamsArray[strtolower($player['opp'])]['opp_fga'] * ($player['fga'] / $teamsArray[strtolower($player['team'])]['team_fga_last_3']), 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3'], 2) ?></td>
									<td class="ast"><?php echo number_format($player['fg3a'], 2) ?></td>
									<td class="ast"><?php echo ($player['fg3a'] > 0) ? number_format(($player['fg3'] / $player['fg3a']) * 100, 2) : 0.00 ?>%</td>
									<td class="ast"><?php echo number_format($player['ast'], 2) ?></td>
									<td class="ast"><?php echo number_format(($player['ast_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_ast']) + $player['ast_intercept']['b'], 2) ?></td>
									<td class="trb"><?php echo number_format($player['trb'], 2) ?></td>
									<td class="trb"><?php echo number_format(($player['reb_intercept']['m'] * $teamsArray[strtolower($player['opp'])]['opp_trb']) + $player['reb_intercept']['b'], 2) ?></td>
									<td class="stl"><?php echo number_format($player['stl'], 2) ?></td>
									<td class="blk"><?php echo number_format($player['blk'], 2) ?></td>
									<td class="tov"><?php echo number_format($player['tov'], 2) ?></td>
									<td class="min_fppg"><?php echo $player['min_fppg'] ?></td>
									<td class="max_fppg"><?php echo $player['max_fppg'] ?></td>
									<td class="fppg"><?php echo number_format($player['fppg_last_3'], 2) ?></td>
									<td class="fppg"><?php echo $player['fppg_last_5'] ?></td>
									<td class="fppg"><?php echo $player['fppg'] ?></td>
									<td class="fppg"><?php echo ($player['fppg'] > 0) ? number_format( $proj_fp, 2) : 0 ?></td>
									<td class="fppg"><?php echo number_format( $player['gm_score'], 2) ?></td>
									<td class="fppg_consist"><?php echo $player['fppg_consist_last_5'] ?>%</td>
									<td class="fppg_consist"><?php echo $player['fppg_consist'] ?>%</td>
									<td class="fppg_consist"><?php echo number_format($player['value2'], 2) ?>(<?php echo number_format($player['value_2'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value3'], 2) ?>(<?php echo number_format($player['value_3'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value4'], 2) ?>(<?php echo number_format($player['value_4'], 2) ?>%)</td>
									<td class="fppg_consist"><?php echo number_format($player['value5'], 2) ?>(<?php echo number_format($player['value_5'], 2) ?>%)</td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<div class="player-table Teams-table  zui-wrapper">
				<div class="zui-scroller">
					<table class="table table-hover table-responsive  zui-table" data-toggle="table">
						<thead>
							<tr>
								<th data-sortable="true">Team</th>
								<th data-sortable="true">Opp</th>
								<th data-sortable="true">Record</th>
								<th data-sortable="true">Last 8 Win %</th>
								<th data-sortable="true">Away Win %</th>
								<th data-sortable="true">Home Win %</th>
								<th data-sortable="true">Win %</th>
								<th data-sortable="true">Pts Diff G</th>
								<th data-sortable="true">Away Pts / G</th>
								<th data-sortable="true">Home Pts / G</th>
								<th data-sortable="true">Pts / G</th>
								<th data-sortable="true">FG % / G</th>
								<th data-sortable="true">FG / G</th>
								<th data-sortable="true">FGA / G</th>
								<th data-sortable="true">Ast / G</th>
								<th data-sortable="true">Orb / G</th>
								<th data-sortable="true">Trb / G</th>
								<th data-sortable="true">Stl / G</th>
								<th data-sortable="true">Blk / G</th>
								<th data-sortable="true">Tov / G</th>
								<th data-sortable="true">Opp Pts / G</th>
								<th data-sortable="true">Opp FG % / G</th>
								<th data-sortable="true">Opp FGA / G</th>
								<th data-sortable="true">Opp Ast / G</th>
								<th data-sortable="true">Opp Orb / G</th>
								<th data-sortable="true">Opp Trb / G</th>
								<th data-sortable="true">Opp Stl / G</th>
								<th data-sortable="true">Opp Blk / G</th>
								<th data-sortable="true">Opp Tov / G</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($teams as $team): ?>
								<tr>
									<td class="team zui-sticky-col"><?php echo $team['team'] ?></td>
									<td class="team"><?php echo $team['opp'] ?></td>
									<td class="team"><?php echo $team['wins'] . " - " . $team['loss'] ?></td>
									<td class="pts"><?php echo number_format($team['win_pct_last_8'], 2) ?>%</td>
									<td class="pts"><?php echo number_format($team['win_pct_away'], 2) ?>%</td>
									<td class="pts"><?php echo number_format($team['win_pct_home'], 2) ?>%</td>
									<td class="pts"><?php echo number_format($team['win_pct'], 2) ?>%</td>
									<td class="pts"><?php echo number_format($team['pts_diff'], 2) ?></td>
									<td class="pts"><?php echo number_format($team['away_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($team['home_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format($team['pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($team['fg'] / $team['fga']) * 100, 2) ?>%</td>
									<td class="fga"><?php echo number_format($team['fg'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['fga'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['ast'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['orb'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['trb'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['stl'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['blk'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['tov'], 2) ?></td>
									<td class="pts"><?php echo number_format($team['opp_pts'], 2) ?></td>
									<td class="pts"><?php echo number_format(($team['opp_fg'] / $team['opp_fga']) * 100, 2) ?>%</td>
									<td class="fga"><?php echo number_format($team['opp_fga'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['opp_ast'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['opp_orb'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['opp_trb'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['opp_stl'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['opp_blk'], 2) ?></td>
									<td class="fga"><?php echo number_format($team['opp_tov'], 2) ?></td>
								</tr>
							<?php endforeach ?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- Modal -->
			<div class="modal player_card fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title player-name" id="myModalLabel"></h4>
			      </div>
			      <div class="modal-body">
			      	<div class="table-holder">
			      		<table>
			      			<thead>
			      				<th>Game Date</th>
			      				<th>Opp</th>
			      				<th>Mp</th>
			      				<th>FGM - FGA</th>
			      				<th>FG%</th>
			      				<th>3PM - 3PA</th>
			      				<th>3PT%</th>
			      				<th>FTM - FTA</th>
			      				<th>FT%</th>
			      				<th>REB</th>
			      				<th>AST</th>
			      				<th>BLK</th>
			      				<th>STL</th>
			      				<th>PF</th>
			      				<th>TO</th>
			      				<th>PTS</th>
			      				<th>FPTS</th>
			      			</thead>
			      		</table>
			      	</div>
			      </div>
			    </div>
			  </div>
			</div>
	</body>
</html>