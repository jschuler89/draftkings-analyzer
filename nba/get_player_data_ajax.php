<?php
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);
ini_set('memory_limit', '-1');
require_once 'classes/DK_DB.php';
require_once 'config.php';
include_once 'functions/get_player_functions.php';

$params = $_GET;
// $params['player_id'] = 1;

$dk_db = new DK_DB();
$playerInfo = $dk_db->getPlayer($params['player_id']);
$playerGameLogs = $dk_db->getPlayerGameLogs($params['player_id']);
$player['info'] = $playerInfo;
$player['game_logs'] = $playerGameLogs;

// $gameLogs['player'] = getPlayerGameLogs($urlparam);
// $gameLogs['team'] = getTeamGameLogs($teamInfo[0]['url_param']);
// $gameLogs['team_stats'] = getTeamStats($teamInfo[0]['url_param']);

echo json_encode($player);